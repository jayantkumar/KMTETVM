﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Configuration.Assemblies;
using System.Diagnostics;
using System.Reflection;
using DataManager;
using System.Globalization;
namespace DataManager
{
    public partial class DownloadWaybill : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        SqlDataAdapter da;

        SqlCommand cmdr = new SqlCommand();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        bool inUse = true;
        int waybillcount = 0;
        string waybillno = string.Empty;
        float amount;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string query = "";
                if (((Request.QueryString["receipno"] != null) && (Request.QueryString["receipno"] != "")) && (Request.QueryString["type"] == "1" || Request.QueryString["type"] == "3"))
                {
                    txtWayBill.Text = Convert.ToString(Request.QueryString["receipno"]);
                    txtdrv.Text = Convert.ToString(Request.QueryString["driverno"]);
                    txtcash.Text = Convert.ToString(Request.QueryString["cash"]);
                    txtvehicle.Text = Convert.ToString(Request.QueryString["vehicleo"]);
                    txtcond.Text = Convert.ToString(Request.QueryString["conno"]);
                }
                else
                {
                    if (Button1.Enabled == true)
                    {
                        Button1.Enabled = false;
                        btnDelBill.Enabled = false;
                        btndelete.Enabled = false;
                    }
                }
                if (Convert.ToString(Session["user_type"]) == "5")
                {
                    trsearch.Visible = true;
                }
                else
                {
                    trsearch.Visible = false;
                }
            }
        }

        public void save_ticket_details()
        {
            amount = float.Parse(Request.QueryString["cash"].ToString());
            try
            {
                if (Convert.ToString(Session["Is_Cashier"]) == "false" && amount > 0)
                {
                    Response.Write("<script>alert ('Sorry you are not Authorize Person.');</script>");
                }
                else if (Convert.ToString(Session["Is_Cashier"]) == "false" && amount == 0)
                {
                    savecashier();
                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }
                    cmd.Transaction = con.BeginTransaction();
                    Save_Data();
                    cmd.Transaction.Commit();
                }
                else
                {
                    savecashier();
                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }
                    cmd.Transaction = con.BeginTransaction();
                    Save_Data();
                    cmd.Transaction.Commit();
                    UPdate_Extra();
                }

                Session["Is_Cashier"] = null;
                while (inUse)
                {
                    inUse = FileInUse();
                    if (inUse)
                        System.Threading.Thread.Sleep(2000);
                    library.WriteErrorLog("loop inside save_ticket_details" + DateTime.Now.ToString());
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in save_ticket_details function " + ex.Message.ToString() + " " + ex.Source);
                cmd.Transaction.Rollback();
                Msg_Failure();
            }
            finally
            {
                con.Close();
            }
        }

        public void savecashier()
        {
            SqlCommand cmd1 = new SqlCommand();
            DataManager dm = new DataManager();
            try
            {
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;
                DataTable dtconcode = dm.GetDataTable("select CondctrCode  from CondctrNo_yrwise  where EmpCode ='" + Request.QueryString["conno"].ToString() + "' order by years desc");
                string CondCode = dtconcode.Rows[0]["CondctrCode"].ToString();
                cmd1 = new SqlCommand("ins_cashier_master", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@waybill", Request.QueryString["receipno"].ToString());
                cmd1.Parameters.AddWithValue("@cond", CondCode); 
                cmd1.Parameters.AddWithValue("@drv", Request.QueryString["driverno"].ToString());
                cmd1.Parameters.AddWithValue("@vehicle", Request.QueryString["vehicleo"].ToString());
                cmd1.Parameters.AddWithValue("@totcash", Request.QueryString["cash"].ToString());
                cmd1.Parameters.AddWithValue("@userid", Session["user_id"].ToString());
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in savecashier function " + ex.Message.ToString() + " " + ex.Source);
                Response.Write(ex.Message);
            }
        }

        public void Show_Msg()
        {
            killprocess();
            string scp = "<script type='text/javascript'>alert('Data Saved Successfully');window.location.href='Add_Manual_ticket_desc.aspx?receipno=" + Request.QueryString["receipno"].ToString() + "&driverno=" + Request.QueryString["driverno"].ToString() + "&conno=" + Request.QueryString["conno"].ToString() + "&vehicleo=" + Request.QueryString["vehicleo"].ToString() + "&cash=" + Request.QueryString["cash"].ToString() + "&Type=3'</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);
        }

        public void Msg_Failure()
        {
            string scp = "<script type='text/javascript'>alert('Sorry some error occured while saving information.);</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);
        }

        public void killprocess()
        {
            Process[] runingProcess = Process.GetProcesses();
            for (int i = 0; i < runingProcess.Length; i++)
            {
                if (runingProcess[i].ProcessName == "ETM_COM")
                {
                    library.WriteErrorLog("Process Name" + runingProcess[i].ProcessName);
                    runingProcess[i].Kill();
                }
            }
        }

        protected void Button1_Click(object sender, System.EventArgs e)
        {
            try
            {
                inUse = true;
                if (txtWayBill.Text.Length < 1 || txtWayBill.Text == "$Error-No" || txtWayBill.Text == "01" || txtWayBill.Text == "$DONE")
                {
                    Response.Write("<script>alert ('Capture ETM Again') </script>");
                }
                else
                {
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter("select distinct waybill_no from ticket_detail where waybill_no='" + txtWayBill.Text + "' ", con);
                    con.Open();
                    da.Fill(ds);
                    con.Close();
                    runcommand();
                    string filePt = @"D:\Pratinidhi\ETM_OUT.txt";
                    if (System.IO.File.Exists(filePt))
                    {
                        while (inUse)
                        {
                            inUse = FileInUse();
                            if (inUse)
                                System.Threading.Thread.Sleep(2000);


                            library.WriteErrorLog("loop inside Button1_Click" + DateTime.Now.ToString());
                        }

                        string[] lines12 = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");
                        library.WriteErrorLog("Length of etvm out file :" + lines12.Count().ToString());
                        Session["waybill_no"] = Convert.ToString(txtWayBill.Text);
                        Session["waybill_count"] = lines12.Count();
                        GenerateFile(lines12, txtWayBill.Text);
                    }
                    btnDelBill.Enabled = true;
                    btndelete.Enabled = false;
                    Response.Write("<script>alert('Data Downloaded');window.location.href='DownloadWaybill.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=1'</script>");
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in Button1_Click function " + ex.Message + " " + ex.Source);
            }
        }

        public static bool FileInUse()
        {
            string filePt = @"D:\Pratinidhi\ETM_OUT.txt";
            try
            {
                using (FileStream logFileStream = new FileStream(filePt, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader logFileReader = new StreamReader(logFileStream))
                    {
                        string text = logFileReader.ReadToEnd();
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        protected void btnCapETM_Click(object sender, EventArgs e)
        {
            try
            {
                getwaybilldetails();
                readwaybill();
            }
            catch (Exception ex)
            {

            }
        }

        public void getwaybilldetails()
        {
            try
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                System.Diagnostics.ProcessStartInfo info1 = new System.Diagnostics.ProcessStartInfo();
                info1.FileName = (@"D:\pratinidhi\etm_com.exe ");

                info1.Arguments = "3";
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = info1;
                process.StartInfo.UseShellExecute = false;
                System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "3");

                process.Close();

                char[] delimiterChars = { ' ' };
                string filePt = @"D:\Pratinidhi\ETM_OUT.txt";

                library.WriteErrorLog("getwaybilldetails File : " + filePt);
                if (System.IO.File.Exists(filePt))
                {
                    string[] lines = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");

                    foreach (string line in lines)
                    {
                        string[] words = line.Split(delimiterChars);
                        library.WriteErrorLog("getwaybilldetails txtWayBill.Text data : " + words[0]);
                        txtWayBill.Text = words[0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in getwaybilldetails func " + ex.Message + " " + ex.Source);
            }
        }

        public void readwaybill()
        {
            try
            {
                char[] delimiterChars = { ' ' };
                string filePt = @"D:\Pratinidhi\ETM_OUT.txt";

                if (System.IO.File.Exists(filePt))
                {
                    string[] lines = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");

                    foreach (string line in lines)
                    {
                        string[] words = line.Split(delimiterChars);

                        library.WriteErrorLog("readwaybill txtWayBill.Text data : " + words[0]);

                        txtWayBill.Text = words[0].ToString();
                        txtdrv.Text = line.Substring(26, 16).ToString().Trim();

                        txtcond.Text = line.Substring(9, 16).ToString().Trim();
                        txtvehicle.Text = line.Substring(43, 10).ToString().Trim();
                        Button1.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in readwaybill func " + ex.Message + " " + ex.Source);
            }
        }

        public void Save_Data()
        {
            try
            {
                string filePt = @"D:\Pratinidhi\ETM_OUT.txt";
                if (System.IO.File.Exists(filePt))
                {
                    string[] lines12 = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");

                    foreach (string line12 in lines12)
                    {
                        string[] Obj = line12.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries); //--line12.Split(delimiterChars);

                        if (Obj[2].ToString().Substring(0, 4) != "CASE" && Obj[9].ToString() != "0")
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();

                            cmd.CommandText = "insert_ticket_data";
                            cmd.Parameters.AddWithValue("@waybillno", Request.QueryString["receipno"].ToString());

                            cmd.Parameters.AddWithValue("@tripno", Obj[0].ToString());
                            library.WriteErrorLog("tripno : " + Obj[0].ToString());

                            cmd.Parameters.AddWithValue("@routeno", Obj[1].ToString());
                            library.WriteErrorLog("routeno : " + Obj[1].ToString());

                            cmd.Parameters.AddWithValue("@ticketno", Obj[2].ToString());
                            library.WriteErrorLog("ticket No : " + Obj[2].ToString());

                            cmd.Parameters.AddWithValue("@startstage", Obj[3].ToString());
                            library.WriteErrorLog("startstage : " + Obj[3].ToString());
                            cmd.Parameters.AddWithValue("@endstage", Obj[4].ToString());
                            library.WriteErrorLog("endstage : " + Obj[4].ToString());
                            //----------New Code Added By Amit on 22/7/2015------------//
                            cmd.Parameters.AddWithValue("@adultcount", Obj[5].ToString());
                            library.WriteErrorLog("adultcount : " + Obj[5].ToString());
                            cmd.Parameters.AddWithValue("@childcount", Obj[6].ToString());
                            library.WriteErrorLog("childcount : " + Obj[6].ToString());
                            cmd.Parameters.AddWithValue("@luggagecount", Obj[7].ToString());
                            library.WriteErrorLog("luggagecount : " + Obj[7].ToString());
                            cmd.Parameters.AddWithValue("@passcount", Obj[8].ToString());
                            library.WriteErrorLog("passcount : " + Obj[8].ToString());
                            library.WriteErrorLog("ticketcount : " + Obj[5].ToString() + "," + Obj[6].ToString() + "," + Obj[7].ToString() + "," + Obj[8].ToString());
                            int totalTicketCount = int.Parse(Obj[5].ToString()) + int.Parse(Obj[6].ToString()) +
                                                   int.Parse(Obj[7].ToString()) + int.Parse(Obj[8].ToString());
                            cmd.Parameters.AddWithValue("@ticketcount", totalTicketCount);
                            library.WriteErrorLog("ticketcount : " + Obj[5].ToString() + "," + Obj[6].ToString() + "," + Obj[7].ToString() + "," + Obj[8].ToString());
                            if (Obj.Count() == 19)// count is 19 when pass/Conc/CASE No. Exist or else the count is 18
                            {
                                cmd.Parameters.AddWithValue("@cardnumber", Obj[12].ToString());
                                library.WriteErrorLog("cardnumber : " + Obj[12].ToString());

                                cmd.Parameters.AddWithValue("@cardtype", Obj[13].ToString());
                                library.WriteErrorLog("cardtype : " + Obj[13].ToString());
                                cmd.Parameters.AddWithValue("@adultamount", Obj[14].ToString());
                                library.WriteErrorLog("adultamount : " + Obj[14].ToString());
                                cmd.Parameters.AddWithValue("@childamount", Obj[15].ToString());
                                library.WriteErrorLog("childamount : " + Obj[15].ToString());
                                cmd.Parameters.AddWithValue("@luggageamount", Obj[16].ToString());
                                library.WriteErrorLog("luggageamount : " + Obj[16].ToString());
                                cmd.Parameters.AddWithValue("@passamount", Obj[17].ToString());
                                library.WriteErrorLog("passamount : " + Obj[17].ToString());
                                cmd.Parameters.AddWithValue("@ticketstatus", Obj[18].ToString());
                                library.WriteErrorLog("ticketstatus : " + Obj[18].ToString());

                            }
                            else if (Obj.Count() == 20) // where pass age in one digit like M 6  instead of M06
                            {
                                cmd.Parameters.AddWithValue("@cardnumber", "");
                                library.WriteErrorLog("cardnumber : " + "");
                                cmd.Parameters.AddWithValue("@cardtype", 0);
                                library.WriteErrorLog("cardtype : " + 0);
                                cmd.Parameters.AddWithValue("@adultamount", Obj[15].ToString());
                                library.WriteErrorLog("adultamount : " + Obj[15].ToString());
                                cmd.Parameters.AddWithValue("@childamount", Obj[16].ToString());
                                library.WriteErrorLog("childamount : " + Obj[16].ToString());
                                cmd.Parameters.AddWithValue("@luggageamount", Obj[17].ToString());
                                library.WriteErrorLog("luggageamount : " + Obj[17].ToString());
                                cmd.Parameters.AddWithValue("@passamount", Obj[18].ToString());
                                library.WriteErrorLog("passamount : " + Obj[18].ToString());
                                cmd.Parameters.AddWithValue("@ticketstatus", Obj[19].ToString());
                                library.WriteErrorLog("ticketstatus : " + Obj[19].ToString());
                            }
                            else if (Obj.Count() == 21) // where pass age in one digit like M 6  instead of M06
                            {
                                cmd.Parameters.AddWithValue("@cardnumber", Obj[12].ToString());
                                library.WriteErrorLog("cardnumber : " + Obj[12].ToString());

                                cmd.Parameters.AddWithValue("@cardtype", Obj[15].ToString());
                                library.WriteErrorLog("cardtype : " + Obj[15].ToString());

                                cmd.Parameters.AddWithValue("@adultamount", Obj[16].ToString());
                                library.WriteErrorLog("adultamount : " + Obj[16].ToString());

                                cmd.Parameters.AddWithValue("@childamount", Obj[17].ToString());
                                library.WriteErrorLog("childamount : " + Obj[17].ToString());

                                cmd.Parameters.AddWithValue("@luggageamount", Obj[18].ToString());
                                library.WriteErrorLog("luggageamount : " + Obj[18].ToString());

                                cmd.Parameters.AddWithValue("@passamount", Obj[19].ToString());
                                library.WriteErrorLog("passamount : " + Obj[19].ToString());

                                cmd.Parameters.AddWithValue("@ticketstatus", Obj[20].ToString());
                                library.WriteErrorLog("ticketstatus : " + Obj[20].ToString());
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@cardnumber", "");
                                library.WriteErrorLog("cardnumber : " + "");
                                cmd.Parameters.AddWithValue("@cardtype", 0);
                                library.WriteErrorLog("cardtype : " + 0);
                                cmd.Parameters.AddWithValue("@adultamount", Obj[13].ToString());
                                library.WriteErrorLog("adultamount : " + Obj[13].ToString());
                                cmd.Parameters.AddWithValue("@childamount", Obj[14].ToString());
                                library.WriteErrorLog("childamount : " + Obj[14].ToString());
                                cmd.Parameters.AddWithValue("@luggageamount", Obj[15].ToString());
                                library.WriteErrorLog("luggageamount : " + Obj[15].ToString());
                                cmd.Parameters.AddWithValue("@passamount", Obj[16].ToString());
                                library.WriteErrorLog("passamount : " + Obj[16].ToString());
                                cmd.Parameters.AddWithValue("@ticketstatus", Obj[17].ToString());
                                library.WriteErrorLog("ticketstatus : " + Obj[17].ToString());
                            }
                            cmd.Parameters.AddWithValue("@ticketamount", Obj[9].ToString());
                            library.WriteErrorLog("ticketamount : " + Obj[9].ToString());

                            if (Obj[2].ToString().Substring(0, 4) == "CONC")
                            {
                                cmd.Parameters.AddWithValue("@consec_code", Obj[12].ToString());
                                library.WriteErrorLog("consec_code : " + Obj[12].ToString());
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@consec_code", "00");
                                library.WriteErrorLog("consec_code : 00");
                            }
                            cmd.Parameters.AddWithValue("@dateandtime", Obj[10].ToString() + ' ' + Obj[11].ToString());
                            library.WriteErrorLog("dateandtime : " + Obj[11].ToString());

                            cmd.Parameters.AddWithValue("@userid", Session["user_id"].ToString());
                            library.WriteErrorLog("userid : " + Session["user_id"].ToString());
                            //-------------------------Ends Here-----------------------//

                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog(ex);
                library.WriteErrorLog("Error occured in Save_Data func " + ex.Message + " " + ex.Source + " waybill no. " + Convert.ToString(Request.QueryString["receipno"].ToString()));
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Response.Write(line);
            }
        }

        public void UPdate_Extra()
        {

            waybillno = Request.QueryString["receipno"].ToString();
            SqlCommand cmd1 = new SqlCommand();
            try
            {
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;

                cmd1 = new SqlCommand("Update_Extra", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@waybillno", waybillno.ToString());
                cmd1.Parameters.AddWithValue("@Amount", txtcash.Text);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();

                Session["waybillno"] = null;
            }
            catch (Exception ex)
            {
                library.WriteErrorLog(ex);
                library.WriteErrorLog("Error occured in Save_Data func " + ex.Message + " " + ex.Source + " waybill no. " + Convert.ToString(Request.QueryString["receipno"].ToString()));
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Response.Write(line);
            }
        }

        public void UPdate_Amount(string waybillno, decimal cashcollected)
        {

            SqlCommand cmd1 = new SqlCommand();
            try
            {
                ExecuteNonQuery("update cashier_collect set totalcashcollected = " + cashcollected + " where waybillno ='" + waybillno + "'");
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;

                cmd1 = new SqlCommand("Update_Manual", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@waybillno", waybillno.ToString());

                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();


            }
            catch (Exception ex)
            {
                library.WriteErrorLog(ex);
                library.WriteErrorLog("Error occured in Save_Data func " + ex.Message + " " + ex.Source + " waybill no. " + Convert.ToString(Request.QueryString["receipno"].ToString()));
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Response.Write(line);
            }
        }

        public void GenerateFile(string[] data, string waybillno)
        {
            string FolderPath = "WayBill Folder\\";

            bool exists = System.IO.Directory.Exists(Server.MapPath(FolderPath));

            if (!exists)
                System.IO.Directory.CreateDirectory(Server.MapPath(FolderPath));

            string FileName = "\\WayBill_" + waybillno + "_" + Convert.ToDateTime(DateTime.Now).ToString("h_mm_ss_tt");
            library.WriteErrorLog(" File : " + FileName + " Length of file " + data.Count().ToString());

            StreamWriter sw = null;
            try
            {
                string line, line2;
                using (System.IO.StreamWriter files = new System.IO.StreamWriter(Server.MapPath(FolderPath) + FileName + ".txt", true))
                {
                    System.IO.StreamReader file = new System.IO.StreamReader(@"D:\Pratinidhi\ETM_OUT.txt");
                    while ((line = file.ReadLine()) != null)
                    {
                        files.WriteLine(line);
                    }
                    file.Close();
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in GenerateFile function " + ex.Message + " " + ex.Source);
            }
        }

        protected void btn_etm_reset_Click(object sender, EventArgs e)
        {
            try
            {
                killprocess();
                if (System.IO.File.Exists(@"d:\\pratinidhi\\ETM_OUT.txt"))
                {
                    System.IO.File.Delete(@"d:\\pratinidhi\\ETM_OUT.txt");
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDelBill_Click(object sender, System.EventArgs e)
        {

            bool is_command_run = false;
            try
            {
                var lineCount21 = File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt").Count();

                if (txtcash.Text == "" || txtcash.Text == "0" && lineCount21 > 0)
                {
                    Response.Write("<script>alert ('Enter total cash collected amount.');</script>");
                    return;
                }
                else if (txtWayBill.Text.Length < 1 || txtWayBill.Text == "$Error-No" || txtWayBill.Text == "01" || txtWayBill.Text == "$DONE")
                {
                    Response.Write("<script>alert ('Capture ETM Again') </script>");
                }
                else
                {
                    runcommand();
                    is_command_run = true;
                    while (inUse)
                    {
                        inUse = FileInUse();
                        if (inUse)
                            System.Threading.Thread.Sleep(2000);
                        library.WriteErrorLog("loop" + DateTime.Now.ToString());
                    }


                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter("select distinct waybill_no from ticket_detail where waybill_no='" + txtWayBill.Text + "' ", con);
                    con.Open();

                    da.Fill(ds);
                    con.Close();
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        DataSet dst = new DataSet();
                        SqlDataAdapter das = new SqlDataAdapter("select  count(waybill_no)waybill_no_count  from ticket_detail where waybill_no ='" + Request.QueryString["receipno"] + "' group by waybill_no", con);
                        con.Open();

                        das.Fill(dst);
                        con.Close();
                        if ((lineCount21 == Convert.ToInt32(Session["waybill_count"]) && Convert.ToInt32(dst.Tables[0].Rows[0][0]) == lineCount21))
                        {
                            btnDelBill.Enabled = true;
                            btndelete.Enabled = true;
                            Response.Write("<script>alert('Details Already Downloaded delete Waybill');window.location.href='DownloadWaybill.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=1'</script>");
                        }
                        else
                        {
                            ExecuteAgain(lineCount21, Convert.ToInt32(dst.Tables[0].Rows[0][0]));
                        }
                    }
                    else if (ds.Tables[0].Rows.Count == 0 && lineCount21 == 0)
                    {
                        btndelete.Enabled = true;
                        ExecuteNonQuery("update WayBill_Details set Is_Delete = 1 where WayBillNo =" + txtWayBill.Text);
                        Response.Write("<script>alert('No waybill found delete data');window.location.href='DownloadWaybill.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=1'</script>");
                    }
                    else
                    {
                        if (!is_command_run)
                            runcommand();
                        while (inUse)
                        {
                            inUse = FileInUse();
                            if (inUse)
                                System.Threading.Thread.Sleep(2000);
                            library.WriteErrorLog("loop" + DateTime.Now.ToString());
                        }
                        string[] lines12 = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");
                        library.WriteErrorLog("Length of etvm out file :" + lines12.Count().ToString());
                        GenerateFile(lines12, txtWayBill.Text);
                        Session["waybill_count"] = lines12.Count();
                        save_ticket_details();
                        btnDelBill.Enabled = true;
                        btndelete.Enabled = true;
                        Response.Write("<script>alert ('Data saved successfully'); window.location.href='DownloadWaybill.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=3'</script>");
                    }
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in btnDelBill_Click function " + ex.Message + " " + ex.Source);
            }
        }

        public void ExecuteAgain(int lineCount21, int dbcount)
        {
            if (Convert.ToString(Session["waybill_no"]) == Convert.ToString(txtWayBill.Text))
            {
                if (lineCount21 == Convert.ToInt32(Session["waybill_count"]) && lineCount21 == Convert.ToInt32(dbcount))
                {

                }
                else
                {
                    string[] lines12 = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");
                    library.WriteErrorLog("Length of etvm out file :" + lines12.Count().ToString());
                    GenerateFile(lines12, txtWayBill.Text);
                    Session["waybill_count"] = lines12.Count();
                }
                SqlCommand cmd1 = new SqlCommand();
                try
                {
                    cmd1.Connection = con;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.Clear();
                    cmd1 = new SqlCommand("Sp_Insert_waybill_alreadydownloaded", con);
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@waybillno", txtWayBill.Text);
                    con.Open();
                    cmd1.ExecuteNonQuery();
                    con.Close();

                    save_ticket_details();
                    btnDelBill.Enabled = true;
                    btndelete.Enabled = true;
                    Response.Write("<script>alert ('Data saved successfully'); window.location.href='DownloadWaybill.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=3'</script>");
                }
                catch (Exception ex)
                {
                    Response.Write("<script>alert ('Error occured'); window.location.href='DownloadWaybill.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=3'</script>");
                }
            }
        }

        protected void btnErsTck_Click(object sender, System.EventArgs e)
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            System.Diagnostics.ProcessStartInfo info1 = new System.Diagnostics.ProcessStartInfo();
            info1.FileName = (@"D:\pratinidhi\etm_com.exe ");
            info1.Arguments = "3";
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo = info1;
            process.StartInfo.UseShellExecute = false;
            System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "5");
            process.Close();
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            Delete_ticket_details();
        }

        public void Delete_ticket_details()
        {
            inUse = true;
            runcommand();
            while (inUse)
            {
                inUse = FileInUse();
                if (inUse)
                    System.Threading.Thread.Sleep(2000);
                library.WriteErrorLog("loop" + DateTime.Now.ToString());
            }
            try
            {
                killprocess();
                var lineCount = File.ReadLines(@"D:\Pratinidhi\ETM_OUT.txt").Count();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select  count(waybill_no)waybill_no_count  from ticket_detail where waybill_no ='" + Request.QueryString["receipno"] + "' group by waybill_no", con);
                con.Open();

                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    library.WriteErrorLog("ds " + ds.Tables[0].Rows[0][0].ToString());
                    library.WriteErrorLog("EtmOut " + lineCount.ToString());
                    library.WriteErrorLog("Session " + Convert.ToInt32(Session["waybill_count"]));

                    if (ds.Tables[0].Rows[0][0].ToString() == lineCount.ToString() && Convert.ToInt32(Session["waybill_count"]) == Convert.ToInt32(lineCount))
                    {
                        library.WriteErrorLog("Deleting data from machine of waybill no :" + txtWayBill.Text);
                        rundeletecommand();
                        Response.Write("<script>alert('Data deleted Successfully');window.location.href='Add_Manual_ticket_desc.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=4'</script>");
                    }
                    else
                    {
                        ExecuteAgain(lineCount, Convert.ToInt32(ds.Tables[0].Rows[0][0]));
                        rundeletecommand();
                        //library.WriteErrorLog("Data not deleted from machine of waybill no :" + txtWayBill.Text);
                        //Response.Write("<script>alert ('Please Contact to system Administrator'); window.location.href='DownloadWaybill.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=3'</script>");
                    }
                }
                else if (ds.Tables[0].Rows.Count == 0 && lineCount == 0)
                {
                    library.WriteErrorLog("Deleting data from machine of waybill no with 0 count :" + txtWayBill.Text);
                    rundeletecommand();
                    Response.Write("<script>alert('Data deleted Successfully');window.location.href='Add_Manual_ticket_desc.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=4'</script>");
                }

            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in Delete_ticket_details function " + ex.Message + " " + ex.Source);
            }

        }

        public void runcommand()
        {
            try
            {
                killprocess();
                Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                System.Diagnostics.ProcessStartInfo info1 = new System.Diagnostics.ProcessStartInfo();

                info1.FileName = (@"D:\pratinidhi\etm_com.exe ");
                info1.Arguments = "1";
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = info1;
                process.StartInfo.UseShellExecute = false;
                System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "1");
                process.Close();
                System.Threading.Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in runcommand func " + ex.Message + " " + ex.Source);
            }
        }

        public void rundeletecommand()
        {
            try
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                System.Diagnostics.ProcessStartInfo info1 = new System.Diagnostics.ProcessStartInfo();

                info1.FileName = (@"D:\pratinidhi\etm_com.exe ");
                info1.Arguments = "3";
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = info1;
                process.StartInfo.UseShellExecute = false;
                System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "9");
                process.Close();
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in rundeletecommand func " + ex.Message + " " + ex.Source);
            }
        }

        public void ExecuteNonQuery(string exeQuery)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand(exeQuery, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }

        }

        protected void GridWayBillBind(String date)
        {
            try
            {
                string dtnew = "'" + date + "'";

                SqlDataAdapter da;
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                cmd.CommandText = "Sp_getdownload_waybill";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 500;
                cmd.Connection = con;
                cmd.Parameters.Add("@date", SqlDbType.NVarChar).Value = date;

                ds = Getdata(cmd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grid.DataSource = ds;
                    grid.DataBind();
                }
                else
                {
                    grid.DataSource = null;
                    grid.DataBind();
                }


            }
            catch (Exception ex)
            {
                con.Close();
            }

        }

        public DataSet Getdata(SqlCommand cmd)
        {
            DataSet dst = new DataSet();
            try
            {
                SqlDataAdapter sda;
                sda = new SqlDataAdapter(cmd);
                sda.Fill(dst);
            }
            catch (Exception ex)
            {

            }
            return dst;
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            GridWayBillBind(txtsearch_by_date.Text);
        }

        protected void grid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grid.EditIndex = e.NewEditIndex;
            GridWayBillBind(txtsearch_by_date.Text);
        }

        protected void grid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                TextBox cashcollected = grid.Rows[e.RowIndex].FindControl("txt_cashcollected") as TextBox;
                TextBox waybillno = grid.Rows[e.RowIndex].FindControl("txt_waybill") as TextBox;

                UPdate_Amount(waybillno.Text, Convert.ToDecimal(cashcollected.Text));
                grid.EditIndex = -1;

                GridWayBillBind(txtsearch_by_date.Text);
            }
            catch (Exception ex)
            {

            }
        }

        protected void grid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grid.EditIndex = -1;
            GridWayBillBind(txtsearch_by_date.Text);
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    if (Convert.ToString(Session["user_type"]) == "5")
            //    {
            //        Button Btn_edit = (e.Row.FindControl("btnedit") as Button);
            //        Btn_edit.Visible = true;

            //    }

            //}
        }
    }
}