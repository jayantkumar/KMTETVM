﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Driver.aspx.cs" Inherits="DataManager.Driver" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">

        function getdriver(obj1) {
            var name = obj1.value;
            $.ajax({
                type: 'POST', url: 'Driver.aspx?empname=' + obj1.value + "&Opt1=getconcession", data: $('#form1').serialize(), success: function (response) {

                    if (response.substring(0, 8) == 'notexist') {
                        alert('Driver Already Exist');
                        obj1.focus();
                    }

                    else if (response.substring(0, 5) == 'exist') {
                        arrobj = response.split("~");
                    }
                }
            });
        }
    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>Driver Master</h2>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Driver Name </td>
            <td>
                <asp:TextBox ID="txtDrvr" runat="server" MaxLength="16" onblur="getdriver(this)"
                    Width="120px"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom" ValidChars=" "
                    TargetControlID="txtDrvr">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="submit-btn"
                    OnClick="btnSave_Click" Text="Save" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:GridView ID="grdDriver" runat="server" AllowPaging="False"
                    AutoGenerateColumns="False" BackColor="White" BorderColor="#999999"
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
                    GridLines="Vertical" OnPageIndexChanging="grdDriver_PageIndexChanging"
                    OnRowCommand="grdDriver_RowCommand">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                        <asp:BoundField DataField="code" Visible="false" HeaderText="Code" />
                        <asp:BoundField DataField="DriverName" HeaderText="Driver Name" />
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server"
                                    CommandArgument='<%# Eval("code") %>' CommandName="DeleteRow"
                                    ImageUrl="~/images/delNew.png" OnClientClick="Delete();" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td>
                <asp:HiddenField ID="hdDriverCode" runat="server" />
                <asp:HiddenField ID="hdDel" runat="server" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

