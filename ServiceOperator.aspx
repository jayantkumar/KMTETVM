﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ServiceOperator.aspx.cs" Inherits="DataManager.ServiceOperator" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


  <script src="../_scripts/jquery-1.6.2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="../_scripts/jquery.autocomplete.js"></script>
    <script language="javascript" src="../_scripts/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".welcome").addClass("");

            $(".welcome a").click(function () {
                $(".welcome ul").toggle();
            });

            $(".welcome ul li a").click(function () {
                var text = $(this).html();

                $(".welcome ul").hide();

            });

        });

        function findValue(li) {
            if (li == null) return alert("No match!");

            // if coming from an AJAX call, let's use the CityId as the value
            if (!!li.extra) var sValue = li.extra[0];

            // otherwise, let's just display the value in the text box
            else var sValue = li.selectValue;

            //alert("The value you selected was: " + sValue);
        }

        function selectItem(li) {
            findValue(li);
        }
        function Delete() {

            var res = confirm("Are you sure..??");
            if (res == false) {
                document.getElementById('hdDel').value = "1";
                return false;

            }
            document.getElementById('hdDel').value = "0";
        }
        function Validation(ToLoc, obj, Msg) {
            switch (ToLoc) {


                // only Numeric values                          
                case 1:
                    {
                        var pattern = /^[0-9]+$/;
                        var input = document.getElementById(obj);
                        if (!pattern.test(obj.value)) {
                            alert('Enter only numeric value');
                            //alert(Msg);
                            obj.focus();
                            return false;
                        }
                        break;
                    }
                case 2:
                    {
                        //var pattern = /^[a-zA-Z][a-zA-Z\s]+$/;
                        var pattern = /^[a-zA-Z]+$/;
                        var input = document.getElementById(obj);
                        if (obj.value.trim() != '') {
                            if (!pattern.test(obj.value.trim())) {
                                alert('Please Enter alphabets only');
                                //alert(Msg);
                                obj.focus();
                                return false;
                            }
                        }

                        break;
                    }
                case 3:
                    {
                        alert("Please Select Options From List");
                        document.getElementById('ddlRutTp').focus();
                        return false;
                        break;
                    }
                case 4:
                    {
                        alert("Please Fill Address Details");
                        if (document.getElementById('txtAd2').value == '')
                            document.getElementById('txtAd2').focus();
                        if (document.getElementById('txtAd1').value == '')
                            document.getElementById('txtAd1').focus();
                        return false;
                        break;
                    }
                default:
                    {
                        alert("Invalid Options");
                        break;
                    }
            }

        }
		
    </script>

    <table style="width: 100%">
        <tr>
            <td colspan="6" align="center">
                <h2>
                    Service Operator Master</h2>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                Service Operator Name:
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtName" runat="server" MaxLength="32"
                    Width="141px"></asp:TextBox>
                      <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom" ValidChars=" "
                    TargetControlID="txtName">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                Address1
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtAd1" runat="server"  MaxLength="32" Width="141px"></asp:TextBox>
                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom" ValidChars=" "
                    TargetControlID="txtAd1">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                Address2
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtAd2" runat="server"  MaxLength="32" Width="141px"></asp:TextBox>
                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom" ValidChars=" "
                    TargetControlID="txtAd2">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                Comment
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtCmt" runat="server"  MaxLength="32" Width="141px"></asp:TextBox>
                 <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom" ValidChars=" "
                    TargetControlID="txtCmt">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnSave" runat="server" CssClass="submit-btn" 
                    OnClick="btnSave_Click" Text="Save" />
              
            </td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hdOprCode" runat="server" />
                <asp:HiddenField ID="hdDel" runat="server" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:GridView ID="grdSO" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" 
                    CellPadding="3" ForeColor="Black" GridLines="Vertical" 
                    OnPageIndexChanging="grdSO_PageIndexChanging" OnRowCommand="grdSO_RowCommand">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                        <asp:BoundField DataField="serviceOprId" HeaderText="Code" />
                        <asp:BoundField DataField="OprName" HeaderText="Operator Name" />
                        <asp:BoundField DataField="Address1" HeaderText="Address 1" />
                        <asp:BoundField DataField="Address2" HeaderText="Address 2" />
                        <asp:BoundField DataField="CommentLn" HeaderText="Comment Line" />
                        <asp:TemplateField HeaderText="Update">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnedit" runat="server" 
                                    CommandArgument='<%# Eval("serviceOprId") %>' CommandName="EditRow" 
                                    ImageUrl="~/images/edit.gif" OnClientClick="return Right(10206,3);" 
                                    TabIndex="1" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" 
                                    CommandArgument='<%# Eval("serviceOprId") %>' CommandName="DeleteRow" 
                                    ImageUrl="~/images/delNew.png" OnClientClick="Delete();" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                    Text="Program" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

