﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using DataManager;
using System.IO;
using TimerGetData;
using System.Reflection;
using System.Configuration;
using System.Diagnostics;
namespace DataManager
{
    public partial class ChangeConductor : System.Web.UI.Page
    {
        DataManager dm = new DataManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        [System.Web.Services.WebMethod]
        public static ConductorDetails validate(string conname,string year)
        {
            string msg = string.Empty;
            ConductorDetails ConductorDetails = new ConductorDetails();
            DataManager dm = new DataManager();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    //DataTable dtcon = dm.GetDataTable("select con_full_name from conductor_master  where emp_code='" + conname.ToString() + "'");
                    DataTable dtcon = dm.GetDataTable("select con_full_name,CondCode from conductor_master  where CondCode=(select CondctrCode from CondctrNo_yrwise where EmpCode='" + conname.ToString() + "' and Years='"+ year + "')");  //updated jatanta on 30-12-2017
                    if (dtcon.Rows.Count == 0)
                    {
                        msg = "true";
                        ConductorDetails = null;
                    }
                    else
                    {
                        ConductorDetails.ConductorName = dtcon.Rows[0][0].ToString();

                        ConductorDetails.EmpCode = Convert.ToString(dtcon.Rows[0]["CondCode"]);
                        
                    }
                }
            }

            return ConductorDetails;
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCompletionconductor(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select emp_code from conductor_master where " +
                    "emp_code like  '%' + @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> customers = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(sdr["emp_code"].ToString());
                        }
                    }
                    conn.Close();
                    return customers;
                }
            }
        }
        [System.Web.Services.WebMethod]
        public static string ChangeConductrCode(string concode,string EmpCode)
        {
            string msg = "Success";
            DataManager dm = new DataManager();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        int year = DateTime.Now.Year + 1;
                        dm.ExecuteNonQuery("insert into CondctrNo_yrwise values('" + concode.ToString() + "' ,'" + EmpCode + "','" + year.ToString() + "')");  //updated jatanta on 30-12-2017
                        
                    }
                    catch(Exception ex)
                    {
                        return ex.Message;
                    }
                    //DataTable dtcon = dm.GetDataTable("select con_full_name from conductor_master  where emp_code='" + conname.ToString() + "'");
                    
                    
                }
            }

            return msg;
        }
    }
}
 