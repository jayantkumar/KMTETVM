﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="rtp_ETVM_ByCardType.aspx.cs" Inherits="rtp_ETVM_ByCardType" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <style media="Print" type="text/css">
        .ctrl {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize {
            font-family: Times New Roman;
            font-size: 12px;
        }

        .style1 {
            height: 28px;
        }
    </style>
</head>
<title>ETVM and Manual</title>
<body class="body">
    <form id="Form1" runat="server">
        <a href="reports.aspx" class="ctrl">Back</a>
        <table style="width: 100%">
            <tr>
                <td colspan="4" align="center">
                    <h3>ETVM RFID Card Type Report</h3>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <table border="0" width="100%">
                        <tr class="ctrl">
                            <td align="right">
                                <font style="font-family: arial; font-size: 12px;">From Date : </font>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtfromdate" runat="server" Width="80px">
                                </asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                    PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                <asp:ScriptManager ID="Scriptmanager1" runat="server">
                                </asp:ScriptManager>
                            </td>

                            <td align="right">
                                <font style="font-family: arial; font-size: 12px;">To Date : </font>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txttodate" runat="server" Width="80px">
                                </asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                    PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                            </td>

                            <td align="right">
                                <font style="font-family: arial; font-size: 12px;">Card Type : </font>
                            </td>
                            <td align="left">
                                 <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" 
                                         Width="120px">
                                        <asp:ListItem Text="Select" Value="0"> </asp:ListItem>
                                         <asp:ListItem Text="StageWise Pass" Value="4"> </asp:ListItem>
                                         <asp:ListItem Text="Mahalaxmi Pass" Value="5"> </asp:ListItem>
                                    </asp:DropDownList>
                            </td>
                        </tr>

                        <tr style="height: 50px" class="ctrl">
                            <td class="style1"></td>
                            <td colspan="2" align="center" class="style1">
                                <asp:Button ID="Search" runat="server" Text="Search" OnClick="Search_Click" />
                                <asp:Button ID="btnexcel" runat="server" OnClick="btnexcel_Click" Text="Convert To Excel" />
                            </td>
                            <td class="style1">&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblfromdate" runat="server"></asp:Label>
                </td>
                <td align="Right">
                    <asp:Label ID="lbltodate" runat="server">></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True"  
                        AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                        Font-Strikeout="False" Font-Underline="False" Width="100%" OnRowDataBound="grdWayBill_RowDataBound">
                      
                          <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                            Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                            Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                        <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                            Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                            ForeColor="#000099" />
                        <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                            Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                            ForeColor="Black" />
                        <Columns>
                            <asp:TemplateField HeaderText="Sr.No">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex  +1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="waybill_no" ItemStyle-Wrap="false" HeaderText="Way Bill" />
                            <asp:BoundField DataField="Card_Number" ItemStyle-Wrap="false" HeaderText="Card Number" />
                            <asp:BoundField DataField="total" HeaderText="Date" />
                            <asp:BoundField DataField="duty" HeaderText="Duty" />
                            <asp:BoundField DataField="emp" HeaderText="Conductor Name" />
                        </Columns>
                        <EmptyDataTemplate>
                            <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid; border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid; height: 100px; background-color: beige; text-align: center">
                                <br />
                                <br />
                                <br />
                                <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                            </div>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

