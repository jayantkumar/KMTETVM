﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ticketReport.aspx.cs" Inherits="ticketReport"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Ticket Report</title>
    <link href="ex.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="header">
        <div id="logo">
            <img src="images/logo.png" width="82" height="80" alt="ETVM_Aeon " align="left" style="margin-left: 100px;" /></div>
        <div id="comp_name">
            <h1>
                ETVM</h1>
        </div>
    </div>
    <div>
        <div style="width: 100%;" align="center">
            <table style="width: 80%">
                <tr>
                    <td colspan="4" align="center">
                        <h2>
                            Ticket Report</h2>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center" style="height: 40px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        From Date
                    </td>
                    <td>
                        <asp:TextBox ID="txtfromdate" runat="server" Width="80px"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                            PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                        </asp:CalendarExtender>
                        <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                        <asp:ScriptManager ID="Scriptmanager1" runat="server">
                        </asp:ScriptManager>
                    </td>
                    <td>
                        To Date
                    </td>
                    <td>
                        <asp:TextBox ID="txttodate" runat="server" Width="80px"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                            PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                        </asp:CalendarExtender>
                        <asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnView" runat="server" class="submit-btn2" OnClick="btnView_Click"
                            Text="View Report" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center" style="height: 40px;">
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 100%;" align="center">
            <asp:Panel runat="server" ID="Panel1" Style="display: inline;">
                <table style="width: 80%;">
                    <tr id="trFomTO" runat="server" visible="false">
                        <td align="left" colspan="2">
                            <b>From :</b>
                            <asp:Label ID="fromLable" runat="server"></asp:Label>
                        </td>
                        <td align="right" colspan="2">
                            <b>To :</b>
                            <asp:Label ID="toLable" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 40px;">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <h3 id="lal" runat="server" visible="false">
                                Lal Darwaza Depot</h3>
                            <br />
                            <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" ShowFooter="True"
                                AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                                BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                                Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                                Font-Strikeout="False" Font-Underline="False" Width="100%">
                                <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                                    Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                                <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                                    Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                                    ForeColor="#000099" />
                                <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                                    Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                                    ForeColor="Black" />
                                <Columns>
                                    <asp:BoundField HeaderText="Date" DataField="Date" />
                                    <asp:BoundField HeaderText="No Of Waybill" DataField="MachineNumber">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ticket Count" DataField="TicketCount">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Amount" DataField="TicketAmount">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                                        border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                                        height: 100px; background-color: beige; text-align: center">
                                        <br />
                                        <br />
                                        <br />
                                        <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                                    </div>
                                </EmptyDataTemplate>
                                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" Font-Size="Medium" />
                            </asp:GridView>
                        </td>
                        <td style="width: 10px;">
                        </td>
                        <td align="center" valign="top">
                            <h3 id="sarang" runat="server" visible="false">
                                Sarangpur Depot</h3>
                                <br />
                                <asp:GridView ID="GridView1" runat="server" AllowSorting="True" ShowFooter="True"
                                    AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                                    BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                                    Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                                    Font-Strikeout="False" Font-Underline="False" Width="100%">
                                    <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                                        Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                                        Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                                    <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                                        Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                                        ForeColor="#000099" />
                                    <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                                        Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                                        ForeColor="Black" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Date" DataField="Date" />
                                        <asp:BoundField HeaderText="No Of Waybill" DataField="MachineNumber">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Ticket Count" DataField="TicketCount">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Amount" DataField="TicketAmount">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                                            border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                                            height: 100px; background-color: beige; text-align: center">
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                                        </div>
                                    </EmptyDataTemplate>
                                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" Font-Size="Medium" />
                                </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button ID="btnexcel" runat="server" OnClick="btnexcel_Click" Text="Convert To Excel" />
        </div>
    </div>
    </form>
</body>
</html>
