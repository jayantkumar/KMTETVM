﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="DownloadWaybill.aspx.cs" Inherits="DataManager.DownloadWaybill" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                alert('An attempt was made to submit this form more than once; this extra attempt will be ignored.');
                return false;
            }
        }
        $(document).ready(function () {
            validate();
        });
        function validate() {
            $('#ContentPlaceHolder1_Button1').click(function () {
                //if ($('#ContentPlaceHolder1_txtcash').val() == '') {

                //    alert('Input can not be left blank');
                //    return false;
                //}
                if ($('#ContentPlaceHolder1_txtcash').val() != "") {
                    var value = $('#ContentPlaceHolder1_txtcash').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                    var intRegex = /^\d+$/;
                    if (!intRegex.test(value)) {
                        alert('Field must be numeric.');
                        return false;
                    }
                }
                CheckIsRepeat();
            });
        }
        function customAlert(msg, duration) {
            var styler = document.createElement("div");
            styler.setAttribute("style", "border: solid 5px Red;width:auto;height:auto;top:50%;left:40%;background-color:#444;color:Silver");
            styler.innerHTML = "<h1>" + msg + "</h1>";
            setTimeout(function () {
                styler.parentNode.removeChild(styler);
            }, duration);
            document.body.appendChild(styler);
        }
        function caller() {
            customAlert("This custom alert box will be closed in 5 seconds", "5000");
        }
    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>Download Data</h2>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>

        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td style="width: 400px;">Waybill Number
            </td>
            <td>
                <asp:TextBox ID="txtWayBill" ReadOnly="true" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td style="width: 400px;">Conductor Name
            </td>
            <td>
                <asp:TextBox ID="txtcond" ReadOnly="true" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>

        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td style="width: 400px;">Driver Name
            </td>
            <td>
                <asp:TextBox ID="txtdrv" ReadOnly="true" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>

        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td style="width: 400px;">Vehicle No
            </td>
            <td>
                <asp:TextBox ID="txtvehicle" ReadOnly="true" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>

        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td style="width: 400px;">Total Cash Collected
            </td>
            <td>
                <asp:TextBox ID="txtcash" Text="0" ReadOnly="false" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>

        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td style="width: 400px;">
                <asp:Button ID="btnCapETM" runat="server" CssClass="submit-btn" OnClick="btnCapETM_Click"
                    OnClientClick="return CheckIsRepeat();" Text="Capture ETM" />

                <asp:Button ID="Button1" runat="server" Text="Download" OnClick="Button1_Click" />

                <asp:Button ID="btnDelBill" runat="server" CssClass="submit-btn" OnClick="btnDelBill_Click"
                    OnClientClick="return CheckIsRepeat();" Text="Save" />

                <asp:Button ID="btndelete" runat="server" CssClass="submit-btn" OnClick="btndelete_Click"
                    Text="Delete" />
            </td>
            <td>
                <asp:Button ID="btn_etm_reset" runat="server" Text="Reset ETM Process" OnClick="btn_etm_reset_Click" />
                <asp:Button ID="btnErsTck" runat="server" Visible="false" CssClass="submit-btn" OnClick="btnErsTck_Click"
                    OnClientClick="return CheckIsRepeat();" Text="Erase Tickets" />
            </td>
            <td>&nbsp;
            </td>

        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr id="trsearch" runat="server">
            <td>&nbsp;
            </td>
            <td>Search by Date
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtsearch_by_date" runat="server" Width="100px">
                </asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                    PopupButtonID="imgPopBtnfrom" TargetControlID="txtsearch_by_date">
                </asp:CalendarExtender>
                <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                &emsp;
                <asp:Button ID="btnsearch" runat="server" Height="23px" OnClick="btnsearch_Click" Text="Search"
                    Width="90px" />
            </td>

        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td colspan="2">
                <asp:GridView ID="grid" runat="server" Width="100%" CellPadding="6" OnRowEditing="grid_RowEditing" OnRowUpdating="grid_RowUpdating"
                    OnRowCancelingEdit="grid_RowCancelingEdit" AutoGenerateColumns="false" OnRowDataBound="grid_RowDataBound">

                    <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                        Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                    <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                        Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                        ForeColor="#000099" />
                    <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                        Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                        ForeColor="Black" />

                    <Columns>
                        <asp:TemplateField HeaderText="Sr No." HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Waybill No." HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <asp:Label ID="lbl_waybill" Text='<%#Eval("WayBillNo") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txt_waybill" Width="140px" Text='<%#Eval("WayBillNo") %>' ReadOnly="true" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Conductor Name" HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <asp:Label ID="lbl_condname" Text='<%#Eval("ConductorName") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txt_condname" Width="140px" Text='<%#Eval("ConductorName") %>' ReadOnly="true" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Driver Name" HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <asp:Label ID="lbl_drivername" Text='<%#Eval("DriverName") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txt_drivername" Width="140px" Text='<%#Eval("DriverName") %>' ReadOnly="true" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Vehicle No" HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <asp:Label ID="lbl_vehicle" Text='<%#Eval("VehicleCode") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txt_vehicle" Width="140px" Text='<%#Eval("DriverName") %>' ReadOnly="true" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Total Cash Collected" HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <asp:Label ID="lbl_cashcollected" Text='<%#Eval("cash_collected") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txt_cashcollected" Width="140px" Text='<%#Eval("cash_collected") %>' runat="server"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="true" />
                        <%--<asp:CommandField ShowDeleteButton="true" />--%>
                    </Columns>
                    <%--                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnedit" Visible="false" runat="server" Text="Edit" CommandName="Edit" />
                            </ItemTemplate>

                            <EditItemTemplate>
                                <asp:Button ID="btncancel" runat="server" Text="Cancel" CommandName="Cancel" />
                                <asp:Button ID="btnupdate" runat="server" Text="Update" CommandName="Update" />
                            </EditItemTemplate>
                        </asp:TemplateField>--%>
                </asp:GridView>
            </td>
            <td>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>

