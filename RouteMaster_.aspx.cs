﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DataManager;
using System.IO;
using System.Text;
using System.Reflection;
using System.Web.Script.Services;
using System.Web.Services;
using System.Text.RegularExpressions;

namespace DataManager
{
    public partial class RouteMaster_ : System.Web.UI.Page
    {
        DataTable Add_Details = default(DataTable);

        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        static SqlConnection cons = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        DataSet dsroot = new DataSet();
        DataSet dsstage = new DataSet();

        String selQuery = "select root_id , Root_Name , start_loc_full_name source , end_loc_full_name destination , no_of_stop ,bustype  from rootmaster order by convert(int,Root_Name) asc";

        string routename = "", routedetail = "";
        int gridcount;
        string fare12345;
        string fare1234;
        string fare123456;
        string rrt = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                spanetm.Visible = false;
                DataSet dst = new DataSet();
                SqlDataAdapter das = new SqlDataAdapter("select COUNT(fare_stepnumber) from stage_wise_FareMaster$", con);
                con.Open();
                das.Fill(dst);
                con.Close();
                hdntotal_step.Value = Convert.ToString(dst.Tables[0].Rows[0][0]);
                lblstage.Text = "Max stage number " + hdntotal_step.Value;
                dr = dm.GetDataReader("select (Max(convert(int,ISNULL(Root_Name,0))) + 1) rootno from rootmaster");
                while (dr.Read())
                {
                    txtrouteno.Text = Convert.ToString(dr["rootno"]);
                }
                txtnoofstop.Attributes.Add("autocomplete", "off");
                txtsource.Attributes.Add("autocomplete", "off");
                txtdestination.Attributes.Add("autocomplete", "off");
                dm.closeconnection();
                dm.ExeCuteGridBind(selQuery, grdRoute);
            }
        }

        protected void grdRoute_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void grdRoute_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string root = e.CommandArgument.ToString();

            if (e.CommandName == "ViewRow")
            {
                DataSet dts = new DataSet();

                try
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter adp = new SqlDataAdapter();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SP_GetRouteDetail";
                    cmd.Parameters.Add(new SqlParameter("@routeno", root));
                    adp.SelectCommand = cmd;
                    adp.Fill(dts);

                    grid.DataSource = dts;
                    grid.DataBind();
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select Root_Name from rootmaster where LTRIM(rtrim(start_loc_full_name)) = '" + hfSourceId.Value + "' and LTRIM(rtrim(end_loc_full_name)) = '" + hfDestinationId.Value + "'", con);
                con.Open();
                da.Fill(ds);
                con.Close();
                if (Convert.ToInt32(txtnoofstop.Text) > Convert.ToInt32(hdntotal_step.Value))
                {
                    txtnoofstop.Focus();
                    Response.Write("<script>alert ('Stage number should be less then " + (Convert.ToInt32(hdntotal_step.Value) + 1) + "') </script>");
                    return;
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtsource.Focus();
                    Response.Write("<script>alert ('Source and destination already exist') </script>");
                    return;
                }
                else if (txtnoofstop.Text == "")
                {
                    txtnoofstop.Focus();
                    Response.Write("<script>alert ('Enter no. of stops') </script>");
                    return;
                }
                else if (txtsource.Text == "")
                {
                    txtsource.Focus();
                    Response.Write("<script>alert ('Enter source') </script>");
                    return;
                }
                else if (txtstartno.Text == "")
                {
                    txtstartno.Focus();
                    Response.Write("<script>alert ('Enter start stage number') </script>");
                    return;
                }
                else if (txtdestination.Text == "")
                {
                    txtdestination.Focus();
                    Response.Write("<script>alert ('Enter destination') </script>");
                    return;
                }
                else
                {
                    if (hdSave.Value.ToString() != "1")
                    {
                        try
                        {
                            con.Open();
                            cmd = new SqlCommand();
                            cmd.CommandText = "Sp_Insert_bus_route";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.Add("@route_no", SqlDbType.NVarChar).Value = txtrouteno.Text;
                            cmd.Parameters.Add("@no_of_stops", SqlDbType.NVarChar).Value = txtnoofstop.Text;
                            cmd.Parameters.Add("@source", SqlDbType.NVarChar).Value = txtsource.Text;
                            cmd.Parameters.Add("@destination", SqlDbType.NVarChar).Value = txtdestination.Text;
                            cmd.Parameters.Add("@start_stage_no", SqlDbType.NVarChar).Value = txtstartno.Text;
                            cmd.Parameters.Add("@bustype", SqlDbType.NVarChar).Value = Ddl_bustype.Text;
                            cmd.Parameters.Add("@userid", SqlDbType.Int).Value = Convert.ToInt32(Session["user_id"]);
                            Session["startstage"] = txtstartno.Text;
                            Session["endstage"] = txtendno.Text;
                            if (btnSave.Text == "Save")
                            {
                                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = 0;
                            }
                            else if (btnSave.Text == "Update")
                            {
                                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = Convert.ToInt32(hdRouteId.Value);
                                btnSave.Text = "Save";
                            }
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        catch (Exception ex)
                        {
                            con.Close();
                        }
                    }
                    dm.ExeCuteGridBind(selQuery, grdRoute);

                    hdSave.Value = "0";
                    Session["src"] = hfSourceId.Value;
                    Session["des"] = hfDestinationId.Value;
                    Session["no_of_stop"] = txtnoofstop.Text;
                    Session["start_stage"] = txtstartno.Text;
                    Session["end_stage"] = Convert.ToInt32(txtnoofstop.Text) + Convert.ToInt32(txtstartno.Text);
                    Session["route_no"] = txtrouteno.Text;
                    Session["type"] = 1;
                    Clear();
                    Response.Write("<script>alert('Data saved successfully');window.location.href='RouteStageMaster.aspx' </script> ");
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Exception occured in btnSave_Click function " + ex.Message + " " + ex.Source);
            }
        }

        protected void Clear()
        {
            dr = dm.GetDataReader("select (Max(convert(int,ISNULL(Root_Name,0))) + 1) rootno from rootmaster");
            while (dr.Read())
            {
                txtrouteno.Text = Convert.ToString(dr["rootno"]);
            }
            txtnoofstop.Text = "";
            txtsource.Text = "";
            txtdestination.Text = "";
            txtstartno.Text = "";
            txtendno.Text = "";
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string[] GetSource(string prefix, string key, string source)
        {
            List<string> stage = new List<string>();
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    if (key == "1")
                        cmd.CommandText = "select distinct top 10  bus_stage , bus_stage_id from bus_stage_master where " + "bus_stage like  '%' + @SearchText + '%'";
                    else if (key == "2")
                        cmd.CommandText = "select distinct top 10  bus_stage , bus_stage_id from bus_stage_master where " + "bus_stage like  '%' + @SearchText + '%' and isnull(bus_stage,'') != '" + source + "'";

                    cmd.Parameters.AddWithValue("@SearchText", prefix);
                    cmd.Connection = cons;
                    cons.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            stage.Add(sdr["bus_stage"].ToString());
                        }
                    }
                    cons.Close();
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in GetSource function " + ex.Message + " " + ex.Source);
            }
            return stage.ToArray();
        }



        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)grdRoute.HeaderRow.FindControl("chkboxSelectAll");
            foreach (GridViewRow row in grdRoute.Rows)
            {
                CheckBox ChkBoxRows = (CheckBox)row.FindControl("Select");
                if (ChkBoxHeader.Checked == true)
                {
                    ChkBoxRows.Checked = true;
                }
                else
                {
                    ChkBoxRows.Checked = false;
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            addcheckboxclk();
        }

        public void serviceoperatorcreate()
        {
            if (grdRoute.Rows.Count > 0)
            {
                DataRow row = default(DataRow);

                grdRoute.Visible = true;

                for (int i = 0; i <= grdRoute.Rows.Count - 1; i++)
                {
                    if (grdRoute.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkbx = (CheckBox)grdRoute.Rows[i].FindControl("Select");
                        CheckBox chkbx1 = (CheckBox)grdRoute.Rows[i].FindControl("chk");

                        if ((chkbx.Checked == true))
                        {
                            row = Add_Details.NewRow();

                            row[0] = grdRoute.Rows[i].Cells[0].Text;
                            row[1] = grdRoute.Rows[i].Cells[1].Text;
                            row[2] = grdRoute.Rows[i].Cells[2].Text;
                            row[3] = grdRoute.Rows[i].Cells[3].Text;

                            getroutedetail(row[0].ToString());
                            getstage(row[0].ToString());
                            gatfare(row[0].ToString());

                            string alldetail = routename + Environment.NewLine + routedetail + Environment.NewLine + fare1234;

                            fare1234 = "";
                            if (rrt == "" && chkbx.Checked == true)
                            {
                                rrt = rrt + alldetail;
                            }
                            else
                                rrt = rrt + Environment.NewLine + alldetail;
                        }
                    }
                }
                File.WriteAllText(@"d:\\pratinidhi\\Routes.txt", String.Empty);

                StreamWriter writer = new StreamWriter(@"d:\\pratinidhi\\Routes.txt", false, Encoding.Default);

                writer.Write(rrt);
                writer.Close();
            }
        }

        public void getroutedetail(string root)
        {

            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getroot";

            cmd.Parameters.Add(new SqlParameter("@root_name", root));

            adp.SelectCommand = cmd;
            adp.Fill(dsroot);
            string bustype, startloc, endloc, aa = "", ab = "", ac = "", totkm = "";

            bustype = dsroot.Tables[0].Rows[0][2].ToString();

            startloc = dsroot.Tables[0].Rows[0][4].ToString();
            totkm = dsroot.Tables[0].Rows[0][15].ToString();
            if (totkm.Length == 2)
            {
                totkm = "0" + totkm + "0";
            }
            if (totkm.Length == 1)
            {
                totkm = "0" + "0" + totkm + "0";
            }
            if (totkm.Length == 3)
            {
                totkm = "0" + totkm;
            }
            endloc = dsroot.Tables[0].Rows[0][5].ToString();
            string noofstop = dsroot.Tables[0].Rows[0][3].ToString(), T;

            if (noofstop.Length == 1)
            {
                noofstop = "0" + "0" + noofstop;
            }
            else
                noofstop = "0" + noofstop;

            if (bustype.Length < 12)
            {
                aa = aa.PadRight(12 - bustype.Length);
            }

            if (startloc.Length < 11)
            {
                ab = ab.PadRight(11 - startloc.Length);
            }

            if (endloc.Length < 11)
            {
                ac = ac.PadRight(11 - endloc.Length);
            }

            
            routedetail = ("0" + dsroot.Tables[0].Rows[0][12].ToString() + "," + bustype + aa + "," + startloc.ToString().Substring(0, 3) + "," + startloc + ab + "," + endloc.ToString().Substring(0, 3) + "," + endloc + ac + "," + noofstop.ToString() + "," + totkm + "," + "T".ToString() + "," + dsroot.Tables[0].Rows[0]["starting_stg_no"].ToString().ToString());

            dsroot.Clear();
            root = "";
        }
        public string NewLocation (string root)
        {
            string New_Location = "", query = "" ;
            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getnewlocation";
            cmd.Parameters.Add(new SqlParameter("@route_name", root));

            adp.SelectCommand = cmd;
            adp.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                New_Location = ds.Tables[0].Rows[0][0].ToString();
            }
            
            
            return New_Location;
        }
        public void getstage(string root)
        {

            string a, b, c, d, e, aa = "", ba = "", ca = "", da = "", ea = "";

            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getstage";
            cmd.Parameters.Add(new SqlParameter("@route_name", root));

            adp.SelectCommand = cmd;
            adp.Fill(dsstage);

            string rrrr = dsstage.Tables[0].Rows[0][1].ToString();

            if (rrrr.Length == 1)
            {
                rrrr = "0" + "0" + rrrr;
            }
            if (rrrr.Length == 2)
            {
                rrrr = "0" + rrrr;
            }
            if (rrrr.Length == 3)
            {
                rrrr = rrrr.ToString();
            }
            routename = "$R" + rrrr;
            dsstage.Clear();
            root = "";
        }
        public void gatfare(string root)
        {
            DataSet dsfare = new DataSet();
            dsfare = GetSet(root);

            string fare12345;
            string stage, stage_name_id, name, kms;
            string faremaster = "";
            DataTable dt = new DataTable();
            for (int j = 0; j < dsfare.Tables[0].Rows.Count; j++)
            {
                for (int i = 0; i <= dsfare.Tables[0].Rows.Count + 9; i++) // stopcolumn  + othercolumn
                {
                    if (i == 4)
                    {
                        i = 10;
                    }
                    fare12345 = dsfare.Tables[0].Rows[j][i].ToString();
                    fare123456 = fare123456 + fare12345;
                }
                if (fare1234 == null || fare1234 == "")
                {
                    fare1234 = fare1234 + fare123456 + dsfare.Tables[0].Rows[j]["stage_in_shivaji_font"].ToString();
                }

                else
                {
                    fare1234 = fare1234 + Environment.NewLine + fare123456 + dsfare.Tables[0].Rows[j]["stage_in_shivaji_font"].ToString();
                }
                fare123456 = "";
            }
            root = "";
        }

        public DataSet GetSet(string root)
        {
            DataSet dts = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();

                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "routedetail";
                cmd.Parameters.Add(new SqlParameter("@routeno", root));
                adp.SelectCommand = cmd;
                
                adp.Fill(dts);
                for (int i = 0; i <= dts.Tables[0].Rows.Count - 1; i++)
                {
                    string name = dts.Tables[0].Rows[i][2].ToString();
                     name = name.TrimEnd(',');
                  
                    string newname = NewLocation(name).ToUpper();
                    int lenth =name.Length;
                    if (name != newname)
                    {

                        if(newname!="")
                        {
                            if (newname.Length < 11)

                         
                            dts.Tables[0].Rows[i][2] = newname.PadRight(11, ' ') + ',';
                            else
                                dts.Tables[0].Rows[i][2] = newname + ',';
                            if(newname.Length>11)
                            {
                                dts.Tables[0].Rows[i][2] = newname.TrimEnd() + ',';
                            }
                        }
                        


                    }
                    //name = dts.Tables[0].Rows[i][2].ToString();
                    //string output = Regex.Replace(name, ", $", "");
                }
                   
                
            }
            catch (Exception ex)
            {

            }

            return dts;
        }

        public void CreateTable()
        {
            DataColumn Root_Name = new DataColumn("Root_Name");
            Root_Name.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(Root_Name);

            DataColumn Start_loc = new DataColumn("source");
            Start_loc.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(Start_loc);

            DataColumn End_loc = new DataColumn("destination");
            End_loc.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(End_loc);

            DataColumn bustype = new DataColumn("bustype");
            bustype.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(bustype);

        }

        public void addcheckboxclk()
        {
            if (grdRoute.Rows.Count > 0)
            {
                gridadd.Visible = true;
                Add_Details = new DataTable();

                CreateTable();
                serviceoperatorcreate();
                if (grdRoute.Rows.Count > 0)
                {
                    DataRow row = default(DataRow);

                    grdRoute.Visible = true;

                    for (int i = 0; i <= grdRoute.Rows.Count - 1; i++)
                    {

                        if (grdRoute.Rows[i].RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chkbx = (CheckBox)grdRoute.Rows[i].FindControl("Select");
                            CheckBox chkbx1 = (CheckBox)grdRoute.Rows[i].FindControl("chk");

                            if ((chkbx.Checked == true))
                            {
                                row = Add_Details.NewRow();
                                row[0] = grdRoute.Rows[i].Cells[0].Text;
                                row[1] = grdRoute.Rows[i].Cells[1].Text;
                                row[2] = grdRoute.Rows[i].Cells[2].Text;
                                row[3] = grdRoute.Rows[i].Cells[3].Text;
                                Add_Details.Rows.Add(row);
                            }
                        }
                    }
                }
            }
            gridadd.DataSource = Add_Details;
            gridadd.DataBind();
            spanetm.Visible = true;
            ViewState["Grid_all_Details"] = Add_Details;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 9;

                if (txtetmcode.Text == "")
                {
                    txtetmcode.Focus();
                    Response.Write("<script>alert ('Enter ETVM code') </script>");
                    return;
                }
                Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();

                info.FileName = (@"D:\pratinidhi\etm_com.exe ");
                info.Arguments = "9";
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = info;
                process.StartInfo.UseShellExecute = false;
                System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "4");
                process.Close();

                dm.ExecuteNonQuery("update etm_master set is_route_update = 0 where code = " + txtetmcode.Text);
            }
            catch (Exception ex)
            {

            }
        }

        [ScriptMethod()]
        [WebMethod]
        public static List<string> GetCompletionList(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select code from [etm_master] where " +
                    "code like @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> customers = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(sdr["code"].ToString());
                        }
                    }
                    conn.Close();
                    return customers;
                }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            System.Data.DataRowView drv;
            drv = (System.Data.DataRowView)e.Row.DataItem;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (drv != null)
                {

                }
            }
        }
    }
}