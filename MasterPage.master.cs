﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            if (Session["UserName"] == null)
            {
                Response.Write("<script>alert('Your Session Expired.');window.location.href='Login.aspx';</script>");
            }

            else
            {
                HtmlGenericControl li = new HtmlGenericControl();

                if (Convert.ToString(Session["user_type"]) == "5")
                {
                    lireset.Style["display"] = "block";
                    lierrortic.Style["display"] = "block";
                    lifare.Style["display"] = "block";
                    liuser.Style["display"] = "block";
                }
                else
                {
                    lireset.Style["display"] = "none";
                    lierrortic.Style["display"] = "none";
                    lifare.Style["display"] = "none";
                    liuser.Style["display"] = "none";
                }
                User.Text = Session["UserName"].ToString().ToUpper();
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();

        }

    }
}
