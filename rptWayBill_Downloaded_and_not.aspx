﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="rptWayBill_Downloaded_and_not.aspx.cs" Inherits="rptWayBill_Downloaded_and_not" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style media="Print" type="text/css">
        .ctrl {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize {
            font-family: Times New Roman;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="3">
                        <a class="ctrl" href="reports.aspx">Back</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <h3>WayBill Downloaded and Not Downloaded Report</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table border="0" width="100%">
                            <tr class="ctrl">
                                <td nowrap width="169">
                                    <font style="font-family: arial; font-size: 12px;">From Date : </font>
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Date" runat="server" Width="80px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnfrom" TargetControlID="Txt_Date">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                    <asp:ScriptManager ID="Scriptmanager1" runat="server">
                                    </asp:ScriptManager>
                                </td>
                                <td class="style2">&nbsp;
                                </td>
                                <td width="53">
                                    <font style="font-family: arial; font-size: 12px;">To Date :</font>
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Todate" runat="server" Width="80px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnto" TargetControlID="Txt_Todate">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                </td>
                                <td width="174">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="ctrl">
                    <td style="width:200px;">  &nbsp;
                    </td>
                   <td style="width:200px;"> &nbsp;</td>
                    <td>
                        <asp:Button ID="Btn_search" runat="server" Text="Search" OnClick="Btn_search_Click" />
                        <asp:Button ID="Btn_ConvertExcl" runat="server" OnClick="Btn_ConvertExcl_Click" Text="Convert To Excel" />
                    </td>

                </tr>
                <tr>
                    <td colspan="4">
                        &emsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Lbl_Date" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Label ID="Lbldate" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divtbl" runat="server">
        </div>
    </form>
</body>



</html>
