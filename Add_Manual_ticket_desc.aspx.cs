﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Collections;
using System.Text.RegularExpressions;

public partial class Add_Manual_ticket_desc : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    static SqlConnection cons = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();

    double a;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Request.QueryString["Opt1"] == "getconcession")
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("check_waybill '" + Request.QueryString["waybillno"].ToString() + "' ", con);
            con.Open();
            da.Fill(ds);
            con.Close();

            if (ds.Tables[0].Rows[0][0].ToString() == "Manual Ticket Entry Exist")
            {
                Response.Write("ManualExist");
            }
            else if (ds.Tables[0].Rows[0][0].ToString() == "Manual Ticket Entry Not Exist")
            {
                Response.Write("ManualNotexist");
            }
            else
            {
                Response.Write("NotGenuine");
            }
        }
        if (Request.QueryString["receipno"] != null)
        {
            txtWBNo.Text = Request.QueryString["receipno"];
            lbldri.Text = Request.QueryString["driverno"];
            lblcon.Text = Request.QueryString["conno"];
            lblcash.Text = Request.QueryString["cash"];
            lblvehicle.Text = Request.QueryString["vehicleo"];
            fillgrid();
        }

        if (!IsPostBack)
        {
            divtab1.Visible = true;
            divtab2.Visible = true;
            divtab3.Visible = false;
            txtWBNo.Attributes.Add("onblur", "getdata(this)");
            btndelete.Visible = false;
            if (Convert.ToString(Session["user_type"]) == "5")
                trsearch.Visible = true;
            else
                trsearch.Visible = false;
            txt_WBDate.Text = DateTime.Now.Date.ToString("dd-MMM-yyyy");
            txtWBNo.Focus();
        }
    }

    public void fillgrid()
    {
        try
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ticket_detail_report";
            cmd.Parameters.Add(new SqlParameter("@waybillno", Request.QueryString["receipno"]));
            cmd.Parameters.Add(new SqlParameter("@ticketno", ""));
            adp.SelectCommand = cmd;
            adp.Fill(ds);
            grdWayBill.DataSource = ds;
            grdWayBill.DataBind();

            lblETVMamt.Text = ds.Tables[1].Rows[0]["ticket_amnt"].ToString();
            Int32 diffamt;
            diffamt = Int32.Parse(Request.QueryString["cash"].ToString()) - Int32.Parse(ds.Tables[1].Rows[0]["ticket_amnt"].ToString());
            lbldiffamt.Text = diffamt.ToString();
            lbletvmandmanual.Text = ds.Tables[1].Rows[0]["ticket_amnt"].ToString();
            lblmanamount.Text = "0";

        }
        catch (Exception ex)
        {
            con.Close();
        }
        finally
        {
            con.Close();
        }

    }
    protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.Cells[7].Text == "O")
            {
                e.Row.Cells[7].Text = "OK";
            }
            else if (e.Row.Cells[7].Text == "F")
            {
                e.Row.Cells[7].Text = "Fail";
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                a += double.Parse(e.Row.Cells[5].Text);
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[5].Text = a.ToString();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("select mtd_waybillno from Manual_ticket_detail where mtd_waybillno = '" + txtWBNo.Text + "' ", con);
            con.Open();
            da.Fill(ds);
            con.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                string scp = "<script type='text/javascript'>alert('This Waybill is already entered');window.location.replace('Add_Manual_ticket_desc.aspx');</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);
            }
            else
            {
                if (Hid_data.Value != "")
                {
                    string s = Hid_data.Value;
                    string[] rowdata = s.Split('|');
                    int i;
                    for (i = 0; i < rowdata.Length - 1; i++)
                    {
                        string[] columndata = rowdata[i].Split('^');
                        cmd = new SqlCommand();
                        cmd.CommandText = "ins_manual_ticket_details";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@mtd_denom", columndata[0].ToString());
                        cmd.Parameters.AddWithValue("@mtd_Ptype", columndata[1].ToString());
                        cmd.Parameters.AddWithValue("@mtd_tic_count", columndata[2].ToString());
                        cmd.Parameters.AddWithValue("@mtd_amount", columndata[3].ToString());
                        cmd.Parameters.AddWithValue("@mtd_waybillno", txtWBNo.Text);
                        cmd.Parameters.AddWithValue("@mtd_trans_date", txt_WBDate.Text);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    updatecashiercash();
                    update_After_Manual();

                    string scp = "<script type='text/javascript'>alert('Data Saved Successfully');window.location.replace('DownloadWaybill.aspx');</script>";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);
                }
            }
        }
        catch (Exception ex)
        {

        }
        finally
        {
            con.Close();
        }
    }

    public void updatecashiercash()
    {
        cmd = new SqlCommand();
        cmd.CommandText = "updatecashiercash";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.AddWithValue("@waybillno", txtWBNo.Text);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
    }

    public void update_After_Manual()
    {
        cmd = new SqlCommand();
        cmd.CommandText = "Update_Manual";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.AddWithValue("@waybillno", txtWBNo.Text);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
    }


    [WebMethod]
    public static string GetData(string waybillno)
    {
        string data = "";
        try
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds1 = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = cons;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Search_Waybill_to_Edit";
            cmd.Parameters.Add(new SqlParameter("@waybillno", waybillno));

            adp.SelectCommand = cmd;
            adp.Fill(ds1);

            int manual_amount = 0, etm_amount = 0, cash_amount = 0;

            if (ds1.Tables[1].Rows.Count > 0)
            {
                manual_amount = Convert.ToInt32(ds1.Tables[1].Rows[0]["manual_amt"]);
                etm_amount = Convert.ToInt32(ds1.Tables[1].Rows[0]["etm_amt"]);
                cash_amount = Convert.ToInt32(ds1.Tables[1].Rows[0]["cash_amt"]);

                data = etm_amount + "~" + manual_amount + "~" + cash_amount + "~" + Convert.ToString(ds1.Tables[1].Rows[0]["conductor"]);
            }

        }
        catch (Exception ex)
        {

        }
        return data;
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        try
        {
            divtab1.Visible = false;
            divtab2.Visible = false;
            divtab3.Visible = true;

            if (txtsearch.Text == "")
            {
                Response.Write("<script>alert ('Enter waybillno') </script>");
            }
            else
            {
                bindGrid();
            }
        }
        catch (Exception ex)
        {
            con.Close();
        }
        finally
        {
            if (con.State == ConnectionState.Open)
                con.Close();
        }
    }

    public void bindGrid()
    {
        try
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds1 = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Search_Waybill_to_Edit";
            cmd.Parameters.Add(new SqlParameter("@waybillno", txtsearch.Text));

            adp.SelectCommand = cmd;
            adp.Fill(ds1);
            if (ds1.Tables[0].Rows.Count > 0)
            {
                int manual_amount = Convert.ToInt32(ds1.Tables[1].Rows[0]["manual_amt"]);
                int etm_amount = Convert.ToInt32(ds1.Tables[1].Rows[0]["etm_amt"]);
                int cash_amount = Convert.ToInt32(ds1.Tables[1].Rows[0]["cash_amt"]);

                gridManual.DataSource = ds1.Tables[0];
                gridManual.DataBind();
                hdticid.Value = Convert.ToString(ds1.Tables[0].Rows[0]["mtd_waybillno"]);
                lblETVMamt.Text = etm_amount.ToString();
                lblcon.Text = Convert.ToString(ds1.Tables[1].Rows[0]["conductor"]);
                lblmanamount.Text = manual_amount.ToString();
                lbletvmandmanual.Text = Convert.ToInt32(etm_amount + manual_amount).ToString();
                lblcash.Text = cash_amount.ToString();
                btndelete.Visible = true;
            }
            else
            {
                gridManual.DataSource = null;
                gridManual.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    public void Clear()
    {
        divtab1.Visible = true;
        divtab2.Visible = true;
        divtab3.Visible = false;
        btndelete.Visible = false;
        txtWBNo.Text = "";
        txtsearch.Text = "";
        lblETVMamt.Text = "";
        lblmanamount.Text = "";
        lbletvmandmanual.Text = "";
        lblcash.Text = "";
        lblcon.Text = "";
        if (Convert.ToString(Session["user_type"]) == "5")
            trsearch.Visible = true;
        else
            trsearch.Visible = false;
    }

    protected void tbncancel_Click(object sender, EventArgs e)
    {
        Clear();
    }

    public void delete()
    {
        SqlCommand cmd1 = new SqlCommand();
        {
            try
            {
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Clear();
                cmd1 = new SqlCommand("Sp_DeleteManualTicket", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@waybillno", hdticid.Value);
                cmd1.Parameters.AddWithValue("@userid", Convert.ToInt32(Session["user_id"]));
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
                hdticid.Value = "0";
                Clear();
            }
            catch (Exception ex)
            {

            }
        }
    }

    protected void btndelete_Click(object sender, EventArgs e)
    {
        if (gridManual.Rows.Count > 0)
        {
            delete();
        }
        else
        {
            Response.Write("<script>alert ('Enter waybillno') </script>");
        }
    }

    
}