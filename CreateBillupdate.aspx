﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CreateBillupdate.aspx.cs" EnableEventValidation="false" Inherits="CreateBillupdate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table style="width: 100%;">
        <tr>
            <td colspan="4" style="height: 20px" align="center">
                <h2>Way Bill Creation</h2>
            </td>
        </tr>
        <tr>
            <td style="width: 246px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Date :"></asp:Label>
            </td>
            <td style="width: 200px">
                <asp:TextBox ID="Txt_date" runat="server" Font-Bold="True" Font-Size="12pt"
                    Width="180px" ReadOnly="true" AutoPostBack="True"></asp:TextBox>

                <asp:ImageButton ID="ImgPopBtn" runat="server"
                    ImageAlign="Middle" ImageUrl="~/images/imgCalendar.png" Width="27px" />
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                    PopupButtonID="ImgPopBtn" TargetControlID="Txt_Date">
                </asp:CalendarExtender>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td style="width: 226px">
                <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Conductor :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="Txt_ConductorName" runat="server" Font-Bold="True"
                    Font-Size="12pt" Width="190px"></asp:TextBox>
                <asp:Label ID="lblconname" runat="server" ForeColor="Red" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 246px; height: 23px;">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Way Bill  Number  :"></asp:Label>
            </td>
            <td style="width: 200px; height: 23px;">
                <asp:TextBox ID="Txt_Waybillnumber" runat="server" Font-Bold="True"
                    Font-Size="12pt" Width="180px" ReadOnly="True"></asp:TextBox>
            </td>
            <td style="width: 226px; height: 23px;">
                <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Vehicle :"></asp:Label>
            </td>
            <td style="height: 23px">
                <asp:DropDownList ID="DDL_Vehicle" runat="server" Font-Bold="True"
                    Font-Size="12pt" Height="25px" Width="180px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 246px; height: 23px;">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Driver  :"></asp:Label>
            </td>
            <td style="width: 200px; height: 23px;">
                <asp:DropDownList ID="Ddl_Driver" runat="server" Font-Bold="True"
                    Font-Size="12pt" Height="30px" Width="180px">
                </asp:DropDownList>
            </td>
            <td style="width: 226px; height: 23px;">
                <asp:Label ID="Label13" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Depo :"></asp:Label>
            </td>
            <td style="height: 23px">
                <asp:DropDownList ID="DDL_Depo" runat="server" Font-Bold="True"
                    Font-Size="12pt" Height="30px" Width="180px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 246px; height: 23px;">
                <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Division  :"></asp:Label>
            </td>
            <td style="width: 200px; height: 23px;">
                <asp:DropDownList ID="DDl_Division" runat="server" Font-Bold="True"
                    Font-Size="12pt" Height="24px" Width="180px">
                </asp:DropDownList>
            </td>
            <td style="width: 226px; height: 23px;">
                <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="ETM Number :"></asp:Label>
            </td>
            <td style="height: 23px">
                <asp:TextBox ID="Txt_Etmnumber" runat="server" Font-Bold="True"
                    Font-Size="12pt" Width="230px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 246px">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Schedule :"></asp:Label>
            </td>
            <td style="width: 200px">
                <asp:TextBox ID="Txt_schedule" runat="server" Font-Bold="True" Font-Size="12pt"
                    Width="230px"></asp:TextBox>
            </td>
            <td style="width: 226px">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Conductor Cash :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="Txt_Conductorcash" runat="server" Font-Bold="True"
                    Font-Size="12pt" Width="230px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 246px; height: 20px;"></td>
            <td style="width: 200px; height: 20px;"></td>
            <td style="width: 226px; height: 20px;"></td>
            <td style="height: 20px"></td>
        </tr>
        <tr>
            <td style="width: 246px">
                <asp:TextBox ID="Txt_search" runat="server" AutoPostBack="True"
                    Font-Bold="True" Font-Size="12pt" Width="136px"
                    placeholder="Way bill Number" ValidationGroup="rs"></asp:TextBox>

                &nbsp;<asp:Button ID="Btn_search" runat="server" Font-Bold="True" Font-Size="12pt"
                    OnClick="Btn_search_Click" Text="Go" Width="40px" />

                &nbsp;<asp:Button ID="btnwaybill" runat="server" Font-Bold="True" Font-Size="12pt"
                    OnClick="btnwaybill_Click" Text="Back" Width="40px" />
            </td>
            <td style="width: 200px">
                <asp:Button ID="Btn_update" runat="server" Font-Bold="True" Font-Size="12pt"
                    Text="Update" OnClick="Btn_update_Click" />
                &nbsp; &nbsp;<asp:Label ID="Lbl_msg" runat="server" Font-Bold="True" Font-Size="12pt"
                    ForeColor="#CC0000"></asp:Label>
            </td>
            <td style="width: 226px">
                <asp:TextBox ID="Txt_test" runat="server" AutoPostBack="True" Font-Bold="True"
                    Font-Size="12pt" placeholder="Way bill Number"
                    Width="136px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 246px; height: 20px;"></td>
            <td style="width: 200px; height: 20px;"></td>
            <td style="width: 226px; height: 20px;"></td>
            <td style="height: 20px"></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="gridwaybill" runat="server" AutoGenerateColumns="false" EmptyDataText="No Data to Edit" EmptyDataRowStyle-Font-Bold="true"
                    OnRowCommand="gridwaybill_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="SrNo.">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%#Container.DataItemIndex + 1%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="WayBillNo" HeaderText="WayBill No" />
                        <asp:BoundField DataField="ConductorName" HeaderText="Conductor Name" />
                        <asp:BoundField DataField="DriverName" HeaderText="Driver Name" />
                        <asp:BoundField DataField="vehiclecode" HeaderText="Vehicle Code" />
                        <asp:BoundField DataField="divcode" HeaderText="Division" />
                        <asp:BoundField DataField="depocode" HeaderText="Depo Name" />
                        <asp:BoundField DataField="Etm_Code" HeaderText="ETM No" />
                        <asp:BoundField DataField="modifieddate" HeaderText="Date" />
                        <asp:BoundField DataField="schedule" HeaderText="Schedule" />

                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:Button ID="Btn_edit" runat="server" Text="Edit" CommandName="show" CommandArgument='<%#Eval("WayBillNo")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 246px">&nbsp;
            </td>
            <td style="width: 200px">&nbsp;
            </td>
            <td style="width: 226px">&nbsp;
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 246px">&nbsp;</td>
            <td style="width: 200px">&nbsp;</td>
            <td style="width: 226px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 246px">&nbsp;</td>
            <td style="width: 200px">&nbsp;</td>
            <td style="width: 226px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 246px">&nbsp;</td>
            <td style="width: 200px">&nbsp;</td>
            <td style="width: 226px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 246px">&nbsp;</td>
            <td style="width: 200px">&nbsp;</td>
            <td style="width: 226px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 246px">&nbsp;</td>
            <td style="width: 200px">&nbsp;</td>
            <td style="width: 226px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <script type="text/javascript">
        $(document).ready(function () {

        });

    </script>
</asp:Content>

