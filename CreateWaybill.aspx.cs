﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using DataManager;
using System.IO;
using TimerGetData;
using System.Reflection;
using System.Configuration;
using System.Diagnostics;

namespace DataManager
{

    public partial class CreateWaybill : System.Web.UI.Page
    {
        DataManager dm = new DataManager();
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        string connect = System.Configuration.ConfigurationSettings.AppSettings["Con"].ToString();
        SqlCommand cmd;
        SqlDataReader dr;
        String selQry = "Sp_Getwaybilldata";

        bool inUse = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnEdit.Visible = false;
            txtcondcash.Enabled = false;
            txtSchd.Attributes.Add("autocomplete", "off");
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnSave);

            if (Request.QueryString["Opt1"] == "getconcession")
            {
                string sttray = "";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select WayBillNo from WayBill_Details where WayBillNo='" + txtWBNo.Text + "' ", con);
                con.Open();
                da.Fill(ds);
                con.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("notexist~");
                }
                else
                {
                    Response.Write("exist~" + sttray.ToString() + "~");
                }
            }

            if (!IsPostBack)
            {
                if (Request.QueryString["type"] != null)
                {
                    checkandupdate_waybill();
                }
                try
                {
                    Literal1.Visible = false;
                    dm.Main();
                    dm.ExecuteGrid(selQry, grdWayBill);
                    string SelddlCond = "select ConductorId,ConductorName from Conductor_Master where Is_Delete=0 order by ConductorName";// and(ConductorName not  in (select conductorname from WayBill_Details)) and convert(varchar(10) ,CreatedDate,110)=convert(varchar(10) ,GETDATE(),110)";
                    string SelddlDrv = "select DriverId,DriverName from Driver_Master where Is_Delete=0 order by DriverName";
                    string SelddlVeh = "select VehicleId,VehicleNo from Vehicle_Master where Is_Delete=0 order by VehicleNo";
                    string SelddlDiv = "select DivCode,DivName from Division_Master where Is_Delete=0";
                    string SelddlDepo = "select depocode,deponame from depo_master where is_delete=0";
                    dm.FillDDl(SelddlDrv, ddlDriver, "DriverId", "DriverName", "---Driver---");
                    dm.FillDDl(SelddlDiv, ddlDiv, "DivCode", "DivName", "---Division---");
                    dm.FillDDl(SelddlVeh, ddlVehcle, "VehicleId", "VehicleNo", "---VehicleCode---");
                    dm.FillDDl(SelddlDepo, ddlDepo, "depocode", "deponame", "---DepoName--");

                    ddlDriver.SelectedValue = "386";
                    ddlVehcle.SelectedValue = "130";


                    txt_WBDate.Text = DateTime.Now.Date.ToString("dd-MMM-yyyy");
                    txtsearch_by_date.Text = DateTime.Now.Date.ToString("dd-MMM-yyyy");
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter("Get_max_waybillno", con);
                    con.Open();
                    da.Fill(ds);

                    txtWBNo.Text = ds.Tables[0].Rows[0][0].ToString();
                    ddlDiv.SelectedItem.Text = "Kolhapur";
                    ddlDepo.SelectedItem.Text = "10";
                }

                catch (Exception ex)
                {
                    library.WriteErrorLog("Error occured in waybill page load function " + ex.Message.ToString() + " " + ex.Source);
                }
            }
        }

        public void checkandupdate_waybill()
        {
            System.Threading.Thread.Sleep(2000);
            library.WriteErrorLog("checkandupdate_waybill " + DateTime.Now.ToString());
            try
            {
                if (System.IO.File.Exists(@"d:\\pratinidhi\\ETM_OUT.txt"))
                {
                    while (inUse)
                    {
                        inUse = FileInUse();
                        if (inUse)
                            System.Threading.Thread.Sleep(2000);
                    }
                    string[] lines12 = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");

                    library.WriteErrorLog("checkandupdate_waybill 1" + lines12[0]);

                    string Obj = lines12[0].ToString().Substring(0, 5);

                    if (Obj == "$DONE")
                    {
                        SqlCommand cmd1 = new SqlCommand("update waybill_details set Is_programmed=1 where WayBillNo='" + Request.QueryString["receipno"].ToString() + "'", con);
                        con.Open();
                        cmd1.ExecuteNonQuery();
                        con.Close();
                        GridWayBillBind(Request.QueryString["receipno"].ToString());
                        lblerror.Text = "";
                    }
                    else
                    {
                        string Error = "'" + lines12[0].ToString() + "'";
                        Response.Write("<script>alert(" + Error + ")</script>");
                    }
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error in checkandupdate_waybill func " + ex.Message);
            }

        }

        public static bool FileInUse()
        {
            string filePt = @"D:\Pratinidhi\ETM_OUT.txt";

            try
            {
                using (FileStream logFileStream = new FileStream(filePt, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader logFileReader = new StreamReader(logFileStream))
                    {
                        string text = logFileReader.ReadToEnd();
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                return true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text == "Save")
            {
                try
                {
                    DataSet ds = new DataSet();
                    DataSet ds12 = new DataSet();
                    DataSet ds11 = new DataSet();
                    DataSet ds13 = new DataSet();

                    SqlDataAdapter da = new SqlDataAdapter("select * from dutymaster where duty_no ='" + txtSchd.Text + "' and ISNULL(is_delete,0) = 0 ", con);
                    //SqlDataAdapter da11 = new SqlDataAdapter("select * from Conductor_Master where emp_code ='" + txtconductor.Text + "' ", con);
                    SqlDataAdapter da11 = new SqlDataAdapter("select * from conductor_master  where CondCode = (select top 1 CondctrCode from CondctrNo_yrwise where EmpCode = '" + txtconductor.Text + "' order by Years desc)", con);  //updated jayant on 02-01-2018
                    
                    SqlDataAdapter da12 = new SqlDataAdapter("select WayBillNo from WayBill_Details where WayBillNo='" + txtWBNo.Text + "' ", con);
                    SqlDataAdapter da13 = new SqlDataAdapter("select * from etm_master where code ='" + txtETM.Text + "' and isnull(is_delete,0) = 0 ", con);

                    con.Open();
                    da.Fill(ds);
                    da12.Fill(ds12);
                    da13.Fill(ds13);
                    da11.Fill(ds11);
                    con.Close();

                    Int32 ds1 = Getdataset();

                    library.WriteErrorLog(" ds1 " + ds1);

                    if (txtWBNo.Text == "")
                    {
                        Response.Write("<script>alert ('Enter Waybill Number') </script>"); GetConductor(); return;
                    }
                    else if (txt_WBDate.Text == "")
                    {
                        Response.Write("<script>alert ('Enter Waybill Date') </script>"); GetConductor(); return;
                    }
                    else if (ddlDriver.SelectedItem.Text == "---Driver---")
                    {
                        Response.Write("<script>alert ('Select Driver Name') </script>"); GetConductor(); return;
                    }
                    else if (txtconductor.Text == "")
                    {
                        Response.Write("<script>alert ('Plaese Enter Conductor Name') </script>"); GetConductor(); return;
                    }
                    else if (ddlVehcle.SelectedItem.Text == "---VehicleCode---")
                    {
                        Response.Write("<script>alert ('Select Vehicle No') </script>"); GetConductor(); return;
                    }
                    else if (txtSchd.Text == "")
                    {
                        Response.Write("<script>alert ('Enter Schedule No') </script>"); GetConductor(); return;
                    }
                    else if (ds.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("<script>alert ('Duty No. does not exists') </script>"); GetConductor(); return;
                    }
                    else if (ds13.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("<script>alert ('Etm No. does not exists') </script>"); GetConductor(); return;
                    }
                    else if (ds11.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("<script>alert ('Conductor does not exists') </script>"); GetConductor(); return;
                    }
                    else if (ds12.Tables[0].Rows.Count > 0)
                    {
                        Response.Write("<script>alert ('Already Exist') </script>"); GetConductor();
                    }
                    else if (Convert.ToInt32(ds1) == 1)
                    {
                        Response.Write("<script>alert ('Please update route, fare has been change') </script>"); GetConductor(); return;
                    }
                    else
                    {
                        waybillcreate();
                        library.WriteErrorLog("Executed waybillcreate ");
                        String[] par = new String[13];
                        par[0] = txtWBNo.Text;
                        par[1] = txt_WBDate.Text;
                        par[2] = txtempcode.Text.ToString();
                        par[3] = ddlDriver.SelectedItem.Text.ToString();
                        par[4] = ddlVehcle.SelectedItem.Text.ToString();
                        par[5] = ddlDiv.SelectedItem.Text.ToString();
                        par[6] = ddlDepo.SelectedItem.Text.ToString();
                        par[7] = txtSchd.Text;
                        par[8] = txtETM.Text;
                        par[11] = Session["user_id"].ToString();
                        par[12] = txtconductor.Text;

                        library.WriteErrorLog("Executed waybillcreate user id");
                        if (btnSave.Text == "Save")
                        {
                            par[9] = "0";
                            par[10] = "0";
                        }
                        else if (btnSave.Text == "Update")
                        {
                            btnSave.Text = "Save";
                        }
                        else
                        {
                            par[9] = "0";
                            par[10] = "0";
                        }
                        library.WriteErrorLog(" Executing WAYBILL_ADD_UPDATE_DYNAMIC procedure");
                        dm.ExecuteProcedure(par, 13, "WAYBILL_ADD_UPDATE_DYNAMIC");
                        library.WriteErrorLog("Successfully Executed WAYBILL_ADD_UPDATE_DYNAMIC procedure");
                        dm.ExeCuteGridBind(selQry, grdWayBill);

                        Response.Redirect("CreateWaybill.aspx");
                    }
                }
                catch (Exception ex)
                {
                    library.WriteErrorLog("Error occured in waybill save function " + ex.Message.ToString() + " " + ex.Source);
                }
            }
            else if (btnSave.Text == "Update")
            {
                try
                {
                    waybillcreate();

                    SqlCommand cmd1 = new SqlCommand();
                    try
                    {
                        //DataTable dtconcode = dm.GetDataTable("select EmpCode from CondctrNo_yrwise  where CondctrCode ='" + txtconductor.Text.ToString() + "' order by years desc"); //jayant on 02-01-2018
                        cmd1.Connection = con;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.Clear();
                        cmd1 = new SqlCommand("Sp_update_create_waybill", con);
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@waybillno", txtWBNo.Text);
                        cmd1.Parameters.AddWithValue("@date", txt_WBDate.Text);
                        cmd1.Parameters.AddWithValue("@conductor", txtempcode.Text);  //jayant on 02-01-2018
                        cmd1.Parameters.AddWithValue("@driver", ddlDriver.SelectedItem.Text.ToString());
                        cmd1.Parameters.AddWithValue("@vehicle", ddlVehcle.SelectedItem.Text.ToString());
                        cmd1.Parameters.AddWithValue("@div", ddlDiv.SelectedItem.Text.ToString());
                        cmd1.Parameters.AddWithValue("@depo", ddlDepo.SelectedItem.Text.ToString());
                        cmd1.Parameters.AddWithValue("@schedule", txtSchd.Text);
                        cmd1.Parameters.AddWithValue("@etm", txtETM.Text);
                        cmd1.Parameters.AddWithValue("@id", hdWayCode.Value.ToString());
                        cmd1.Parameters.AddWithValue("@userid", Session["user_id"].ToString());
                        cmd1.Parameters.AddWithValue("@Old_conductor", txtconductor.Text);
                        con.Open();
                        cmd1.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        library.WriteErrorLog("Error occured in waybill update function " + ex.Message.ToString() + " " + ex.Source);
                    }
                    btnSave.Text = "Save";
                    dm.ExecuteGrid(selQry, grdWayBill);
                    Response.Redirect("CreateWaybill.aspx");
                }
                catch (Exception ex)
                {
                    library.WriteErrorLog("Error occured in btnSave_Click function " + ex.Message.ToString() + " " + ex.Source);
                }
            }

        }

        public Int32 Getdataset()
        {
            DataSet ds1 = new DataSet();
            Int32 val = 0;

            try
            {
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd = new SqlCommand();
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Sp_checketvm";
                cmd.Parameters.Add(new SqlParameter("@etvm", txtETM.Text));
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    string i = Convert.ToString(dr[0]);
                    val = Convert.ToInt32(i);
                }
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                library.WriteErrorLog("Error occured in Getdataset function " + ex.Message.ToString() + " " + ex.Source);
            }
            return val;
        }

        public void waybillcreate()
        {

            File.WriteAllText(@"d:\\pratinidhi\\WAYBILL.txt", String.Empty);

            FileStream fs1 = new FileStream("d:\\pratinidhi\\WAYBILL.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs1);

            string d, r = "", m, n = "", o, p = "", f, fa = "", g, ga, h, ha = "", i, ia = "", j, ja = "", k, ka = "", l, la = "", q, qa = "", s, sa = "", t, ta = "", u, ua = "", v, va = "", cond, condcash = "";

            m = txtWBNo.Text;
            o = txtconductor.Text;


            d = "   ";  /////////  blank Spaces
            f = "   ";  /////////  blank Spaces
            h = ddlDiv.SelectedItem.Text;
            i = "01";
            j = ddlDepo.SelectedItem.Text;
            k = "01";
            l = txtSchd.Text;
            q = txtETM.Text;
            s = DateTime.Now.ToString("dd/MM/yy");
            cond = txtcondcash.Text;

            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            t = DateTime.Now.ToString("HH:mm:ss");
            u = "00.00";

            if (m.Length < 8)
            {
                n = n.PadRight(8 - m.Length);
            }
            if (d.Length < 16)
            {
                r = r.PadRight(16 - d.Length);
            }

            if (o.Length < 16)
            {
                p = p.PadRight(16 - o.Length);
            }

            if (f.Length < 10)
            {
                fa = fa.PadRight(10 - f.Length);
            }
            if (h.Length < 12)
            {
                ha = ha.PadRight(12 - h.Length);
            }
            if (i.Length < 2)
            {
                ia = ia.PadRight(2 - i.Length);
            }
            if (j.Length < 12)
            {
                ja = ja.PadRight(12 - j.Length);
            }
            if (k.Length < 2)
            {
                ka = ka.PadRight(2 - k.Length);
            }
            if (l.Length < 4)
            {
                la = la.PadRight(4 - l.Length);
            }
            if (q.Length < 4)
            {
                qa = qa.PadRight(4 - q.Length);
            }
            if (s.Length < 8)
            {
                sa = sa.PadRight(8 - s.Length);
            }
            if (t.Length < 8)
            {
                ta = ta.PadRight(8 - t.Length);
            }
            if (u.Length < 9)
            {
                ua = ua.PadRight(9 - u.Length);
            }
            if (cond.Length < 6)
            {
                condcash = condcash.PadLeft((6 - cond.Length), '0');
            }
            writer.Write(m.ToString() + n + "," + o.ToString() + p + "," + d.ToString() + r + "," + f.ToString() + fa + "," + h.ToString() + ha + "," + i.ToString() + ia + "," + j.ToString() + ja + "," + k.ToString() + ka + "," + l.ToString() + la + "," + q.ToString() + qa + "," + s.ToString() + sa + "," + t.ToString() + ta + "," + condcash + cond.ToString() + "." + "0" + "0");
            writer.Close();
        }
        protected void ddlDiv_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ddlDepoQry = "select depocode,deponame from Depo_Master where Is_Delete=0 and DivCode=" + ddlDiv.SelectedIndex.ToString();
            dm.FillDDl(ddlDepoQry, ddlDepo, "depocode", "deponame", "---Depo---");
        }
        protected void txt_WBDate_TextChanged(object sender, EventArgs e)
        {

            try
            {
                GridWayBillBind(txt_WBDate.Text);
            }

            catch (Exception ex)
            { }

        }
        protected void GridWayBillBind(String date)
        {
            try
            {
                string dtnew = "'" + date + "'";
                con.Open();
                SqlDataAdapter da;
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                cmd.CommandText = "get_WayBill";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 500;
                cmd.Connection = con;
                cmd.Parameters.Add("@date", SqlDbType.NVarChar).Value = date;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                grdWayBill.DataSource = ds;
                grdWayBill.DataBind();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }

        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (System.IO.File.Exists(@"d:\\pratinidhi\\ETM_OUT.txt"))
                {
                    System.IO.File.Delete(@"d:\\pratinidhi\\ETM_OUT.txt");
                }
                char[] delimiterChars = { ' ' };
                string filePt = @"D:\Pratinidhi\WAYBILL.txt";
                if (System.IO.File.Exists(filePt))
                {
                    string[] lines = System.IO.File.ReadAllLines(@"D:\Pratinidhi\WAYBILL.txt");
                    foreach (string line in lines)
                    {
                        string[] words = line.Split(delimiterChars);

                        Literal1.Text = words[0].ToString();
                    }
                }
                if (Literal1.Text == "")
                {
                    library.WriteErrorLog("Alert not program");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "myconfirm", "OpenConfirmDialog();", true);

                }
                else
                {
                    DataSet ds = new DataSet();
                    DataSet dsrs = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter("select distinct WayBillNo from waybill_details where  Convert(varchar,WayBillNo) ='" + Literal1.Text + "' and Is_programmed=1 ", con);
                    SqlDataAdapter dtrs = new SqlDataAdapter("select distinct top 1 WayBillNo from waybill_details where DepoCode = " + (Literal1.Text).ToString().Substring(0, 2), con);
                    con.Open();
                    da.Fill(ds);
                    dtrs.Fill(dsrs);
                    con.Close();


                    

                    if (dsrs.Tables[0].Rows.Count == 0)
                    {
                        library.WriteErrorLog("Waybill Already Programmed");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "myconfirm", "OpenConfirmDialog();", true);
                    }
                    else if (ds.Tables[0].Rows.Count > 0)
                    {
                        library.WriteErrorLog("Waybill Already Programmed");
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert(' Waybill Already Programmed ');", true);
                    }
                    else
                    {
                        int i = 9;

                        Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                        System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();

                        info.FileName = (@"D:\pratinidhi\etm_com.exe ");
                        info.Arguments = "9";
                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        process.StartInfo = info;
                        process.StartInfo.UseShellExecute = false;
                        System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "8");
                        process.Close();
                        library.WriteErrorLog("Program");
                        Response.Redirect("CreateWaybill.aspx?receipno=" + Literal1.Text + "&Type=1'");
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCompletionList(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select code from [etm_master] where " +
                    "code like @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> customers = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(sdr["code"].ToString());
                        }
                    }
                    conn.Close();
                    return customers;
                }
            }
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCompletionconductor(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select emp_code from conductor_master where " +
                    "emp_code like  '%' + @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> customers = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(sdr["emp_code"].ToString());
                        }
                    }
                    conn.Close();
                    return customers;
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCompletionduty(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from dutymaster where ISNULL(is_delete,0) = 0 and " +
                    "duty_no like  '%' + @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> duty = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            duty.Add(sdr["duty_no"].ToString());
                        }
                    }
                    conn.Close();
                    return duty;
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static ConductorDetails validate(string conname)
        {
            string msg = string.Empty;
            ConductorDetails ConductorDetails = new ConductorDetails();
            DataManager dm = new DataManager();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    //DataTable dtcon = dm.GetDataTable("select con_full_name from conductor_master  where emp_code='" + conname.ToString() + "'");
                    DataTable dtcon = dm.GetDataTable("select top 1 con_full_name,CondCode from conductor_master  where CondCode=(select top 1 CondctrCode from CondctrNo_yrwise where EmpCode='" + conname.ToString() + "' order by Years desc)");  //updated jatanta on 30-12-2017
                    if (dtcon.Rows.Count == 0)
                    {
                        msg = "true";
                        ConductorDetails = null;
                    }
                    else
                    {
                        ConductorDetails.ConductorName = dtcon.Rows[0][0].ToString();

                        ConductorDetails.EmpCode = Convert.ToString(dtcon.Rows[0]["CondCode"]);
                        
                    }
                }
            }

            return ConductorDetails;
        }
        [System.Web.Services.WebMethod]
        public static string validateetmdevice(string etmdevice)
        {
            string msg = string.Empty;
            DataManager dm = new DataManager();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {

                    DataTable dtetm = dm.GetDataTable("select code  from etm_master where code ='" + etmdevice.ToString() + "'");
                    if (dtetm.Rows.Count == 0)
                    {
                        msg = "true";
                    }
                    else
                    {
                        msg = dtetm.Rows[0][0].ToString();
                    }
                }
            }
            return msg;
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect("createbillupdate.aspx");
        }

        public string GetConductor()
        {
            string msg = string.Empty;
            try
            {

                DataManager dm = new DataManager();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        DataTable dtcon = dm.GetDataTable("select con_full_name from conductor_master  where emp_code='" + txtconductor.Text + "'");
                        if (dtcon.Rows.Count == 0)
                        {
                            msg = "true";
                        }
                        else
                        {
                            lblconname.Text = dtcon.Rows[0][0].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return msg;
        }


        protected void grdWayBill_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            try
            {
                if (e.CommandName == "show")
                {
                    string index = Convert.ToInt32(e.CommandArgument).ToString();
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select * from WayBill_Details where WayBillId =" + index, con);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {

                        DateTime time1 = Convert.ToDateTime(dt.Rows[0]["sysdate"]);

                        DateTime time2 = DateTime.Now;
                        int t = (time2 - time1).Minutes;
                        //DataTable dtcon = dm.GetDataTable("select con_full_name from conductor_master  where emp_code='" + dt.Rows[0]["ConductorName"].ToString() + "'");
                        DataTable dtcon = dm.GetDataTable("select con_full_name from conductor_master  where CondCode ='" + dt.Rows[0]["ConductorName"].ToString() + "'");
                        DataTable dtconcode = dm.GetDataTable("select EmpCode from CondctrNo_yrwise  where CondctrCode ='" + dt.Rows[0]["ConductorName"].ToString() + "' order by years desc");
                        hdWayCode.Value = index;
                        txt_WBDate.Text = Convert.ToDateTime(dt.Rows[0]["CreatedDate"]).ToString("dd/MM/yyyy");
                        txtWBNo.Text = dt.Rows[0]["WayBillNo"].ToString();
                        txtETM.Text = dt.Rows[0]["Etm_Code"].ToString();
                        txtempcode.Text = dt.Rows[0]["ConductorName"].ToString();
                        ddlDriver.Items.Add(dt.Rows[0]["DriverName"].ToString());
                        ddlDiv.Items.Add(dt.Rows[0]["divcode"].ToString());

                        txtconductor.Text = dtconcode.Rows[0]["EmpCode"].ToString();
                        ddlVehcle.Items.Add(dt.Rows[0]["vehiclecode"].ToString());
                        ddlDepo.Items.Add(dt.Rows[0]["depocode"].ToString());
                        lblconname.Text = Convert.ToString(dtcon.Rows[0]["con_full_name"]);
                        txtSchd.Text = dt.Rows[0]["Schedule"].ToString();
                        txtcondcash.Text = dt.Rows[0]["Cashier_Amount"].ToString();

                        btnSave.Text = "Update";
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String status = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "stat"));
                string allowtoupdate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsUpdate"));
                Int32 isdownloaded = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IsDownloaded"));

                if (Convert.ToString(Session["user_type"]) == "5" && isdownloaded > 0)
                {
                    Button Btn_edit = (e.Row.FindControl("Btn_edit") as Button);
                    Btn_edit.Visible = true;
                }
                else if (status == "Not Programmed" && allowtoupdate == "1")
                {
                    Button Btn_edit = (e.Row.FindControl("Btn_edit") as Button);
                    Btn_edit.Visible = true;
                }
                else
                {
                    Button Btn_edit = (e.Row.FindControl("Btn_edit") as Button);
                    Btn_edit.Visible = false;
                }
            }
        }

        protected void txtsearch_by_date_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridWayBillBind(txtsearch_by_date.Text);
            }
            catch (Exception ex)
            { }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            GridWayBillBind(txtsearch_by_date.Text);
        }
    }
}

