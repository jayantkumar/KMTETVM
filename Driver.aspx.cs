﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DataManager;
namespace DataManager
{

    public partial class Driver : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQuery = "select code,drivername from driver_master where is_delete=0 order by ltrim(rtrim(drivername))  ";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["Opt1"] == "getconcession")
            {

                string sttray = "";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select DriverName from Driver_Master where DriverName='" + txtDrvr.Text + "' and  is_delete=0  order by DriverName ", con);
                con.Open();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("notexist~");
                }
                else
                {
                    Response.Write("exist~" + sttray.ToString() + "~");
                }
            }
            if (!IsPostBack)
            {
                dm.ExeCuteGridBind(selQuery, grdDriver);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("select DriverName from Driver_Master where DriverName='" + txtDrvr.Text.Trim() + "' and  is_delete=0 ", con);
            con.Open();
            da.Fill(ds);

            string drivername = txtDrvr.Text;
            con.Close();
            if (drivername.ToString().Trim() == "")
            {
                Response.Write("<script>alert ('Enter Driver Name') </script>");
            }
            else if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert ('Already Exist') </script>");
            }
            else
            {
                try
                {
                    con.Open();
                    cmd = new SqlCommand();
                    cmd.CommandText = "Driver_MASTER_ADD_UPDATE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.Add("@Drivername", SqlDbType.NVarChar).Value = txtDrvr.Text;

                    if (btnSave.Text == "Save")
                    {
                        cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@code", SqlDbType.Int).Value = 0;
                    }
                    else if (btnSave.Text == "Update")
                    {
                        cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                        cmd.Parameters.Add("@code", SqlDbType.Int).Value = Convert.ToInt32(hdDriverCode.Value);
                        btnSave.Text = "Save";
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                }
                dm.ExeCuteGridBind(selQuery, grdDriver);
                Response.Redirect("Driver.aspx");
            }
        }
        protected void grdDriver_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdDriver.PageIndex = e.NewPageIndex;
            dm.ExeCuteGridBind(selQuery, grdDriver);
        }
        protected void grdDriver_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            hdDriverCode.Value = e.CommandArgument.ToString();
            if (e.CommandName == "EditRow")
            {
                btnSave.Text = "Update";
                dr = dm.GetDataReader("Select drivername from driver_master where code=" + e.CommandArgument);
                while (dr.Read())
                {
                    txtDrvr.Text = dr[0].ToString();
                }
            }
            if (e.CommandName == "DeleteRow")
            {
                if (hdDel.Value.ToString() != "1")
                {
                    dm.ExecuteNonQuery("update driver_master set is_delete=1 where code=" + e.CommandArgument);
                    dm.ExeCuteGridBind(selQuery, grdDriver);
                }
                hdDel.Value = "0";
            }
        }
    }
}