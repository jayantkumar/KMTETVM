﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Createroot.aspx.cs" Inherits="Createroot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <table style="width: 100%;">
                <tr>
                    <td colspan="3" align="center">
                        <h2>Route Master</h2>
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="12pt"
                            Text="Route No :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 278px">
                        <asp:TextBox ID="Txt_rootname" runat="server" Font-Bold="True" Font-Size="12pt"
                            Width="220px"></asp:TextBox>
                        &nbsp;&nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="Txt_rootname" ErrorMessage="You Can't Empty RootName."
                            Font-Bold="True" Font-Size="11pt" ForeColor="#CC0000" ValidationGroup="rs"></asp:RequiredFieldValidator>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">
                        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="12pt"
                            Text="Bus Type :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 278px">
                        <asp:DropDownList ID="Ddl_bustype" runat="server" Font-Bold="True"
                            Font-Size="12pt" Height="25px" Width="220px">
                            <asp:ListItem Selected>Ordinary</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">
                        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="12pt"
                            Text="Start Stage :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 278px">
                        <asp:TextBox ID="Txt_startloc" runat="server" Font-Bold="True" Font-Size="12pt"
                            Width="220px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">
                        <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="12pt"
                            Text="End Stage :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 278px">
                        <asp:TextBox ID="Txt_endloc" runat="server" Font-Bold="True" Font-Size="12pt"
                            Width="220px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="12pt"
                            Text="No of stop :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 278px">
                        <asp:TextBox ID="Txt_noofstop" runat="server" Font-Bold="True" Font-Size="12pt"
                            Width="220px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">&nbsp;</td>
                    <td class="modal-sm" style="width: 278px">
                        <asp:Button ID="Btn_Submit" runat="server" OnClick="Btn_Submit_Click"
                            Text="Submit" Width="98px" Font-Bold="True" Font-Size="12pt"
                            ValidationGroup="rs" />
                        &nbsp;<asp:Button ID="Btn_update" runat="server" Font-Bold="True" Font-Size="12pt"
                            OnClick="Btn_update_Click" Text="Update" Width="98px" />
                        &nbsp;<asp:Button ID="Btn_reset" runat="server" Font-Bold="True" Font-Size="12pt"
                            OnClick="Btn_reset_Click" Text="Reset" Width="98px" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">&nbsp;</td>
                    <td class="modal-sm" style="width: 278px">
                        <asp:Label ID="Lbl_msg" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">&nbsp;</td>
                    <td colspan="2">
                        <asp:GridView ID="Grid_RouteMaster" runat="server" AutoGenerateColumns="False"
                            OnRowCommand="Grid_RouteMaster_RowCommand" EmptyDataText="No Records Available !!!"
                            BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px"
                            Font-Strikeout="False" Font-Underline="False" Width="946px"
                            CellPadding="4" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Large"
                            GridLines="None">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <RowStyle BackColor="#E3EAEB" CssClass="datagrid_row1" Font-Names="Verdana" Font-Size="Smaller" />
                            <HeaderStyle BackColor="#1C5E55" CssClass="datagrid_heading" Font-Bold="True" Font-Names="Arial"
                                Font-Size="Small" ForeColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="SrNo.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Text='<%#Container.DataItemIndex + 1%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Root No" DataField="Root_Name" />
                                <asp:BoundField HeaderText="No_Of_Stop" DataField="no_of_stop" />
                                <asp:BoundField HeaderText="BusType" DataField="bustype" />
                                <asp:BoundField HeaderText="Start Location" DataField="Start_loc" />
                                <asp:BoundField HeaderText="End Location" DataField="End_loc" />
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:Button ID="Btn_update" runat="server" Text="Edit" CommandName="show" CommandArgument='<%#Eval("Root_Name")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">&nbsp;</td>
                    <td class="modal-sm" style="width: 278px">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">&nbsp;</td>
                    <td class="modal-sm" style="width: 278px">
                        <asp:GridView ID="Gridstg" runat="server" AutoGenerateColumns="False">


                            <Columns>
                                <asp:BoundField DataField="rootname" HeaderText="Root_Name" />
                                <asp:BoundField DataField="stage_no" HeaderText="Stage_No" />
                                <asp:BoundField DataField="stage_name_id" HeaderText="Stage_Name_ID" />
                                <asp:BoundField DataField="stage_name" HeaderText="Stage_name" />
                                <asp:BoundField DataField="fare_change_stage_no" HeaderText="Fare_stage" />
                                <asp:BoundField DataField="stg_regional_name" HeaderText="Regional_Name" />
                            </Columns>

                        </asp:GridView>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 204px">&nbsp;</td>
                    <td class="modal-sm" style="width: 278px">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

