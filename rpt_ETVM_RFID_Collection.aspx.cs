﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class rpt_ETVM_RFID_Collection : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter("select distinct UserId , UserName from User_Master", con);
                adapter.Fill(dt);

                ddlUser.DataSource = dt;
                ddlUser.DataTextField = "UserName";
                ddlUser.DataValueField = "UserId";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch (Exception ex)
            {

            }
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {

    }

    protected void Search_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlUser.SelectedValue == "0")
            {
                Response.Write("<script>alert ('Select User') </script>");
            }
            else
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Sp_ETVM_RFID_Collection";
                cmd.Parameters.Add(new SqlParameter("@fromdate", txtfromdate.Text));
                cmd.Parameters.Add(new SqlParameter("@todate", txttodate.Text));
                cmd.Parameters.Add(new SqlParameter("@User", ddlUser.SelectedValue));
                adp.SelectCommand = cmd;
                adp.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    grdWayBill.DataSource = ds;
                    grdWayBill.DataBind();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblfromdate.Text = "From Date :" + txtfromdate.Text;
                        lbltodate.Text = "To Date :" + txttodate.Text;
                        lblAmount.Text = Convert.ToDecimal(ds.Tables[1].Rows[0][0]).ToString();
                    }
                }
                else
                {
                    grdWayBill.DataSource = null;
                    grdWayBill.DataBind();

                    lblfromdate.Text = "";
                    lbltodate.Text = "";
                }
            }

        }
        catch (Exception ex)
        {
            con.Close();
        }
        finally
        {
            if (con.State == ConnectionState.Open)
                con.Close();
        }
    }

    protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}