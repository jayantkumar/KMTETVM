﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

using System.Text.RegularExpressions;
public partial class changepassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Txt_password.Attributes.Add("minimum", "3");
    }
    protected void Btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Txt_password.Text.Length < 3)
            {

                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('should have miniumum 3 Digits.');", true);
            }
            else
            {

                char[] delimiterChars = { ' ' };
                string filePt = @"D:\pratinidhi\password.txt";

                if (System.IO.File.Exists(filePt))
                {
                    using (FileStream logFileStream = new FileStream(filePt, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
                    {
                        using (StreamWriter sw = new StreamWriter(logFileStream))
                        {
                            sw.WriteLine(Txt_password.Text);
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Password set successfully.');", true);
                            Txt_password.Text = "";
                        }
                    }
                }

            }
        }
        catch (Exception ex)
        {

        }
    }
}