﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="changepassword.aspx.cs" Inherits="changepassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%;">
                <tr>
                    <td colspan="3" align="center">
                        <h2>
                            Change Password </h2>
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 214px">
                        &nbsp;
                        <asp:Label ID="Label1" runat="server" Text="Enter the password :" 
                            Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                    <td style="width: 255px">
                        &nbsp;
                        <asp:TextBox ID="Txt_password" MaxLength="4" MinimumValue="3"   runat="server" Font-Bold="True" Font-Size="12pt" 
                            Height="25px" Width="220px" onkeypress='return isNumberKey(event)' required></asp:TextBox>
                            <asp:RequiredFieldValidator ID="requiredValidator1" runat="server" ControlToValidate="Txt_password"
     ValidationGroup="valid" ForeColor="Red" ErrorMessage="Cannot be blank" />
                    </td>
                    <td>
                        <asp:Button ID="Btn_submit" runat="server" Font-Bold="True" Font-Size="12pt" 
                            Height="29px" onclick="Btn_submit_Click" Text="Submit" Width="103px" />
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 214px">
                        &nbsp;
                    </td>
                    <td style="width: 255px">
                        &nbsp;
                        <asp:Label ID="Lbl_msg" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 214px">
                        &nbsp;</td>
                    <td style="width: 255px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 214px">
                        &nbsp;</td>
                    <td style="width: 255px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 214px">
                        &nbsp;</td>
                    <td style="width: 255px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>
</asp:Content>

