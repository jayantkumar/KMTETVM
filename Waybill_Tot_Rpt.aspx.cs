﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Waybill_Tot_Rpt : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "way_bill_detailrpt";
            cmd.Parameters.Add(new SqlParameter("@waybillno", txtWayNo.Text));
            // cmd.Parameters.Add(new SqlParameter("@date2", txttodate.Text));

            //'  cmd.Parameters.Add(New SqlParameter("@bustype", DropDownList1.SelectedItem))

            adp.SelectCommand = cmd;
            adp.Fill(ds);
            grdWayBill.DataSource = ds;

            grdWayBill.DataBind();




        }
    }
}