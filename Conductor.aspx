﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Conductor.aspx.cs" Inherits="DataManager.Conductor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script type="text/javascript" language="javascript">

        function getconductor(obj1) {
            var name = obj1.value;
            $.ajax({
                type: 'POST', url: 'Conductor.aspx?empname=' + obj1.value + "&Opt1=getconcession", data: $('#form1').serialize(), success: function (response) {

                    if (response.substring(0, 8) == 'notexist') {
                        alert('Conductor Already Exist');
                        obj1.focus();
                    }
                    else if (response.substring(0, 5) == 'exist') {
                        arrobj = response.split("~");
                    }

                }
            });
        }

    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>Conductor Master</h2>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td>&emsp;</td>
            <td style="width: 160px;">Conductor Name </td>
            <td>
                <asp:TextBox ID="txtCndtr" runat="server" MaxLength="35" Style="width: 200px;"
                    onblur="getconductor(this)" Width="120px"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom,numbers" ValidChars=" "
                    TargetControlID="txtCndtr">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>&emsp;</td>
            <td style="width: 160px;">Conductor No </td>
            <td>
                <asp:TextBox ID="txtcode" runat="server" MaxLength="4" Style="width: 200px;" Onblur="return Validation(1,this,'');"
                    Width="120px"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,numbers"
                    TargetControlID="txtcode">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>&emsp;</td>
            <td style="width: 160px;">Division Name </td>
            <td>
                <asp:TextBox ID="ddlDiv" runat="server" Text="Kolhapur" MaxLength="12"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>&emsp;</td>
            <td style="width: 160px;">Conductor Type</td>
            <td>
                <asp:DropDownList ID="ddlcontype" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td colspan="2" style="text-align: left">
                <asp:Button ID="btnSave" runat="server" CssClass="submit-btn"
                    OnClick="btnSave_Click" Text="Save" />
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:GridView ID="grdConductor" runat="server"
                    AutoGenerateColumns="false" BackColor="White" BorderColor="#999999"
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
                    GridLines="Vertical" OnPageIndexChanging="grdConductor_PageIndexChanging"
                    OnRowCommand="grdConductor_RowCommand">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                        <asp:BoundField DataField="condcode" Visible="false" HeaderText="Code" />
                        <asp:BoundField DataField="emp_code" HeaderText="Conductor No" />
                        <asp:BoundField DataField="condfullname" HeaderText="Conductor Name" />
                        <asp:BoundField DataField="Condcode" HeaderText="Employee Code" />
                        <asp:BoundField DataField="con_emp_type" HeaderText="Employee Type " />

                        <asp:TemplateField HeaderText="Update">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnedit" runat="server"
                                    CommandArgument='<%# Eval("condcode") %>' CommandName="EditRow"
                                    ImageUrl="~/images/edit.gif" OnClientClick="return Right(10206,3);"
                                    TabIndex="1" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:HiddenField ID="hdCondCode" runat="server" />
                <asp:HiddenField ID="hdDel" runat="server" />
                <asp:HiddenField ID="hdSave" runat="server" />
            </td>

        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>

        </tr>
    </table>
</asp:Content>

