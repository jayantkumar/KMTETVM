﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="rpt_UserWise_CashCollection.aspx.cs" Inherits="rpt_UserWise_CashCollection" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style media="Print" type="text/css">
        .ctrl {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize {
            font-family: Times New Roman;
            font-size: 12px;
        }
    </style>
</head>

<body class="body">
    <form id="Form1" runat="server">
        <a href="reports.aspx" class="ctrl">Back</a>
        <table style="width: 100%">

            <tr>
                <td colspan="7" align="center">
                    <h2>User Wise Report</h2>
                </td>
            </tr>
            <tr style="height: 40px" class="ctrl">
                <td>
                    <font style="font-family: arial; font-size: 12px;">From Date : </font>
                </td>
                <td>
                    <asp:TextBox ID="txtfromdate" runat="server" Width="80px">
                    </asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                        PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                    </asp:CalendarExtender>
                    <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                    <asp:ScriptManager ID="Scriptmanager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td>
                    <font style="font-family: arial; font-size: 12px;">To Date : </font>
                </td>
                <td>
                    <asp:TextBox ID="txttodate" runat="server" Width="80px">
                    </asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                    </asp:CalendarExtender>
                    <asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                </td>
                <td>User
                </td>
                <td>
                    <asp:TextBox ID="txtuser" runat="server">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnView" runat="server" Font-Bold="true" class="submit-btn2" OnClick="btnView_Click"
                        Text="View Report" />
                </td>
            </tr>
             <tr>
                <td colspan="7">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" ShowFooter="true"
                        AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                        Font-Strikeout="False" Font-Underline="False" OnRowDataBound="grdWayBill_RowDataBound"
                        Width="100%">
                        <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                            Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                            Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                        <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                            Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                            ForeColor="#000099" />
                        <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                            Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                            ForeColor="Black" />
                        <Columns>
                            <asp:TemplateField HeaderText="Sr.No">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex  +1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="waybilldate" HeaderText="Date" />
                            <asp:BoundField DataField="waybill_no" HeaderText="Way bill No." />
                            <asp:BoundField DataField="con_full_name" HeaderText="Conductor Name" />
                            <asp:BoundField DataField="ConductorName" HeaderText="Conductor no." />
                            <asp:BoundField DataField="Schedule" HeaderText="Duty No" />
                            <asp:BoundField DataField="Cash_Collected" HeaderText="Cash Collected By Cashier" />
                            <asp:BoundField DataField="etvm_tic_amt" HeaderText="ETM Cash" />
                            <asp:BoundField DataField="manual_tic_amt" HeaderText="Manual Cash" />
                            <asp:BoundField DataField="total_tic_amt" HeaderText="Total Cash" />
                            <asp:BoundField DataField="excess_amount" HeaderText="Amount Diff" />
                        </Columns>
                        <EmptyDataTemplate>
                            <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid; border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid; height: 100px; background-color: beige; text-align: center">
                                <br />
                                <br />
                                <br />
                                <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" Font-Size="Medium" />
                    </asp:GridView>
                </td>
                <td>&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>

        </table>
    </form>
</body>
</html>

<script src="Autocomplete Script/jquery-1.10.0.min.js" type="text/javascript"></script>
<script src="Autocomplete Script/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://ajax.cdnjs.com/ajax/libs/json2/20110223/json2.js"></script>
<link href="Autocomplete Script/jquery-ui.css" rel="Stylesheet" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {
        $("[id$=txtuser]").autocomplete({
            source: function (request, response) {
                txtid = this.element[0].id.replace('txtsource_', '');
                $.ajax({
                    url: '<%=ResolveUrl("~/rpt_UserWise_CashCollection.aspx/GetUser") %>',
                    data: "{ 'prefix': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('-')[0],
                                val: item.split('-')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                var item = i.item.value;
            },
            minLength: 1
        });
    });
</script>
