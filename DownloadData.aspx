﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DownloadData.aspx.cs" Inherits="DownloadData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                alert('An attempt was made to submit this form more than once; this extra attempt will be ignored.');
                return false;
            }
        }

        $(document).ready(function () {
            validate();
        });

        function validate() {
            $('#ContentPlaceHolder1_Button1').click(function () {
                if ($('#ContentPlaceHolder1_txtcash').val() == '') {

                    alert('Input can not be left blank');
                    return false;
                }
                if ($('#ContentPlaceHolder1_txtcash').val() != "") {
                    var value = $('#ContentPlaceHolder1_txtcash').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                    var intRegex = /^\d+$/;
                    if (!intRegex.test(value)) {
                        alert('Field must be numeric.');
                        return false;
                    }
                }
                CheckIsRepeat();
            });
        }
        function customAlert(msg, duration) {
            var styler = document.createElement("div");
            styler.setAttribute("style", "border: solid 5px Red;width:auto;height:auto;top:50%;left:40%;background-color:#444;color:Silver");
            styler.innerHTML = "<h1>" + msg + "</h1>";
            setTimeout(function () {
                styler.parentNode.removeChild(styler);
            }, duration);
            document.body.appendChild(styler);
        }
        function caller() {
            customAlert("This custom alert box will be closed in 5 seconds", "5000");
        }
    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>Download Data</h2>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>Waybill Number
            </td>
            <td>
                <asp:TextBox ID="txtWayBill" ReadOnly="true" runat="server" Width="190px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>Conductor Name
            </td>
            <td>
                <asp:TextBox ID="txtcond" ReadOnly="true" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>Driver Name
            </td>
            <td>
                <asp:TextBox ID="txtdrv" ReadOnly="true" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>Vehicle No
            </td>
            <td>
                <asp:TextBox ID="txtvehicle" ReadOnly="true" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>Total Cash Collected
            </td>
            <td>
                <asp:TextBox ID="txtcash" Text="0" ReadOnly="true" runat="server" Width="241px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnCapETM" runat="server" CssClass="submit-btn" OnClick="btnCapETM_Click"
                    OnClientClick="return CheckIsRepeat();" Text="Capture ETM" />

                <asp:Button ID="Button1" runat="server" Text="Get Ticket Detail" OnClick="Button1_Click" />

                <asp:Button ID="btnDwnTct" Style="display: none" runat="server" CssClass="submit-btn"
                    OnClick="btnDwnTct_Click" OnClientClick="return CheckIsRepeat();" Text="Download Tickets" />

                <asp:Button ID="btnDelBill" runat="server" CssClass="submit-btn" OnClick="btnDelBill_Click"
                    OnClientClick="return CheckIsRepeat();" Text="Delete Way Bill" />

            </td>
            <td>

                <asp:Button ID="btn_etm_reset" runat="server" Text="Reset ETM Process" OnClick="btn_etm_reset_Click" />
                <asp:Button ID="btnErsTck" runat="server" Visible="false" CssClass="submit-btn" OnClick="btnErsTck_Click"
                    OnClientClick="return CheckIsRepeat();" Text="Erase Tickets" />

            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
