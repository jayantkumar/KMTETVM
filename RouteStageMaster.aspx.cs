﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DataManager;
using System.Text;
using System.Web.Services;

namespace DataManager
{
    public partial class RouteStageMaster : System.Web.UI.Page
    {
        StringBuilder strTemp = new StringBuilder();
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        static SqlConnection cons = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        DataManager dm = new DataManager();
        StringBuilder strTemp1 = new StringBuilder();
        static List<string> src = GetSourc();
        StringBuilder strFare = new StringBuilder();

        protected void Page_Load(object sender, EventArgs e)
        {
            //bindGrid(201);
            //vret(193);
            Int32 no_of_stop = Convert.ToInt32(Session["no_of_stop"]);
            Int32 start_stage = Convert.ToInt32(Session["start_stage"]);
            Int32 end_stage = Convert.ToInt32(Session["end_stage"]);
            string route_no = "", source = "", destination = "";

            if (Convert.ToString(Session["route_no"]) == null || Convert.ToInt32(Session["route_no"]) == 0)
            {
                Response.Write("<script>window.location.href='RouteMaster_.aspx';</script>");
                return;
            }
            else if (Convert.ToString(Session["rout_no"]) != null && Convert.ToString(Session["rout_no"]) != "" && Convert.ToString(Session["type"]) == "2")
            {
                bindGrid(Convert.ToInt32(Session["rout_no"]));
                btnsubmit.Visible = true;
                Session["type"] = 0;
            }
            else if (Session["type"].ToString() == "1")
            {
                route_no = Session["route_no"].ToString();
                source = Session["src"].ToString();
                destination = Session["des"].ToString();
                btnsubmit.Visible = false;
            }
            else
            {
                btnsubmit.Visible = false;
            }
            string stg_marathi1 = "", stg_marathi2 = "";

            if (!IsPostBack && Convert.ToString(Session["type"]) == "1")
            {
                strTemp.Append("<div id='con'><table id='Table1' runat='server' width='100%' border='1' cellpadding='5' cellspacing='5' width='100%'>");
                strTemp.Append("<tbody runat='server' id='tbodyR'>");
                strTemp.Append("<tr style='font-weight:bold;text-align:center;' class='gridhead'>");
                strTemp.Append("<td>Sr No.</td><td>Stage no.</td><td>Stage name in English</td><td>Stage name in Marathi</td><td>Fare</td></tr>");

                for (int i = 0; i < no_of_stop; i++)
                {
                    strTemp.Append("<tr style='height: 30px; '>");
                    strTemp.Append("<td align='Center' id='tdsr_" + i + "'>" + (i + 1) + "</td>");

                    strTemp.Append("<td align='Center'  id='tdstg_" + i + "'><input  id='txtstage_" + i + "'   type='text' value='" + start_stage + "' readonly='true' name='txtstage_" + i + "'></td>");
                    if (i == 0)
                    {
                        strTemp.Append("<td align='Center' id='tdsrc_" + i + "'><input  id='txtsource_" + i + "' maxlength='11' value='" + source + "' readonly='true' type='text' name='txtsource_" + i + "'></td>");
                        stg_marathi1 = Convert.ToString(dm.GetDataTable("select (bus_stage_regional) bus_stage from bus_stage_master where bus_stage ='" + source + "'").Rows[0][0]);
                    }
                    else if (i == (no_of_stop - 1))
                    {
                        strTemp.Append("<td align='Center' id='tdsrc_" + i + "'><input  id='txtsource_" + i + "' maxlength='11' value='" + destination + "' readonly='true' type='text' name='txtsource_" + i + "'></td>");
                        stg_marathi2 = Convert.ToString(dm.GetDataTable("select (bus_stage_regional) bus_stage from bus_stage_master where bus_stage ='" + destination + "'").Rows[0][0]);
                    }
                    else
                        strTemp.Append("<td align='Center' id='tdsrc_" + i + "'><input  id='txtsource_" + i + "' maxlength='11' type='text' name='txtsource_" + i + "'></td>");

                    if (i == 0)
                        strTemp.Append("<td align='Center' id='tddes_" + i + "'><input  id='txtsourcemarathi_" + i + "' maxlength='16' type='text' value='" + stg_marathi1 + "' readonly='true' name='txtsourcemarathi_" + i + "'></td>");
                    else if (i == (no_of_stop - 1))
                        strTemp.Append("<td align='Center' id='tddes_" + i + "'><input  id='txtsourcemarathi_" + i + "' maxlength='16' type='text' value='" + stg_marathi2 + "' readonly='true' name='txtsourcemarathi_" + i + "'></td>");
                    else
                        strTemp.Append("<td align='Center' id='tddes_" + i + "'><input  id='txtsourcemarathi_" + i + "' maxlength='16' type='text' readonly='true' name='txtsourcemarathi_" + i + "'></td>");

                    if (i == 0)
                        strTemp.Append("<td align='Center' id='tdfare_" + i + "'><input  id='txtfarestage_" + i + "' maxlength='2' type='text' readonly='true' autocomplete='off' value='0' name='txtfarestage_" + i + "'></td>");
                    else if (i == (no_of_stop - 1))
                        strTemp.Append("<td align='Center' id='tdfare_" + i + "'><input  id='txtfarestage_" + i + "' maxlength='2' type='text' readonly='true' autocomplete='off' value='" + Getfarestage(i) + "' name='txtfarestage_" + i + "'></td>");
                    else
                        strTemp.Append("<td align='Center' id='tdfare_" + i + "'><input  id='txtfarestage_" + i + "' maxlength='2' type='text' readonly='true' autocomplete='off' name='txtfarestage_" + i + "'></td>");

                    strTemp.Append("</tr>");
                    start_stage++;
                }
                strTemp.Append("<tr><td colspan='5' align='center'><input type='button' id='btnSubmit' value='Save' /></td></tr>");
                strTemp.Append("</tbody></table></div>");
                divtbl.InnerHtml = strTemp.ToString();
                Session["routedetail"] = no_of_stop + "," + start_stage + "," + end_stage + "," + route_no + "," + source + "," + destination;
            }

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string[] GetSource(string prefix)
        {
            List<string> stage = new List<string>();
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "select distinct top 10  bus_stage , bus_stage_id from bus_stage_master where " + "bus_stage like  '%' + @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefix);
                    cmd.Connection = cons;
                    cons.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            stage.Add(sdr["bus_stage"].ToString());
                        }
                    }
                    cons.Close();
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in GetSource function " + ex.Message + " " + ex.Source);
            }
            return stage.ToArray();
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string[] GetSourceInMarath(string prefix)
        {
            List<string> stage = new List<string>();
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "select distinct top 10  bus_stage_regional bus_stage, bus_stage_id  from bus_stage_master where " + "bus_stage like  '%' + @SearchText + '%' and isnull(bus_stage,'') != '" + prefix + "'";
                    cmd.Parameters.AddWithValue("@SearchText", prefix);
                    cmd.Connection = cons;
                    cons.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            stage.Add(sdr["bus_stage"].ToString());
                        }
                    }
                    cons.Close();
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in GetSource function " + ex.Message + " " + ex.Source);
            }
            return stage.ToArray();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string Execute(List<routestage> str)
        {
            string result = "";

            List<routestage> stage = str.OrderBy(x => x.start_stage).ToList();


            List<string> query = stage.GroupBy(x => x.stage_english).Where(g => g.Count() > 1).Select(y => y.Key).ToList();

            var results = stage.Where(p => !src.Any(p2 => p2 == p.stage_english)).Select(x => x.stage_english).FirstOrDefault();

            SqlCommand cmd1 = new SqlCommand();
            try
            {
                if (results != "" && results != null)
                {
                    return result = "140" + "," + results;
                }
                if (query.Count > 0)
                {
                    return result = "150" + "," + query[0];
                }
                for (int i = 0; i < stage.Count; i++)
                {
                    if (stage[i].fare_stage_no < (i > 0 ? stage[i - 1].fare_stage_no : 0))
                    {
                        return result = "404" + "," + (i + 1);
                    }
                }

                for (int i = 0; i < stage.Count; i++)
                {
                    cmd1.Connection = cons;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.Clear();
                    //cmd1 = new SqlCommand("Sp_Insert_routestage", cons);
                    cmd1 = new SqlCommand("Sp_Insert_route_stage_with_fare", cons);
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@route_no", stage[i].routeno);
                    cmd1.Parameters.AddWithValue("@stage_no", (i + 1));
                    cmd1.Parameters.AddWithValue("@stage_name", stage[i].stage_english);
                    cmd1.Parameters.AddWithValue("@created_by", Convert.ToString(HttpContext.Current.Session["user_id"]));
                    cmd1.Parameters.AddWithValue("@stg_regional_name", stage[i].stage_marathi);
                    cmd1.Parameters.AddWithValue("@ActionId", 0);
                    cmd1.Parameters.AddWithValue("@fare_stage_no", stage[i].start_stage);
                    cons.Open();
                    cmd1.ExecuteNonQuery();
                    cons.Close();
                }

                HttpContext.Current.Session["type"] = 2;
                HttpContext.Current.Session["rout_no"] = stage[0].routeno;
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in savecashier function " + ex.Message.ToString() + " " + ex.Source);
            }

            return result;
        }

        protected void bindGrid(int routeno)
        {
            StringBuilder strHead = new StringBuilder();
            DataManager dms = new DataManager();

            DataTable dtr = new DataTable();
            divfare.InnerHtml = "";
            divtbldr.InnerHtml = "";

            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Sp_routefare_stage";
                cmd.Parameters.Add(new SqlParameter("@route_no", routeno));
                adp.SelectCommand = cmd;
                adp.Fill(ds);

                DataTable dts = new DataTable();
                DataTable dtroute = new DataTable();

                dtroute = ds.Tables[1];

                for (int i = 8; i >= 0; i--)
                {
                    dtroute.Columns.RemoveAt(i);
                }

                DataView dvdetails = new DataView(dtroute, "", "", DataViewRowState.CurrentRows);
                DataTable dtnew = dvdetails.ToTable(true, "stage_name");

                dts = ds.Tables[0];
                DataTable dtge = dts.Clone();

                int columncount = dts.Columns.Count;
                for (Int32 z = 0; z < dts.Rows.Count; z++)
                {
                    object[] array = new object[columncount];

                    for (int i = 0; i < columncount; i++)
                    {
                        string value = Convert.ToString(dts.Rows[z][i]).Replace(",", "") == "" ? "0" : Convert.ToString(dts.Rows[z][i]).Replace(",", "");

                        if (Convert.ToString(value) == "0000.00")
                            array[i] = "0.00";
                        else if (Convert.ToDecimal(value) == 0)
                            array[i] = "";
                        else
                            array[i] = Convert.ToDecimal(value);
                    }
                    dtge.Rows.Add(array);
                }

                #region stage

                DataTable dtu = dms.GetDataTable("select stage_no, stage_name ,stg_regional_name , fare_change_stage_no from stagemaster_by_route$ where rootname = '" + routeno + "' order by stage_no");

                strTemp1.Append("<div id='con'><table id='Table1' runat='server' width='100%' border='1' cellpadding='5' cellspacing='5' width='100%'>");
                strTemp1.Append("<tbody runat='server' id='tbodyR'>");
                strTemp1.Append("<tr style='font-weight:bold;text-align:center;' class='gridhead'>");
                strTemp1.Append("<td>Sr No.</td><td>Stage no.</td><td>Stage name in English</td><td>Stage name in Marathi</td><td>Fare</td></tr>");

                for (int i = 0; i < dtu.Rows.Count; i++)
                {
                    strTemp1.Append("<tr style='height: 30px; '>");
                    strTemp1.Append("<td align='Center' id='tdsr_" + i + "'>" + (i + 1) + "</td>");

                    strTemp1.Append("<td align='Center'  id='tdstg_" + i + "'><input  id='txtstage_" + i + "'   type='text' value='" + Convert.ToString(dtu.Rows[i]["fare_change_stage_no"]) + "' readonly='true' name='txtstage_" + i + "'></td>");

                    strTemp1.Append("<td align='Center' id='tdsrc_" + i + "'><input  id='txtsource_" + i + "' maxlength='11' value='" + Convert.ToString(dtu.Rows[i]["stage_name"]) + "' readonly='true' type='text' name='txtsource_" + i + "'></td>");

                    strTemp1.Append("<td align='Center' id='tddes_" + i + "'><input  id='txtsourcemarathi_" + i + "' maxlength='16' type='text' readonly='true' value='" + Convert.ToString(dtu.Rows[i]["stg_regional_name"]) + "' readonly='true' name='txtsourcemarathi_" + i + "'></td>");

                    if (i == 0)
                        strTemp1.Append("<td align='Center' id='tddes_" + i + "'><input  id='txtfarestage_" + i + "' maxlength='2' type='text' value='0' readonly='true'  name='txtfarestage_" + i + "'></td>");
                    else
                        strTemp1.Append("<td align='Center' id='tddes_" + i + "'><input  id='txtfarestage_" + i + "' maxlength='2' type='text' value='" + Getfarestage(i) + "' readonly='true'  name='txtfarestage_" + i + "'></td>");

                    strTemp1.Append("</tr>");

                }
                strTemp1.Append("<tr><td colspan='5' align='center'><input type='button' disabled='true' id='btnSubmit' value='Save' /></td></tr>");
                strTemp1.Append("</tbody></table></div>");


                #endregion

                #region gridfare

                strHead.Append("<td>&emsp;</td>");
                for (int j = 0; j < dtnew.Rows.Count; j++)
                {
                    strHead.Append("<td>" + Convert.ToString(dtnew.Rows[j]["stage_name"]) + "</td>");
                }

                strFare.Append("<div id='con'><table id='Table1' runat='server' width='100%' border='1' cellpadding='5' cellspacing='5' width='100%'>");
                strFare.Append("<tbody runat='server' id='tbodyR'>");
                strFare.Append("<tr style='font-weight:bold;font-size:14px;text-align:center;' class='gridhead'>");

                strFare.Append(strHead.ToString());


                for (int i = 0; i < (dtnew.Rows.Count); i++)
                {
                    strFare.Append("<tr>");
                    strFare.Append("<td style='font-weight:bold;font-size:14px'>" + Convert.ToString(dtnew.Rows[i]["stage_name"]) + "</td>");

                    bool iscnt = true;
                    for (int z = 0; z < dtge.Columns.Count; z++)
                    {
                        strFare.Append("<td>" + Convert.ToString(dtge.Rows[i][z]) + "</td>");
                    }
                    strFare.Append("</tr>");
                }
                strFare.Append("</tbody></table></div>");

                #endregion

                divtbl.Visible = false;
                divfare.InnerHtml = strFare.ToString();

                divtbldr.InnerHtml = strTemp1.ToString();


            }
            catch (Exception ex)
            {

            }
        }

        //[WebMethod]
        public static string checkautocomplete(string prefix)
        {
            string result = "";
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "select (bus_stage_regional) bus_stage from bus_stage_master where bus_stage ='" + prefix + "'";

                    cmd.Connection = cons;
                    cons.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            result = sdr["bus_stage"].ToString();
                        }
                    }
                    cons.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        [WebMethod]
        public static string GetFare(string source, int stepno)
        {
            string result = "", marathi_stage = "";
            decimal amount = 0;
            try
            {
                amount = Getfarestage(stepno);
                marathi_stage = checkautocomplete(source);

                result = marathi_stage + "," + amount;

            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static decimal Getfarestage(int stepno)
        {
            decimal amount = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Convert(numeric(10,2),adult_fare) fare from [dbo].[stage_wise_FareMaster$] where fare_stepnumber = " + stepno;
                    cmd.Connection = cons;
                    cons.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            amount = Convert.ToDecimal(sdr["fare"]);
                        }
                    }
                    cons.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return amount;
        }

        public static List<string> GetSourc()
        {
            List<string> stage = new List<string>();
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "select distinct bus_stage  from bus_stage_master";
                    cmd.Connection = cons;
                    cons.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            stage.Add(sdr["bus_stage"].ToString());
                        }
                    }
                    cons.Close();
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in GetSource function " + ex.Message + " " + ex.Source);
            }
            return stage;
        }

        public class routestage
        {
            public int routeno { get; set; }
            public int start_stage { get; set; }
            public int end_stage { get; set; }
            public int no_of_stop { get; set; }
            public string stage_english { get; set; }
            public string stage_marathi { get; set; }
            public decimal fare_stage_no { get; set; }
        }

        public void chart(int routeno)
        {

            StringBuilder strHead = new StringBuilder();
            DataManager dms = new DataManager();

            DataTable dtr = new DataTable();
            divfare.InnerHtml = "";
            divtbl.InnerHtml = "";
            try
            {
                DataTable dtu = dtr = dms.GetDataTable("select stage_no, stage_name ,stg_regional_name , fare_change_stage_no from stagemaster where rootname = '" + routeno + "' order by stage_no");

                strTemp1.Append("<div id='con'><table id='Table1' runat='server' width='100%' border='1' cellpadding='5' cellspacing='5' width='100%'>");
                strTemp1.Append("<tbody runat='server' id='tbodyR'>");
                strTemp1.Append("<tr style='font-weight:bold;text-align:center;' class='gridhead'>");
                strTemp1.Append("<td>Sr No.</td><td>Stage no.</td><td>Stage name in English</td><td>Stage name in Marathi</td><td>Fare</td></tr>");

                for (int i = 0; i < dtu.Rows.Count; i++)
                {
                    strTemp1.Append("<tr style='height: 30px; '>");
                    strTemp1.Append("<td align='Center' id='tdsr_" + i + "'>" + (i + 1) + "</td>");

                    strTemp1.Append("<td align='Center'  id='tdstg_" + i + "'><input  id='txtstage_" + i + "'   type='text' value='" + Convert.ToString(dtu.Rows[i]["stage_no"]) + "' readonly='true' name='txtstage_" + i + "'></td>");

                    strTemp1.Append("<td align='Center' id='tdsrc_" + i + "'><input  id='txtsource_" + i + "' maxlength='11' value='" + Convert.ToString(dtu.Rows[i]["stage_name"]) + "' readonly='true' type='text' name='txtsource_" + i + "'></td>");


                    strTemp1.Append("<td align='Center' id='tddes_" + i + "'><input  id='txtsourcemarathi_" + i + "' maxlength='16' type='text' readonly='true' value='" + Convert.ToString(dtu.Rows[i]["stg_regional_name"]) + "' readonly='true' name='txtsourcemarathi_" + i + "'></td>");

                    strTemp1.Append("<td align='Center' id='tddes_" + i + "'><input  id='txtfarestage_" + i + "' maxlength='2' type='text' value='" + Convert.ToString(dtu.Rows[i]["fare_change_stage_no"]) + "' readonly='true'  name='txtfarestage_" + i + "'></td>");

                    strTemp1.Append("</tr>");

                }
                strTemp1.Append("<tr><td colspan='5' align='center'><input type='button' disabled='true' id='btnSubmit' value='Save' /></td></tr>");
                strTemp1.Append("</tbody></table></div>");


                dtr = dms.GetDataTable("select rootname,stage_no,stage_name ,stage_full_name,fare_change_stage_no from stagemaster where rootname = '" + routeno + "' order by stage_no");

                IEnumerable<string> query = from dt in dtr.AsEnumerable()
                                            select dt.Field<string>("stage_name");

                strHead.Append("<td>&emsp;</td>");
                foreach (var i in query)
                {
                    strHead.Append("<td>" + i + "</td>");
                }

                strFare.Append("<div id='con'><table id='Table1' runat='server' width='100%' border='1' cellpadding='5' cellspacing='5' width='100%'>");
                strFare.Append("<tbody runat='server' id='tbodyR'>");
                strFare.Append("<tr style='font-weight:bold;font-size:14px;text-align:center;' class='gridhead'>");

                strFare.Append(strHead.ToString());

                int k = 0, count = 0;
                for (int i = 0; i < dtr.Rows.Count; i++)
                {
                    count = (Convert.ToInt32(dtr.Rows.Count) - 1);

                    strFare.Append("<tr>");
                    strFare.Append("<td style='font-weight:bold;font-size:14px'>" + Convert.ToString(dtr.Rows[i]["stage_name"]) + "</td>");

                    k = i;
                    bool istrue = true;
                    for (int j = 0; j <= count; j++)
                    {
                        if (j > k)
                            strFare.Append("<td>" + Convert.ToString(dtr.Rows[j]["fare_change_stage_no"]) + "</td>");
                        else
                            strFare.Append("<td>" + Convert.ToString(dtr.Rows[k - j]["fare_change_stage_no"]) + "</td>");

                        if (k > 0 && (k - j) == 0 && istrue)
                        {
                            count = count - k;
                            j = 0;
                            k = 0;
                            istrue = false;
                        }
                    }

                    strFare.Append("</tr>");
                }
                strFare.Append("</tbody></table></div>");

                divfare.InnerHtml = strFare.ToString();
                divtbl.InnerHtml = strTemp1.ToString();
                //HttpContext.Current.Session["rout_no"] = "";
            }
            catch (Exception ex)
            {

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int route_nos = Convert.ToInt32(hfdroute.Value);
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand();
            try
            {
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Clear();
                cmd1 = new SqlCommand("Sp_Insert_routestage", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@routeno", Convert.ToInt32(Session["rout_no"]));
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
                Session["rout_no"] = 1;
                Response.Write("<script>alert('Data Saved successfully');window.location.href='RouteMaster_.aspx'</script>");
            }
            catch (Exception ex)
            {

            }
        }
    }
}