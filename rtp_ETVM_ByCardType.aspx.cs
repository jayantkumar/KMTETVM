﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class rtp_ETVM_ByCardType : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter("select distinct Id , TypeTitle from PassType order by Id asc", con);
                adapter.Fill(dt);

                ddlType.DataSource = dt;
                ddlType.DataTextField = "TypeTitle";
                ddlType.DataValueField = "Id";
                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch (Exception ex)
            {

            }
        }
    }

    protected void Search_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlType.SelectedValue == "0")
            {
                Response.Write("<script>alert ('Select Card Type') </script>");
            }
            else
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetRfidPunchedCards";
                cmd.Parameters.Add(new SqlParameter("@fdate", txtfromdate.Text));
                cmd.Parameters.Add(new SqlParameter("@todate", txttodate.Text));
                cmd.Parameters.Add(new SqlParameter("@card", ddlType.SelectedValue));
                adp.SelectCommand = cmd;
                adp.Fill(ds);
                if(ds.Tables.Count>0)
                {
                    grdWayBill.DataSource = ds;
                    grdWayBill.DataBind();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblfromdate.Text = "From Date :" + txtfromdate.Text;
                        lbltodate.Text = "To Date :" + txttodate.Text;
                    }
                }
                else
                {
                    grdWayBill.DataSource = null;
                    grdWayBill.DataBind();

                    lblfromdate.Text = "";
                    lbltodate.Text = "";
                }
            }

        }
        catch (Exception ex)
        {
            con.Close();
        }
        finally
        {
            if (con.State == ConnectionState.Open)
                con.Close();
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("ETVM_cash_collection_By_CardType.xls", grdWayBill);
    }

    protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[1].Text == "Total")
            {
                e.Row.Cells[0].Text = " ";
                e.Row.Cells[1].Font.Size = 12;
                e.Row.Cells[2].Font.Size = 12;
                e.Row.Cells[3].Font.Size = 12;
                e.Row.Cells[4].Font.Size = 12;
                e.Row.Cells[5].Font.Size = 12;
                e.Row.Cells[6].Font.Size = 12;
                e.Row.Cells[7].Font.Size = 12;
                e.Row.Cells[8].Font.Size = 12;
                e.Row.Cells[9].Font.Size = 12;
                e.Row.Cells[10].Font.Size = 12;

                e.Row.Cells[1].Font.Bold = true;
                e.Row.Cells[2].Font.Bold = true;
                e.Row.Cells[3].Font.Bold = true;
                e.Row.Cells[4].Font.Bold = true;
                e.Row.Cells[5].Font.Bold = true;
                e.Row.Cells[6].Font.Bold = true;
                e.Row.Cells[7].Font.Bold = true;
                e.Row.Cells[8].Font.Bold = true;
                e.Row.Cells[9].Font.Bold = true;
                e.Row.Cells[10].Font.Bold = true;
            }
        }
    }

}