﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DataManager;
namespace DataManager
{
    public partial class Conductor : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQuery = "select *,DivCode as Name, Upper(con_full_name) condfullname,DepoCode as DName from conductor_master where Is_Delete=0 and ISNULL(emp_code,'') != '' and isnull(con_full_name,'')!='' order by emp_code asc";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Opt1"] == "getconcession")
            {
                string sttray = "";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select conductorname from Conductor_Master where conductorname='" + txtCndtr.Text + "' order by conductorname ", con);
                con.Open();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("notexist~");
                }
                else
                {
                    Response.Write("exist~" + sttray.ToString() + "~");
                }
            }
            if (!IsPostBack)
            {
                dm.ExeCuteGridBind(selQuery, grdConductor);
                txtCndtr.Attributes.Add("autocomplete", "off");
                txtcode.Attributes.Add("autocomplete", "off");
                bindddl();
                ddlDiv.Enabled = false;
            }
        }
        protected void Clear()
        {
            txtCndtr.Text = "";
            txtcode.Text = "";
        }
        protected void grdConductor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdConductor.PageIndex = e.NewPageIndex;
            dm.ExeCuteGridBind(selQuery, grdConductor);
        }
        protected void grdConductor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            hdCondCode.Value = e.CommandArgument.ToString();
            if (e.CommandName == "EditRow")
            {
                btnSave.Text = "Update";
                dr = dm.GetDataReader("Select con_full_name,divcode,depocode,emp_code, con_emp_type from conductor_master where condcode=" + e.CommandArgument);
                while (dr.Read())
                {
                    txtCndtr.Text = dr[0].ToString();
                    txtcode.Text = dr[3].ToString();
                    ddlDiv.Text = (dr[1].ToString());
                    ddlcontype.SelectedValue = (dr[4].ToString());
                }
            }
            if (e.CommandName == "DeleteRow")
            {
                if (hdDel.Value.ToString() != "1")
                {
                    dm.ExecuteNonQuery("update conductor_master set is_delete=1 where condcode=" + e.CommandArgument);
                    dm.ExeCuteGridBind(selQuery, grdConductor);
                }
                hdDel.Value = "0";
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                DataSet ds = new DataSet(); DataSet ds1 = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select conductorname from Conductor_Master where conductorname='" + txtCndtr.Text.Trim() + "' ", con);
                SqlDataAdapter da1 = new SqlDataAdapter("select distinct isnull(emp_code,'') code from Conductor_Master where isnull(emp_code,'') != '' and isnull(emp_code,'') = '" + txtcode.Text.Trim() + "' ", con);
                con.Open();
                da.Fill(ds);
                da1.Fill(ds1);
                con.Close();

                if (Convert.ToInt32(ds1.Tables[0].Rows.Count) > 0)
                {
                    if (Convert.ToString(ds1.Tables[0].Rows[0][0]) != "" && (Convert.ToString(hdCondCode.Value) == "0" || Convert.ToString(hdCondCode.Value) == "")|| Convert.ToString(hdCondCode.Value) =="")
                    {
                        Response.Write("<script>alert ('Conductor No already exist') </script>"); return;
                    }
                }
                if (ddlcontype.SelectedValue == "")
                {
                    Response.Write("<script>alert ('Select Conductor Type') </script>"); return;
                }
                else if (txtCndtr.Text == "")
                {
                    Response.Write("<script>alert ('Enter Conductor Name') </script>"); return;
                }
                else if (txtcode.Text == "")
                {
                    Response.Write("<script>alert ('Enter ETM No.') </script>"); return;
                }
                else if (ddlDiv.Text == "")
                {
                    Response.Write("<script>alert ('Enter Division Name') </script>"); return;
                }
                else if (ddlcontype.Text == "")
                {
                    Response.Write("<script>alert ('Enter Depo Name') </script>"); return;
                }
                else
                {
                    string[] conductor = Convert.ToString(txtCndtr.Text).Split(' ');
                    if (hdSave.Value.ToString() != "1")
                    {
                        try
                        {
                            con.Open();
                            cmd = new SqlCommand();
                            cmd.CommandText = "CONDuctor_MASTER_ADD_UPDATE";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.Add("@Conductorfullname", SqlDbType.NVarChar).Value = txtCndtr.Text;
                            cmd.Parameters.Add("@ConductorName", SqlDbType.NVarChar).Value = (txtCndtr.Text).Length > 16 ? (txtCndtr.Text).Substring(0, 16) : (txtCndtr.Text);
                            cmd.Parameters.Add("@ETmCode", SqlDbType.NVarChar).Value = 1;//ddlCnType.SelectedItem.Value.ToString();
                            cmd.Parameters.Add("@Div", SqlDbType.NVarChar).Value = ddlDiv.Text.ToString();
                            cmd.Parameters.Add("@EMPCODE", SqlDbType.VarChar).Value = txtcode.Text.ToString();
                            cmd.Parameters.Add("@CONDTYPE", SqlDbType.VarChar).Value = ddlcontype.SelectedValue.ToString();
                            cmd.Parameters.Add("@Depo", SqlDbType.NVarChar).Value = "10";
                            cmd.Parameters.Add("@Conductor", SqlDbType.VarChar).Value = conductor[0].ToString();

                            if (btnSave.Text == "Save")
                            {
                                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                                cmd.Parameters.Add("@condcode", SqlDbType.Int).Value = 0;
                                Clear();
                            }
                            else if (btnSave.Text == "Update")
                            {
                                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                                cmd.Parameters.Add("@condcode", SqlDbType.Int).Value = Convert.ToInt32(hdCondCode.Value);
                                btnSave.Text = "Save";
                                Clear();
                            }
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        catch (Exception ex)
                        {
                            con.Close();
                        }
                    }
                    Response.Redirect("Conductor.aspx");
                    dm.ExeCuteGridBind(selQuery, grdConductor);
                    hdSave.Value = "0";
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void bindddl()
        {
            try
            {
                string com = "select distinct  cond_type from Conductoe_Type";
                SqlDataAdapter adpt = new SqlDataAdapter(com, con);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                ddlcontype.DataSource = dt;
                ddlcontype.DataBind();
                ddlcontype.DataTextField = "cond_type";
                ddlcontype.DataValueField = "cond_type";
                ddlcontype.DataBind();
                ddlcontype.Items.Insert(0, "Select");
            }
            catch (Exception ex)
            {

            }
        }
    }
}