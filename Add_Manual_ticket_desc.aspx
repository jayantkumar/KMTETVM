﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="Add_Manual_ticket_desc.aspx.cs" Inherits="Add_Manual_ticket_desc" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript">

        function edit_row(no) {
            document.getElementById("edit_button" + no).style.display = "none";
            document.getElementById("save_button" + no).style.display = "block";

            var Denom = document.getElementById("Denom_row" + no);
            var PType = document.getElementById("Ptype_row" + no);
            var ticcount = document.getElementById("ticcount_row" + no);
            var amount = document.getElementById("amount_row" + no);

            var Denom_data = Denom.innerHTML;
            var Ptype_data = PType.innerHTML;
            var ticcount_data = ticcount.innerHTML;
            var amount_data = amount.innerHTML;

            Denom.innerHTML = "<input  onblur=fnmultiply(" + no + ") type='text' id='Denom_text" + no + "' class = 'numeric'  value='" + Denom_data + "' >";
            PType.innerHTML = "<input  onblur=fnmultiply(" + no + ") type='text' id='Ptype_text" + no + "' class = 'numeric'  value='" + Ptype_data + "' >";
            ticcount.innerHTML = "<input  onblur=fnmultiply(" + no + ") type='text' id='ticcount_text" + no + "' class = 'numeric'  value='" + ticcount_data + "'>";
            amount.innerHTML = "<input  readonly ='readonly'  type='text' id='amount_text" + no + "' value='" + amount_data + "'>";
            callnumeric();

        }
        function fnmultiply(no) {
            var Denom_val = document.getElementById("Denom_text" + no).value;
            var ticcount_val = document.getElementById("ticcount_text" + no).value;
            var objamount = document.getElementById("amount_text" + no);
            objamount.value = Denom_val * ticcount_val;
        }


        function save_row(no) {
            alert(no);
            var Denom_val = document.getElementById("Denom_text" + no).value;
            var Ptype_val = document.getElementById("Ptype_text" + no).value;
            var ticcount_val = document.getElementById("ticcount_text" + no).value;
            var amount_val = document.getElementById("amount_text" + no).value;

            if (Denom_val == '') {
                alert('Please Enter Denomination');
                document.getElementById("Denom_text" + no).focus();
                return false;
            }
            if (Ptype_val == '0') {
                alert('Please select Passenger');
                document.getElementById("Ptype_row" + no).focus();
                return false;
            }
            if (Denom_val == '0') {
                alert('Please Enter Denomination');
                document.getElementById("Denom_text" + no).focus();
                return false;
            }
            if (ticcount_val == '') {
                alert('Please Enter Ticket Count');
                document.getElementById("ticcount_text" + no).focus();
                return false;
            }
            if (ticcount_val == '0') {
                alert('Please Enter Ticket Count');
                document.getElementById("ticcount_text" + no).focus();
                return false;
            }

            document.getElementById("Denom_row" + no).innerHTML = Denom_val;
            document.getElementById("Ptype_row" + no).innerHTML = Ptype_val;
            document.getElementById("ticcount_row" + no).innerHTML = ticcount_val;
            document.getElementById("amount_row" + no).innerHTML = amount_val;

            document.getElementById("edit_button" + no).style.display = "block";
            document.getElementById("save_button" + no).style.display = "none";
            getsrandtotalamt()
        }

        function delete_row(no) {

            document.getElementById("row" + no + "").outerHTML = "";
            getsrandtotalamt()

        }

        function add_row() {
            var new_Denom = document.getElementById("new_Denom").value;
            var new_PType = document.getElementById("ddlPassengertype").value;
            var new_ticcount = document.getElementById("new_ticcount").value;
            var new_amount = document.getElementById("new_amount").value;
            var new_sr = document.getElementById("new_sr").value;

            if (new_Denom == '') {
                alert('Please Enter Denomination');
                document.getElementById("new_Denom").focus();
                return false;
            }
            if (new_PType == '0') {
                alert('Please select Passenger Type');
                document.getElementById("ddlPassengertype").focus();
                return false;
            }
            if (new_Denom == '0') {
                alert('Please Enter Denomination');
                document.getElementById("new_Denom").focus();
                return false;
            }
            if (new_ticcount == '') {
                alert('Please Enter Ticket Count');
                document.getElementById("new_ticcount").focus();
                return false;
            }
            if (new_ticcount == '0') {
                alert('Please Enter Ticket Count');
                document.getElementById("new_ticcount").focus();
                return false;
            }

            var table = document.getElementById("data_table");
            var table_len = (table.rows.length) - 1;
            var row = table.insertRow(table_len).outerHTML = "<tr id='row" + table_len + "'><td align='center' id='sr_row" + table_len + "'  '>" + new_sr + "</td><td align='center' id='Denom_row" + table_len + "'  '>" + new_Denom + "</td><td align='center' id='Ptype_row" + table_len + "'  '>" + new_PType + "</td><td align='center' id='ticcount_row" + table_len + "'>" + new_ticcount + "</td><td align='center' id='amount_row" + table_len + "'>" + new_amount + "</td><td><input type='button' id='edit_button" + table_len + "' value='Edit' class='edit' onclick='edit_row(" + table_len + ")'> <input type='button' id='save_button" + table_len + "' value='Save' class='save' onclick='save_row(" + table_len + ")'> <input type='button' value='Delete' class='delete' onclick='delete_row(" + table_len + ")'></td></tr>";
            getsrandtotalamt()

            document.getElementById("new_Denom").value = "";
            document.getElementById("new_ticcount").value = "";
            document.getElementById("new_amount").value = "";
            //document.getElementById("ddlPassengertype").value="0";
            document.getElementById("new_Denom").focus();
        }

        /// calculating the Total amount 
        function getsrandtotalamt() {
            var total = '0';
            var i = 0;
            var row_num = 0;
            var hdnrwcnt = document.getElementById("data_table").rows.length;

            for (i = 1; i < hdnrwcnt; i++) {
                if (document.getElementById('amount_row' + i) != null) {

                    var amount = document.getElementById("amount_row" + i);
                    var serialno = document.getElementById("sr_row" + i);
                    var amount_data = amount.innerHTML;
                    row_num = row_num + 1;
                    serialno.innerHTML = row_num;

                    total = parseFloat(total) + parseFloat(amount_data);
                }
            }
            document.getElementById("ContentPlaceHolder1_lbltotal").innerHTML = total;
            document.getElementById("ContentPlaceHolder1_lblmanamount").innerHTML = total;


            objtotalcash = document.getElementById("ContentPlaceHolder1_lblcash");
            objetmamt = document.getElementById("ContentPlaceHolder1_lblETVMamt");
            objetm_and_manual_amt = document.getElementById("ContentPlaceHolder1_lbletvmandmanual");
            objdiffamt = document.getElementById("ContentPlaceHolder1_lbldiffamt");

            objetm_and_manual_amt.innerHTML = parseFloat(objetmamt.innerHTML) + parseFloat(total);
            objdiffamt.innerHTML = parseFloat(objtotalcash.innerHTML) - parseFloat(objetm_and_manual_amt.innerHTML);

            return true;
        }

        function Adddata() {

            if (document.getElementById("ContentPlaceHolder1_txtWBNo").value == '') {
                alert("Please Enter Way Bill No")
                document.getElementById("ContentPlaceHolder1_txtWBNo").focus();
                return false;

            }


            var detvalues = '';
            var i = 0;
            var hdnrwcnt = document.getElementById("data_table").rows.length;

            for (i = 1; i < hdnrwcnt; i++) {
                if (document.getElementById('Denom_row' + i) != null) {
                    var Denom = document.getElementById("Denom_row" + i);
                    var Ptype = document.getElementById("Ptype_row" + i);
                    var ticcount = document.getElementById("ticcount_row" + i);
                    var amount = document.getElementById("amount_row" + i);

                    var Denom_data = Denom.innerHTML;
                    var Ptype_data = Ptype.innerHTML;
                    var ticcount_data = ticcount.innerHTML;
                    var amount_data = amount.innerHTML;

                    detvalues = detvalues + Denom_data + '^' + Ptype_data + '^' + ticcount_data + '^' + amount_data + '|';
                }
            }
            if (detvalues != '') {
                document.getElementById("ContentPlaceHolder1_Hid_data").value = detvalues
                return true;
            }
            else {
                alert('Please Enter Data')
                document.getElementById("new_Denom").focus();
                return false;

            }

        }

        function getdata(val) {
            $.ajax({
                url: '<%=ResolveUrl("~/Add_Manual_ticket_desc.aspx/GetData") %>',
                data: "{ 'waybillno': '" + val.value + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    //etm,manual,cash,conductor
                    var obj = data.d.split("~");
                    document.getElementById("ContentPlaceHolder1_lblETVMamt").innerHTML = parseFloat(obj[0]);
                    document.getElementById("ContentPlaceHolder1_lblmanamount").innerHTML = parseFloat(obj[1]);
                    document.getElementById("ContentPlaceHolder1_lblcash").innerHTML = obj[2];
                    document.getElementById("ContentPlaceHolder1_lblcon").innerHTML = obj[3];
                    document.getElementById("ContentPlaceHolder1_lbletvmandmanual").innerHTML = parseFloat(obj[0]) + parseFloat(obj[1]);
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });

            //alert('hi');
        }

    </script>
    <div id="wrapper" style="width: 100%;">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:HiddenField ID="Hid_data" runat="server" />
        <table align='center' cellspacing="0" cellpadding="0" border="0" width="80%">
            <tr style="height: 50">
                <td colspan="4" align="center">
                    <h2>Add Manual Ticket
                    </h2>
                </td>
            </tr>
            <tr style="height: 50px">
                <td align="right">Date :
                </td>
                <td>
                    <asp:TextBox ID="txt_WBDate" ReadOnly="true" runat="server" Width="100px"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        PopupButtonID="imgPopBtnissue" TargetControlID="txt_WBDate">
                    </asp:CalendarExtender>
                    <asp:ImageButton ID="imgPopBtnissue" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                </td>
                <td></td>
                <td></td>
                <td align="right">Way Bill No : &nbsp;
                </td>
                <td>
                    <asp:TextBox ID="txtWBNo" runat="server" class="numeric" onchange="return checkwaybill(this)"
                        MaxLength="8" Width="120px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">Driver No : &nbsp;
                </td>
                <td>
                    <asp:Label ID="lbldri" Text="" Font-Bold="true" runat="server"></asp:Label>
                </td>
                <td></td>
                <td></td>
                <td align="right">Conductor No : &nbsp;
                </td>
                <td>
                    <asp:Label ID="lblcon" Text="" Font-Bold="true" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">Vehicle NO : &nbsp;
                </td>
                <td>
                    <asp:Label ID="lblvehicle" Text="" Font-Bold="true" runat="server"></asp:Label>
                </td>
                <td></td>
                <td></td>
                <td align="right">Total Cash (A) : &nbsp;
                </td>
                <td>
                    <asp:Label ID="lblcash" Text="" Font-Bold="true" runat="server"></asp:Label>
                    &nbsp; &#8377;
                </td>
            </tr>
            <tr>
                <td align="right">ETVM Ticket Amount(B) : &nbsp;
                </td>
                <td>
                    <asp:Label ID="lblETVMamt" Text="" Font-Bold="true" runat="server"></asp:Label>
                    &nbsp; &#8377;
                </td>
                <td align="right">Manual Ticket Amount (C) : &nbsp;
                </td>
                <td>
                    <asp:Label ID="lblmanamount" Text="" Font-Bold="true" runat="server"></asp:Label>
                    &nbsp; &#8377;
                </td>
                <td align="right">(D = B + C) Amount : &nbsp;
                </td>
                <td>
                    <asp:Label ID="lbletvmandmanual" Text="" Font-Bold="true" runat="server"></asp:Label>&nbsp;
                    &#8377;
                </td>
            </tr>
            <tr>
                <td colspan="6">&emsp;</td>
            </tr>
            <tr runat="server" id="trsearch">
                <td style="font-weight: bold" align="right">Search Waybill : &nbsp;
                </td>
                <td>
                    <asp:TextBox ID="txtsearch" Font-Bold="true" runat="server"></asp:TextBox>
                </td>
                <td colspan="4">
                    <asp:Button ID="btnsearch" Text="Search" Font-Bold="true" runat="server" OnClick="btnsearch_Click" />&nbsp;
                    <asp:Button ID="btndelete" Text="Delete" Font-Bold="true" runat="server" OnClick="btndelete_Click" OnClientClick="return confirm('Are you sure you want to delete this Manual ticket?');" />&nbsp;
                    <asp:Button ID="tbncancel" Text="Cancel" Font-Bold="true" runat="server" OnClick="tbncancel_Click" />

                </td>

            </tr>
            <tr style="height: 30px">
                <td colspan="4" align="center">
                    <span class="error" style="color: Red; display: none">* Input digits (0 - 9)</span>
                </td>
                <td align="right" style="display: none">(E = A - D) Difference Amount : &nbsp;
                </td>
                <td style="display: none">
                    <asp:Label ID="lbldiffamt" Text="" Font-Bold="true" ForeColor="Red" runat="server"></asp:Label>&nbsp;
                    &#8377;
                </td>
            </tr>
        </table>
        <div runat="server" id="divtab1">
            <table align='center' cellspacing="0" cellpadding="0" id="data_table" border="1" width="80%">
                <tr>
                    <th>Serial No
                    </th>
                    <th>Denom
                    </th>
                    <th>
                        Passenger Type
                    </th>
                    <th>Tic Count
                    </th>
                    <th>Amount
                    </th>
                </tr>
                <tr>
                    <td align="center">
                        <input type="text" id="new_sr" readonly="readonly">
                    </td>
                    <td align="center">
                        <input type="text" id="new_Denom" class="numeric" maxlength="2">
                    </td>
                     <td align="center">
                        
                         <select id="ddlPassengertype">
                        <option value="0">Select</option>
                        <option value="1">Child</option>
                        <option value="2">Adult</option>
                        
                     </select>
                    </td>
                    <td align="center">
                        <input type="text" id="new_ticcount" class="numeric" maxlength="3">
                    </td>
                    <td align="center">
                        <input type="text" id="new_amount" readonly="readonly">
                    </td>
                    <td>
                        <input type="button" class="add" onclick="add_row();" value="Add Record">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="center">
                        <asp:Label ID="lbltotal" ForeColor="red" Text="0" runat="server"></asp:Label>
                    </td>
                    <td></td>
                </tr>
                <tr style="height: 50px">
                    <td colspan="4" align="center">
                        <asp:Button ID="btnsubmit" runat="server" Text="Save" OnClientClick="return Adddata()"
                            OnClick="btnsubmit_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div runat="server" id="divtab2">
            <table width="100%">
                <tr>
                    <td align="center">
                        <h2>ETVM Ticket Description</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" ShowFooter="true"
                            AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                            BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                            Font-Strikeout="False" Font-Underline="False" OnRowDataBound="grdWayBill_RowDataBound"
                            Width="100%">
                            <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                                Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                                Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                            <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                                Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                                ForeColor="#000099" />
                            <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                                Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                                ForeColor="Black" />
                            <Columns>
                                <asp:TemplateField HeaderText="Sr.No">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex  +1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ticket_no" HeaderText="Ticket no" />
                                <asp:BoundField DataField="startstage" HeaderText="Start Stage" />
                                <asp:BoundField DataField="end_stage" HeaderText="End Stage" />
                                <asp:BoundField DataField="no_of_tic" HeaderText="No Of Passenger" />
                                <asp:BoundField DataField="ticket_amnt" HeaderText="Ticket Amount" />
                                <asp:BoundField DataField="transdate" HeaderText="Ticket DateTime" />
                                <asp:BoundField DataField="ticket_status" HeaderText="Ticket Status" />
                                <asp:BoundField DataField="trip_no" HeaderText="Trip No" />
                                <asp:BoundField DataField="conname11" HeaderText="Concession/Normal" />
                            </Columns>
                            <EmptyDataTemplate>
                                <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid; border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid; height: 100px; background-color: beige; text-align: center">
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                                </div>
                            </EmptyDataTemplate>
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" Font-Size="Medium" />
                        </asp:GridView>
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <div runat="server" id="divtab3">
            <table width="100%">
                <tr>

                    <td colspan="3" align="center">
                        <h2>ETVM Ticket Description</h2>
                    </td>
                </tr>
                <tr>
                    <td>&emsp;
                    </td>
                    <td>
                        <asp:GridView ID="gridManual" AutoGenerateColumns="false" runat="server"
                            BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                            BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Medium"
                            Font-Strikeout="False" Font-Underline="False" Width="100%">
                            <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                                Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                                Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                            <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                                Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                                ForeColor="#000099" />
                            <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                ForeColor="Black" />
                            <Columns>
                                <asp:TemplateField HeaderText="Serial No">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex  +1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="mtd_denom" HeaderText="Denom" />
                                <asp:BoundField DataField="mtd_tic_count" HeaderText="Tic Count" />
                                <asp:BoundField DataField="mtd_amount" HeaderText="Amount" />
                                <%--   <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDel" runat="server"
                                        CommandArgument='<%# Eval("mtd_waybillno") %>' CommandName="DeleteRow" OnClientClick="return confirm('Are you sure you want to delete this Manual ticket?');"
                                        ImageUrl="~/images/delNew.png" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td>&emsp;
                          <asp:HiddenField ID="hdticid" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script>

        $(document).ready(function () {
            $("#new_ticcount").blur(function () {
                $("#new_amount").val($("#new_ticcount").val() * $("#new_Denom").val());
            });
        });




        $(document).ready(function () {
            callnumeric();
            $("#new_Denom").blur(function () {
                $("#new_amount").val($("#new_ticcount").val() * $("#new_Denom").val());
            });
        });


        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        function callnumeric() {
            $(".numeric").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                $(".error").css("display", ret ? "none" : "inline");
                return ret;
            });
            $(".numeric").bind("paste", function (e) {
                return false;
            });
            $(".numeric").bind("drop", function (e) {
                return false;
            });
        };

        function checkwaybill(obj1) {
            //getdata();
            // debugger
            var name = obj1.value;
            if (obj1.value != '') {
                $.ajax({
                    type: 'POST', url: 'Add_Manual_ticket_desc.aspx?waybillno=' + obj1.value + "&Opt1=getconcession", data: $('#form1').serialize(), success: function (response) {

                        if (response.substring(0, 11) == 'ManualExist') {
                            alert('This Waybill is already entered');
                            obj1.focus();
                            return false;
                        }
                        else if (response.substring(0, 10) == 'NotGenuine') {
                            alert('This Waybill Number is not correct');
                            obj1.focus();
                            return false;
                        }
                        else if (response.substring(0, 14) == 'ManualNotexist') {
                            arrobj = response.split("~");
                            return true;

                        }

                    }
                });
            }
        }
    </script>
</asp:Content>
