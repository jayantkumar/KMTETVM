﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RouteMaster.aspx.cs" Inherits="DataManager.RouteMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">

        function getroute(obj1) {

            // debugger
            var name = obj1.value;
            $.ajax({ type: 'POST', url: 'RouteMaster.aspx?empname=' + obj1.value + "&Opt1=getconcession", data: $('#form1').serialize(), success: function (response) {

                if (response.substring(0, 8) == 'notexist') {
                    // document.getElementById('lblname0').innerHTML = 'Not Exist'

                    //alert(document.getElementById('lblname0').innerHTML)

                    alert('Route Already Exist');
                    obj1.focus();
                }

                else if (response.substring(0, 5) == 'exist') {
                    arrobj = response.split("~");
                    // document.getElementById('lblname0').innerHTML = arrobj[1];

                    //  document.getElementById('traycode').value = arrobj[1];

                }

            }
            });
        }
        function ChkLength(obj) {
            var Val = obj.value;
            if (Val.length < 3) {
                alert("Minimum 3 character required..!!");
                obj.focus();
                return false;
            }
        }


    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>
                    Route Master</h2>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Route No
            </td>
            <td>
                <asp:TextBox ID="txtrouteno" ReadOnly ="true"  onblur="getroute(this)" MaxLength="3" runat="server"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="txtfuel_FilteredTextBoxExtender" runat="server"
                    Enabled="True" FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txtrouteno">
                </asp:FilteredTextBoxExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="vs1"
                    ControlToValidate="txtrouteno" ErrorMessage="Route Cannot be empty"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Bus Type
            </td>
            <td>
                <asp:DropDownList ID="ddlbustype" runat="server" DataSourceID="SqlDataSource1" DataTextField="BusType"
                    DataValueField="BusCode">
                    <%--   <asp:ListItem Value="0">City Runner</asp:ListItem>
                    <asp:ListItem Value="1">AC</asp:ListItem>
                    <asp:ListItem Value="2">Deluxe</asp:ListItem>
                    <asp:ListItem Value="3">Semi-Deluxe</asp:ListItem>
                    <asp:ListItem Value="4">Staff Service</asp:ListItem>
                    <asp:ListItem Value="5">Reserved</asp:ListItem>
                    <asp:ListItem Value="6">School Duty</asp:ListItem>
                    <asp:ListItem Value="7">Army Duty</asp:ListItem>--%>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ETVMConnectionString %>"
                    SelectCommand="SELECT [BusCode], [BusType] FROM [Bus_Details] WHERE ([Is_Delete] = 0)">
                </asp:SqlDataSource>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Start Stage
            </td>
            <td>
                <asp:TextBox ID="txtstartstage" MaxLength="11" onblur="return ChkLength(this);" runat="server"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="vs1"
                    ControlToValidate="txtstartstage" ErrorMessage="Start Stage Cannot be empty"></asp:RequiredFieldValidator>--%>
                <%--  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                    Enabled="True" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,numbers" TargetControlID="txtstartstage">
                </asp:FilteredTextBoxExtender>--%>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                End stage
            </td>
            <td>
                <asp:TextBox ID="txtendstage" MaxLength="11" onblur="return ChkLength(this);" runat="server"></asp:TextBox>
                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="vs1"
                    ControlToValidate="txtendstage" ErrorMessage="Ens Stage Cannot be empty"></asp:RequiredFieldValidator>--%>
                <%--  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                    Enabled="True" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="txtendstage">
                </asp:FilteredTextBoxExtender>--%>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Km
            </td>
            <td>
                <asp:TextBox ID="txtkm" MaxLength="4" runat="server"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True"
                    FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtkm"
                    ValidChars=".">
                </asp:FilteredTextBoxExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="vs1"
                    ControlToValidate="txtkm" ErrorMessage="Km Cannot be empty"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                No. Of Stop
            </td>
            <td>
                <asp:TextBox ID="txtnoofstop" MaxLength="2" runat="server"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                    FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txtnoofstop">
                </asp:FilteredTextBoxExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="vs1"
                    ControlToValidate="txtnoofstop" ErrorMessage=" Cannot be empty"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" ValidationGroup="vs1" OnClick="Button1_Click"
                    Text="Create Route" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:ValidationSummary ID="vs1" runat="server" ShowMessageBox="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="grdCons" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                    BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                    Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                    Font-Strikeout="False" Font-Underline="False" ShowFooter="False" 
                    Width="946px" onselectedindexchanged="grdCons_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Names="Verdana"
                        Font-Size="Smaller" />
                    <RowStyle BackColor="#E3EAEB" CssClass="datagrid_row1" Font-Names="Verdana" Font-Size="Smaller" />
                    <HeaderStyle BackColor="#1C5E55" CssClass="datagrid_heading" Font-Bold="True" Font-Names="Arial"
                        Font-Size="Small" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Root_Name" HeaderText="Route No" />
                        <asp:BoundField DataField="Start_loc" HeaderText="Start Location" />
                        <asp:BoundField DataField="End_loc" HeaderText="End Location" />
                        <asp:BoundField DataField="bustype" HeaderText="Bus Type" />
                        <asp:TemplateField ItemStyle-Width="40px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged=" grdCons_SelectedIndexChanged" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:CheckBox ID="Select" runat="server" AutoPostBack="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                            border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                            height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" Text="Add" OnClick="Button2_Click" />
                <br />
                <br />
                <asp:GridView ID="Grid_all_Details" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                    BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                    Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                    Font-Strikeout="False" Font-Underline="False" ShowFooter="False" Width="946px">
                    <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Names="Verdana"
                        Font-Size="Smaller" />
                    <RowStyle BackColor="#E3EAEB" CssClass="datagrid_row1" Font-Names="Verdana" Font-Size="Smaller" />
                    <HeaderStyle BackColor="#1C5E55" CssClass="datagrid_heading" Font-Bold="True" Font-Names="Arial"
                        Font-Size="Small" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Root_Name_" HeaderText="Route Name" />
                        <asp:BoundField DataField="Start_loc_" HeaderText="Start Location" />
                        <asp:BoundField DataField="End_loc_" HeaderText="End Location" />
                        <asp:BoundField DataField="bustype_" HeaderText="Bus Type" />
                    </Columns>
                    <EmptyDataTemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                            border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                            height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
                <br />
                <br />
                <br />
                <asp:Button ID="Button3" runat="server" Text="Program" OnClick="Button3_Click" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
