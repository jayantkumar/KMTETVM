﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DataManager
{
    public partial class RouteandDutyWise : System.Web.UI.Page
    {
        string connect = System.Configuration.ConfigurationSettings.AppSettings["Con"].ToString();
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader dr;
        SqlDataAdapter da;
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        DataManager dm = new DataManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            try
            {
                con = new SqlConnection(connect);
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandTimeout = 500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Sp_earning_per_km";
                cmd.Parameters.Add(new SqlParameter("@fromdate", txtfromdate.Text));
                cmd.Parameters.Add(new SqlParameter("@todate", txttodate.Text));
                cmd.Parameters.Add(new SqlParameter("@route", txtroute.Text));
                cmd.Parameters.Add(new SqlParameter("@duty", txtduty.Text));

                adp.SelectCommand = cmd;
                adp.Fill(ds);
                DataSet ds2 = new DataSet();

                if (Convert.ToInt32(ds.Tables.Count) > 0)
                {
                    grdWayBill.DataSource = ds;
                    grdWayBill.DataBind();
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnexcel_Click(object sender, EventArgs e)
        {
            if (grdWayBill.Rows.Count > 0)
                GridViewExportUtil.Export("EPKM" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm") + ".xls", grdWayBill);
            else
                return;
        }

        protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        [WebMethod]
        public static List<string> GetCompletionduty(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select distinct top 10 code from etm_master where " +
                    "code like  '%' + @SearchText + '%' order by code";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> customers = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(sdr["code"].ToString());
                        }
                    }
                    conn.Close();
                    return customers;
                }
            }
        }
    }
}