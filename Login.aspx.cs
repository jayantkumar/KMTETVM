﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using NATUPNPLib;
using System.Data.SqlClient;
using DataManager;
using System.Net;
using System.DirectoryServices;
using System.IO;
using System.Web.Services;

namespace DataManager
{
    public partial class Login : System.Web.UI.Page
    {

        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQuery = "select *,DivCode as Name,DepoCode as DName from conductor_master where Is_Delete=0";
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["username"] == null)
            //{
            //    Response.Redirect("Login.aspx");
            //}
        }

        protected void Login1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {


                library.WriteErrorLog("ConnectionString " + con.ConnectionString.ToString());


                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select userid, UserName,Password,usertype,Is_Cashier from User_Master where UserName='" + urlusername.Value + "' and Password='" + urlPassword.Value + "'", con);
                con.Open();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Alert.Text = "Invalid User Name and Password.";
                    // Response.Write("<script>alert('Invalid User Name and Password.')</script> ");
                }

                else if (File.Exists(@"D:\\pratinidhi\\etm_com.exe") != true)
                {
                    Alert.Text = "Invalid User Name and Password.";
                    // Response.Write("<script>alert('Invalid User Name and Password.')</script> ");
                }
                else
                {
                    string userid = ds.Tables[0].Rows[0][0].ToString();
                    Session.Add("user_id", userid.ToString());
                    Session.Add("user_type", Convert.ToInt32(ds.Tables[0].Rows[0]["usertype"]));
                    Session.Add("UserName", urlusername.Value);
                    Session.Add("IpAdress", Request.UserHostAddress.ToString());
                    Session.Add("MacIp", Request.UserHostName.ToString());
                    Session.Add("Is_Cashier", ds.Tables[0].Rows[0]["Is_Cashier"]);
                    string strHostName = System.Net.Dns.GetHostName();
                    string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();
                    string clientip = clientIPAddress.ToString();


                    string ClientIP = GetIP4Address();


                    Session.Add("strHostName", ClientIP.ToString());
                    Response.Write("<script>window.location.href='ServiceOperator.aspx';</script>");
                }

                //UPnPNATClass uPnP = new UPnPNATClass();
                //IStaticPortMappingCollection map = uPnP.StaticPortMappingCollection;
                //Debug.Print(item.ExternalIPAddress);

                //con.Open();
                //cmd = new SqlCommand();
                //cmd.CommandText = "CheckLoginDetail";
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Connection = con;
                //cmd.Parameters.Add("@ConductorName", SqlDbType.NVarChar).Value = urlusername.Value;
                //cmd.Parameters.Add("@ETmCode", SqlDbType.NVarChar).Value = urlPassword.Value;//ddlCnType.SelectedItem.Value.ToString();
                //cmd.ExecuteNonQuery();
                //con.Close();
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured while login" + ex.Message + " " + ex.Source);
                con.Close();
            }
        }

        public static string GetIP4Address()
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            if (IP4Address != String.Empty)
            {
                return IP4Address;
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            return IP4Address;
        }

        [WebMethod]
        public static string SignUp(string username, string password)
        {
            SqlDataAdapter adp = new SqlDataAdapter();
            string msg = string.Empty;
            DataManager dm = new DataManager();
            DataSet ds = new DataSet();

            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"].ToString();
                        cmd.CommandText = "Sp_CreateUser";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new SqlParameter("@username", username));
                        cmd.Parameters.Add(new SqlParameter("@password", password));
                        cmd.Parameters.Add(new SqlParameter("@Action", 1));
                        adp.SelectCommand = cmd;
                        adp.Fill(ds);

                        if (ds.Tables[0].Rows.Count >= 0)
                        {
                            msg = Convert.ToString(ds.Tables[0].Rows[0][0]);
                        }
                        else
                        {
                            msg = "Error occured";
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return msg;
        }

    }
}