﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class denominationwisereport : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    double a, b, z;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        }
    }

    protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            a += double.Parse(e.Row.Cells[3].Text);
            b += double.Parse(e.Row.Cells[4].Text);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[3].Text = a.ToString();
            e.Row.Cells[4].Text = b.ToString();
        }
    }


    protected void Search_Click(object sender, EventArgs e)
    {
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ticket_denominationwiserpt_with_Concession";
            cmd.Parameters.Add(new SqlParameter("@date1", txtfromdate.Text));
            cmd.Parameters.Add(new SqlParameter("@date2", txttodate.Text));
            adp.SelectCommand = cmd;
            adp.Fill(ds);
            grdWayBill.DataSource = ds;

            grdWayBill.DataBind();

            if (ds.Tables[0].Rows.Count > 0)
            {
                lblfromdate.Text = "From Date :" + txtfromdate.Text;
                lbltodate.Text = "To Date :" + txttodate.Text;
            }
            else
            {
                lblfromdate.Text = "";
                lbltodate.Text = "";
            }
        }
    }

    protected void grdWayBill_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("denominationwise.xls", grdWayBill);
    }
}