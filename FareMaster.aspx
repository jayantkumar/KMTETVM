﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FareMaster.aspx.cs"
    Inherits="DataManager.FareMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <br />
        <table>
            <tr>
                <td colspan="2" style="width: 210px">&emsp; </td>
                <td colspan="2">&emsp; </td>

            </tr>
            <tr>
                <td colspan="4">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 210px">&emsp; </td>
                <td colspan="2" style="text-align: center;">
                    <asp:GridView ID="gridFare" Width="100%" AutoGenerateColumns="false" OnRowDataBound="gridFare_RowDataBound" runat="server">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="180px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="24px" ItemStyle-HorizontalAlign="Left" ItemStyle-Height="28px" HeaderText="Fare Step No.">
                                <ItemTemplate>
                                    &emsp;
                                    <asp:Label runat="server" Style="width: 150px; height: 20px;" Text='<%# Eval("fare_stepnumber") %>' ID="txt_fare_stepno" />
                                    <asp:Label runat="server" Style="width: 150px; height: 20px;" Text='<%# Eval("fare_id") %>' ID="lbl_id" Visible="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="180px" HeaderStyle-Height="24px" ItemStyle-HorizontalAlign="Left" ItemStyle-Height="28px" HeaderText="Start Km">
                                <ItemTemplate>
                                    <%-- &emsp;--%>
                                    <asp:Label runat="server" Style="width: 150px; height: 20px;" Text='<%# Eval("startkm") %>' ID="txt_start_km" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="180px" HeaderStyle-Height="24px" ItemStyle-HorizontalAlign="Left" ItemStyle-Height="28px" HeaderText="End Km">
                                <ItemTemplate>
                                    <%--  &emsp;--%>
                                    <asp:Label runat="server" Style="width: 150px; height: 20px;" Text='<%# Eval("endkm") %>' ID="txt_end_km" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="180px" HeaderStyle-Height="24px" ItemStyle-HorizontalAlign="Center" ItemStyle-Height="28px" HeaderText="Adult Fare">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" AutoComplete="off" Style="width: 150px; height: 20px;" Text='<%# Eval("adult_fare") %>' ID="txt_adult_fare" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                        Enabled="True" FilterType="numbers"
                                        TargetControlID="txt_adult_fare">
                                    </asp:FilteredTextBoxExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false" ItemStyle-Width="180px" HeaderStyle-Height="24px" ItemStyle-HorizontalAlign="Center" ItemStyle-Height="28px" HeaderText="Child Fare">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" Style="width: 150px; height: 20px;" Text='<%# Eval("child_fare") %>' ID="txt_child_fare" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td runat="server">&emsp;
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center">
                    <asp:Button ID="btnSubmit" runat="server" Style="height: 30px; width: 100px;" Text="Update" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

