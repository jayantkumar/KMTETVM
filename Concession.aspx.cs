﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DataManager;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
//using System.Data.SqlClient;
//using System.Web.UI;
using System.Web.Services;
using System.IO;
using System.Reflection;
namespace DataManager
{
    public partial class Concession : System.Web.UI.Page
    {
        DataTable Add_Details = default(DataTable);
        string rrt = "";
        string a, b, c, d, e, aa = "", ba = "", ca = "", da = "", ea = "", asdf = "", bb = "";
       
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQuery = "select conscode,conname,amounttype,amount_per,min_amt from concession_entry where is_delete=0 ";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Opt1"] == "getconcession")
            {
                string sttray = "";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select ConName  from concession_entry where ConName='" + txtCon.Text + "' and is_delete=0 ", con);
                con.Open();
                da.Fill(ds);
                con.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("notexist~");
                }
                else
                {
                    Response.Write("exist~" + sttray.ToString() + "~");
                }
            }
            if (!IsPostBack)
            {
                Button2.Visible = false;
                dm.ExeCuteGridBind(selQuery, grdCons);
            }
        }
        protected void Clear()
        {
            txtCon.Text = "";
            txtDis.Text = "";
            txtMinAmt.Text = "";
            ddlCnType.SelectedItem.Value = "0";
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("select ConName  from concession_entry where ConName='" + txtCon.Text.Trim() + "' ", con);
            con.Open();
            da.Fill(ds);
            con.Close();

            if (txtCon.Text.ToString().Trim() == "")
            {
                Response.Write("<script>alert ('Enter Concession') </script>");
            }
            else if (ddlCnType.SelectedItem.Text == "---Select---")
            {
                Response.Write("<script>alert ('Select Concession Type') </script>");
            }
            else if (txtDis.Text == "")
            {
                Response.Write("<script>alert ('Select Discount %') </script>");
            }
            else if (txtMinAmt.Text == "")
            {
                Response.Write("<script>alert ('Select Minimum Amount') </script>");
            }
            else
            {
                if (hdSave.Value.ToString() != "1")
                {
                    try
                    {
                        con.Open();
                        cmd = new SqlCommand();
                        cmd.CommandText = "Concession_Master_Add_Update";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.Add("@ConName", SqlDbType.NVarChar).Value = txtCon.Text.Trim();
                        cmd.Parameters.Add("@amttype", SqlDbType.NVarChar).Value = ddlCnType.SelectedItem.Text.Trim();
                        cmd.Parameters.Add("@amount", SqlDbType.NVarChar).Value = txtDis.Text.Trim();
                        cmd.Parameters.Add("@minamt", SqlDbType.NVarChar).Value = txtMinAmt.Text;
                        if (btnSave.Text == "Save")
                        {
                            cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                            cmd.Parameters.Add("@concode", SqlDbType.Int).Value = 0;
                            Clear();
                        }
                        else if (btnSave.Text == "Update")
                        {
                            cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                            cmd.Parameters.Add("@concode", SqlDbType.Int).Value = Convert.ToInt32(hdConCode.Value);
                            btnSave.Text = "Save";
                            Clear();
                        }
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        con.Close();
                    }
                }
                hdSave.Value = "0";
                Response.Redirect("Concession.aspx");
                dm.ExeCuteGridBind(selQuery, grdCons);
            }
        }
        public void serviceoperatorcreate()
        {
            if (grdCons.Rows.Count > 0)
            {
                DataRow row = default(DataRow);

                grdCons.Visible = true;

                for (int i = 0; i <= grdCons.Rows.Count - 1; i++)
                {
                    if (grdCons.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkbx = (CheckBox)grdCons.Rows[i].FindControl("Select");
                        CheckBox chkbx1 = (CheckBox)grdCons.Rows[i].FindControl("chk");
                        if ((chkbx.Checked == true))
                        {
                            row = Add_Details.NewRow();
                            row[0] = grdCons.Rows[i].Cells[0].Text;
                            row[1] = grdCons.Rows[i].Cells[1].Text;
                            row[2] = grdCons.Rows[i].Cells[2].Text;
                            row[3] = grdCons.Rows[i].Cells[3].Text;
                            row[4] = grdCons.Rows[i].Cells[4].Text;

                            a = 0 + row[0].ToString();
                            b = row[1].ToString();
                            c = row[2].ToString().Substring(0, 1);
                            d = row[3].ToString() + 0 + 0;
                            e = row[4].ToString() + 0 + 0;
                            if (a.Length < 2)
                            {
                                aa = aa.PadRight(2 - a.Length);
                            }
                            if (b.Length < 12)
                            {
                                int cz = 12 - b.Length;
                                bb = bb.PadRight(cz);
                            }
                            if (c.Length < 1)
                            {

                            }
                            if (d.Length < 6)
                            {
                                da = da.PadLeft(6 - d.Length, '0');
                            }
                            if (e.Length < 6)
                            {
                                ea = ea.PadLeft(6 - e.Length, '0');
                            }
                            string space = "";

                            space = space.PadRight(4);

                            asdf = (a.ToString() + aa + "," + b.ToString() + bb + "," + c.ToString() + "," + da + d.ToString() + "," + ea + e.ToString()).ToString() + "," + space;

                            bb = "";
                            ea = "";
                            if (rrt == "" && chkbx.Checked == true)
                            {
                                rrt = rrt + asdf;
                            }
                            else

                                rrt = rrt + Environment.NewLine + asdf;
                        }
                    }
                }
                File.WriteAllText(@"d:\\pratinidhi\\DISCOUNT.txt", String.Empty);
                FileStream fs2 = new FileStream("d:\\pratinidhi\\DISCOUNT.txt", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs2);

                writer.Write(rrt);
                writer.Close();
            }
        }
        public void addcheckboxclk()
        {
            if (grdCons.Rows.Count > 0)
            {
                Grid_all_Details.Visible = true;
                Add_Details = new DataTable();

                CreateTable();

                serviceoperatorcreate();
                if (grdCons.Rows.Count > 0)
                {
                    DataRow row = default(DataRow);

                    grdCons.Visible = true;

                    for (int i = 0; i <= grdCons.Rows.Count - 1; i++)
                    {
                        if (grdCons.Rows[i].RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chkbx = (CheckBox)grdCons.Rows[i].FindControl("Select");
                            CheckBox chkbx1 = (CheckBox)grdCons.Rows[i].FindControl("chk");

                            if ((chkbx.Checked == true))
                            {
                                row = Add_Details.NewRow();
                                row[0] = grdCons.Rows[i].Cells[0].Text;
                                row[1] = grdCons.Rows[i].Cells[1].Text;
                                row[2] = grdCons.Rows[i].Cells[2].Text;
                                row[3] = grdCons.Rows[i].Cells[3].Text;
                                row[4] = grdCons.Rows[i].Cells[4].Text;

                                Add_Details.Rows.Add(row);
                            }
                        }
                    }
                }
            }
            Grid_all_Details.DataSource = Add_Details;
            Grid_all_Details.DataBind();
            ViewState["Grid_all_Details"] = Add_Details;

        }
        public void CreateTable()
        {
            DataColumn ConsCode = new DataColumn("ConsCode_");
            ConsCode.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(ConsCode);

            DataColumn ConName = new DataColumn("ConName_");
            ConName.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(ConName);

            DataColumn AmountType = new DataColumn("AmountType_");
            AmountType.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(AmountType);

            DataColumn Amount_per = new DataColumn("Amount_per_");
            Amount_per.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(Amount_per);

            DataColumn Min_Amt = new DataColumn("Min_Amt_");
            Min_Amt.DataType = System.Type.GetType("System.String");
            Add_Details.Columns.Add(Min_Amt);
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            addcheckboxclk();
            if (Grid_all_Details.Rows.Count == 0)
            {
                Response.Write("<script>alert ('Select Concession') </script>");
                Grid_all_Details.Visible = false;
            }

            else if (Grid_all_Details.Rows.Count > 8)

            {
                Response.Write("<script>alert ('You Can select maximum eight Concession') </script>");
                Grid_all_Details.Visible = false;
            }

            else
            {
                Button2.Visible = true;
                grdCons.Visible = false;
                Button1.Visible = true;
            }

        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();

            info.FileName = (@"D:\pratinidhi\etm_com.exe ");
            info.Arguments = "11";
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo = info;
            process.StartInfo.UseShellExecute = false;
            System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "11");

            process.Close();
        }
        protected void grdCons_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCons.PageIndex = e.NewPageIndex;
            dm.ExeCuteGridBind(selQuery, grdCons);
        }
        protected void grdCons_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            hdConCode.Value = e.CommandArgument.ToString();
            if (e.CommandName == "EditRow")
            {
                btnSave.Text = "Update";
                dr = dm.GetDataReader("Select conname,amounttype,amount_per,min_amt from CONCESSION_ENTRY where conscode=" + e.CommandArgument);
                while (dr.Read())
                {
                    txtCon.Text = dr[0].ToString();
                    ddlCnType.SelectedItem.Text = (dr[1]).ToString();
                    txtDis.Text = dr[2].ToString();
                    txtMinAmt.Text = dr[3].ToString();
                }
            }
            if (e.CommandName == "DeleteRow")
            {
                if (hdDel.Value.ToString() != "1")
                {
                    dm.ExecuteNonQuery("update concession_entry set is_delete=1 where conscode=" + e.CommandArgument);
                    dm.ExeCuteGridBind(selQuery, grdCons);
                }
                hdDel.Value = "0";
            }
        }
    }
}