﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DutyMaster.aspx.cs" Inherits="DataManager.DutyMaster" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">

        function getduty(obj1) {
            var name = obj1.value;
            //$.ajax({
            //    type: 'POST', url: 'etm_master.aspx?empname=' + obj1.value + "&Opt1=getconcession", data: $('#form1').serialize(), success: function (response) {

            //        //if (response.substring(0, 8) == 'notexist') {
            //        //    alert('Already Exist');
            //        //    obj1.focus();
            //        //}
            //        //else
            //        if (response.substring(0, 5) == 'exist') {
            //            arrobj = response.split("~");
            //        }
            //    }
            //});
        }
    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>Duty Master</h2>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Duty No
            </td>
            <td>
                <asp:TextBox ID="txtduty" runat="server" MaxLength="5" onblur="getduty(this)"
                    Width="120px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="submit-btn"
                    OnClick="btnSave_Click" Text="Save" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:GridView ID="grdduty" runat="server" AllowPaging="False"
                    AutoGenerateColumns="False" BackColor="White" BorderColor="#999999"
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
                    GridLines="Vertical" OnPageIndexChanging="grdduty_PageIndexChanging"
                    OnRowCommand="grdduty_RowCommand">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                        
                        <asp:TemplateField HeaderText="Sr.No">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex  +1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        <asp:BoundField DataField="Duty_No" HeaderText="Duty No." />
                        <asp:TemplateField HeaderText="Update">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnedit" runat="server"
                                    CommandArgument='<%# Eval("Duty_Id") %>' CommandName="EditRow"
                                    ImageUrl="~/images/edit.gif" OnClientClick="return Right(10206,3);"
                                    TabIndex="1" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server"
                                    CommandArgument='<%# Eval("Duty_Id") %>' CommandName="DeleteRow"
                                    ImageUrl="~/images/delNew.png"  OnClientClick="return confirm('Are you sure you want to delete this Duty No.?');"   />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td>
                <asp:HiddenField ID="hdDutyId" runat="server" />
                <asp:HiddenField ID="hdDel" runat="server" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
