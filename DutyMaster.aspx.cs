﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using DataManager;
using System.Windows;
namespace DataManager
{

    public partial class DutyMaster : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQuery = "select Duty_Id , Duty_No from DutyMaster where isnull(is_delete,0) = 0";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dm.ExeCuteGridBind(selQuery, grdduty);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SqlDataAdapter da;
            DataSet ds = new DataSet();
            if (Convert.ToString(hdDutyId.Value) == "")
                da = new SqlDataAdapter("select Duty_No from DutyMaster where Duty_No='" + txtduty.Text.Trim() + "' and isnull(is_delete,0) = 0", con);
            else
                da = new SqlDataAdapter("select Duty_No from DutyMaster where Duty_No='" + txtduty.Text.Trim() + "' and Duty_Id !=" + hdDutyId.Value + " and isnull(is_delete,0) = 0", con);
            con.Open();
            da.Fill(ds);
            con.Close();
            string drivername = txtduty.Text;

            if (drivername.ToString().Trim() == "")
            {
                Response.Write("<script>alert ('Enter Duty No') </script>");

            }
            else if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert ('Already Exist') </script>");
            }
            else
            {
                try
                {
                    cmd = new SqlCommand();
                    cmd.CommandText = "Duty_MASTER_ADD_UPDATE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.Add("@duty_no", SqlDbType.NVarChar).Value = txtduty.Text;

                    if (btnSave.Text == "Save")
                    {
                        cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@Id", SqlDbType.Int).Value = 0;
                    }
                    else if (btnSave.Text == "Update")
                    {
                        cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                        cmd.Parameters.Add("@Id", SqlDbType.Int).Value = Convert.ToInt32(hdDutyId.Value);
                        btnSave.Text = "Save";

                    }
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                }

                dm.ExeCuteGridBind(selQuery, grdduty);

                Response.Redirect("DutyMaster.aspx");

            }
        }

        protected void grdduty_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void grdduty_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            {
                hdDutyId.Value = e.CommandArgument.ToString();
                if (e.CommandName == "EditRow")
                {
                    btnSave.Text = "Update";
                    dr = dm.GetDataReader("Select Duty_No from dutymaster where Duty_Id=" + e.CommandArgument);
                    while (dr.Read())
                    {
                        txtduty.Text = dr[0].ToString();

                    }
                }
                if (e.CommandName == "DeleteRow")
                {
                    if (hdDel.Value.ToString() != "1")
                    {
                        dm.ExecuteNonQuery("update dutymaster set is_delete=1 where Duty_Id=" + e.CommandArgument);
                        dm.ExeCuteGridBind(selQuery, grdduty);
                    }
                    hdDel.Value = "0";
                }
            }
        }
    }
}