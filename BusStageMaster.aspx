﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BusStageMaster.aspx.cs" Inherits="DataManager.BusStageMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
 
        function assignShortname() {
            var name = document.getElementById('<%=txtstage.ClientID %>').value;
            document.getElementById('<%=txtsshortname.ClientID %>').value = name.substring(0, 3).toUpperCase();
        }
    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="3" align="center">
                <h2>Bus Stage Master</h2>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 200px;">Bus Stop  </td>
            <td>
                <asp:TextBox ID="txtstage" runat="server" MaxLength="11" Width="170px"
                      onkeypress="assignShortname(this)"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom,numbers" ValidChars=" "
                    TargetControlID="txtstage">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 200px;">Bus Stop Marathi </td>
            <td>
                <asp:TextBox ID="txtxstagemarathi" runat="server" MaxLength="16"
                    Width="170px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;
       
            </td>
        </tr>
        <tr>
            <td style="width: 200px;">Short Name </td>
            <td>
                <asp:TextBox ID="txtsshortname" runat="server" MaxLength="3" Width="170px"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom,numbers" ValidChars=" "
                    TargetControlID="txtsshortname">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="submit-btn"
                    OnClick="btnSave_Click" Text="Save" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:GridView ID="grdStage" runat="server" AllowPaging="true" PageSize="25"  
                    AutoGenerateColumns="false" BackColor="White" BorderColor="#999999" Width="700px"
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
                    GridLines="Vertical" OnPageIndexChanging="grdStage_PageIndexChanging"
                    OnRowCommand="grdStage_RowCommand">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                        <asp:TemplateField HeaderText="SrNo.">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%#Container.DataItemIndex + 1%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="bus_stage_id" Visible="false" HeaderText="Id" />
                        <asp:BoundField DataField="bus_stage" HeaderText="Bus Stage" />
                        <asp:BoundField DataField="bus_stage_regional" HeaderText="Bus Stage Regional" />
                        <asp:BoundField DataField="bus_stage_short_name" HeaderText="Short Name " />

                        <asp:TemplateField HeaderText="Update">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnedit" runat="server"
                                    CommandArgument='<%# Eval("bus_stage_id") %>' CommandName="EditRow"
                                    ImageUrl="~/images/edit.gif" TabIndex="1" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:HiddenField ID="hdStageId" runat="server" />
                <asp:HiddenField ID="hdDel" runat="server" />
                <asp:HiddenField ID="hdSave" runat="server" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

