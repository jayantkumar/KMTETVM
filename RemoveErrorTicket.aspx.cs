﻿using DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace DataManager
{
    public partial class RemoveErrorTicket : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        static SqlConnection cons = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["user_type"]) != "5")
            {
                Response.Redirect("ServiceOperator.aspx");
            }
        }

        protected void tbnsearch_Click(object sender, EventArgs e)
        {
            bindGrid();
        }



        [WebMethod]
        public static string[] Getwaybill(string prefix)
        {
            List<string> waybill = new List<string>();
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select distinct top 10 WayBillNo from WayBill_Details where Is_Delete = 0 and WayBillNo in (select distinct waybill_no from ticket_detail where convert(date,sysdate) = convert(date,getdate())) and  WayBillNo like  '%' + @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefix);
                    cmd.Connection = cons;
                    cons.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            waybill.Add(sdr["WayBillNo"].ToString());
                        }
                    }
                    cons.Close();
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in GetSource function " + ex.Message + " " + ex.Source);
            }
            return waybill.ToArray();
        }

        [WebMethod]
        public static string[] Getticket(string prefix, string waybill)
        {
            List<string> ticket = new List<string>();
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select distinct top 10 ticket_no from ticket_detail where waybill_no = '" + waybill + "' and ticket_no like '%" + prefix + "%'";
                    cmd.Connection = cons;
                    cons.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ticket.Add(sdr["ticket_no"].ToString());
                        }
                    }
                    cons.Close();
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in GetSource function " + ex.Message + " " + ex.Source);
            }
            return ticket.ToArray();
        }


        protected void bindGrid()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ticket_detail_report";
                cmd.Parameters.Add(new SqlParameter("@waybillno", txtwaybill.Text));
                cmd.Parameters.Add(new SqlParameter("@ticketno", txtticketno.Text));

                adp.SelectCommand = cmd;
                adp.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    grdWayBill.DataSource = ds;
                    grdWayBill.DataBind();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void grdWayBill_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand();
            hdticid.Value = e.CommandArgument.ToString();
            if (e.CommandName == "DeleteRow")
            {
                if (hdticid.Value.ToString() != "1")
                {
                    try
                    {
                        cmd1.Connection = con;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.Clear();
                        cmd1 = new SqlCommand("Sp_UpdateTicketStatus", con);
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@tic_id", Convert.ToInt32(hdticid.Value));
                        cmd1.Parameters.AddWithValue("@userId", Convert.ToInt32(Session["user_id"]));
                        con.Open();
                        cmd1.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    bindGrid();
                }
                hdticid.Value = "0";
            }
        }
    }
}