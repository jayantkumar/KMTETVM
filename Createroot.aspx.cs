﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using System.Configuration;

using System.Drawing;
public partial class Createroot : System.Web.UI.Page
{
    string connect = System.Configuration.ConfigurationSettings.AppSettings["Con"].ToString();
    SqlConnection con;
    SqlCommand cmd;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bind();
            Txt_rootname.Attributes.Add("autocomplete", "off");
            Txt_noofstop.Attributes.Add("autocomplete", "off");
            Txt_startloc.Attributes.Add("autocomplete", "off");
            Txt_endloc.Attributes.Add("autocomplete", "off");
            Btn_update.Visible = false;
        }
    }

  
    protected void Btn_Submit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Txt_rootname.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter Route Name.');", true);
               // Response.Write("<script>alert ('Enter Route Name') </script>");

            }

             if (Txt_noofstop.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter No_Of_stop.');", true);
            }

            if (Ddl_bustype.Text == "")
            {
               
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter Route Name.');", true);

            }
             if (Txt_startloc.Text == "")
            {
            
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter Start Location.');", true);
            }

            if (Txt_endloc.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter End Location.');", true);
            }

            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("select * from rootmaster where Root_Name='"+Txt_rootname.Text+"' ", con);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
          
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Root Name already Exist.');", true);
            }
            else
            {
                con = new SqlConnection(connect);
                con.Open();
                cmd = new SqlCommand("pro_rootmaster", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Root_Name", Txt_rootname.Text);
                cmd.Parameters.AddWithValue("@no_of_stop", Txt_noofstop.Text);
                cmd.Parameters.AddWithValue("@bustype", Ddl_bustype.SelectedValue);
                cmd.Parameters.AddWithValue("@Start_loc", Txt_startloc.Text);
                cmd.Parameters.AddWithValue("@End_loc", Txt_endloc.Text);

                cmd.Parameters.AddWithValue("@is_deleted", "0");
                cmd.Parameters.AddWithValue("@locid", "10");
                cmd.Parameters.AddWithValue("@created_by", "0");
                cmd.Parameters.AddWithValue("@create_date", DateTime.Now);
                cmd.Parameters.AddWithValue("@sys_date", DateTime.Now);
                cmd.Parameters.AddWithValue("@km", "0");
                cmd.Parameters.AddWithValue("@bus_id", "1");
                cmd.Parameters.AddWithValue("@end_loc_full_name", Txt_endloc.Text);
                cmd.Parameters.AddWithValue("@Start_loc_full_name", Txt_startloc.Text);
                
                cmd.ExecuteNonQuery();
             
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Data saved successfully.');", true);
                  bind();
                  Session["RootNo"] = Txt_rootname.Text;
                  Response.Redirect("~/Createstage.aspx");
           
            }
        }
        catch(Exception ex)
        {
          //  Response.Write("<script>alert('You are unable to save data.')</script>");
            con.Close();
        }
    }
    public void bind()
    {
        try
        {
            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("select * from rootmaster", con);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            Grid_RouteMaster.DataSource = ds;
            Grid_RouteMaster.DataBind();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }

    public void bindstage()
    {
         try
         {
             con = new SqlConnection(connect);
             con.Open();
             cmd = new SqlCommand("select * from stagemaster where rootname='" + Txt_rootname.Text + "' order by stage_id", con);
             /*      SqlDataAdapter adp = new SqlDataAdapter(cmd);
                   DataSet ds = new DataSet();
                   adp.Fill(ds);*/
             SqlDataReader dr = cmd.ExecuteReader();
             if (dr.HasRows)
             {
                 Gridstg.DataSource = dr;
                 Gridstg.DataBind();
                 Grid_RouteMaster.Visible = false;
             }
             else
             {
                 ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('No Stage Available.');", true);
                 Grid_RouteMaster.Visible = true;
             }
         }
        catch (Exception ex)
        {
            con.Close();
        }

    }

    protected void Grid_RouteMaster_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "show")
        {
            string index = Convert.ToInt32(e.CommandArgument).ToString();
            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("select * from rootmaster where Root_Name=" + index, con);
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
         
            adp.Fill(dt);
            if(dt.Rows.Count>=0)
            {
                Txt_rootname.Text = dt.Rows[0]["Root_Name"].ToString();
                Txt_noofstop.Text = dt.Rows[0]["no_of_stop"].ToString();
                Ddl_bustype.Text = dt.Rows[0]["bustype"].ToString();
                Txt_startloc.Text = dt.Rows[0]["Start_loc"].ToString();
                Txt_endloc.Text = dt.Rows[0]["End_loc"].ToString();
                Btn_update.Visible = true;
                Btn_Submit.Visible = false;
                Lbl_msg.Text = "";
                bindstage();
            }
           

        }
    }
    protected void Btn_update_Click(object sender, EventArgs e)
    {

        try
        {
         
                con = new SqlConnection(connect);
                con.Open();
                cmd = new SqlCommand("updateRootmaster", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@no_of_stop", Txt_noofstop.Text);
                cmd.Parameters.AddWithValue("@bustype", Ddl_bustype.SelectedValue);

                cmd.Parameters.AddWithValue("@Start_loc", Txt_startloc.Text);
                cmd.Parameters.AddWithValue("@End_loc", Txt_endloc.Text);

                cmd.Parameters.AddWithValue("@is_deleted", "0");
                cmd.Parameters.AddWithValue("@locid", "10");
                cmd.Parameters.AddWithValue("@created_by", "0");
                cmd.Parameters.AddWithValue("@create_date", DateTime.Now);
                cmd.Parameters.AddWithValue("@sys_date", DateTime.Now);
                cmd.Parameters.AddWithValue("@km", "0");
                cmd.Parameters.AddWithValue("@bus_id", "1");
                cmd.Parameters.AddWithValue("@end_loc_full_name", Txt_endloc.Text);
                cmd.Parameters.AddWithValue("@Start_loc_full_name", Txt_startloc.Text);


                cmd.Parameters.AddWithValue("@Root_Name", Txt_rootname.Text);

                cmd.ExecuteNonQuery();
               // Lbl_msg.Text = "Data updated successfully.";
              //  Lbl_msg.ForeColor = Color.Black;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Data updated successfully.');", true);
                bind();
            
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }
    protected void Btn_reset_Click(object sender, EventArgs e)
    {
       reset();
      // bind();
    }

    public void reset()
    {
        Txt_rootname.Text = "";
        Txt_noofstop.Text = "";
        Ddl_bustype.Text = "";
        Txt_startloc.Text = "";
        Txt_endloc.Text = "";
        Btn_update.Visible = false;
        Btn_Submit.Visible = true;
    }
}