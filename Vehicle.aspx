﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Vehicle.aspx.cs" Inherits="DataManager.Vehicle" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript" language="javascript">

     function getvehicle(obj1) {

         // debugger
         var name = obj1.value;
         $.ajax({ type: 'POST', url: 'Vehicle.aspx?empname=' + obj1.value + "&Opt1=getconcession", data: $('#form1').serialize(), success: function (response) {

             if (response.substring(0, 8) == 'notexist') {
                 // document.getElementById('lblname0').innerHTML = 'Not Exist'

                 //alert(document.getElementById('lblname0').innerHTML)

                 alert('Vehicle Already Exist');
                 obj1.focus();
             }

             else if (response.substring(0, 5) == 'exist') {
                 arrobj = response.split("~");
                 // document.getElementById('lblname0').innerHTML = arrobj[1];

                 //  document.getElementById('traycode').value = arrobj[1];

             }

         }
         });
     }


  </script>
    <table style="width: 100%">
        <tr>
            <td colspan="2" align="center">
                <h2>
                    Vehicle Master</h2>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td>
                Vehicle Number </td>
            <td>
                <asp:TextBox ID="txtVehCd" runat="server" MaxLength="10" onblur="getvehicle(this)" Width="120px"></asp:TextBox>
                 <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,numbers"
                    TargetControlID="txtVehCd">
                </asp:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="submit-btn" 
                    OnClick="btnSave_Click" Text="Save" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="grdVeh" runat="server" AllowPaging="False" 
                    AutoGenerateColumns="false" BackColor="White" BorderColor="#999999" 
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" 
                    GridLines="Vertical" OnPageIndexChanging="grdVeh_PageIndexChanging" 
                    OnRowCommand="grdVeh_RowCommand" PageSize="3">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                        <asp:BoundField DataField="VehicleID" Visible="false" HeaderText="Vehicle ID" />
                        <asp:BoundField DataField="VehicleNo" HeaderText="Vehicle Number" />
                     <%--   <asp:TemplateField HeaderText="Update">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnedit" runat="server" 
                                    CommandArgument='<%# Eval("VehicleID") %>' CommandName="EditRow" 
                                    ImageUrl="~/images/edit.gif" OnClientClick="return Right(10206,3);" 
                                    TabIndex="1" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" 
                                    CommandArgument='<%# Eval("VehicleID") %>' CommandName="DeleteRow" 
                                    ImageUrl="~/images/delNew.png" OnClientClick="Delete();" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:HiddenField ID="hdVehCode" runat="server" />
                <asp:HiddenField ID="hdDel" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

