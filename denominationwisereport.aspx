﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="denominationwisereport.aspx.cs"
    Inherits="denominationwisereport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <style media="Print" type="text/css">
        .ctrl {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize {
            font-family: Times New Roman;
            font-size: 12px;
        }
    </style>
</head>
<head>
    <title>Waybill Detail Report</title>
    <body class="body">
        <form id="Form1" runat="server">
            <a href="reports.aspx" class="ctrl">Back</a>
            <table style="width: 100%">
                <tr>
                    <td colspan="4" align="center">
                        <h2>Denominationwise Report</h2>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table border="0" width="100%">
                            <tr class="ctrl">
                                <td nowrap width="169">
                                    <font style="font-family: arial; font-size: 12px;">Date From: </font>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtfromdate" runat="server" Width="80px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                    <asp:ScriptManager ID="Scriptmanager1" runat="server">
                                    </asp:ScriptManager>
                                </td>
                                <td class="style2">&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td width="189">&nbsp;
                                </td>
                                <td width="53">
                                    <font style="font-family: arial; font-size: 12px;">Date To </font>
                                </td>
                                <td>
                                    <asp:TextBox ID="txttodate" runat="server" Width="80px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                </td>
                                <td width="174">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="ctrl">
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="Search" runat="server" Text="Search" onclick="Search_Click" />
                        <asp:Button ID="btnexcel" runat="server" onclick="btnexcel_Click" Text="Convert To Excel" />
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:label id="lblfromdate" runat="server"></asp:label>
                    </td>
                    <td align="Right">
                        <asp:label id="lbltodate" runat="server">></asp:label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" ShowFooter="true"
                            AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                            BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                            Font-Strikeout="False" Font-Underline="False" Width="100%" onrowdatabound="grdWayBill_RowDataBound"
                            onselectedindexchanged="grdWayBill_SelectedIndexChanged">
                            <alternatingrowstyle backcolor="White" cssclass="datagrid_row" font-bold="False"
                                font-italic="False" font-names="Verdana" font-overline="False" font-size="Medium"
                                font-strikeout="False" font-underline="False" forecolor="Black" />
                            <rowstyle cssclass="datagrid_row1" font-bold="False" font-italic="False" font-names="Verdana"
                                font-overline="False" font-size="Medium" font-strikeout="False" font-underline="False"
                                forecolor="#000099" />
                            <headerstyle cssclass="datagrid_heading" font-bold="True" font-italic="False" font-names="Arial"
                                font-overline="False" font-size="Small" font-strikeout="False" font-underline="False"
                                forecolor="Black" />
                            <columns>
                     <asp:TemplateField HeaderText="Sr.No" >
                            <ItemTemplate>
                                <%#Container.DataItemIndex  +1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ticket_type" HeaderText="Ticket Type" />
                         <asp:BoundField DataField="perticket" HeaderText="Denomination" />
                        <asp:BoundField DataField="TotalperCount" HeaderText="Ticket Count" />
                         <asp:BoundField DataField="TotalTicketAmount" HeaderText="Total Amount" />
                    </columns>
                            <emptydatatemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                    border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                    height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" 
                                ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </emptydatatemplate>
                            <footerstyle backcolor="#1C5E55" font-bold="True" forecolor="White" font-size="Medium" />
                        </asp:GridView>
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
