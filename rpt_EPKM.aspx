﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="rpt_EPKM.aspx.cs" Inherits="DataManager.rpt_EPKM" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script src="Scripts/json2.js" type="text/javascript"></script>
<script src="scripts/jquery-1.6.2.js" type="text/javascript"></script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style media="Print" type="text/css">
        .ctrl {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize {
            font-family: Times New Roman;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <a href="reports.aspx" class="ctrl">Back</a>

        <table style="width: 100%">
            <tr>
                <td colspan="8" align="center">
                    <h3>EPKM Report</h3>
                </td>
            </tr>
            <tr>
                <td style="width: 130px">From Date :</td>
                <td>
                    <asp:TextBox ID="txtfromdate" runat="server" Width="85px" Height="22px"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                        PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                    </asp:CalendarExtender>
                    <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                    <asp:ScriptManager ID="Scriptmanager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td style="width: 130px">To Date :</td>
                <td>
                    <asp:TextBox ID="txttodate" runat="server" Width="85px" Height="22px"></asp:TextBox>
                    &nbsp;<asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                    </asp:CalendarExtender>
                    &nbsp;</td>
                <td style="width: 130px">WayBill No: </td>
                <td>
                    <asp:TextBox ID="txt_waybill" MaxLength="11" runat="server"></asp:TextBox>
                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionwaybill" MinimumPrefixLength="1"
                        CompletionInterval="10" EnableCaching="false"
                        CompletionSetCount="1" TargetControlID="txt_waybill"
                        ID="AutoCompleteExtender2" runat="server"
                        FirstRowSelected="false">
                    </asp:AutoCompleteExtender>

                </td>
                <td style="width: 130px">Route No: </td>
                <td>
                    <asp:TextBox ID="txtrouteno" MaxLength="3" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="8" align="center">&emsp;
                </td>
            </tr>
            <tr>
                <td colspan="8" align="center">
                    <asp:Button ID="btnSearch" Style="height: 32px; width: 60px;" runat="server" Text="Search" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnExcel" Style="height: 32px; width: 120px;" runat="server" Text="Convert To Excel" OnClick="btnExcel_Click" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblfromdate" runat="server"></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
                <td align="Right">
                    <asp:Label ID="lbltodate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="8" align="center">
                    <asp:GridView ID="grd_epkm" runat="server" AllowSorting="True" ShowFooter="false"
                        AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                        Font-Strikeout="False" Font-Underline="False" Width="100%"
                        OnRowDataBound="grd_epkm_RowDataBound">
                        <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                            Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                            Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                        <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                            Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                            ForeColor="#000099" />
                        <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                            Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                            ForeColor="Black" />
                        <Columns>
                            <asp:BoundField DataField="srno" ItemStyle-Wrap="false" HeaderText="Sr No" />
                            <asp:BoundField DataField="waybilldate" HeaderText="Way Bill Date" />
                            <asp:BoundField DataField="download_date" HeaderText="DownLoad Date" />
                            <asp:BoundField DataField="waybill_no" ItemStyle-Wrap="false" HeaderText="Waybill No" />
                            <asp:BoundField DataField="con_full_name" HeaderText="Conductor Name" />
                            <asp:BoundField DataField="conductorname" HeaderText="Conductor No." />
                            <asp:BoundField DataField="route_no" HeaderText="Route No." />
                            <asp:BoundField DataField="km" HeaderText="Km" />
                            <asp:BoundField DataField="totalcashcollected" HeaderText="Amount" />
                            <asp:BoundField DataField="epkm" HeaderText="EPKM" />
                                 <asp:BoundField DataField="duty" HeaderText="Duty No" />

                        </Columns>
                        <EmptyDataTemplate>
                            <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid; border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid; height: 100px; background-color: beige; text-align: center">
                                <br />
                                <br />
                                <br />
                                <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                            </div>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
