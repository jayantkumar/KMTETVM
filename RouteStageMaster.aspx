﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RouteStageMaster.aspx.cs" Inherits="DataManager.RouteStageMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="Autocomplete Script/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="Autocomplete Script/jquery-ui.min.js" type="text/javascript"></script>
    <link href="Autocomplete Script/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <style>
        .box {
            width: 700px;
            height: 300px;
            background-color: #d9d9d9;
            left: 30%;
        }
    </style>
    <script src="Autocomplete Script/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="Autocomplete Script/jquery-ui.min.js" type="text/javascript"></script>
    <script src="http://ajax.cdnjs.com/ajax/libs/json2/20110223/json2.js"></script>
    <link href="Autocomplete Script/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">

        $(document).ready(function () {
            var vals = '<%= Session["routedetail"]%>'

            var no_of_stop = parseInt(vals.split(',')[0]);
            var route = parseInt(vals.split(',')[3]);
            for (var i = 0; i < no_of_stop; i++) {
                var txtid = "";
                $("[id$=txtsource_" + i + "]").autocomplete({
                    source: function (request, response) {
                        txtid = this.element[0].id.replace('txtsource_', '');
                        $.ajax({
                            url: '<%=ResolveUrl("~/RouteStageMaster.aspx/GetSource") %>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[0],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                alert(response.responseText);
                            },
                            failure: function (response) {
                                alert(response.responseText);
                            }
                        });
                    },
                    select: function (e, i) {
                        var item = i.item.value;

                        $.ajax({
                            url: '<%=ResolveUrl("~/RouteStageMaster.aspx/GetFare") %>',
                            data: "{ 'source': '" + item + "','stepno':'" + parseInt(txtid) + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data != null && data != "") {
                                    document.getElementById('txtsourcemarathi_' + txtid).value = data.d.split(',')[0];
                                    document.getElementById('txtfarestage_' + txtid).value = data.d.split(',')[1];

                                }
                            },
                            error: function (response) {
                                alert(response.responseText);
                            }
                        });
                    },
                    minLength: 1
                });
            }

            $('#btnSubmit').click(function () {
                var myTableArray = []; var dats = {};

                for (var j = 0; j < no_of_stop; j++) {
                    dats = { start_stage: $('#txtstage_' + j).val(), end_stage: 0, no_of_stop: no_of_stop, stage_english: $('#txtsource_' + j).val(), stage_marathi: $('#txtsourcemarathi_' + j).val(), routeno: route, fare_stage_no: $('#txtfarestage_' + j).val() }
                    myTableArray.push(dats);
                }
                document.getElementById("<%=hfdroute.ClientID%>").value = route;
                var data = myTableArray;

                var dt = JSON.stringify({ str: data });
                $.ajax({
                    type: "POST",
                    url: "RouteStageMaster.aspx/Execute",
                    data: dt,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        alert("Wrong");
                    }
                });
            });
        });

        function OnSuccess(response) {

            var splt = response.d.split(',');
            if (parseInt(splt[0]) == "404") {
                alert('Fare stage no should be greater than previous fare stage at row ' + parseInt(splt[1]));
                return;
            }
            else if (parseInt(splt[0]) == "150") {
                alert((splt[1]) + ' Used multiple time');
                return;
            }
            else if (parseInt(splt[0]) == "140") {
                alert('Stage name ' + (splt[1]) + ' not found');
                return;
            }
            else {
                //
                $('#btnSubmit').attr('disabled', 'disabled');
                alert("Data saved successfuly");
                document.getElementById("<%= Button1.ClientID %>").click();

                window.location = "RouteStageMaster.aspx";
            }

}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function checkautocomplete(td) {
    var id = $('#' + td.id).val();
    var txtval = td.id.replace('txtsource_', '');

    if (id != "" && id != null) {
        myFunction(id, td);
        $.ajax({
            url: '<%=ResolveUrl("~/RouteStageMaster.aspx/checkautocomplete") %>',
            data: "{ 'prefix': '" + id + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data != null && data != "") {
                    document.getElementById('txtsourcemarathi_' + txtval).value = data.d;
                }
            },
            error: function (response) {
                alert(response.responseText);
            },
            failure: function (response) {
                alert(response.responseText);
            }
        });
    }

}

function check_fare(thi) {
    var ti = 0;
    var filter, table, tr, fare, i;

    filter = $('#' + thi.id).val();
    table = document.getElementById("Table1");
    tr = table.getElementsByTagName("tr");
    //for (i = 0; i < (tr.length - 2) ; i++) {
    //    fare = document.getElementById('txtfarestage_' + i).value;
    //    if ($('#txtfarestage_' + i)[0].id != thi.id) {
    //        if (parseInt(filter) < parseInt(fare == "" || fare == null ? 0 : fare)) {
    //            //if (ti > 0) {
    //            alert('Fare stage no should be greater than previous fare stage');
    //            return;
    //            //}
    //            //ti++;
    //        }
    //    }
    //}
}

function myFunction(id, tds) {
    var ti = 0;
    var filter, table, tr, td, i;

    filter = id;
    table = document.getElementById("Table1");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < (tr.length - 2) ; i++) {
        td = document.getElementById('txtsource_' + i).value;
        if (document.getElementById('txtsource_' + i).value == filter) {
            if (ti > 0) {
                alert('Already selected');
                return;
            }
            ti++;
        }
    }
}
    </script>

    <div runat="server" id="divtbl">
    </div>
    <br />
    <div runat="server" id="divtbldr">
    </div>
    <br />
    <div runat="server" id="divfare">
    </div>

    <br />

    <asp:Button ID="Button1" Style="display: none;" runat="server" Text="Download" OnClick="Button1_Click" />

    <asp:HiddenField ID="hfd" runat="server" />
    <asp:HiddenField ID="hfdroute" runat="server" />
    &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
    &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
     &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
     &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
     <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" Text="Submit" />
    
</asp:Content>

