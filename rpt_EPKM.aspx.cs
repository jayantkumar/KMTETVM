﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace DataManager
{
    public partial class rpt_EPKM : System.Web.UI.Page
    {
        string connect = System.Configuration.ConfigurationSettings.AppSettings["Con"].ToString();
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader dr;
        SqlDataAdapter da;
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        DataManager dm = new DataManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            }
        }


        [System.Web.Services.WebMethod]
        public static List<string> GetCompletionwaybill(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select WayBillNo from WayBill_Details where " +
                    "WayBillNo like  '%' + @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> customers = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(sdr["WayBillNo"].ToString());
                        }
                    }
                    conn.Close();
                    return customers;
                }
            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                con = new SqlConnection(connect);
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandTimeout = 500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Sp_earning_per_km";
                cmd.Parameters.Add(new SqlParameter("@fromdate", txtfromdate.Text));
                cmd.Parameters.Add(new SqlParameter("@todate", txttodate.Text));
                cmd.Parameters.Add(new SqlParameter("@waybillno", txt_waybill.Text));
                cmd.Parameters.Add(new SqlParameter("@routeno", txtrouteno.Text));

                adp.SelectCommand = cmd;
                adp.Fill(ds);
                DataSet ds2 = new DataSet();

                if (Convert.ToInt32(ds.Tables.Count) > 0)
                {
                    grd_epkm.DataSource = ds;
                    grd_epkm.DataBind();
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            if (grd_epkm.Rows.Count > 0)
                GridViewExportUtil.Export("EPKM" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm") + ".xls", grd_epkm);
            else
                return;
        }

        protected void grd_epkm_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}