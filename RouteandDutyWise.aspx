﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RouteandDutyWise.aspx.cs" Inherits="DataManager.RouteandDutyWise" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style media="Print" type="text/css">
        .ctrl {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize {
            font-family: Times New Roman;
            font-size: 12px;
        }

        .style1 {
            height: 28px;
        }
    </style>
</head>
<body>

    <div>
        <form id="Form1" runat="server">
            <a href="reports.aspx" class="ctrl">Back</a>
            <table style="width: 100%">
                <tr>
                    <td colspan="4" align="center">
                        <h3>ETVM and Manual cash Collection Report</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <table border="0" width="100%">
                            <tr class="ctrl">
                                <td align="right">
                                    <font style="font-family: arial; font-size: 12px;">From Date : </font>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtfromdate" runat="server" Width="80px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                    <asp:ScriptManager ID="Scriptmanager1" runat="server">
                                    </asp:ScriptManager>
                                </td>
                                <td align="right">
                                    <font style="font-family: arial; font-size: 12px;">To Date : </font>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txttodate" runat="server" Width="80px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                </td>
                                <td align="right">
                                    <font style="font-family: arial; font-size: 12px;">Duty : </font>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtduty" runat="server"></asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionduty" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false"
                                        CompletionSetCount="1" TargetControlID="txtduty"
                                        ID="AutoCompleteExtender2" runat="server"
                                        FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                </td>
                                <td align="right">
                                    <font style="font-family: arial; font-size: 12px;">Route : </font>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtroute" runat="server"></asp:TextBox>

                                </td>
                            </tr>
                            <tr style="height: 50px" class="ctrl">
                                <td class="style1"></td>
                                <td colspan="2" align="center" class="style1">
                                    <asp:Button ID="Search" runat="server" Text="Search" OnClick="Search_Click" />
                                    <asp:Button ID="btnexcel" runat="server" OnClick="btnexcel_Click" Text="Convert To Excel" />
                                </td>
                                <td colspan="5" class="style1">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblfromdate" runat="server"></asp:Label>
                    </td>
                    <td align="Right">
                        <asp:Label ID="lbltodate" runat="server">></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" ShowFooter="true"
                            AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                            BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                            Font-Strikeout="False" Font-Underline="False" Width="100%" OnRowDataBound="grdWayBill_RowDataBound">
                            <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                                Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                                Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                            <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                                Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                                ForeColor="#000099" />
                            <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                                Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                                ForeColor="Black" />


                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</body>
</html>
