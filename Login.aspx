﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="DataManager.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title>Login</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="robots" content="index, follow" />
    <meta charset="utf-8" />

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../js/rainbows.js"></script>

    <link type="text/css" rel="stylesheet" href="css/style1.css" media="screen" />
    <script type="text/javascript" language="javascript">

        function validate() {
            if (document.getElementById('urlusername').value == '') {
                document.getElementById('Alert').value = 'Please Enter User Name';
                document.getElementById('urlusername').focus = true;
                return false;
            }
        }

    </script>
</head>
<body>
    <form id="Form1" runat="server">
        <div id="wrapper">
            <div id="wrappertop">
            </div>
            <div id="wrappermiddle">
                <h2>Login</h2>
                <div id="username_input">
                    <div id="username_inputleft">
                    </div>
                    <div id="username_inputmiddle">
                        <input type="text" runat="server" name="link" id="urlusername" value=""
                            onclick="this.value = ''" />
                        <img id="url_user" src="images/mailicon.png" alt="" />
                    </div>
                    <div id="username_inputright">
                    </div>
                </div>
                <div id="password_input">
                    <div id="password_inputleft">
                    </div>
                    <div id="password_inputmiddle">
                        <input type="password" runat="server" name="link" id="urlPassword" value="Password"
                            onclick="this.value = ''" />
                        <img id="url_password" src="images/passicon.png" alt="" />
                    </div>
                    <div id="password_inputright">
                    </div>
                </div>
                <div id="submit">
                    <asp:ImageButton ID="Login1" src="images/submit.jpg" value="Sign In" OnClientClick="validate();"
                        runat="server" OnClick="Login1_Click" />
                </div>
                <div id="links_left">
                    <a href="#" onclick="document.getElementById('id02').style.display='block'" style="width: auto;">Forgot your Password?</a>
                </div>
             <%--   <div id="links_right">
                    <a href="#" onclick="document.getElementById('id01').style.display='block'" style="width: auto;">Not a Member Yet? </a>
                </div>--%>
            </div>
            <div id="DivAlert">
                <asp:Label ID="Alert" ForeColor="Red" runat="server" Text=""></asp:Label>
            </div>
            <div id="wrapperbottom">
            </div>
            <div id="powered">
                <p>
                    Powered by <a href="www.aeonsoftware.net">Aeon Software PVT LTD</a>
                </p>
            </div>
        </div>
    </form>
   

</body>
</html>
