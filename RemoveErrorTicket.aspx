﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RemoveErrorTicket.aspx.cs" Inherits="DataManager.RemoveErrorTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="Autocomplete Script/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="Autocomplete Script/jquery-ui.min.js" type="text/javascript"></script>
    <link href="Autocomplete Script/jquery-ui.css" rel="Stylesheet" type="text/css" />


    <script type="text/javascript">

        $(function () {
            $("[id$=txtwaybill]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/RemoveErrorTicket.aspx/Getwaybill") %>',
                        data: "{ 'prefix': '" + request.term + "' }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {

                },
                minLength: 1
            });
        });

        $(function () {
            $("[id$=txtticketno]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/RemoveErrorTicket.aspx/Getticket") %>',
                        data: "{ 'prefix': '" + request.term + "','waybill': '" + document.getElementById('<%=txtwaybill.ClientID %>').value + "' }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {

                },
                minLength: 1
            });
        });
    </script>

    <div>
        <table style="width: 100%">
            <tr>
                <td colspan="5" style="text-align: center;">
                    <h3>Remove Error Ticket</h3>
                </td>
            </tr>
            <tr>
                <td colspan="5">&emsp;</td>
            </tr>
            <tr>
                <td>&emsp; </td>
                <td style="width: 150px;">
                    <asp:Label runat="server" Text="Waybill No."></asp:Label></td>
                <td style="width: 250px;">
                    <asp:TextBox ID="txtwaybill" runat="server"></asp:TextBox></td>
                <td style="width: 150px;">
                    <asp:Label runat="server" Text="Ticket No."></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtticketno" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="5">&emsp;</td>
            </tr>
            <tr>
                <td colspan="3">&emsp;</td>
                <td>
                    <asp:Button ID="tbnsearch" Style="width: 100px; height: 25px;" runat="server" Text="Search" OnClick="tbnsearch_Click" />
                </td>
                <td style="text-align: center;">&emsp;</td>
            </tr>
            <tr>
                <td colspan="5">&emsp;</td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True"
                        AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                        Font-Strikeout="False" Font-Underline="False" OnRowCommand="grdWayBill_RowCommand"
                        Width="100%">
                        <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                            Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                            Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                        <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                            Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                            ForeColor="#000099" />
                        <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                            Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                            ForeColor="Black" />
                        <Columns>
                            <asp:TemplateField HeaderText="Sr.No">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex  +1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ticket_no" HeaderText="Ticket no" />
                            <asp:BoundField DataField="startstage" HeaderText="Start Stage" />
                            <asp:BoundField DataField="end_stage" HeaderText="End Stage" />
                            <asp:BoundField DataField="no_of_tic" HeaderText="No Of Passenger" />
                            <asp:BoundField DataField="ticket_amnt" HeaderText="Ticket Amount" />
                            <asp:BoundField DataField="transdate" HeaderText="Ticket DateTime" />
                            <asp:BoundField DataField="ticket_status" HeaderText="Ticket Status" />
                            <asp:BoundField DataField="trip_no" HeaderText="Trip No" />
                            <asp:BoundField DataField="conname11" HeaderText="Concession/Normal" />
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDel" runat="server"
                                        CommandArgument='<%# Eval("tic_id") %>' CommandName="DeleteRow" OnClientClick="return confirm('Are you sure you want to delete this ticket?');"
                                        ImageUrl="~/images/delNew.png" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid; border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid; height: 100px; background-color: beige; text-align: center">
                                <br />
                                <br />
                                <br />
                                <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                            </div>
                        </EmptyDataTemplate>

                    </asp:GridView>
                </td>
                <asp:HiddenField ID="hdticid" runat="server" />
            </tr>
        </table>

    </div>
</asp:Content>

