﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using DataManager;
using System.Windows;
namespace DataManager
{

    public partial class Vehicle : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQuery = "select vehicleid,vehicleno from Vehicle_Master where is_delete=0 order by vehicleno ";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Opt1"] == "getconcession")
            {
                string sttray = "";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select VehicleNo  from Vehicle_Master where VehicleNo='" + txtVehCd.Text + "' order by VehicleNo ", con);
                con.Open();
                da.Fill(ds);
                con.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("notexist~");
                }
                else
                {
                    Response.Write("exist~" + sttray.ToString() + "~");
                }
            }
            if (!IsPostBack)
            {
                try
                {
                    dm.ExeCuteGridBind(selQuery, grdVeh);
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("select VehicleNo  from Vehicle_Master where VehicleNo='" + txtVehCd.Text + "' ", con);
            con.Open();
            da.Fill(ds);
            con.Close();
            if (txtVehCd.Text == "")
            {
                Response.Write("<script>alert ('Enter Vehicle Number') </script>");
            }
            else if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert ('Already Exist') </script>");
            }
            else
            {
                try
                {
                    con.Open();
                    cmd = new SqlCommand();
                    cmd.CommandText = "VEHICLE_MASTER_ADD_UPDATE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.Add("@VEHCODE", SqlDbType.NVarChar).Value = txtVehCd.Text;
                    if (btnSave.Text == "Save")
                    {
                        cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@code", SqlDbType.Int).Value = 0;
                        txtVehCd.Text = "";
                    }
                    else if (btnSave.Text == "Update")
                    {
                        cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                        cmd.Parameters.Add("@code", SqlDbType.Int).Value = Convert.ToInt32(hdVehCode.Value);
                        btnSave.Text = "Save";
                        txtVehCd.Text = "";
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                }
                dm.ExeCuteGridBind(selQuery, grdVeh);
            }
        }
        protected void grdVeh_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdVeh.PageIndex = e.NewPageIndex;
            dm.ExeCuteGridBind(selQuery, grdVeh);
        }
        protected void grdVeh_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            hdVehCode.Value = e.CommandArgument.ToString();
            if (e.CommandName == "EditRow")
            {
                btnSave.Text = "Update";
                dr = dm.GetDataReader("Select VehicleNo from Vehicle_Master where vehicleID=" + e.CommandArgument);
                while (dr.Read())
                {
                    txtVehCd.Text = dr[0].ToString();
                }
            }
            if (e.CommandName == "DeleteRow")
            {
                if (hdDel.Value.ToString() != "1")
                {
                    dm.ExecuteNonQuery("update Vehicle_Master set is_delete=1 where vehicleId=" + e.CommandArgument);
                    dm.ExeCuteGridBind(selQuery, grdVeh);
                }
                hdDel.Value = "0";
            }
        }
    }
}