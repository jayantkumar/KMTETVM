﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="ResetPassword.aspx.cs" Inherits="ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>

        <table style="width: 100%">
            <tr>
                <td>&emsp;</td>
                <td colspan="2">
                    <h3>Change Password</h3>
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td colspan="4">&emsp;</td>
            </tr>
            <tr>
                <td>&emsp; </td>
                <td style="width: 150px;">
                    <asp:Label runat="server" Text="Username"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtusername" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4">&emsp;</td>
            </tr>
            <tr>
                <td>&emsp;  </td>
                <td style="width: 150px;">
                    <asp:Label runat="server" Text="New Password"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtpassword" TextMode="Password" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4">&emsp;</td>
            </tr>
            <tr>
                <td>&emsp; </td>
                <td style="width: 150px;">
                    <asp:Label runat="server" Text="Confirm Password"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtconfirm" TextMode="Password" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4">&emsp;</td>
            </tr>
            <tr>
                <td colspan="2">&emsp;</td>
                <td>
                    <asp:Button ID="btnsubmit" runat="server" Text="Reset Password" OnClick="btnsubmit_Click" /></td>
                <td style="text-align: center;">&emsp;</td>


            </tr>
        </table>

    </div>

</asp:Content>
