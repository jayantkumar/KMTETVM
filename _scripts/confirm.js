/*
jQuery(function ($) {
	$('#confirm-dialog input.confirm, #confirm-dialog a.confirm').click(function (e) {
		e.preventDefault();

		// example of calling the confirm function
		// you must use a callback function to perform the "yes" action
		confirm("Continue to the SimpleModal Project page?", function () {
			window.location.href = 'swapnil';
		});
	});
});
*/

function confirm1(message, callback) {
	$('#confirm').modal({
		closeHTML: "<a href='javascript:void(0);' title='Close' class='modal-close'>X</a>",
		position: ["5%",],
		
		containerId: 'confirm-container',
		overlayId: 'confirm-overlay',
		onShow: function (dialog) {
			var modal = this;

			$('.message', dialog.data[0]).append(message);

			// if the user clicks "yes"
			$('.yes', dialog.data[0]).click(function () {
				// call the callback
				if ($.isFunction(callback)) {
					callback.apply();
				}
				// close the dialog
				modal.close(); // or $.modal.close();
			});
		}
	});
}