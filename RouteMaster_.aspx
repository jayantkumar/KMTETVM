﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RouteMaster_.aspx.cs" Inherits="DataManager.RouteMaster_" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="Autocomplete Script/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="Autocomplete Script/jquery-ui.min.js" type="text/javascript"></script>
    <link href="Autocomplete Script/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <script type="text/javascript">

        $(function () {
            $("[id$=txtsource]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/RouteMaster_.aspx/GetSource") %>',
                        data: "{ 'prefix': '" + request.term + "','key': '1','source': '" + $("[id$=hfSourceId]").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("[id$=hfSourceId]").val(i.item.value);
                },
                minLength: 1
            });
        });

        $(function () {
            $("[id$=txtdestination]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/RouteMaster_.aspx/GetSource") %>',
                        data: "{ 'prefix': '" + request.term + "','key': '2','source': '" + $("[id$=hfSourceId]").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("[id$=hfDestinationId]").val(i.item.value);
                },
                minLength: 1
            });
        });
        function etmcheck() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "createwaybill.aspx/validateetmdevice",
                data: "{'etmdevice':'" + document.getElementById("ContentPlaceHolder1_txtetmcode").value + "'}",
                dataType: "json",
                success: function (data) {
                    var obj = data.d;
                    if (obj == 'true') {
                        alert("Please Enter Correct ETM Number");
                        document.getElementById("ContentPlaceHolder1_txtetmcode").focus();
                        $("#ContentPlaceHolder1_txtetmcode").css("color", "red");

                        return false;
                    }
                    else {
                        $("#ContentPlaceHolder1_txtetmcode").css("color", "black");

                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        function checkstage() {
            var stageno = document.getElementById('<%=txtstartno.ClientID %>').value == "" ? "0" : document.getElementById('<%=txtstartno.ClientID %>').value;

            if (document.getElementById('<%=txtnoofstop.ClientID %>').value != "" && stageno != "0") {
                document.getElementById('<%=txtendno.ClientID %>').value = parseInt(document.getElementById('<%=txtnoofstop.ClientID %>').value) + parseInt(stageno - 1);
            }
        }

        function isNumberKey(evt) {
            var total_stage = $("[id$=hdntotal_step]").val();
            //var charCode = (evt.which) ? evt.which : event.keyCode;
            //var num = total_stage;
            //if ((charCode > 31 && (charCode < 48 || charCode > 57)) || (num > 10)) {
            //    if (num > 20) {
            //        alert('choose number from 1-10');
            //    }
            //    return false;
            //} else {
            //    return true;
            //}
        }

    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>Route Master</h2>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 200px;">Route No  </td>
            <td>
                <asp:TextBox ID="txtrouteno" runat="server" Enabled="false" Width="170px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 200px;">No. Of Stage </td>
            <td>
                <asp:TextBox ID="txtnoofstop" runat="server" onkeyup="checkstage()" MaxLength="2"
                    Width="170px"></asp:TextBox>
                <asp:Label ID="lblstage" Font-Bold="true" runat="server"></asp:Label>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                    Enabled="True" FilterType="numbers"
                    TargetControlID="txtnoofstop">
                </asp:FilteredTextBoxExtender>
            </td>
            <td style="width: 200px;">Start stage no </td>
            <td>
                <asp:DropDownList ID="Ddl_bustype" runat="server" Font-Bold="True"
                    Font-Size="12pt" Height="23px" Width="170px">
                    <asp:ListItem Selected>Ordinary</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 200px;">Source </td>
            <td>
                <asp:TextBox ID="txtsource" runat="server" MaxLength="11" Width="170px"></asp:TextBox>
            </td>
            <td style="width: 200px;">Start stage no </td>
            <td>
                <asp:TextBox ID="txtstartno" runat="server" onkeyup="checkstage()" MaxLength="2" Width="170px"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                    Enabled="True" FilterType="numbers"
                    TargetControlID="txtstartno">
                </asp:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;
       
            </td>
        </tr>
        <tr>
            <td style="width: 200px;">Destination </td>
            <td>
                <asp:TextBox ID="txtdestination" runat="server" MaxLength="11" Width="170px"></asp:TextBox>
            </td>
            <td style="width: 200px;">End stage no </td>
            <td>
                <asp:TextBox ID="txtendno" runat="server" Enabled="false" MaxLength="2" Width="170px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="submit-btn"
                    OnClick="btnSave_Click" Text="Save" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">
                <asp:GridView ID="grdRoute" runat="server"
                    AutoGenerateColumns="false" BackColor="White" BorderColor="#999999" Width="800px"
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="5" ForeColor="Black" AlternatingRowStyle-Width="100px"
                    GridLines="Vertical" OnPageIndexChanging="grdRoute_PageIndexChanging"
                    OnRowCommand="grdRoute_RowCommand">
                    <AlternatingRowStyle BackColor="#e1e1e1" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#b2b2b2" ForeColor="BlueViolet" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />

                    <Columns>
                        <asp:BoundField DataField="Root_Name" HeaderText="Route No" />
                        <asp:BoundField DataField="source" HeaderText="Source" />
                        <asp:BoundField DataField="destination" HeaderText="Destination" />
                        <asp:BoundField DataField="bustype" HeaderText="Bus Type" />
                        <asp:TemplateField ItemStyle-Width="40px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:CheckBox ID="Select" runat="server" AutoPostBack="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>

                                <asp:Button ID="btnview" runat="server" CommandArgument='<%# Eval("Root_Name") %>' Text="Show Details" CommandName="ViewRow" />
                            </ItemTemplate>

                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" Text="Add" OnClick="Button2_Click" />
                <br />
                <asp:GridView ID="gridadd" runat="server"
                    AutoGenerateColumns="false" BackColor="White" BorderColor="#999999" Width="700px"
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
                    GridLines="Vertical">
                    <AlternatingRowStyle BackColor="#e1e1e1" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#b2b2b2" ForeColor="BlueViolet" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                        <asp:BoundField DataField="Root_Name" HeaderText="Route No" />
                        <asp:BoundField DataField="source" HeaderText="Source" />
                        <asp:BoundField DataField="destination" HeaderText="Destination" />
                        <asp:BoundField DataField="bustype" HeaderText="Bus Type" />
                    </Columns>
                </asp:GridView>
                <br />
                <br />
                <span id="spanetm" runat="server">
                    <asp:Label runat="server" Font-Bold="true" Text="ETM Code" ID="lbletmcode"></asp:Label>
                    <asp:TextBox runat="server" MaxLength="4" onchange="etmcheck();" ID="txtetmcode"></asp:TextBox>
                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtetmcode"
                        ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                    </asp:AutoCompleteExtender>
                </span>
                <br />
                <br />
                <asp:Button ID="Button3" runat="server" Text="Program" OnClick="Button3_Click" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <div runat="server" id="divtbl">
    </div>

    <div id="myModal" class="modal">
        <div class="modal-cont">
            <span style="color: red;" class="close">&times;</span>
            <p style="text-align: center;">Route Details..</p>
        </div>
        <div class="modal-content">

            <asp:GridView runat="server" ID="grid" Font-Bold="false" CssClass="beta" OnRowDataBound="grid_RowDataBound">
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="#eeeeee" />
            </asp:GridView>
        </div>

    </div>
    <asp:HiddenField ID="hdRouteId" runat="server" />
    <asp:HiddenField ID="hdDel" runat="server" />
    <asp:HiddenField ID="hdSave" runat="server" />
    <asp:HiddenField ID="hfSourceId" runat="server" />
    <asp:HiddenField ID="hdntotal_step" runat="server" />
    <asp:HiddenField ID="hfDestinationId" runat="server" />





    <link type="text/css" rel="stylesheet" href="css/PopUp.css" media="screen" />
    <link href="http://code.jquery.com/ui/1.11.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            var rowscount = document.getElementById('ContentPlaceHolder1_grid').rows.length;
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the button that opens the modal
            var btn = document.getElementById("myBtn");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks the button, open the modal 
            //btn.onclick = function () {
            modal.style.display = "block";
            //}

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
            if (rowscount > 1) {
                //$("#popupdiv").show();
                //$("#popupdiv").dialog({
                //    title: "jQuery Show Gridview in Popup",
                //    width: 900,
                //    height: 500,
                //    modal: true,
                //    buttons: {
                //        Close: function () {
                //            $(this).dialog('close');
                //        }
                //    }
                //});
            }
        });
    </script>
</asp:Content>

