﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DataManager;
using System.IO;
//using TimerGetData;
using System.Reflection;
namespace DataManager
{
    public partial class ServiceOperator : System.Web.UI.Page
    {


        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        SqlDataAdapter adp;
        DataSet ds;
        DataManager dm = new DataManager();
        String selQry = "select top 1  serviceOprId,OprName,Address1,Address2,CommentLn from ServiceOperator_Entry where is_delete=0 order by sysdate desc ";
        protected void Clear()
        {
            txtName.Text = "";
            txtAd1.Text = "";
            txtAd2.Text = "";
            txtCmt.Text = "";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                dm.ExeCuteGridBind(selQry, grdSO);
                if (grdSO.Rows.Count < 1)
                {

                    Button1.Visible = false;
                }

            }
        }

        public void bindgrid()
        {

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_serviceoperator";
            //'  cmd.Parameters.Add(New SqlParameter("@bustype", DropDownList1.SelectedItem))
            cmd.Connection = con;
            adp.SelectCommand = cmd;
            adp.Fill(ds);
            grdSO.DataSource = ds;
            grdSO.DataBind();


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (txtName.Text == "")

            {
                Response.Write("<script>alert ('Enter Operator Name') </script>");
            
            }

            else if (txtAd1.Text =="")

            {
                Response.Write("<script>alert ('Enter Adress1') </script>");
            }

            else if (txtAd2.Text == "")
            {
                Response.Write("<script>alert ('Enter Adress2') </script>"); 
            }

            else if (txtCmt.Text == "")

            { Response.Write("<script>alert ('Enter Comment Line') </script>"); }
            else
            {

                serviceoperatorcreate();


                if (txtAd1.Text == "" || txtAd2.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "foo", "  Validation(4,'','')", true);
                }
                else
                {
                    try
                    {
                        con.Open();
                        cmd = new SqlCommand();
                        cmd.CommandText = "SERVICE_OPERATOR_MASTER_ADD_UPDATE";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.Add("@SONAME", SqlDbType.NVarChar).Value = txtName.Text.Trim();
                        cmd.Parameters.Add("@ADD1", SqlDbType.NVarChar).Value = txtAd1.Text.Trim();
                        cmd.Parameters.Add("@ADD2", SqlDbType.NVarChar).Value = txtAd2.Text.Trim();
                        cmd.Parameters.Add("@CMTLN", SqlDbType.NVarChar).Value = txtCmt.Text.Trim();
                        if (btnSave.Text == "Save")
                        {
                            cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                            cmd.Parameters.Add("@code", SqlDbType.Int).Value = 0;
                            Clear();
                        }
                        else if (btnSave.Text == "Update")
                        {
                            cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                            cmd.Parameters.Add("@code", SqlDbType.Int).Value = Convert.ToInt32(hdOprCode.Value);
                            btnSave.Text = "Save";
                            Clear();
                        }
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        con.Close();
                    }
                }
                dm.ExeCuteGridBind(selQry, grdSO);
                Response.Redirect("ServiceOperator.aspx");
            }
        }
        public void serviceoperatorcreate()
        {

            FileStream fs2 = new FileStream("d:\\pratinidhi\\ODETAILS.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs2);


            string a, b, c, d, e, f, g, h, aa = "", ba = "", ca = "", da = "", ea = "", fa = "", ga = "", ha = "";



            a = txtName.Text.Trim();
            b = txtAd1.Text.Trim();
            c = txtAd2.Text.Trim();
            d = txtCmt.Text.Trim();
            e = "Non Transferrable";
            f = "Aeon";
            g = "Aeon S/W PVT LTD";

            if (a.Length < 32)
            {
                int aaaa = a.Length % 2;
                int aaa = (32 - a.Length) / 2;


                a = a.PadLeft(a.Length + aaa);

                aa = aa.PadRight(aaa + aaaa);
            }





            if (b.Length < 32)
            {
                int bb = (32 - b.Length);

                //  ba = ba.PadRight(32 - b.Length);


                int bbbb = b.Length % 2;
                int bbb = (32 - b.Length) / 2;


                b = b.PadLeft(b.Length + bbb);

                ba = ba.PadRight(bbb + bbbb);



            }
            if (c.Length < 32)
            {

                // ca = ca.PadRight(32 - c.Length);


                int cc = (32 - c.Length);

                //  ba = ba.PadRight(32 - b.Length);


                int cccc = c.Length % 2;
                int ccc = (32 - c.Length) / 2;


                c = c.PadLeft(c.Length + ccc);

                ca = ca.PadRight(ccc + cccc);
            }

            if (d.Length < 32)
            {

                // da = da.PadRight(32 - d.Length);



                //  ba = ba.PadRight(32 - b.Length);


                int dddd = d.Length % 2;
                int ddd = (32 - d.Length) / 2;


                d = d.PadLeft(d.Length + ddd);

                da = da.PadRight(ddd + dddd);

            }

            if (e.Length < 32)
            {

                // ea = ea.PadRight(32 - e.Length);


                int ee = (32 - e.Length);




                int eeee = e.Length % 2;
                int eee = (32 - e.Length) / 2;


                e = e.PadLeft(e.Length + eee);

                ea = ea.PadRight(eee + eeee);
            }

            if (f.Length < 32)
            {

                // fa = fa.PadRight(32 - f.Length);
                int ff = (32 - f.Length);




                int ffff = f.Length % 2;
                int fff = (32 - f.Length) / 2;


                f = f.PadLeft(f.Length + fff);

                fa = fa.PadRight(fff + ffff);
            }


            if (g.Length < 20)
            {

                // ga = ga.PadRight(20 - g.Length);
                int gg = (32 - g.Length);




                int gggg = g.Length % 2;
                int ggg = (20 - g.Length) / 2;


                g = g.PadLeft(g.Length + ggg);

                ga = ga.PadRight(ggg + gggg);
            }

            writer.Write(a.ToString() + aa + "," + b.ToString() + ba + "," + c.ToString() + ca + "," + d.ToString() + da + "," + e.ToString() + ea + "," + f.ToString() + fa + "," + g.ToString() + ga);
            writer.Close();
        
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
            // info.FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\etm_com.exe";

            info.FileName = (@"D:\pratinidhi\etm_com.exe ");
            info.Arguments = "9";
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo = info;
            process.StartInfo.UseShellExecute = false;
            System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "10");



            //   System.Diagnostics.Process.Start("D:\myapps\sample.exe", "/s /v/qn");




            //MessageBox.Show(info.Arguments.ToString());
            process.Close();
            //File.Delete("D:\\PRATINIDHI\\ODETAILS.txt");
           // File.WriteAllText(@"d:\\pratinidhi\\ODETAILS.txt", String.Empty);
        }
        protected void grdSO_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSO.PageIndex = e.NewPageIndex;
            dm.ExeCuteGridBind(selQry, grdSO);
        }
        protected void grdSO_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            hdOprCode.Value = e.CommandArgument.ToString();
            if (e.CommandName == "EditRow")
            {
                btnSave.Text = "Update";
                dr = dm.GetDataReader("Select OprName,Address1,Address2,CommentLn from ServiceOperator_Entry where serviceOprId=" + e.CommandArgument);
                while (dr.Read())
                {
                    txtName.Text = dr[0].ToString();
                    txtAd1.Text = dr[1].ToString();
                    txtAd2.Text = dr[2].ToString();
                    txtCmt.Text = dr[3].ToString();
                }
            }
            if (e.CommandName == "DeleteRow")
            {
                if (hdDel.Value.ToString() != "1")
                {
                    dm.ExecuteNonQuery("update ServiceOperator_Entry set is_delete=1 where serviceOprId=" + e.CommandArgument);
                    dm.ExeCuteGridBind("select serviceOprId,OprName,Address1,Address2,CommentLn from ServiceOperator_Entry where is_delete=0 ", grdSO);
                }
                hdDel.Value = "0";
            }
        }
    }
}