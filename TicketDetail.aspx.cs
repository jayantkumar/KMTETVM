﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


public partial class Report_TicketDetail : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    double a;
    double failtic_amt;
    double Alltic_amt;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ticket_detail_report";
                cmd.Parameters.Add(new SqlParameter("@waybillno", txtWayNo.Text));
                cmd.Parameters.Add(new SqlParameter("@ticketno", ""));
                adp.SelectCommand = cmd;
                adp.Fill(ds);
                grdWayBill.DataSource = ds;
                grdWayBill.DataBind();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblwaybillno.Text = "Way Bill No : " + txtWayNo.Text;
                    lblbusno.Text = "Bus No : " + ds.Tables[2].Rows[0]["vehiclecode"].ToString();
                    lbldriver.Text = "Driver Name : " + ds.Tables[2].Rows[0]["drivername"].ToString();
                    lblconductor.Text = "Conductor Name : " + ds.Tables[2].Rows[0]["conductorname"].ToString();
                    lbletvmno.Text = "ETVM NO : " + ds.Tables[2].Rows[0]["etm_Code"].ToString();
                }
                else
                {
                    lblwaybillno.Text = "";
                    lblbusno.Text = "";
                    lbldriver.Text = "";
                    lblconductor.Text = "";
                    lbletvmno.Text = "";
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }

        }
    }
    protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.Cells[7].Text == "O")
            {
                e.Row.Cells[7].Text = "OK";
            }
            else if (e.Row.Cells[7].Text == "F")
            {
                e.Row.Cells[7].Text = "Fail";
                e.Row.BackColor = System.Drawing.Color.Pink;
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[7].Text != "Fail")
                {
                    a += double.Parse(e.Row.Cells[5].Text);
                }
                else
                {
                    failtic_amt += double.Parse(e.Row.Cells[5].Text);
                }
                Alltic_amt += double.Parse(e.Row.Cells[5].Text);
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Total Amount";
                e.Row.Cells[1].Text = Alltic_amt.ToString();
                e.Row.Cells[2].Text = "Failed Tic Amount";
                e.Row.Cells[3].Text = failtic_amt.ToString();
                e.Row.Cells[4].Text = "Sucess Tic Amount";
                e.Row.Cells[5].Text = a.ToString();
            }

        }
        catch (Exception ex)
        {
        }
    }
}