﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DataManager;

namespace DataManager
{
    public partial class BusStageMaster : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQuery = "select distinct bus_stage_id , upper(bus_stage) bus_stage, upper(bus_stage_regional) bus_stage_regional, upper(bus_stage_short_name) bus_stage_short_name from bus_stage_master  where ISNULL(IS_stage_delete,0) = 0 order by bus_stage asc";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtstage.Attributes.Add("autocomplete", "off");
                txtxstagemarathi.Attributes.Add("autocomplete", "off");
                txtsshortname.Attributes.Add("autocomplete", "off");
                dm.ExeCuteGridBind(selQuery, grdStage);
            }
        }

        protected void grdStage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdStage.PageIndex = e.NewPageIndex;
            dm.ExeCuteGridBind(selQuery, grdStage);
        }

        protected void grdStage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            hdStageId.Value = e.CommandArgument.ToString();
            if (e.CommandName == "EditRow")
            {
                btnSave.Text = "Update";
                dr = dm.GetDataReader("select distinct bus_stage_id , bus_stage , bus_stage_regional ,bus_stage_short_name from bus_stage_master where bus_stage_id = " + hdStageId.Value);
                while (dr.Read())
                {
                    txtstage.Text = dr["bus_stage"].ToString();
                    txtxstagemarathi.Text = dr["bus_stage_regional"].ToString();
                    txtsshortname.Text = dr["bus_stage_short_name"].ToString();
                }
            }
            if (e.CommandName == "DeleteRow")
            {
                if (hdDel.Value.ToString() != "1")
                {
                    dm.ExecuteNonQuery("update bus_stage_master set Is_Stage_Delete = 1 where bus_stage_id = " + hdStageId.Value);
                    dm.ExeCuteGridBind(selQuery, grdStage);
                }
                hdDel.Value = "0";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select distinct bus_stage   from bus_stage_master where LTRIM(rtrim(bus_stage)) ='" + txtstage.Text.Trim() + "' ", con);
                con.Open();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0 && Convert.ToInt32(hdStageId.Value) < 0)
                {
                    txtstage.Focus();
                    Response.Write("<script>alert ('Stage name already exist') </script>");
                    return;
                }
                else if (txtstage.Text == "")
                {
                    txtstage.Focus();
                    Response.Write("<script>alert ('Enter stage name') </script>");
                    return;
                }
                else if (txtxstagemarathi.Text == "")
                {
                    Response.Write("<script>alert ('Enter stage name in marathi') </script>");
                    return;
                }

                else if (txtsshortname.Text == "")
                {
                    Response.Write("<script>alert ('Enter short name') </script>");
                    return;
                }
                else
                {
                    if (hdSave.Value.ToString() != "1")
                    {
                        try
                        {
                            con.Open();
                            cmd = new SqlCommand();
                            cmd.CommandText = "Sp_Insert_bus_stage";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.Add("@stage", SqlDbType.NVarChar).Value = txtstage.Text;
                            cmd.Parameters.Add("@stagemarathi", SqlDbType.NVarChar, 50).Value = txtxstagemarathi.Text;
                            cmd.Parameters.Add("@shortname", SqlDbType.NVarChar).Value = txtsshortname.Text.ToString();

                            if (btnSave.Text == "Save")
                            {
                                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = 0;
                            }
                            else if (btnSave.Text == "Update")
                            {
                                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = Convert.ToInt32(hdStageId.Value);
                                btnSave.Text = "Save";
                            }
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        catch (Exception ex)
                        {
                            con.Close();
                        }
                    }
                    dm.ExeCuteGridBind(selQuery, grdStage);
                    Clear();
                    hdSave.Value = "0";
                    Response.Write("<script>alert('Data saved successfully');window.location.href='BusStageMaster.aspx'</script>");
                }
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Exception occured in btnSave_Click function " + ex.Message + " " + ex.Source);
            }
        }

        protected void Clear()
        {
            txtstage.Text = "";
            txtxstagemarathi.Text = "";
            txtsshortname.Text = "";
        }
    }
}