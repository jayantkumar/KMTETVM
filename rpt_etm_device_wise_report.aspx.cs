﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class rpt_etm_device_wise_report : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    double a, b, z;
    string strETMno;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            // txttodate.Text = Now.Date().ToString("dd-MMM-yyyy");
            txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");

        }
    }
    protected void Search_Click(object sender, EventArgs e)
    {
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ETM_device_wise_report";
                cmd.Parameters.Add(new SqlParameter("@fromdate", txtfromdate.Text));
                cmd.Parameters.Add(new SqlParameter("@todate", txttodate.Text));
                //'  cmd.Parameters.Add(New SqlParameter("@bustype", DropDownList1.SelectedItem))
                adp.SelectCommand = cmd;
                adp.Fill(ds);
                grdWayBill.DataSource = ds;
                grdWayBill.DataBind();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblfromdate.Text = "From Date :" + txtfromdate.Text;
                    lbltodate.Text = "To Date :" + txttodate.Text;
                }
                else
                {
                    lblfromdate.Text = "";
                    lbltodate.Text = "";
                }

            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("Denominationwise_tax_report.xls", grdWayBill);
    }
    protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[0].Text == strETMno)
            {
                e.Row.Cells[0].Text = " ";


            }
            else
            {
                strETMno = e.Row.Cells[0].Text;
                e.Row.Cells[0].Font.Bold = true;
            }
            //a +=double.Parse(e,r)

        }
        //if (e.Row.RowType == DataControlRowType.Footer)
        //{
        //    //a +=double.Parse(e,r)

        //    e.Row.Cells[3].Text = a.ToString();
        //    e.Row.Cells[4].Text = b.ToString();
        //    e.Row.Cells[6].Text = z.ToString();

        //}
    }
    protected void grdWayBill_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
