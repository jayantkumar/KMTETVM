﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class CreateBillupdate : System.Web.UI.Page
{
    string connect = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
    SqlConnection con;
    SqlCommand cmd;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bind();
            Btn_update.Visible = false;
            Txt_test.Visible = false;
        }
    }


    protected void Btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("Waybillupdate", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ModifiedDate", Txt_date.Text);
            cmd.Parameters.AddWithValue("@DriverName", Ddl_Driver.Text);
            cmd.Parameters.AddWithValue("@DivCode", DDl_Division.Text);
            cmd.Parameters.AddWithValue("@Schedule", Txt_schedule.Text);
            cmd.Parameters.AddWithValue("@ConductorName", Txt_ConductorName.Text);
            cmd.Parameters.AddWithValue("@VehicleCode", DDL_Vehicle.Text);
            cmd.Parameters.AddWithValue("@DepoCode", DDL_Depo.Text);
            cmd.Parameters.AddWithValue("@Etm_Code", Txt_Etmnumber.Text);
            cmd.Parameters.AddWithValue("@Cashier_Amount", Txt_Conductorcash.Text);
            cmd.Parameters.AddWithValue("@WayBillNo", Txt_Waybillnumber.Text);

            cmd.ExecuteNonQuery();
            Lbl_msg.Text = "Data updated successfully.";
            reset();
            bind();

        }
        catch (Exception ex)
        {
            con.Close();
        }
    }

    public void reset()
    {
        Txt_date.Text = "";
        Txt_Etmnumber.Text = "";
        Txt_schedule.Text = "";
        Txt_ConductorName.Text = "";
        Txt_Conductorcash.Text = "";
        Txt_Waybillnumber.Text = "";
        DDL_Depo.Text = "";
        DDl_Division.Text = "";
        DDL_Vehicle.Text = "";
        Ddl_Driver.Text = "";
    }
    public void bind()
    {
        try
        {
            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("select * from WayBill_Details where DATEADD(minute,10,sysdate) >= GETDATE()", con);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            gridwaybill.DataSource = ds;
            gridwaybill.DataBind();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }

    protected void gridwaybill_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "show")
        {
            string index = Convert.ToInt32(e.CommandArgument).ToString();
            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("select * from WayBill_Details where WayBillNo=" + index, con);
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DateTime time1 = Convert.ToDateTime(dt.Rows[0]["sysdate"]);

                DateTime time2 = DateTime.Now;

                int t = (time2 - time1).Minutes;

                if (t <= 10)
                {
                    Txt_date.Text = dt.Rows[0]["CreatedDate"].ToString();
                    Txt_Waybillnumber.Text = dt.Rows[0]["WayBillNo"].ToString();
                    Txt_Etmnumber.Text = dt.Rows[0]["Etm_Code"].ToString();

                    Ddl_Driver.Items.Add(dt.Rows[0]["DriverName"].ToString());
                    DDl_Division.Items.Add(dt.Rows[0]["divcode"].ToString());

                    Txt_ConductorName.Text = dt.Rows[0]["ConductorName"].ToString();
                    DDL_Vehicle.Items.Add(dt.Rows[0]["vehiclecode"].ToString());
                    DDL_Depo.Items.Add(dt.Rows[0]["depocode"].ToString());

                    Txt_schedule.Text = dt.Rows[0]["schedule"].ToString();
                    Txt_Conductorcash.Text = dt.Rows[0]["Cashier_Amount"].ToString();
                    Btn_update.Visible = true;
                    Lbl_msg.Text = "";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('You must modified data upto 10 minutes from created time.');", true);
                }
            }
        }
        else
        {
            Btn_update.Visible = false;
        }
    }
    protected void Btn_search_Click(object sender, EventArgs e)
    {
        try
        {
            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("select * from WayBill_Details where WayBillNo=" + Txt_search.Text + "", con);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                Lbl_msg.Text = "";
                DataTable dt = new DataTable();
                dt.Load(dr);
                gridwaybill.DataSource = dt;
                gridwaybill.DataBind();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Way bill number not available in the Records.');", true);
                gridwaybill.DataSource = null;
                gridwaybill.DataBind();
                Btn_update.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Lbl_msg.Text = "Enter the Way bill number..";
        }
    }

    protected void btnwaybill_Click(object sender, EventArgs e)
    {
        Response.Redirect("CreateWaybill.aspx");
    }
}