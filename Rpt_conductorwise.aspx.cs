﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using DataManager;
using System.IO;
using TimerGetData;
using System.Reflection;
using System.Configuration;

namespace DataManager
{
    public partial class Rpt_conductorwise : System.Web.UI.Page
    {
        DataManager dm = new DataManager();
        bool inUse = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            }
        }
        string connect = System.Configuration.ConfigurationSettings.AppSettings["Con"].ToString();
        SqlConnection con;
        SqlCommand cmd;
        protected void Search_Click(object sender, EventArgs e)
        {
            try
            {
                con = new SqlConnection(connect);
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (txtconductor.Text != "")
                    cmd.CommandText = "conductor_wise_collection_name";
                else
                    cmd.CommandText = "conductor_wise_collection";
                cmd.Parameters.Add(new SqlParameter("@fromdate", txtfromdate.Text));
                cmd.Parameters.Add(new SqlParameter("@todate", txttodate.Text));
                cmd.Parameters.Add(new SqlParameter("@conductorname", txtconductor.Text));
                adp.SelectCommand = cmd;
                adp.Fill(ds);
                Grid_conductor.DataSource = ds;
                Grid_conductor.DataBind();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Lbl_fromdate.Text = "From Date :" + txtfromdate.Text;
                    Lbl_Todate.Text = "To Date :" + txttodate.Text;
                }
                else
                {
                    Lbl_fromdate.Text = "";
                    Lbl_Todate.Text = "";
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
        }

        [System.Web.Services.WebMethod]
        public static string validate(string conname)
        {
            string msg = string.Empty;
            DataManager dm = new DataManager();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    DataTable dtcon = dm.GetDataTable("select con_full_name from conductor_master  where emp_code='" + conname.ToString() + "'");
                    if (dtcon.Rows.Count == 0)
                    {
                        msg = "true";
                    }
                    else
                    {
                        msg = dtcon.Rows[0][0].ToString();
                    }
                }
            }

            return msg;
        }
        [System.Web.Services.WebMethod]
        public static List<string> GetCompletionconductor(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select emp_code from conductor_master where " +
                    "emp_code like  '%' + @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> customers = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(sdr["emp_code"].ToString());
                        }
                    }
                    conn.Close();
                    return customers;
                }
            }
        }

    }
}