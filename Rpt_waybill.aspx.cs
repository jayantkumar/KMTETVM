﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using System.IO;
public partial class Rpt_waybill : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Txt_Date.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            Txt_Todate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            trheader_download.Visible = false;
            trheader_notdownload.Visible = false;
        }
    }
    //public override void VerifyRenderingInServerForm(Control control)
    //{
    //    //base.VerifyRenderingInServerForm(control);
    //}
    string connect = System.Configuration.ConfigurationSettings.AppSettings["Con"].ToString();
    SqlConnection con;
    SqlCommand cmd;
    protected void Btn_ConvertExcl_Click(object sender, EventArgs e)
    {
        // GridViewExportUtil.Export("Waybill.xls", Grid_report);
        string attachment = "attachment; filename=waybill" + DateTime.Now.ToString("_dd_MM_yyyy") + ".xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        tbl.RenderControl(htw);
        Grid_report.RenderControl(htw);
        Table1.RenderControl(htw);
        Grid_Notdownload.RenderControl(htw);
        // GridView3.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    double totalwaybill;
    protected void Btn_search_Click(object sender, EventArgs e)
    {
        {
            try
            {

                con = new SqlConnection(connect);
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandTimeout = 500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Sp_WayBill_Download_or_not";
                cmd.Parameters.Add(new SqlParameter("@fromdate", Txt_Date.Text));
                cmd.Parameters.Add(new SqlParameter("@todate", Txt_Todate.Text));

                adp.SelectCommand = cmd;
                adp.Fill(ds);
                if (Convert.ToInt32(ds.Tables.Count) > 0)
                {
                    Grid_report.DataSource = ds;
                    Grid_report.DataBind();

                    double waybilltotal = ds.Tables[0].Rows.Count;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Lbl_Date.Text = "From Date :" + Txt_Date.Text;
                        Lbldate.Text = "To Date:" + Txt_Todate.Text;

                    }
                    else
                    {
                        Lbl_Date.Text = "";
                        Grid_report.Columns.Clear();
                    }
                    Lbl_Msg_download.Visible = true;
                    trheader_download.Visible = true;
                    trheader_notdownload.Visible = true;
                    Grid_report.FooterRow.Cells[0].Text = "Total";
                    Grid_report.FooterRow.Cells[1].Text = waybilltotal.ToString();
                    totalwaybill = waybilltotal;

                    Grid_report.FooterRow.Cells[10].Text = waybilltotal.ToString();
                    Lbl_Totaldownload.Text = waybilltotal.ToString();

                    Grid_report.FooterRow.Cells[3].Text = waybilltotal.ToString();

                    lblduties.Text = waybilltotal.ToString();

                    Grid_report.FooterRow.Cells[7].Text = etmcash.ToString();
                    Lbl_Totaletm.Text = etmcash.ToString();
                    Grid_report.FooterRow.Cells[8].Text = manualcash.ToString();
                    Lbl_Totalmanual.Text = manualcash.ToString();

                    Grid_report.FooterRow.Cells[9].Text = totalcash.ToString();
                    Lbl_Grandtotal.Text = totalcash.ToString();
                    Grid_report.FooterRow.Cells[10].Text = "";
                    notdownload();
                }
                else
                {
                    trheader_download.Visible = false;
                    trheader_notdownload.Visible = false;
                    Lbl_Msg_download.Visible = false;
                    //Lbl_Msg_notdownload.Visible = false;
                    Grid_report.DataSource = null;
                    Grid_report.DataBind();
                    Grid_Notdownload.DataSource = null;
                    Grid_Notdownload.DataBind();
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
        }
    }
    protected void ImgPopBtn_Click(object sender, ImageClickEventArgs e)
    {

    }

    Double totalcash = 0;
    Double etmcash = 0;
    Double manualcash = 0;

    protected void Grid_report_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            totalcash += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "total_tic_amt"));
            etmcash += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "etvm_tic_amt"));
            manualcash += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "manual_tic_amt"));

        }


    }

    public void notdownload()
    {

        try
        {
            con = new SqlConnection(connect);
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "notdownloadedwaybill";
            cmd.CommandTimeout = 480;
            cmd.Parameters.Add(new SqlParameter("@fromdate", Txt_Date.Text));
            cmd.Parameters.Add(new SqlParameter("@todate", Txt_Todate.Text));

            adp.SelectCommand = cmd;
            adp.Fill(ds);
            Grid_Notdownload.DataSource = ds;
            Grid_Notdownload.DataBind();

            double waybilltotal1 = ds.Tables[0].Rows.Count;
            if (ds.Tables[0].Rows.Count > 0)
            {
                Lbl_Date.Text = "From Date :" + Txt_Date.Text;
                Date.Text = Txt_Date.Text;
                Lbldate.Text = "To Date:" + Txt_Todate.Text;
            }
            else
            {
                Grid_Notdownload.Columns.Clear();
            }

            Grid_Notdownload.FooterRow.Cells[0].Text = "Total";
            Grid_Notdownload.FooterRow.Cells[1].Text = waybilltotal1.ToString();
            // Grid_report.FooterRow.Cells[10].Text = "Total waybill Downloaded :" + waybilltotal.ToString();
            totalwaybill = totalwaybill + waybilltotal1;
            Lbl_Totalwaybill.Text = totalwaybill.ToString();

            Grid_Notdownload.FooterRow.Cells[4].Text = waybilltotal1.ToString();

            //Grid_Notdownload.FooterRow.Cells[8].Text = etmcash1.ToString();
            //Grid_Notdownload.FooterRow.Cells[9].Text = manualcash1.ToString();

            //Grid_Notdownload.FooterRow.Cells[10].Text =   totalcash1.ToString();

        }
        catch (Exception ex)
        {
            con.Close();
        }
        finally
        {
            con.Close();
            con.Dispose();
            SqlConnection.ClearPool(con);
        }
    }

    Double totalcash1 = 0;
    Double etmcash1 = 0;
    Double manualcash1 = 0;

    protected void Grid_Notdownload_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            totalcash1 += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "total_tic_amt"));
            etmcash1 += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "etvm_tic_amt"));
            manualcash1 += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "manual_tic_amt"));

        }
    }

}