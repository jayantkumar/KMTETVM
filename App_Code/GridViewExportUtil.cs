﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public class GridViewExportUtil
{

    public static void Export(string fileName, GridView gv)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        //  Create a form to contain the grid
        Table table = new Table();
        table.GridLines = gv.GridLines;
        //  add the header row to the table
        if ((((gv.HeaderRow) != null)))
        {
            GridViewExportUtil.PrepareControlForExport(gv.HeaderRow);
            table.Rows.Add(gv.HeaderRow);
        }
        //  add each of the data rows to the table

        foreach (GridViewRow row in gv.Rows)
        {
            GridViewExportUtil.PrepareControlForExport(row);
            table.Rows.Add(row);
        }
        //  add the footer row to the table
        if ((((gv.FooterRow) != null)))
        {
            GridViewExportUtil.PrepareControlForExport(gv.FooterRow);
            table.Rows.Add(gv.FooterRow);
        }
        //  render the table into the htmlwriter
        table.RenderControl(htw);
        //  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString());
        HttpContext.Current.Response.End();
    }



    public static void Export1(DataSet ds, string filename)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";


        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        //  Create a form to contain the grid
        GridView dg = new GridView();
       // dg.DataSource = ds.Tables[0];

        dg.DataSource = ds.Tables[0];
        dg.DataBind();

        //  Create a form to contain the grid
        Table table = new Table();
        table.GridLines = dg.GridLines;
        //  add the header row to the table
        if ((((dg.HeaderRow) != null)))
        {
            GridViewExportUtil.PrepareControlForExport(dg.HeaderRow);
            table.Rows.Add(dg.HeaderRow);
        }
        //  add each of the data rows to the table

        foreach (GridViewRow row in dg.Rows)
        {
            GridViewExportUtil.PrepareControlForExport(row);
            table.Rows.Add(row);
        }
        //  add the footer row to the table
        if ((((dg.FooterRow) != null)))
        {
            GridViewExportUtil.PrepareControlForExport(dg.FooterRow);
            table.Rows.Add(dg.FooterRow);
        }
        //  render the table into the htmlwriter
        table.RenderControl(htw);
        //  render the htmlwriter into the response
        //HttpContext.Current.Response.Write(sw.ToString);
        HttpContext.Current.Response.Write(sw.ToString());
        HttpContext.Current.Response.End();
    }
    // Replace any of the contained controls with literals
    private static void PrepareControlForExport(Control control)
    {
        int i = 0;
        while ((i < control.Controls.Count))
        {
           // Control current = control.Controls(i);
            Control current = control.Controls[i];

            //  Dim current As Control = control.Controls(i)
            if ((current is LinkButton))
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl(((LinkButton)current).Text));
            }
            else if ((current is ImageButton))
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl(((ImageButton)current).AlternateText));
            }
            else if ((current is TextBox))
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl(((TextBox)current).Text));
            }
            else if ((current is HyperLink))
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl(((HyperLink)current).Text));
            }
            else if ((current is DropDownList))
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl(((DropDownList)current).SelectedItem.Text));
            }
            else if ((current is CheckBox))
            {
                control.Controls.Remove(current);
               //control.Controls.AddAt(i, new LiteralControl(((CheckBox)current).Checked));

                control.Controls.AddAt(i, new LiteralControl(((CheckBox)current).Checked.ToString()));
                //TODO: Warning!!!, inline IF is not supported ?
            }
            //if (current.HasControls)
            if(current.HasControls())
            //(current.HasControls)
            {
                GridViewExportUtil.PrepareControlForExport(current);
            }
            i = (i + 1);
        }
    }


    public static void ExportSpecial(string fileName, GridView gv)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        //  Create a form to contain the grid
        Table table = new Table();
        table.GridLines = gv.GridLines;
        //  add the header row to the table
        if (gv.Caption != string.Empty)
        {
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            tc.ColumnSpan = gv.Columns.Count;
            tc.Text = gv.Caption;
            tc.HorizontalAlign = HorizontalAlign.Center;
            tr.ControlStyle.Font.Bold = true;
            tr.Cells.Add(tc);
            table.Rows.Add(tr);
            tr = new TableRow();
            table.Rows.Add(tr);
        }
        if ((((gv.HeaderRow) != null)))
        {
            GridViewExportUtil.PrepareControlForExport(gv.HeaderRow);
            table.Rows.Add(gv.HeaderRow);
        }
        //  add each of the data rows to the table

        foreach (GridViewRow row in gv.Rows)
        {
            GridViewExportUtil.PrepareControlForExport(row);
            table.Rows.Add(row);
        }
        //  add the footer row to the table
        if ((((gv.FooterRow) != null)))
        {
            GridViewExportUtil.PrepareControlForExport(gv.FooterRow);
            table.Rows.Add(gv.FooterRow);
        }
        //  render the table into the htmlwriter
        table.RenderControl(htw);
        //  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString());
        HttpContext.Current.Response.End();
    }
    public static void ExportSpecial1(string fileName, GridView gv)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        //  Create a form to contain the grid
        Table table = new Table();
        table.GridLines = gv.GridLines;
        //  add the header row to the table
        if (gv.Caption != string.Empty)
        {
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            tc.ColumnSpan = gv.Columns.Count;
            tc.Text = gv.Caption;
            tc.HorizontalAlign = HorizontalAlign.Center;
            tr.ControlStyle.Font.Bold = true;
            tr.Cells.Add(tc);
            table.Rows.Add(tr);
            tr = new TableRow();
            table.Rows.Add(tr);
        }
        if ((((gv.HeaderRow) != null)))
        {
            GridViewExportUtil.PrepareControlForExport(gv.HeaderRow);
            table.Rows.Add(gv.HeaderRow);
        }
        //  add each of the data rows to the table

        foreach (GridViewRow row in gv.Rows)
        {
            GridViewExportUtil.PrepareControlForExport(row);
            table.Rows.Add(row);
        }
        //  add the footer row to the table
        if ((((gv.FooterRow) != null)))
        {
            GridViewExportUtil.PrepareControlForExport(gv.FooterRow);
            table.Rows.Add(gv.FooterRow);
        }
        //  render the table into the htmlwriter
        table.RenderControl(htw);
        //  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString());
        //HttpContext.Current.Response.End()
        HttpContext.Current.Response.BufferOutput = true;
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.Close();

    }

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
