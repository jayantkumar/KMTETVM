﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Runtime.InteropServices;
//using Scripting;
using System.IO;
using System.Timers;
/// <summary>
/// Summary description for DataManager
/// </summary>
namespace DataManager
{
    public class DataManager
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        SqlDataAdapter da;
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        String _dllLocation = "scrrun.dll";
        public DataManager()
        {


        }
        public DataTable GetDataTable(string Query)
        {
            dt = new DataTable();
            con.Open();
            cmd = new SqlCommand(Query, con);
            da = new SqlDataAdapter(cmd);
            da.SelectCommand.CommandTimeout = 0;
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                da.Dispose();
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return dt;
        }
        public void FillDDl(string Query, DropDownList source, string valuefield, string textfield, string firstItem)
        {
            con.Open();
            cmd = new SqlCommand(Query, con);
            da = new SqlDataAdapter(cmd);
            da.SelectCommand.CommandTimeout = 0;
            try
            {
                dt.Clear();
                da.Fill(dt);
                source.DataValueField = valuefield;
                source.DataTextField = textfield;
                source.DataSource = dt;
                source.DataBind();
                source.Items.Insert(0, new ListItem(firstItem, "0"));
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                da.Dispose();
                con.Close();
            }
            finally
            {
                con.Close();
            }
        }

        public void ExeCuteGridBind(string qrySel, GridView Source)
        {
            try
            {
                con.Open();
                da = new SqlDataAdapter(qrySel, con);
                da.Fill(ds);
                Source.DataSource = ds;
                Source.DataBind();
                con.Close();
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured :" + ex.Message.ToString() + " " + ex.Source.ToString());
                con.Close();
            }
        }

        public void ExecuteGrid(string procedure, GridView grid)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procedure;
                adp.SelectCommand = cmd;
                adp.Fill(ds);
                grid.DataSource = ds;
                grid.DataBind();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }

        public void ExecuteNonQuery(string exeQuery)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand(exeQuery, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }

        }
        public SqlDataReader GetDataReader(string exeQuery)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand(exeQuery, con);
                dr = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                con.Close();
            }
            return dr;
        }
        public void ExecuteProcedure(string[] obj, int param, string procName)
        {
            string cmdPara = "";
            cmd = new SqlCommand();
            try
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procName;
                cmd.Connection = con;
                for (int i = 0; i < param; i++)
                {
                    cmd.Parameters.Add("@Para" + i, SqlDbType.NVarChar).Value = obj[i].ToString();
                }
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                library.WriteErrorLog("Error occured in ExecuteProcedure function " + ex.Message + " " + ex.Source);
                con.Close();
            }
            finally
            {
                con.Close();
            }
        }
        public void InsertDriver(string Name)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "Driver_MASTER_ADD_UPDATE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@Drivername", SqlDbType.NVarChar).Value = Name;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@code", SqlDbType.Int).Value = 0;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public void InsertConductor(string[] Obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "CONDuctor_MASTER_ADD_UPDATE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@ConductorName", SqlDbType.NVarChar).Value = Obj[1].ToString();
                cmd.Parameters.Add("@ETmCode", SqlDbType.NVarChar).Value = Obj[9].ToString();//ddlCnType.SelectedItem.Value.ToString();
                cmd.Parameters.Add("@Div", SqlDbType.NVarChar).Value = Obj[5].ToString();
                cmd.Parameters.Add("@Depo", SqlDbType.NVarChar).Value = Obj[7].ToString();
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@condcode", SqlDbType.Int).Value = 0;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public void InsertDiv(string Div)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "DIVISION_MASTER_ADD_UPDATE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@DivName", SqlDbType.NVarChar).Value = Div;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@DivCode", SqlDbType.Int).Value = 0;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public void InsertDepo(string[] Obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "DEPO_MASTER_ADD_UPDATE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@DepoName", SqlDbType.NVarChar).Value = Obj[6].ToString();
                cmd.Parameters.Add("@DivCode", SqlDbType.NVarChar).Value = Obj[5].ToString();
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@DEpoCode", SqlDbType.Int).Value = 0;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public void InsertVehicle(string code)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "VEHICLE_MASTER_ADD_UPDATE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@VEHCODE", SqlDbType.NVarChar).Value = code.ToString();
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@code", SqlDbType.Int).Value = 0;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public void InsertConcession(string[] Obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "Concession_Master_Add_Update";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@ConName", SqlDbType.NVarChar).Value = Obj[1].ToString();
                cmd.Parameters.Add("@amttype", SqlDbType.NVarChar).Value = Obj[2].ToString();
                cmd.Parameters.Add("@amount", SqlDbType.NVarChar).Value = Obj[3].ToString();
                cmd.Parameters.Add("@minamt", SqlDbType.NVarChar).Value = Obj[4].ToString();
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@concode", SqlDbType.Int).Value = 0;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public void InsertExpense(string[] Obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "EXPENSE_MASTER_ADD_UPDATE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@EXPENSETYPE", SqlDbType.NVarChar).Value = Obj[1].ToString();
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@code", SqlDbType.Int).Value = Obj[0].ToString();
                cmd.Parameters.Add("@Receipt", SqlDbType.NVarChar).Value = Obj[2].ToString();
                cmd.Parameters.Add("@Docket", SqlDbType.NVarChar).Value = Obj[3].ToString();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public void InsertLuggage(string Lug, string time)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "LUGGAGE_MASTER_ADD_UPDATE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@Fare", SqlDbType.Int).Value = Lug;
                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@Time", SqlDbType.NVarChar).Value = time;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }

        public void closeconnection()
        {
            try
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
            catch (Exception ex)
            {

            }
        }

        public static System.Timers.Timer aTimer;
        public static System.Timers.Timer bTimer;
        public void Start()
        {
            bTimer = new System.Timers.Timer(10000);
            bTimer.Elapsed += new ElapsedEventHandler(GetConcession);

            bTimer.AutoReset = true;
            bTimer.Enabled = true;

        }
        public void Main()
        {
            aTimer = new System.Timers.Timer(10000);
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }
        private void GetConcession(object source, ElapsedEventArgs e)
        {
            char[] delimiterChars = { ',' };
            string filePt = @"D:\Pratinidhi\DISCOUNT.txt";
            if (System.IO.File.Exists(filePt))
            {
                string[] lines = System.IO.File.ReadAllLines(@"D:\Pratinidhi\DISCOUNT.txt");
                foreach (string line in lines)
                {
                    string[] words = line.Split(delimiterChars);
                    int isConS = Check_WayBill(words[0].ToString(), 7);

                    if (isConS != 1)
                        InsertConcession(words);
                }
            }
            string fileExp = @"D:\Pratinidhi\EXPEN.txt";
            if (System.IO.File.Exists(fileExp))
            {
                string[] linesexp = System.IO.File.ReadAllLines(@"D:\Pratinidhi\EXPEN.txt");
                foreach (string line in linesexp)
                {
                    string[] words = line.Split(delimiterChars);
                    int isExp = Check_WayBill(words[0].ToString(), 8);

                    if (isExp != 1)
                        InsertExpense(words);
                }
            }
            string fileLug = @"D:\Pratinidhi\LUGGAGE.txt";
            if (System.IO.File.Exists(fileLug))
            {
                System.IO.FileInfo file1 = new System.IO.FileInfo(fileLug);
                string time = file1.LastWriteTime.ToString();
                int isLugMod = Check_WayBill(time, 9);
                char[] Separator = { '.' };
                string[] lineLug = System.IO.File.ReadAllLines(fileLug);
                foreach (string line in lineLug)
                {
                    string[] LugVal = line.Split(Separator);
                    if (isLugMod != 1)
                        InsertLuggage(LugVal[0].ToString(), time);
                }
            }
        }

        public void Insert_StopMaster(String[] Obj)
        {
            System.IO.FileInfo file1 = new System.IO.FileInfo(@"D:\Pratinidhi\Routes.txt");
            string time = file1.LastWriteTime.ToString();
            int isRouteStage = Check_WayBill(time, 11);
            try
            {
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "Insert_StopStages";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.Add("@StopID", SqlDbType.NVarChar).Value = Obj[0].ToString();
                cmd.Parameters.Add("@SCode", SqlDbType.NVarChar).Value = Obj[1].ToString();
                cmd.Parameters.Add("@StopName", SqlDbType.NVarChar).Value = Obj[2].ToString();
                cmd.Parameters.Add("@Fare", SqlDbType.NVarChar).Value = Obj[3].ToString();
                cmd.Parameters.Add("@Distance", SqlDbType.NVarChar).Value = Obj[4].ToString();
                cmd.Parameters.Add("@RootCode", SqlDbType.NVarChar).Value = Obj[5].ToString();
                cmd.Parameters.Add("@Time", SqlDbType.NVarChar).Value = time.ToString();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public int Check_WayBill(string id, int Action)
        {
            int Is_exist = 0;
            try
            {
                SqlCommand cmd = new SqlCommand();
                if (Action == 1)
                    cmd = new SqlCommand("select 1 from WayBill_Details where is_delete=0 and  WayBillNo=" + id, con);
                if (Action == 2)
                    cmd = new SqlCommand("select 1 from Conductor_Master where is_delete=0 and  ConductorName='" + id + "'", con);
                if (Action == 3)
                    cmd = new SqlCommand("select 1 from Driver_Master where is_delete=0 and  DriverName='" + id + "'", con);
                if (Action == 4)
                    cmd = new SqlCommand("select 1 from Division_Master where is_delete=0 and  DivCode=" + id, con);
                if (Action == 5)
                    cmd = new SqlCommand("select 1 from Depo_Master where is_delete=0 and  DepoCode=" + id, con);
                if (Action == 6)
                    cmd = new SqlCommand("select 1 from vehicle_master where vehicleNo='" + id + "'", con);
                if (Action == 7)
                    cmd = new SqlCommand("select 1 from Concession_entry where conscode=" + id, con);
                if (Action == 8)
                    cmd = new SqlCommand("select 1 from Expense_Master where code=" + id, con);
                if (Action == 9)
                    cmd = new SqlCommand("select 1 from luggage_entry where Time='" + id + "'", con);
                if (Action == 10)
                    cmd = new SqlCommand("select 1 from Root_master where RootNo=" + id, con);
                if (Action == 11)
                    cmd = new SqlCommand("select 1 from RootStage_Master where Modified='" + id + "'", con);
                con.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    Is_exist = 1;
                }
                else
                {
                    Is_exist = 0;
                }
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
            return Is_exist;
        }

        public void GetRoute_Master_Collection()
        {
            string rootcode = "";
            int stops = 0;

            char[] delimiterChars = { ',' };
            string filePt = @"D:\Pratinidhi\Routes.txt";
            if (System.IO.File.Exists(filePt))
            {
                string[] lines = System.IO.File.ReadAllLines(@"D:\Pratinidhi\Routes.txt");
                foreach (string line in lines)
                {
                    int Count = 0;
                    if (line.StartsWith("$"))
                    {
                        char[] Del = { 'R', '$' };
                        string[] wordRt = line.Split(Del);
                        rootcode = wordRt[2].ToString();
                    }
                    else
                    {
                        string[] words = line.Split(delimiterChars);
                        if (words.Length == 9)
                        {
                            int number;
                            bool num = int.TryParse(words[3].ToString(), out number);
                            if (num == true)
                            {
                                //Fill Fare & Distance with Length 9
                                String[] List = new String[6];
                                int length = words.Length;
                                // int loop = stops - 1;
                                if (length < 5)
                                {
                                    string[] newArr = words;
                                    for (int k = 1; k <= length - 5; k++)
                                    {
                                        List[0] = words[0].ToString();
                                        List[1] = words[1].ToString();
                                        List[2] = words[2].ToString();
                                        List[3] = words[length + 1].ToString();
                                        List[4] = "0";
                                        List[5] = rootcode;
                                    }
                                }
                                else
                                {

                                    List[0] = words[0].ToString();
                                    List[1] = words[1].ToString();
                                    List[2] = words[2].ToString();
                                    List[3] = words[4].ToString();
                                    if (length == 5)
                                    {
                                        List[4] = words[3].ToString();
                                    }
                                    else
                                    {
                                        List[4] = "0";
                                    }
                                    List[5] = rootcode;
                                    Insert_StopMaster(List);
                                }
                            }
                            else
                            {
                                int isRoute = Check_WayBill(rootcode, 10);

                                if (isRoute == 0)
                                {
                                    stops = Convert.ToInt32(words[6].ToString());
                                    String[] par = new String[8];
                                    par[0] = rootcode;
                                    par[1] = words[8].ToString();
                                    par[2] = words[1].ToString();
                                    par[3] = words[3].ToString();
                                    par[4] = words[5].ToString();
                                    par[5] = stops.ToString();
                                    par[6] = "0";
                                    par[7] = words[7].ToString();
                                    ExecuteProcedure(par, 8, "ROUTE_MASTER_ADD_UPDATE_DYNAMIC");
                                }
                            }
                        }
                        else
                        {
                            //Fill Fare & Distance without length 9  
                            System.IO.FileInfo file1 = new System.IO.FileInfo(@"D:\Pratinidhi\Routes.txt");
                            string time = file1.LastWriteTime.ToString();
                            int isRouteStage = Check_WayBill(time, 11);

                            if (Count == 0 && isRouteStage == 0)
                            {
                                try
                                {
                                    con.Open();
                                    cmd = new SqlCommand("truncate table rootstage_master", con);
                                    //cmd.ExecuteNonQuery();
                                    con.Close();
                                }
                                catch (Exception ex)
                                {
                                    con.Close();
                                }
                            }
                            Count++;
                            if (1 == 1)
                            {
                                int number;
                                bool num = int.TryParse(words[3].ToString(), out number);
                                if (num == true)
                                {
                                    String[] List = new String[6];
                                    int length = words.Length;
                                    // int loop = stops - 1;
                                    if (length > 5)
                                    {
                                        string[] newArr = words;
                                        for (int k = 0; k < length - 5; k++)
                                        {
                                            List[0] = words[0].ToString();
                                            List[1] = words[1].ToString();
                                            List[2] = words[2].ToString();
                                            List[3] = words[5 + k].ToString();
                                            List[4] = "0";
                                            List[5] = rootcode;
                                            Insert_StopMaster(List);
                                        }
                                    }
                                    else
                                    {

                                        List[0] = words[0].ToString();
                                        List[1] = words[1].ToString();
                                        List[2] = words[2].ToString();
                                        List[3] = words[4].ToString();
                                        if (length == 5)
                                        {
                                            List[4] = words[3].ToString();
                                        }
                                        else
                                        {
                                            List[4] = "0";
                                        }
                                        List[5] = rootcode;
                                        Insert_StopMaster(List);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static class library
    {
        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logfile1.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":  " + ex.Source.ToString() + ";  " + ex.Message.ToString());
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteErrorLog(string message)
        {
            StreamWriter sw = null;
            string FolderPath = "Log Folder\\";

            bool exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(FolderPath));

            if (!exists)
                System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(FolderPath));
            try
            {
                 
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + FolderPath + "\\logfile_" + DateTime.Now.ToString("ddMMyyyy") + ".txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":  " + message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
    }
}