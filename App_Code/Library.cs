﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Library
{
    public static void WriteErrorLog(Exception ex)
    {
        StreamWriter sw = null;
        try
        {
            sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logfile_" + DateTime.Now.ToString("ddMMyyyy") + ".txt", true);
            sw.WriteLine(DateTime.Now.ToString() + ":  " + ex.Source.ToString() + ";  " + ex.Message.ToString());
            sw.Flush();
            sw.Close();
        }
        catch
        {
        }
    }

    public static void WriteErrorLog(string message)
    {
        StreamWriter sw = null;
        try
        {
            sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logfile_" + DateTime.Now.ToString("ddMMyyyy") + ".txt", true);
            sw.WriteLine(DateTime.Now.ToString() + ":  " + message);
            sw.Flush();
            sw.Close();
        }
        catch
        {
        }
    }
}

