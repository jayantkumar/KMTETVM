﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConductorDetails
/// </summary>
public class ConductorDetails
{
    public string ConductorName { get; set; }
    public string EmpCode { get; set; }
}