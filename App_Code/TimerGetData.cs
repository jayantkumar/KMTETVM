﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Timers;
using DataManager;
using System.IO;
/// <summary>
/// Summary description for Class1
/// </summary>

    namespace TimerGetData
    {
        public class TimerGetData
        {
            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
            SqlCommand cmd;
            SqlDataReader dr;
            SqlDataAdapter da;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();


            public TimerGetData()
            {
                //
                // TODO: Add constructor logic here
                //


            }
            public static System.Timers.Timer aTimer;
            public void Main(string[] args)
            {
                aTimer = new System.Timers.Timer(10000);
                aTimer.Elapsed += new ElapsedEventHandler(RunThis);
                aTimer.AutoReset = true;
                aTimer.Enabled = true;

            }
            private void RunThis(object source, ElapsedEventArgs e)
            {
                
                char[] delimiterChars = { ',' };
                //string text = System.IO.File.ReadAllText(@"D:\Pratinidhi\WAYBILL.txt");
                //Response.Write(text);
                string[] lines = System.IO.File.ReadAllLines(@"D:\Pratinidhi\WAYBILL.txt");
                //string filepath = System.Web.HttpContext.Current.Server.MapPath("./WAYBILL.txt");
                string filePt = @"D:\Pratinidhi\WAYBILL.txt";
                if (File.Exists(filePt))
                {
                    //Response.Write("Ahere Bho");
                    foreach (string line in lines)
                    {
                        //Response.Write(@"<br />" + line);
                        string[] words = line.Split(delimiterChars);
                        int isWay = Check_WayBill(words[0].ToString(), 1);
                        int isConductor = Check_WayBill(words[1].ToString(), 2);
                        int isDriver = Check_WayBill(words[2].ToString(), 3);
                        int isDiv = Check_WayBill(words[5].ToString(), 4);
                        int isDepo = Check_WayBill(words[7].ToString(), 5);
                        int isVehicle = Check_WayBill(words[3].ToString(), 6);
                        if (isConductor != 1)
                            //dm.InsertDriver(words[2].ToString());
                        if (isDriver != 1)
                           // dm.InsertConductor(words);
                        if (isDiv != 1)
                           // dm.InsertDiv(words[4].ToString());
                        if (isDepo != 1)
                           // dm.InsertDepo(words);
                        if (isVehicle != 1)
                           // dm.InsertVehicle(words[3].ToString());
                        if (isWay == 1)
                        {
                            //ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", "<script type='text/javascript'>alert('WayBill Is Exists AllReady');</script>");
                        }
                        else
                        {
                            String[] par = new String[11];
                            par[0] = words[0].ToString();
                            par[1] = words[10].ToString();
                            par[2] = words[1].ToString();
                            par[3] = words[2].ToString();
                            par[4] = words[3].ToString();
                            par[5] = words[5].ToString();
                            par[6] = words[7].ToString();
                            par[7] = words[8].ToString();
                            par[8] = words[9].ToString();
                            par[9] = "0";
                            par[10] = "0";
                           // dm.ExecuteProcedure(par, 11, "WAYBILL_ADD_UPDATE_DYNAMIC");


                        }
                    }
                }

            }
            public int Check_WayBill(string id, int Action)
            {
                int Is_exist = 0;
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    if (Action == 1)
                        cmd = new SqlCommand("select 1 from WayBill_Details where is_delete=0 and  WayBillNo=" + id, con);
                    if (Action == 2)
                        cmd = new SqlCommand("select 1 from Conductor_Master where is_delete=0 and  ConductorName='" + id + "'", con);
                    if (Action == 3)
                        cmd = new SqlCommand("select 1 from Driver_Master where is_delete=0 and  DriverName='" + id + "'", con);
                    if (Action == 4)
                        cmd = new SqlCommand("select 1 from Division_Master where is_delete=0 and  DivCode=" + id, con);
                    if (Action == 5)
                        cmd = new SqlCommand("select 1 from Depo_Master where is_delete=0 and  DepoCode=" + id, con);
                    if (Action == 6)
                        cmd = new SqlCommand("select 1 from vehicle_master where vehicleNo='" + id + "'", con);
                    con.Open();
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        Is_exist = 1;
                    }
                    else
                    {
                        Is_exist = 0;
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                }
                return Is_exist;
            }
        }
    }
