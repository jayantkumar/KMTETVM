﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Update_fare.aspx.cs" Inherits="DataManager.Update_fare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:GridView ID="grdCons" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                    BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                    Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                    Font-Strikeout="False" Font-Underline="False" ShowFooter="False" 
                    Width="946px" onselectedindexchanged="grdCons_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Names="Verdana"
                        Font-Size="Smaller" />
                    <RowStyle BackColor="#E3EAEB" CssClass="datagrid_row1" Font-Names="Verdana" Font-Size="Smaller" />
                    <HeaderStyle BackColor="#1C5E55" CssClass="datagrid_heading" Font-Bold="True" Font-Names="Arial"
                        Font-Size="Small" ForeColor="White" />
                    <Columns>
                     
                        <asp:BoundField DataField="Root_Name" HeaderText="Route Name" />
                        <asp:BoundField DataField="Start_loc" HeaderText="Start Location" />
                        <asp:BoundField DataField="End_loc" HeaderText="End Location" />
                        <asp:BoundField DataField="bustype" HeaderText="Bus Type" />
                         <asp:CommandField ShowSelectButton="True" />
                      
                    </Columns>
                    <EmptyDataTemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                            border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                            height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </EmptyDataTemplate>
                </asp:GridView>
                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

