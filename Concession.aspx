﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Concession.aspx.cs" Inherits="DataManager.Concession" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script type="text/javascript" language="javascript">

        function getconcession(obj1) {

            var name = obj1.value;
            $.ajax({
                type: 'POST', url: 'Concession.aspx?empname=' + obj1.value + "&Opt1=getconcession", data: $('#form1').serialize(), success: function (response) {

                    if (response.substring(0, 8) == 'notexist') {
                        alert('Concession Type Exist');
                        obj1.focus();
                    }

                    else if (response.substring(0, 5) == 'exist') {
                        arrobj = response.split("~");
                    }
                }
            });
        }
    </script>
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>Concession Master</h2>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Concession Type: </td>
            <td>
                <asp:TextBox ID="txtCon" runat="server" MaxLength="12" onblur="getconcession(this)" Width="120px"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                    Enabled="True" FilterType="UppercaseLetters,Lowercaseletters,custom" ValidChars="-"
                    TargetControlID="txtCon">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Amount Type </td>
            <td>
                <asp:DropDownList ID="ddlCnType" runat="server" Width="100px">
                    <asp:ListItem Value="0">---Select---</asp:ListItem>
                    <asp:ListItem Value="2">Percentage</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Discount (%)</td>
            <td>
                <asp:TextBox ID="txtDis" runat="server" MaxLength="2" Onblur="return Validation(1,this,'');"
                    Width="120px"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="txtfuel_FilteredTextBoxExtender"
                    runat="server" Enabled="True" FilterMode="ValidChars" FilterType="Numbers"
                    TargetControlID="txtDis">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Minimum Amount </td>
            <td>
                <asp:TextBox ID="txtMinAmt" runat="server" MaxLength="2"
                    Onblur="return Validation(1,this,'');" Width="120px"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1"
                    runat="server" Enabled="True" FilterMode="ValidChars" FilterType="Numbers"
                    TargetControlID="txtMinAmt">
                </asp:FilteredTextBoxExtender>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="submit-btn"
                    OnClick="btnSave_Click" Text="Save" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:GridView ID="grdCons" runat="server" AutoGenerateColumns="False"
                    BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="3" ForeColor="Black" GridLines="Vertical"
                    OnPageIndexChanging="grdCons_PageIndexChanging"
                    OnRowCommand="grdCons_RowCommand" PageSize="3">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                        <asp:BoundField DataField="ConsCode" HeaderText="Code" />
                        <asp:BoundField DataField="ConName" HeaderText="Concession Name" />
                        <asp:BoundField DataField="AmountType" HeaderText="Amount Type" />
                        <asp:BoundField DataField="Amount_per" HeaderText="Amount" />
                        <asp:BoundField DataField="Min_Amt" HeaderText="Min Amount" />
                        <asp:TemplateField HeaderText="Update">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnedit" runat="server"
                                    CommandArgument='<%# Eval("ConsCode") %>' CommandName="EditRow"
                                    ImageUrl="~/images/edit.gif" OnClientClick="return Right(10206,3);"
                                    TabIndex="1" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server"
                                    CommandArgument='<%# Eval("ConsCode") %>' CommandName="DeleteRow"
                                    ImageUrl="~/images/delNew.png" OnClientClick="Delete();" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="Select" runat="server" AutoPostBack="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Add" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:GridView ID="Grid_all_Details" runat="server" AllowSorting="False"
                    AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040"
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" class="datagrid_heading"
                    EmptyDataText="No Records Available !!!" Font-Bold="False" Font-Italic="False"
                    Font-Overline="False" Font-Size="Smaller" Font-Strikeout="False"
                    Font-Underline="False" ShowFooter="False" Width="946px">
                    <AlternatingRowStyle BackColor="White" CssClass="datagrid_row"
                        Font-Names="Verdana" Font-Size="Smaller" />
                    <RowStyle BackColor="#E3EAEB" CssClass="datagrid_row1" Font-Names="Verdana"
                        Font-Size="Smaller" />
                    <HeaderStyle BackColor="#1C5E55" CssClass="datagrid_heading" Font-Bold="True"
                        Font-Names="Arial" Font-Size="Small" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="ConsCode_" HeaderText="Code" />
                        <asp:BoundField DataField="ConName_" HeaderText="Concession Name" />
                        <asp:BoundField DataField="AmountType_" HeaderText="Concession Type" />
                        <asp:BoundField DataField="Amount_per_" HeaderText="Amount percentage" />
                        <asp:BoundField DataField="Min_Amt_" HeaderText="Min Amount" />
                    </Columns>
                    <EmptyDataTemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid; border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid; height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small"
                                ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:HiddenField ID="hdConCode" runat="server" />
                <asp:HiddenField ID="hdDel" runat="server" />
                <asp:HiddenField ID="hdSave" runat="server" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click"
                    Text="Program" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

