﻿using System;
using System.Data.SqlClient;
using System.Data;

public partial class Not_downloaded_waybill : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        }
    }
    protected void Search_Click(object sender, EventArgs e)
    {
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 500;
                cmd.CommandText = "notdownloadedwaybillrpt";
                cmd.Parameters.Add(new SqlParameter("@date1", txtfromdate.Text));
                cmd.Parameters.Add(new SqlParameter("@date2", txttodate.Text));
                adp.SelectCommand = cmd;
                adp.Fill(ds);
                grdWayBill.DataSource = ds;

                grdWayBill.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
    }

    public void bindgrid()
    {
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "notdownloadedwaybillrpt";
            cmd.Parameters.Add(new SqlParameter("@date1", txtfromdate.Text));
            cmd.Parameters.Add(new SqlParameter("@date2", txttodate.Text));
             
            adp.SelectCommand = cmd;
            adp.Fill(ds);
            grdWayBill.DataSource = ds;
            grdWayBill.DataBind();
        }

    }
    protected void btnexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("NotReceivedWaybill.xls", grdWayBill);
         
    }
}