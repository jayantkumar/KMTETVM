﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CreateWaybill.aspx.cs" Inherits="DataManager.CreateWaybill" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">

        function checkwaybill(obj1) {

            // debugger
            var name = obj1.value;
            $.ajax({
                type: 'POST', url: 'CreateWaybill.aspx?empname=' + obj1.value + "&Opt1=getconcession", data: $('#form1').serialize(), success: function (response) {

                    if (response.substring(0, 8) == 'notexist') {
                        alert('waybill Already Exist');
                        obj1.focus();
                    }

                    else if (response.substring(0, 5) == 'exist') {
                        arrobj = response.split("~");
                    }

                }
            });
        }

        function concheck() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "createwaybill.aspx/validate",
                data: "{'conname':'" + document.getElementById("ContentPlaceHolder1_txtconductor").value + "'}",
                dataType: "json",
                success: function (data) {
                    var obj = data.d;
                    if (obj == null) {
                        //alert("Please select Correct Conductor name");
                        document.getElementById("ContentPlaceHolder1_txtconductor").focus();
                        $("#ContentPlaceHolder1_txtconductor").css("color", "red");
                        document.getElementById("ContentPlaceHolder1_lblconname").innerHTML = "";
                        return false;
                    }
                    else {
                        $("#ContentPlaceHolder1_txtconductor").css("color", "black");
                        document.getElementById("ContentPlaceHolder1_lblconname").innerHTML = obj.ConductorName;
                        document.getElementById("ContentPlaceHolder1_txtempcode").value = obj.EmpCode;
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }

        function etmcheck() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "createwaybill.aspx/validateetmdevice",
                data: "{'etmdevice':'" + document.getElementById("ContentPlaceHolder1_txtETM").value + "'}",
                dataType: "json",
                success: function (data) {
                    var obj = data.d;
                    if (obj == 'true') {
                        //alert("Please Enter Correct ETM Number");
                        document.getElementById("ContentPlaceHolder1_txtETM").focus();
                        $("#ContentPlaceHolder1_txtETM").css("color", "red");

                        return false;
                    }
                    else {
                        $("#ContentPlaceHolder1_txtETM").css("color", "black");

                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }


        function OpenConfirmDialog() {
            alert('Create Waybill');
            //if (confirm('No Rule Type Ids found for This Rule.')) {
            //    //True .. do something
            //}
            //else {
            //    //False .. do something
            //}
        }
        function CheckValues() {

            return concheck();
            return etmcheck();

            if (document.getElementById('<%=ddlDriver.ClientID%>').selectedIndex == 0) {
                alert("Please Select Driver from list..!!");
                return false;
            }

            if (document.getElementById('<%=ddlVehcle.ClientID%>').selectedIndex == 0) {
                alert("Please Select Vehicle from list..!!");
                return false;
            }

            if (document.getElementById('<%=txtSchd.ClientID%>').value == "") {
                alert("Please insert schedule..!!");
                return false;
            }
            if (document.getElementById('<%=txtETM.ClientID%>').value == "") {
                alert("Please Enter ETM number..!!");
                return false;
            }
        }

        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                alert('An attempt was made to submit this form more than once; this extra attempt will be ignored.');
                return false;
            }
        }

    </script>
    <script language="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                alert('An attempt was made to submit this form more than once; this extra attempt will be ignored.');
                return false;
            }
        }
    </script>
    <table style="width: 100%">
        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table style="width: 100%">
                            <tr>
                                <td colspan="4" align="center">
                                    <h2>Way Bill Creation</h2>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_WBDate" runat="server" ReadOnly="true" AutoPostBack="True" OnTextChanged="txt_WBDate_TextChanged"
                                        Width="100px"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnissue" TargetControlID="txt_WBDate">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnissue" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>Way Bill Number
                                </td>
                                <td>
                                    <asp:TextBox ID="txtWBNo" ReadOnly="true" runat="server" onblur="checkwaybill(this)"
                                        MaxLength="8" CssClass="textfield ac_input" Width="120px"></asp:TextBox>
                                </td>
                                <td>Conductor:
                                </td>
                                <td>
                                    <%--<asp:DropDownList ID="ddlConductor" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlConductor_SelectedIndexChanged"
                                        Width="120px">
                                        <asp:ListItem>Conductor</asp:ListItem>
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtconductor" MaxLength="11" runat="server" onchange="concheck();"></asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionconductor" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtconductor"
                                        ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                    <asp:Label ID="lblconname" runat="server" ForeColor="Red" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>Driver
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDriver" Enabled="false" runat="server" Width="120px">
                                        <asp:ListItem>Driver</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>Vehicle
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlVehcle" Enabled="false" runat="server" Width="120px">
                                        <asp:ListItem>Vehicle</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Division
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDiv" runat="server" AutoPostBack="true" Enabled="False"
                                        OnSelectedIndexChanged="ddlDiv_SelectedIndexChanged" Width="120px">
                                        <asp:ListItem>Division</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>Depo
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDepo" runat="server" AutoPostBack="true" Enabled="False"
                                        Width="120px" Height="17px">
                                        <asp:ListItem>Depo</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Schedule Duty
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSchd" MaxLength="4" runat="server"></asp:TextBox>
                                      <asp:AutoCompleteExtender ServiceMethod="GetCompletionduty" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtSchd"
                                        ID="AutoCompleteExtender3" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                </td>
                                <td>ETM Code
                                </td>
                                <td>
                                    <asp:TextBox ID="txtETM" MaxLength="4" runat="server" onchange="etmcheck();"></asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtETM"
                                        ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>Conductor Cash
                                </td>
                                <td>
                                    <asp:TextBox ID="txtcondcash" MaxLength="5" Text="0" runat="server"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="txtfuel_FilteredTextBoxExtender" runat="server"
                                        Enabled="True" FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txtcondcash">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:TextBox ID="txtempcode" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                     
                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" OnClientClick="return CheckValues();" Height="29px"
                                        OnClick="btnSave_Click" Text="Save" Width="66px" />
                                    <asp:Button ID="Button2" runat="server" Width="66px" Height="29px" OnClick="Button2_Click"
                                        OnClientClick="return CheckIsRepeat();" Text="Program" />
                                    <asp:Label ID="lblerror" runat="server" ForeColor="Red" Text=""></asp:Label>
                                    &emsp; &emsp;
                                    <asp:Button ID="btnEdit" runat="server" Height="29px" OnClick="btnEdit_Click" Text="update"
                                        Width="90px" />
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&emsp;</td>
                            </tr>
                            <tr>
                                <td>Search by Date
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txtsearch_by_date" runat="server" Width="100px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnfrom" TargetControlID="txtsearch_by_date">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                    <%--   <asp:TextBox ID="txtsearch_by_date" runat="server" ReadOnly="true"  Width="100px"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="ImageButton1" TargetControlID="txtsearch_by_date">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />--%>
                                    &emsp;
                                    <asp:Button ID="btnsearch" runat="server" Height="23px" OnClick="btnsearch_Click" Text="Search"
                                        Width="90px" />
                                </td>
                                <%--  <td>--%>

                                <%-- </td>--%>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&emsp;</td>
                            </tr>
                            <tr>
                                <td>&emsp;</td>
                                <td colspan="3">
                                    <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                                        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                                        Font-Strikeout="False" Font-Underline="False" Width="946px"
                                        OnRowCommand="grdWayBill_RowCommand" OnRowDataBound="grdWayBill_RowDataBound">
                                        <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" Font-Bold="False"
                                            Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="Medium"
                                            Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                                        <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" Font-Names="Verdana"
                                            Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False"
                                            ForeColor="#000099" />
                                        <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                                            Font-Overline="False" Font-Size="Small" Font-Strikeout="False" Font-Underline="False"
                                            ForeColor="Black" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Srn">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="WayBillNo" HeaderText="WayBill No" />
                                            <asp:BoundField DataField="Old_ConductorName" HeaderText="Conductor Name" />
                                            <asp:BoundField DataField="DriverName" HeaderText="Driver Name" />
                                            <asp:BoundField DataField="vehiclecode" HeaderText="Vehicle Code" />
                                            <asp:BoundField DataField="divcode" HeaderText="Division" />
                                            <asp:BoundField DataField="depocode" HeaderText="Depo Name" />
                                            <asp:BoundField DataField="etm_code" HeaderText="ETM No" />
                                            <asp:BoundField DataField="modifieddate" HeaderText="Date" />
                                            <asp:BoundField DataField="Schedule" HeaderText="Schedule" />
                                            <asp:BoundField DataField="stat" HeaderText="Status" />
                                            <asp:BoundField DataField="IsUpdate" Visible="false" />
                                            <asp:BoundField DataField="IsDownloaded" Visible="false" HeaderText="Download Status" />
                                            <asp:BoundField DataField="is_delete" HeaderText="Is Waybill Deleted?" />
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:Button ID="Btn_edit" runat="server" Width="70px" Style="text-align: center" Text="Edit" CommandName="show" CommandArgument='<%#Eval("WayBillId")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid; border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid; height: 100px; background-color: beige; text-align: center">
                                                <br />
                                                <br />
                                                <br />
                                                <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                                            </div>
                                        </EmptyDataTemplate>
                                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:HiddenField ID="hdWayCode" runat="server" />
                                    <asp:HiddenField ID="hdDel" runat="server" />
                                    <asp:HiddenField ID="WayExist" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 23px"></td>
                                <td style="height: 23px"></td>
                                <td style="height: 23px"></td>
                                <td style="height: 23px"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <script>

        $(document).ready(function () {
            callnumeric();
        });


        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        function callnumeric() {
            $(".numeric").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                $(".error").css("display", ret ? "none" : "inline");
                return ret;
            });
            $(".numeric").bind("paste", function (e) {
                return false;
            });
            $(".numeric").bind("drop", function (e) {
                return false;
            });
        };

    </script>
</asp:Content>
