﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="reports.aspx.cs" Inherits="reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrap-body">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-10 lft">
                    <div class="row card ps">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4 style="margin-left: 80px;">ETM Report
                                </h4>
                                <ul>
                                    <ul>
                                        <li><a href="TicketDetail.aspx">Waybill Detail</a></li>
                                        <li><a href="Datewise_all_over.aspx">All Over Cash</a></li>
                                        <li><a href="Not_downloaded_waybill.aspx">Not Received Waybill</a></li>
                                        <li><a href="rpttaxreport.aspx">Denomination wise Tax Report</a></li>
                                        <li><a href="rpt_EPKM.aspx">EPKM Report</a></li>
                                        <li><a href="Rpt_conductorwise.aspx">Conductor wise detail Report</a></li>
                                        <li><a href="rptWayBill_Downloaded_and_not.aspx">Waybill  downloaded and not downloaded Report</a></li>
                                        <li><a href="rpt_CardTypeWise.aspx">ETVM Card Type Wise Report</a></li>
                                    </ul>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h4 style="margin-left: 80px;">Datewise
                                </h4>
                                <ul>
                                    <li><a href="cashier_rpt.aspx">Cashier Report</a></li>
                                    <li><a href="denominationwisereport.aspx">Denomination Report</a></li>
                                    <li><a href="rpt_ETVM_and_Manual_cash_collectio_datewise.aspx">ETVM and Manual cash Report</a></li>
                                    <li><a href="rpt_etm_device_wise_report.aspx">ETVM Device Wise Report</a></li>
                                    <li><a href="ETVMWiseConductorcashCollection.aspx">ETVM Wise Conductor Cash Collection Report</a></li>
                                    <li><a href="RouteWiseConductorCashCollection.aspx">Route Wise Conductor Cash Collection Report</a></li>

                                    <li><a href="rtp_ETVM_ByCardType.aspx">ETVM Card Wise Report</a></li>
                                    
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h4 style="margin-left: 80px;"></h4>
                                <ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
