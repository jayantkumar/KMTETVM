﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Rpt_waybill.aspx.cs" Inherits="Rpt_waybill" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style media="Print" type="text/css">
        .ctrl {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize {
            font-family: Times New Roman;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="3">
                        <a class="ctrl" href="reports.aspx">Back</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <h2>WayBill Downloaded and Not Downloaded Report</h2>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table border="0" width="100%">
                            <tr class="ctrl">
                                <td nowrap width="169">
                                    <font style="font-family: arial; font-size: 12px;">From Date : </font>
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Date" runat="server" Width="80px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnfrom" TargetControlID="Txt_Date">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                    <asp:ScriptManager ID="Scriptmanager1" runat="server">
                                    </asp:ScriptManager>
                                </td>
                                <td class="style2">&nbsp;
                                </td>
                                <td width="53">
                                    <font style="font-family: arial; font-size: 12px;">To Date :</font>
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Todate" runat="server" Width="80px">
                                    </asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                        PopupButtonID="imgPopBtnto" TargetControlID="Txt_Todate">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                </td>
                                <td width="174">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="ctrl">
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="Btn_search" runat="server" Text="Search" OnClick="Btn_search_Click" />
                        <asp:Button ID="Btn_ConvertExcl" runat="server" OnClick="Btn_ConvertExcl_Click" Text="Convert To Excel" />
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Lbl_Date" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Label ID="Lbldate" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                </tr>
                <tr runat="server" id="trheader_download">
                    <td colspan="4">
                        <table id="tbl" width="100%" border="1" cellspacing="0" cellpadding="0" runat="server">
                            <tbody>
                                <tr>
                                    <td>
                                        <lable>Date </lable>
                                    </td>
                                    <td>
                                        <asp:Label ID="Date" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                                    </td>
                                    <td colspan="9"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <lable>Total waybill Created </lable>
                                    </td>
                                    <td>
                                        <asp:Label ID="Lbl_Totalwaybill" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                                    </td>
                                    <td>
                                        <lable>Total waybill downloaded </lable>
                                    </td>
                                    <td>
                                        <asp:Label ID="Lbl_Totaldownload" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                                    </td>
                                    <td>
                                        <lable>Total cash From ETM </lable>
                                    </td>
                                    <td>
                                        <asp:Label ID="Lbl_Totaletm" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                                    </td>
                                    <td>
                                        <lable>Total manual Cash </lable>
                                    </td>
                                    <td>
                                        <asp:Label ID="Lbl_Totalmanual" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                                    </td>
                                    <td>
                                        <lable>Total cash </lable>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label ID="Lbl_Grandtotal" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <lable>Total duties issued </lable>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblduties" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                                    </td>
                                    <td colspan="9">
                                        <lable> </lable>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11" align="center">
                                        <asp:Label ID="Lbl_Msg_download" runat="server" Font-Bold="True" Text="Downloaded way bill" Font-Size="12pt"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style3" colspan="6">
                        <asp:GridView ID="Grid_report" runat="server" AllowPaging="true" PageSize="50" AutoGenerateColumns="false" BackColor="WhiteSmoke"
                            BorderColor="#404040" BorderStyle="Solid" EmptyDataText="No Records Available !!!" ShowFooter="True" OnRowDataBound="Grid_report_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderStyle-BackColor="Bisque" HeaderText="Sr.No">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex  +1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="WayBill No" DataField="waybill_no" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Conductor Name" DataField="con_full_name" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Conductor No" DataField="emp_code" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Duty No" DataField="Schedule" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Machine Number" DataField="ETM_code" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Way Bill Date" DataField="ModifiedDate" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Etm Cash" DataField="etvm_tic_amt" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Manual Cash" DataField="manual_tic_amt" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Total Cash" DataField="total_tic_amt" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Downloaded Time" DataField="sysdate" />
                            </Columns>
                            <EmptyDataTemplate>
                                <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid; border-left: peachpuff thin solid; width: 500px; border-bottom: peachpuff thin solid; height: 80px; background-color: beige; text-align: center">
                                    <br />
                                    <br />
                                    <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                                </div>
                            </EmptyDataTemplate>
                            <FooterStyle BackColor="Bisque" ForeColor="Black" Font-Bold="True" Font-Size="12pt" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="style12">&nbsp;
                    </td>
                    <td class="style13">&nbsp;
                    </td>
                    <td class="style10">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4" id="trheader_notdownload" runat="server">
                        <table id="Table1" width="100%" border="1" cellspacing="0" cellpadding="0" runat="server">
                            <tbody>
                                <tr style="display: none;">
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                    <td>&emsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="11" style="font-weight: bold" align="center">Not Downloaded way bill
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style3" colspan="6">
                        <asp:GridView ID="Grid_Notdownload" runat="server" AutoGenerateColumns="false" OnRowDataBound="Grid_Notdownload_RowDataBound"
                            ShowFooter="True">
                            <Columns>

                                <asp:TemplateField HeaderStyle-BackColor="Bisque" HeaderText="Sr.No">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex  +1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="WayBill No" DataField="waybillNo" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Conductor_Name" DataField="con_full_name" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Conductor No" DataField="emp_code" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Duty No" DataField="Schedule" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Machine Number" DataField="Etm_Code" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Way Bill Date" DataField="ModifiedDate" />
                                <asp:BoundField HeaderStyle-BackColor="Bisque" HeaderText="Status" DataField="Is_programmed" />
                                <%--  <asp:BoundField HeaderText="Etm Cash" DataField="etvm_tic_amt" />
                                <asp:BoundField HeaderText="Manual Cash" DataField="manual_tic_amt" />
                                <asp:BoundField HeaderText="Total Cash" DataField="total_tic_amt" />--%>
                                <%--<asp:BoundField HeaderText="Downloaded Time" DataField="sysdate" />--%>
                            </Columns>
                            <FooterStyle BackColor="Bisque" ForeColor="Black" Font-Bold="True" Font-Size="12pt" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="style12">&nbsp;
                    </td>
                    <td class="style13">&nbsp;
                    </td>
                    <td class="style10">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style12">&nbsp;
                    </td>
                    <td class="style13">&nbsp;
                    </td>
                    <td class="style10">&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

