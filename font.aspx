﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="font.aspx.cs" Inherits="font" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  
    <script type="text/javascript" src="https://www.google.com/jsapi">
    </script>
    <script type="text/javascript">
        // Load the Google Transliterate API
        google.load("elements", "1", {
            packages: "transliteration"
        });

        function onLoad() {
            var options = {
                sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
                destinationLanguage:
                [google.elements.transliteration.LanguageCode.MARATHI],
                shortcutKey: 'ctrl+e',
                transliterationEnabled: true
            };

            // Create an instance on TransliterationControl with the required
            // options.
            var control =
            new google.elements.transliteration.TransliterationControl(options);

            // Enable transliteration in the textbox with id
            // 'transliterateTextarea'.
            control.makeTransliteratable(['transliterateTextarea']);


        }
        google.setOnLoadCallback(onLoad);
    </script>
   
    <body>
        <span>English to Marathi</span><br>
        <form  >
            <div>
                <asp:TextBox ID="transliterateTextarea" runat="server" TextMode="MultiLine" Rows="5"
                    Columns="50" />
            </div>
        </form>
    </body>



</asp:Content>

