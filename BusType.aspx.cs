﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using DataManager;
namespace DataManager
{

    public partial class BusType : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQry = "select BusCode,BusType from Bus_Details where is_delete=0 ";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

                
                dm.ExeCuteGridBind(selQry, grdBsu);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("select BusCode,BusType from Bus_Details where BusType='" + txtBusTp.Text.Trim() + "' and  is_delete=0 ", con);
            con.Open();
            da.Fill(ds);
            if (txtBusTp.Text == "")
            {
                Response.Write("<script>alert ('Enter Bus Type') </script>");

            }

            else if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert ('Already Exist') </script>");

            }

            else
            {

                {
                    if (txtBusTp.Text == "")
                    {
                        hdSave.Value = "1";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "foo", " Fill();", true);
                    }
                    if (hdSave.Value.ToString() != "1")
                    {
                        try
                        {
                            dr = dm.GetDataReader("Select BusType from Bus_Details where BusType='" + txtBusTp.Text + "'");
                            //if (dr.HasRows)
                            //{
                            //    Response.Write("<script> alert('This Bus type already exist.'); </script>");
                            //    return;
                            //}
                            dr.Close();
                            con.Close();
                            con.Open();
                            cmd = new SqlCommand();
                            cmd.CommandText = "BusType_Master_Add_Update";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.Add("@BusType", SqlDbType.NVarChar).Value = txtBusTp.Text;
                            if (btnSave.Text == "Save")
                            {
                                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                                cmd.Parameters.Add("@BusId", SqlDbType.Int).Value = 0;
                                txtBusTp.Text = "";
                            }
                            else if (btnSave.Text == "Update")
                            {
                                cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                                cmd.Parameters.Add("@BusId", SqlDbType.Int).Value = Convert.ToInt32(hdBusID.Value);
                                btnSave.Text = "Save";
                                txtBusTp.Text = "";
                            }
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        catch (Exception ex)
                        {
                            con.Close();
                        }
                    }

                    dm.ExeCuteGridBind(selQry, grdBsu);
                    hdSave.Value = "0";

                    Response.Redirect("BusType.aspx");
                }
            }
        }
        protected void grdBsu_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdBsu.PageIndex = e.NewPageIndex;
            dm.ExeCuteGridBind(selQry, grdBsu);
        }
        protected void grdBsu_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            hdBusID.Value = e.CommandArgument.ToString();
            if (e.CommandName == "EditRow")
            {
                btnSave.Text = "Update";
                dr = dm.GetDataReader("Select BusType from Bus_Details where busid=" + e.CommandArgument);
                while (dr.Read())
                {
                    txtBusTp.Text = dr[0].ToString();
                }
            }
            if (e.CommandName == "DeleteRow")
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "foo", "Delete();", true);
                if (hdDel.Value.ToString() != "1")
                {
                    dm.ExecuteNonQuery("UPDATE Bus_Details SET Is_Delete=1 WHERE BUSCODE=" + e.CommandArgument);
                    dm.ExeCuteGridBind("select BusCode,BusType from Bus_Details where is_delete=0", grdBsu);
                }
                hdDel.Value = "0";
            }
        }
        protected void txtBusTp_TextChanged(object sender, EventArgs e)
        {

            BusTypeCheck(txtBusTp.Text);
        }


        protected void BusTypeCheck(string BusType)
        {
            dr = dm.GetDataReader("Select BusType from Bus_Details where BusType='" + txtBusTp.Text + "'");
            if (dr.Read())
            {
                Response.Write("<script> alert('This Bus type already exist.'); </script>");

                //return;
            }
            dr.Close();
            con.Close();
        }

}
}