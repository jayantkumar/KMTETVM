﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ResetPassword : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);

    protected void Page_Load(object sender, EventArgs e)
    {
        txtusername.Attributes.Add("autocomplete", "off");

        if (Convert.ToString(Session["user_type"]) != "5")
        {
            Response.Redirect("ServiceOperator.aspx");
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        SqlDataAdapter adp = new SqlDataAdapter();
        string msg = string.Empty;
        DataSet ds = new DataSet();

        if (txtusername.Text == "")
        {
            txtusername.Focus();
            Response.Write("<script>alert ('Enter username') </script>");
            return;
        }
        else if (txtpassword.Text == "")
        {
            txtpassword.Focus();
            Response.Write("<script>alert ('Enter password') </script>");
            return;
        }
        if (txtpassword.Text != txtconfirm.Text)
        {
            Response.Write("<script>alert ('Password do not match.') </script>");
            return;
        }

        try
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Sp_CreateUser";
                cmd.Connection = con;
                cmd.Parameters.Add(new SqlParameter("@username", txtusername.Text));
                cmd.Parameters.Add(new SqlParameter("@password", txtpassword.Text));
                cmd.Parameters.Add(new SqlParameter("@Action", 2));
                adp.SelectCommand = cmd;
                adp.Fill(ds);

                if (ds.Tables[0].Rows.Count >= 0)
                {
                    msg = Convert.ToString(ds.Tables[0].Rows[0][0]);

                    if (msg == "1")
                    {
                        Response.Write("<script>alert ('User does not exist') </script>");
                        clear();
                        return;
                    }
                    else if (msg == "2")
                    {
                        Response.Write("<script>alert ('Password change successfuly') </script>");
                        clear();
                        return;
                    }

                }
                else
                {
                    msg = "Error occured";
                }
            }
        }
        catch (Exception ex)
        {

        }

    }

    public void clear()
    {
        txtusername.Text = "";
        txtpassword.Text = "";
        txtconfirm.Text = "";
    }
}