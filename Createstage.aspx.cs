﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Createstage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bind();
            Txt_Fare.Attributes.Add("autocomplete", "off");
            Txt_Marathi.Attributes.Add("autocomplete", "off");
            Txt_English.Attributes.Add("autocomplete", "off");
            //   Txt_Fare.Attributes.Add("onblur", "CheckFareStage(this);");
            // Txt_English.Attributes.Add("onblur", "CheckStageName();");
        }
        Txt_rootname.Text = Convert.ToString(Session["RootNo"]);
    }

    string connect = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
    SqlConnection con;
    SqlCommand cmd;

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
       {

            if (Txt_rootname.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter Root_Name.');", true);
            }
            if (Txt_English.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter Stage_Name in English.');", true);
                // Response.Write("<script>alert('Enter Stage_Name in English.')</script>");
            }
            if (Txt_Marathi.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter Stage_Name in Marathi.');", true);
            }
            if (Txt_Fare.Text == "")
            {
                // Response.Write("<script>alert ('Enter Fare stage.') </script>");
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Enter Fare stage.');", true);
            }
            if (Txt_Fare.Text.Length < 2)
            {
              
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('should have miniumum 2 Digits.');", true);
                //  Lbl_msg.Text = "should have miniumum 2 Digits.";
            }

          /*  for (int j = 0; j <= Gridstage.Rows.Count - 1; j++)
            {

                int fare1 = ((TextBox)Gridstage.Rows[j].FindControl("Txt_Fare")).Text;
                int  fare2 = ((TextBox)Gridstage.Rows[j].Cells[5].FindControl("Txt_Fare")).Text;
                if (fare1 > fare2)
                { 
                
                }
            }*/

            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("select stage_name,fare_change_stage_no from stagemaster where stage_name='"+Txt_English.Text+"' and fare_change_stage_no=" + Txt_Fare.Text + "", con);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();

                //    Lbl_msg.Text = "Root Name already Exist.";
                // Lbl_msg.ForeColor =Color.Red;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Stage Name and Fare Stage are Already Exist.');", true);
            }
            else
            {
                con = new SqlConnection(connect);
                con.Open();
                cmd = new SqlCommand("stage_pro", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@rootname", Txt_rootname.Text);
                cmd.Parameters.AddWithValue("@stage_name", Txt_English.Text);
                cmd.Parameters.AddWithValue("@stage_full_name", Txt_English.Text);
                cmd.Parameters.AddWithValue("@fare_change_stage_no", Txt_Fare.Text);
                cmd.Parameters.AddWithValue("@stg_regional_name", Txt_Marathi.Text);

                cmd.Parameters.AddWithValue("@create_date",DateTime.Now);
                cmd.Parameters.AddWithValue("@sys_date", DateTime.Now);

                cmd.ExecuteNonQuery();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Data added successfully.');", true);
                bind();
            }
        }
     catch (Exception ex)
        {
           
        }
    
    }

    public void bind()
    {
        try
        {
            con = new SqlConnection(connect);
            con.Open();
            cmd = new SqlCommand("select * from stagemaster where rootname ="+Txt_rootname.Text+"  order by stage_id", con);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            Gridstage.DataSource = ds;
            Gridstage.DataBind();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }
    protected void Btn_back_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Createroot.aspx");

    }


  
}