﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" EnableViewState="true" CodeFile="ChangeConductor.aspx.cs" Inherits="DataManager.ChangeConductor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" language="javascript">
        function onCalendarShown() {
            var cal = $find("calendar1");
            cal._switchMode("years", true);
            if (cal._yearsBody) {
                for (var i = 0; i < cal._yearsBody.rows.length; i++) {
                    var row = cal._yearsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }

        function onCalendarHidden() {
            var cal = $find("calendar1");
            if (cal._yearsBody) {
                for (var i = 0; i < cal._yearsBody.rows.length; i++) {
                    var row = cal._yearsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }

        function call(eventElement) {
            var target = eventElement.target;
            switch (target.mode) {
                case "year":
                    var cal = $find("calendar1");
                    cal.set_selectedDate(target.date);
                    cal._blur.post(true);
                    cal.raiseDateSelectionChanged(); break;
            }
        }
        function concheck() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChangeConductor.aspx/validate",
                data: "{'conname':'" + document.getElementById("ContentPlaceHolder1_txtCondctCode").value + "','year':'" + document.getElementById("ContentPlaceHolder1_txtyear").value + "'}",
                dataType: "json",
                success: function (data) {
                    var obj = data.d;
                    
                    if (obj == null) {
                        //alert("Please select Correct Conductor name");
                        document.getElementById("ContentPlaceHolder1_txtCondctCode").focus();
                        $("#ContentPlaceHolder1_txtCondctCode").css("color", "red");
                        document.getElementById("ContentPlaceHolder1_lblConductrName").innerHTML = "";
                        return false;
                    }
                    else {
                        $("#ContentPlaceHolder1_txtCondctCode").css("color", "black");
                        document.getElementById("ContentPlaceHolder1_lblConductrName").innerHTML = obj.ConductorName;
                        document.getElementById("ContentPlaceHolder1_txtempcode").value = obj.EmpCode;
                       
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        function UpdateConductor() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChangeConductor.aspx/ChangeConductrCode",
                data: "{'concode':'" + document.getElementById("ContentPlaceHolder1_txtnewCondctrCode").value + "','EmpCode':'" + document.getElementById("ContentPlaceHolder1_txtempcode").value + "'}",
                dataType: "json",
                success: function (data) {
                    var obj = data.d;
                    if (obj == 'true') {
                       
                        return false;
                    }
                    else {
                        document.getElementById("ContentPlaceHolder1_lblConductrName").innerHTML = "";
                        alert(obj);
                    }
                },
                error: function (result) {
                    alert(obj);
                }
            });
        }
</script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="padding-top:20px; padding-bottom:10px;" align="center">
                <h2><span>Change Conductor</span></h2>
            </div>
            <div class="col-md-12" style="padding-bottom:10px;">
                <div class="col-md-2">
                    Select year
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtyear" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" OnClientHidden="onCalendarHidden"  
 OnClientShown="onCalendarShown" Format="yyyy" BehaviorID="calendar1"  TargetControlID="txtyear">
</asp:CalendarExtender>
                </div>
                <div class="col-md-2">
                    Employee Code :-
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtCondctCode" runat="server" CssClass="form-control" onchange="concheck();" ></asp:TextBox>
                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionconductor" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtCondctCode"
                                        ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                </div>
                <div class="col-md-4">
                    <asp:Label ID="lblConductrName" runat="server"></asp:Label>
                    
                    
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2">
                    Conductor Code
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtempcode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="col-md-2">
                    New Employee Code
                </div>
                
                <div class="col-md-2">
                    <asp:TextBox ID="txtnewCondctrCode" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionconductor" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtnewCondctrCode"
                                        ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                </div>
                <div class="col-md-2">
                    <%--<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-sm" Text="Submit" OnClick="UpdateConductor()" />--%>
                    <input type="button" onclick="UpdateConductor()" runat="server" value="Submit" class="btn btn-sm" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

