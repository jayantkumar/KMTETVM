﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="TicketDetail.aspx.cs"
    Inherits="Report_TicketDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <style media="Print" type="text/css">
        .ctrl
        {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize
        {
            font-family: Times New Roman;
            font-size: 12px;
        }
    </style>
    <title>Waybill Detail Report</title>
</head>
 
    
    <body class="body">
        <form id="Form1" runat="server">
        <a href="reports.aspx" class="ctrl">Back</a>
        <table style="width: 100%">
            <tr>
                <td colspan="4" align="center">
                    <h2>
                        Way Bill Detail Report</h2>
                </td>
            </tr>
            <tr style="height: 40px" class="ctrl">
                <td>
                    Way Bill Number
                </td>
                <td>
                    <asp:TextBox ID="txtWayNo" runat="server">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnView" runat="server" Font-Bold="true" class="submit-btn2" OnClick="btnView_Click"
                        Text="View Report" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblwaybillno" Font-Bold="true" Text="" runat="server"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbldriver" Font-Bold="true" Text="" runat="server"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="lblconductor" Text="" Font-Bold="true" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblbusno" Text="" Font-Bold="true" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="lbletvmno" Text="" Font-Bold="true" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" ShowFooter="true"
                        AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                        Font-Strikeout="False" Font-Underline="False" OnRowDataBound="grdWayBill_RowDataBound"
                        Width="100%">
                        <alternatingrowstyle backcolor="White" cssclass="datagrid_row" font-bold="False"
                            font-italic="False" font-names="Verdana" font-overline="False" font-size="Medium"
                            font-strikeout="False" font-underline="False" forecolor="Black" />
                        <rowstyle cssclass="datagrid_row1" font-bold="False" font-italic="False" font-names="Verdana"
                            font-overline="False" font-size="Medium" font-strikeout="False" font-underline="False"
                            forecolor="#000099" />
                        <headerstyle cssclass="datagrid_heading" font-bold="True" font-italic="False" font-names="Arial"
                            font-overline="False" font-size="Small" font-strikeout="False" font-underline="False"
                            forecolor="Black" />
                        <columns>
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%#Container.DataItemIndex  +1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ticket_no" HeaderText="Ticket no" />
                        <asp:BoundField DataField="startstage" HeaderText="Start Stage" />
                        <asp:BoundField DataField="end_stage" HeaderText="End Stage" />
                        <asp:BoundField DataField="no_of_tic" HeaderText="No Of Passenger" />
                        <asp:BoundField DataField="ticket_amnt" HeaderText="Ticket Amount" />
                        <asp:BoundField DataField="transdate" HeaderText="Ticket DateTime" />
                        <asp:BoundField DataField="ticket_status" HeaderText="Ticket Status" />
                        <asp:BoundField DataField="trip_no" HeaderText="Trip No" />
                        <asp:BoundField DataField="conname11" HeaderText="Concession/Normal" />
                    </columns>
                        <emptydatatemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                            border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                            height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </emptydatatemplate>
                        <footerstyle backcolor="#1C5E55" font-bold="True" forecolor="White" font-size="Medium" />
                    </asp:GridView>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        </form>
    </body>
</html>
