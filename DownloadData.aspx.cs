﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Configuration.Assemblies;
using System.Diagnostics;
using System.Reflection;
using DataManager;
using System.Globalization;

public partial class DownloadData : System.Web.UI.Page
{

    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd = new SqlCommand();
    SqlDataReader dr;
    SqlDataAdapter da;

    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    bool inUse = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string query = "";

            if ((Request.QueryString["receipno"] != null) && (Request.QueryString["type"] == "1"))
            {
                save_ticket_details();
            }
            else if (Request.QueryString["type"] == "3")
            {
                txtWayBill.Text = Convert.ToString(Request.QueryString["receipno"]);
                txtdrv.Text = Convert.ToString(Request.QueryString["driverno"]);
                txtcash.Text = Convert.ToString(Request.QueryString["cash"]);
                txtvehicle.Text = Convert.ToString(Request.QueryString["vehicleo"]);
                txtcond.Text = Convert.ToString(Request.QueryString["conno"]);
                
            }
            else
            {
                Button1.Enabled = false;
                btnDelBill.Enabled = false;
            }
            //else if ((Request.QueryString["receipno"] != null) && (Request.QueryString["type"] == "2"))
            //{
            //    Delete_ticket_details();
            //}
            btnDwnTct.Visible = true;
        }
    }

    public void killprocess()
    {
        Process[] runingProcess = Process.GetProcesses();
        for (int i = 0; i < runingProcess.Length; i++)
        {
            // compare equivalent process by their name
            if (runingProcess[i].ProcessName == "ETM_COM")
            {
                library.WriteErrorLog("Process Name" + runingProcess[i].ProcessName);
                // kill  running process
                runingProcess[i].Kill();
            }
        }
    }

    protected void Button1_Click(object sender, System.EventArgs e)
    {
        try
        {

            if (txtWayBill.Text.Length < 1 || txtWayBill.Text == "$Error-No" || txtWayBill.Text == "01" || txtWayBill.Text == "$DONE")
            {
                Response.Write("<script>alert ('Capture ETM Again') </script>");
            }
            else
            {
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select distinct waybill_no from ticket_detail where waybill_no='" + txtWayBill.Text + "' ", con);
                con.Open();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("<script>alert ('Details Already Downloaded') </script>");
                    // Response.Write("Details Already Downloaded");
                }
                else
                {
                    killprocess();
                    Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                    System.Diagnostics.ProcessStartInfo info1 = new System.Diagnostics.ProcessStartInfo();
                    // info.FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\etm_com.exe";
                    info1.FileName = (@"D:\pratinidhi\etm_com.exe ");
                    info1.Arguments = "1";
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    process.StartInfo = info1;
                    process.StartInfo.UseShellExecute = false;
                    System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "1");
                    process.Close();
                    System.Threading.Thread.Sleep(2000);
                    while (inUse)
                    {
                        inUse = FileInUse();
                        if (inUse)
                            System.Threading.Thread.Sleep(2000);
                        library.WriteErrorLog("loop" + DateTime.Now.ToString());
                    }

                    btnDwnTct.Visible = true;
                    Response.Write("<script>alert('Downloading In Process');window.location.href='DownloadWaybill.aspx?receipno=" + txtWayBill.Text + "&driverno=" + txtdrv.Text + "&conno=" + txtcond.Text + "&vehicleo=" + txtvehicle.Text + "&cash=" + txtcash.Text + "&Type=1'</script>");
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    public static bool FileInUse()
    {

        //FileStream fs1 = null;
        string filePt = @"D:\Pratinidhi\ETM_OUT.txt";
        try
        {
            //fs = File.Open(filePt, FileMode.OpenOrCreate, FileAccess.ReadWrite,FileShare.None);

            //FileStream fs = new FileStream(filePt, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            using (FileStream logFileStream = new FileStream(filePt, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
            {
                using (StreamReader logFileReader = new StreamReader(logFileStream))
                {
                    string text = logFileReader.ReadToEnd();
                }
            }
            return false;
        }
        catch (Exception ex)
        {

            return true;
        }
    }

    public void save_ticket_details()
    {
        try
        {
            savecashier();
            cmd = new SqlCommand();
            cmd.Connection = con;
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            cmd.Transaction = con.BeginTransaction();
            int i = 0;
            //savecashier();

            Save_Data();
            cmd.Transaction.Commit();
            while (inUse)
            {
                inUse = FileInUse();
                if (inUse)
                    System.Threading.Thread.Sleep(2000);
                library.WriteErrorLog("loop inside save_ticket_details" + DateTime.Now.ToString());
            }
            btnDelBill.Enabled = true;
            //save_etvm_detail();
            Show_Msg();
            //btnDelBill.Enabled = true;
            //btnDelBill.Attributes.Add();
        }
        catch (Exception ex)
        {
            // Log_Exceptions bo = new Log_Exceptions();
            //bo.LogException(ex);
            library.WriteErrorLog("Error occured in save_ticket_details function " + ex.Message.ToString() + " " + ex.Source);
            cmd.Transaction.Rollback();
            Msg_Failure();
        }
        finally
        {
            con.Close();
        }
    }

    public void savecashier()
    {
        SqlCommand cmd1 = new SqlCommand();
        try
        {
            cmd1.Connection = con;
            cmd1.CommandType = CommandType.StoredProcedure;

            cmd1 = new SqlCommand("ins_cashier_master", con);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@waybill", Request.QueryString["receipno"].ToString());
            cmd1.Parameters.AddWithValue("@cond", Request.QueryString["conno"].ToString());
            cmd1.Parameters.AddWithValue("@drv", Request.QueryString["driverno"].ToString());
            cmd1.Parameters.AddWithValue("@vehicle", Request.QueryString["vehicleo"].ToString());
            cmd1.Parameters.AddWithValue("@totcash", Request.QueryString["cash"].ToString());
            cmd1.Parameters.AddWithValue("@userid", Session["user_id"].ToString());
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            library.WriteErrorLog("Error occured in savecashier function " + ex.Message.ToString() + " " + ex.Source);
            Response.Write(ex.Message);
        }
    }

    public void Save_Data()
    {
        try
        {
            string filePt = @"D:\Pratinidhi\ETM_OUT.txt";
            if (System.IO.File.Exists(filePt))
            {
                string[] lines12 = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");

                var f = File.ReadAllLines(filePt);

                GenerateFile(lines12);

                foreach (string line12 in lines12)
                {
                    string[] Obj = line12.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries); //--line12.Split(delimiterChars);
                    //library.WriteErrorLog("Length of Obj : " + Obj.Count());

                    //if (Obj[2].ToString().Substring(0, 4) != "DTLS")
                    //{
                    if (Obj[2].ToString().Substring(0, 4) != "CASE" && Obj[9].ToString() != "0")
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        //cmd = new SqlCommand("insert_ticket_data", con);
                        cmd.CommandText = "insert_ticket_data";
                        cmd.Parameters.AddWithValue("@waybillno", Request.QueryString["receipno"].ToString());
                        library.WriteErrorLog("waybillno : " + Request.QueryString["receipno"].ToString());
                        cmd.Parameters.AddWithValue("@tripno", Obj[0].ToString());
                        library.WriteErrorLog("tripno : " + Obj[0].ToString());

                        cmd.Parameters.AddWithValue("@routeno", Obj[1].ToString());
                        library.WriteErrorLog("routeno : " + Obj[1].ToString());

                        cmd.Parameters.AddWithValue("@ticketno", Obj[2].ToString());
                        library.WriteErrorLog("ticket No : " + Obj[2].ToString());

                        cmd.Parameters.AddWithValue("@startstage", Obj[3].ToString());
                        library.WriteErrorLog("startstage : " + Obj[3].ToString());
                        cmd.Parameters.AddWithValue("@endstage", Obj[4].ToString());
                        library.WriteErrorLog("endstage : " + Obj[4].ToString());
                        //----------New Code Added By Amit on 22/7/2015------------//
                        cmd.Parameters.AddWithValue("@adultcount", Obj[5].ToString());
                        library.WriteErrorLog("adultcount : " + Obj[5].ToString());
                        cmd.Parameters.AddWithValue("@childcount", Obj[6].ToString());
                        library.WriteErrorLog("childcount : " + Obj[6].ToString());
                        cmd.Parameters.AddWithValue("@luggagecount", Obj[7].ToString());
                        library.WriteErrorLog("luggagecount : " + Obj[7].ToString());
                        cmd.Parameters.AddWithValue("@passcount", Obj[8].ToString());
                        library.WriteErrorLog("passcount : " + Obj[8].ToString());
                        library.WriteErrorLog("ticketcount : " + Obj[5].ToString() + "," + Obj[6].ToString() + "," + Obj[7].ToString() + "," + Obj[8].ToString());
                        int totalTicketCount = int.Parse(Obj[5].ToString()) + int.Parse(Obj[6].ToString()) +
                                               int.Parse(Obj[7].ToString()) + int.Parse(Obj[8].ToString());
                        cmd.Parameters.AddWithValue("@ticketcount", totalTicketCount);
                        library.WriteErrorLog("ticketcount : " + Obj[5].ToString() + "," + Obj[6].ToString() + "," + Obj[7].ToString() + "," + Obj[8].ToString());
                        if (Obj.Count() == 19)// count is 19 when pass/Conc/CASE No. Exist or else the count is 18
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", Obj[12].ToString());
                            library.WriteErrorLog("cardnumber : " + Obj[12].ToString());

                            cmd.Parameters.AddWithValue("@cardtype", Obj[13].ToString());
                            library.WriteErrorLog("cardtype : " + Obj[13].ToString());
                            cmd.Parameters.AddWithValue("@adultamount", Obj[14].ToString());
                            library.WriteErrorLog("adultamount : " + Obj[14].ToString());
                            cmd.Parameters.AddWithValue("@childamount", Obj[15].ToString());
                            library.WriteErrorLog("childamount : " + Obj[15].ToString());
                            cmd.Parameters.AddWithValue("@luggageamount", Obj[16].ToString());
                            library.WriteErrorLog("luggageamount : " + Obj[16].ToString());
                            cmd.Parameters.AddWithValue("@passamount", Obj[17].ToString());
                            library.WriteErrorLog("passamount : " + Obj[17].ToString());
                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[18].ToString());
                            library.WriteErrorLog("ticketstatus : " + Obj[18].ToString());

                        }
                        else if (Obj.Count() == 20) // where pass age in one digit like M 6  instead of M06
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", "");
                            library.WriteErrorLog("cardnumber : " + "");
                            cmd.Parameters.AddWithValue("@cardtype", 0);
                            library.WriteErrorLog("cardtype : " + 0);
                            cmd.Parameters.AddWithValue("@adultamount", Obj[15].ToString());
                            library.WriteErrorLog("adultamount : " + Obj[15].ToString());
                            cmd.Parameters.AddWithValue("@childamount", Obj[16].ToString());
                            library.WriteErrorLog("childamount : " + Obj[16].ToString());
                            cmd.Parameters.AddWithValue("@luggageamount", Obj[17].ToString());
                            library.WriteErrorLog("luggageamount : " + Obj[17].ToString());
                            cmd.Parameters.AddWithValue("@passamount", Obj[18].ToString());
                            library.WriteErrorLog("passamount : " + Obj[18].ToString());
                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[19].ToString());
                            library.WriteErrorLog("ticketstatus : " + Obj[19].ToString());
                        }
                        else if (Obj.Count() == 21) // where pass age in one digit like M 6  instead of M06
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", Obj[12].ToString());
                            library.WriteErrorLog("cardnumber : " + Obj[12].ToString());

                            cmd.Parameters.AddWithValue("@cardtype", Obj[15].ToString());
                            library.WriteErrorLog("cardtype : " + Obj[15].ToString());

                            cmd.Parameters.AddWithValue("@adultamount", Obj[16].ToString());
                            library.WriteErrorLog("adultamount : " + Obj[16].ToString());

                            cmd.Parameters.AddWithValue("@childamount", Obj[17].ToString());
                            library.WriteErrorLog("childamount : " + Obj[17].ToString());

                            cmd.Parameters.AddWithValue("@luggageamount", Obj[18].ToString());
                            library.WriteErrorLog("luggageamount : " + Obj[18].ToString());

                            cmd.Parameters.AddWithValue("@passamount", Obj[19].ToString());
                            library.WriteErrorLog("passamount : " + Obj[19].ToString());

                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[20].ToString());
                            library.WriteErrorLog("ticketstatus : " + Obj[20].ToString());
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", "");
                            library.WriteErrorLog("cardnumber : " + "");
                            cmd.Parameters.AddWithValue("@cardtype", 0);
                            library.WriteErrorLog("cardtype : " + 0);
                            cmd.Parameters.AddWithValue("@adultamount", Obj[13].ToString());
                            library.WriteErrorLog("adultamount : " + Obj[13].ToString());
                            cmd.Parameters.AddWithValue("@childamount", Obj[14].ToString());
                            library.WriteErrorLog("childamount : " + Obj[14].ToString());
                            cmd.Parameters.AddWithValue("@luggageamount", Obj[15].ToString());
                            library.WriteErrorLog("luggageamount : " + Obj[15].ToString());
                            cmd.Parameters.AddWithValue("@passamount", Obj[16].ToString());
                            library.WriteErrorLog("passamount : " + Obj[16].ToString());
                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[17].ToString());
                            library.WriteErrorLog("ticketstatus : " + Obj[17].ToString());
                        }
                        cmd.Parameters.AddWithValue("@ticketamount", Obj[9].ToString());
                        library.WriteErrorLog("ticketamount : " + Obj[9].ToString());

                        if (Obj[2].ToString().Substring(0, 4) == "CONC")
                        {
                            cmd.Parameters.AddWithValue("@consec_code", Obj[12].ToString());
                            library.WriteErrorLog("consec_code : " + Obj[12].ToString());
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@consec_code", "00");
                            library.WriteErrorLog("consec_code : 00");
                        }
                        cmd.Parameters.AddWithValue("@dateandtime", Obj[10].ToString() + ' ' + Obj[11].ToString());
                        library.WriteErrorLog("dateandtime : " + Obj[11].ToString());

                        cmd.Parameters.AddWithValue("@userid", Session["user_id"].ToString());
                        library.WriteErrorLog("userid : " + Session["user_id"].ToString());
                        //-------------------------Ends Here-----------------------//

                        cmd.ExecuteNonQuery();
                    }
                    //}
                }
            }
        }
        catch (Exception ex)
        {
            library.WriteErrorLog(ex);
            library.WriteErrorLog("Error occured in Save_Data func " + ex.Message + " " + ex.Source + " waybill no. " + Convert.ToString(Request.QueryString["receipno"].ToString()));
            var st = new StackTrace(ex, true);
            // Get the top stack frame
            var frame = st.GetFrame(0);
            // Get the line number from the stack frame
            var line = frame.GetFileLineNumber();
            Response.Write(line);
        }
    }

    public void Show_Msg()
    {
        killprocess();
        string scp = "<script type='text/javascript'>alert('Data Saved Successfully');window.location.href='DownloadWaybill.aspx?receipno=" + Request.QueryString["receipno"].ToString() + "&driverno=" + Request.QueryString["driverno"].ToString() + "&conno=" + Request.QueryString["conno"].ToString() + "&vehicleo=" + Request.QueryString["vehicleo"].ToString() + "&cash=" + Request.QueryString["cash"].ToString() + "&Type=3'</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);
    }

    public void Msg_Failure()
    {
        string scp = "<script type='text/javascript'>alert('Sorry some error occured while saving information.);</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);
    }

    protected void btnCapETM_Click(object sender, EventArgs e)
    {
        try
        {
            getwaybilldetails();
            readwaybill();
        }
        catch (Exception ex)
        {

        }
    }

    public void getwaybilldetails()
    {
        try
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            System.Diagnostics.ProcessStartInfo info1 = new System.Diagnostics.ProcessStartInfo();

            info1.FileName = (@"D:\pratinidhi\etm_com.exe ");

            library.WriteErrorLog("getwaybilldetails " + info1.FileName);

            info1.Arguments = "3";
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo = info1;
            process.StartInfo.UseShellExecute = false;
            System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "3");

            process.Close();

            char[] delimiterChars = { ' ' };
            string filePt = @"D:\Pratinidhi\ETM_OUT.txt";

            library.WriteErrorLog("getwaybilldetails File : " + filePt);
            if (System.IO.File.Exists(filePt))
            {
                string[] lines = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");

                foreach (string line in lines)
                {
                    string[] words = line.Split(delimiterChars);

                    library.WriteErrorLog("getwaybilldetails txtWayBill.Text data : " + words[0]);

                    txtWayBill.Text = words[0].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            library.WriteErrorLog("Error occured in getwaybilldetails func " + ex.Message + " " + ex.Source);
        }
    }

    public void readwaybill()
    {
        try
        {
            char[] delimiterChars = { ' ' };
            string filePt = @"D:\Pratinidhi\ETM_OUT.txt";

            library.WriteErrorLog("readwaybill File : " + filePt);

            if (System.IO.File.Exists(filePt))
            {
                string[] lines = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");

                foreach (string line in lines)
                {
                    string[] words = line.Split(delimiterChars);

                    library.WriteErrorLog("readwaybill txtWayBill.Text data : " + words[0]);

                    txtWayBill.Text = words[0].ToString();
                    txtdrv.Text = line.Substring(26, 16).ToString().Trim();

                    txtcond.Text = line.Substring(9, 16).ToString().Trim();
                    txtvehicle.Text = line.Substring(43, 10).ToString().Trim();
                    Button1.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            library.WriteErrorLog("Error occured in readwaybill func " + ex.Message + " " + ex.Source);
        }

    }

    protected void btnDwnTct_Click(object sender, System.EventArgs e)
    {
        try
        {
            if (txtWayBill.Text.Length < 1 || txtWayBill.Text == "$Error-No" || txtWayBill.Text == "01")
            {
                Response.Write("<script>alert ('Capture ETM Again') </script>");
            }
            else
            {
                string filePt = @"D:\Pratinidhi\ETM_OUT.txt";
                if (System.IO.File.Exists(filePt))
                {
                    string[] lines12 = System.IO.File.ReadAllLines(@"D:\Pratinidhi\ETM_OUT.txt");
                    foreach (string line12 in lines12)
                    {
                        string[] words12 = line12.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        insertticketdata(words12);
                    }
                }
                File.WriteAllText(@"d:\\pratinidhi\\ETM_OUT.txt", String.Empty);
            
                String scp = "<script type='text/javascript'>alert('Data Saved Successfully');window.location.href='DownloadWaybill.aspx?receipno=" + Request.QueryString["receipno"].ToString() + "&driverno=" + Request.QueryString["driverno"].ToString() + "&conno=" + Request.QueryString["conno"].ToString() + "&vehicleo=" + Request.QueryString["vehicleo"].ToString() + "&cash=" + Request.QueryString["cash"].ToString() + "&Type=3'</script>";
                ClientScript.RegisterClientScriptBlock(GetType(), "script1", scp);
            }
        }
        catch (Exception ex)
        {
            library.WriteErrorLog("Error occured in btnDwnTct_Click func " + ex.Message + " " + ex.Source);
        }
    }

    public void insertticketdata(string[] Obj)
    {
        try
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd = new SqlCommand("insert_ticket_data", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@waybillno", txtWayBill.Text);
            cmd.Parameters.AddWithValue("@tripno", Obj[0].ToString());
            library.WriteErrorLog("tripno : " + Obj[0].ToString());
            cmd.Parameters.AddWithValue("@routeno", Obj[1].ToString());
            library.WriteErrorLog("route no : " + Obj[1].ToString());
            cmd.Parameters.AddWithValue("@ticketno", Obj[2].ToString());
            library.WriteErrorLog("ticket No : " + Obj[2].ToString());
            cmd.Parameters.AddWithValue("@startstage", Obj[3].ToString());
            library.WriteErrorLog("Start Stage : " + Obj[3].ToString());
            cmd.Parameters.AddWithValue("@endstage", Obj[4].ToString());
            library.WriteErrorLog("end Stage : " + Obj[4].ToString());
            //----------New Code Added By Amit on 22/7/2015------------//
            cmd.Parameters.AddWithValue("@adultcount", Obj[5].ToString());
            library.WriteErrorLog("adultcount : " + Obj[5].ToString());
            cmd.Parameters.AddWithValue("@childcount", Obj[6].ToString());
            library.WriteErrorLog("childcount : " + Obj[6].ToString());
            cmd.Parameters.AddWithValue("@luggagecount", Obj[7].ToString());
            library.WriteErrorLog("luggagecount : " + Obj[7].ToString());
            cmd.Parameters.AddWithValue("@passcount", Obj[8].ToString());
            library.WriteErrorLog("passcount : " + Obj[8].ToString());
            int totalTicketCount = int.Parse(Obj[5].ToString()) + int.Parse(Obj[6].ToString()) +
                                   int.Parse(Obj[7].ToString()) + int.Parse(Obj[8].ToString());
            cmd.Parameters.AddWithValue("@ticketcount", totalTicketCount);
            library.WriteErrorLog("ticketcount : " + totalTicketCount);
            if (Obj.Count() == 19)// count is 19 when pass/Conc/CASE No. Exist or else the count is 18
            {
                cmd.Parameters.AddWithValue("@adultamount", Obj[14].ToString());
                library.WriteErrorLog("passcount : " + Obj[14].ToString());
                cmd.Parameters.AddWithValue("@childamount", Obj[15].ToString());
                library.WriteErrorLog("passcount : " + Obj[15].ToString());
                cmd.Parameters.AddWithValue("@luggageamount", Obj[16].ToString());
                library.WriteErrorLog("passcount : " + Obj[16].ToString());
                cmd.Parameters.AddWithValue("@passamount", Obj[17].ToString());
                library.WriteErrorLog("passcount : " + Obj[17].ToString());
                cmd.Parameters.AddWithValue("@ticketstatus", Obj[18].ToString());
                library.WriteErrorLog("passcount : " + Obj[18].ToString());
            }
            else if (Obj.Count() == 20) // where pass age in one digit like M 6  instead of M06
            {
                cmd.Parameters.AddWithValue("@adultamount", Obj[15].ToString());
                library.WriteErrorLog("adultamount : " + Obj[15].ToString());
                cmd.Parameters.AddWithValue("@childamount", Obj[16].ToString());
                library.WriteErrorLog("childamount : " + Obj[16].ToString());
                cmd.Parameters.AddWithValue("@luggageamount", Obj[17].ToString());
                library.WriteErrorLog("luggageamount : " + Obj[17].ToString());
                cmd.Parameters.AddWithValue("@passamount", Obj[18].ToString());
                library.WriteErrorLog("passamount : " + Obj[18].ToString());
                cmd.Parameters.AddWithValue("@ticketstatus", Obj[19].ToString());
                library.WriteErrorLog("ticketstatus : " + Obj[19].ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@adultamount", Obj[13].ToString());
                library.WriteErrorLog("adultamount : " + Obj[13].ToString());
                cmd.Parameters.AddWithValue("@childamount", Obj[14].ToString());
                library.WriteErrorLog("childamount : " + Obj[14].ToString());
                cmd.Parameters.AddWithValue("@luggageamount", Obj[15].ToString());
                library.WriteErrorLog("luggageamount : " + Obj[15].ToString());
                cmd.Parameters.AddWithValue("@passamount", Obj[16].ToString());
                library.WriteErrorLog("passamount : " + Obj[16].ToString());
                cmd.Parameters.AddWithValue("@ticketstatus", Obj[17].ToString());
                library.WriteErrorLog("ticketstatus : " + Obj[17].ToString());
            }
            cmd.Parameters.AddWithValue("@ticketamount", Obj[9].ToString());
            library.WriteErrorLog("ticketamount : " + Obj[9].ToString());
            if (Obj[2].ToString().Substring(0, 4) == "CONC")
            {
                cmd.Parameters.AddWithValue("@consec_code", Obj[12].ToString());
                library.WriteErrorLog("consec_code : " + Obj[12].ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@consec_code", "00");

            }
            cmd.Parameters.AddWithValue("@dateandtime", Obj[10].ToString() + ' ' + Obj[11].ToString());
            library.WriteErrorLog("dateandtime : " + Obj[10].ToString() + ' ' + Obj[11].ToString());

            cmd.Parameters.AddWithValue("@userid", Session["user_id"].ToString());
            library.WriteErrorLog("userid : " + Session["user_id"].ToString());
            //-------------------------Ends Here-----------------------//

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
            library.WriteErrorLog("Error occured in insertticketdata func " + ex.Message + " " + ex.Source);
        }
    }

    protected void btnErsTck_Click(object sender, System.EventArgs e)
    {
        Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
        System.Diagnostics.ProcessStartInfo info1 = new System.Diagnostics.ProcessStartInfo();
        info1.FileName = (@"D:\pratinidhi\etm_com.exe ");
        info1.Arguments = "3";
        System.Diagnostics.Process process = new System.Diagnostics.Process();
        process.StartInfo = info1;
        process.StartInfo.UseShellExecute = false;
        System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "5");
        process.Close();
    }

    protected void btnDelBill_Click(object sender, System.EventArgs e)
    {
        try
        {
            if (txtWayBill.Text.Length < 1 || txtWayBill.Text == "$Error-No" || txtWayBill.Text == "01" || txtWayBill.Text == "$DONE")
            {
                Response.Write("<script>alert ('Capture ETM Again') </script>");
            }
            else
            {
                /*Directly Deleting data*/

                /*End*/
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select distinct waybill_no from ticket_detail where waybill_no='" + txtWayBill.Text + "' ", con);
                con.Open();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Delete_ticket_details();
                }
                else
                {
                    Response.Write("<script>alert ('Please Download waybill') </script>");
                }
            }
        }

        catch (Exception ex)
        { }
    }

    public void Delete_ticket_details()
    {

        killprocess();
        var lineCount = File.ReadLines(@"D:\Pratinidhi\ETM_OUT.txt").Count();
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter("select  count(waybill_no)waybill_no_count  from ticket_detail where waybill_no ='" + Request.QueryString["receipno"] + "' group by waybill_no", con);
        con.Open();
        da.Fill(ds);
        con.Close();
        if (ds.Tables[0].Rows[0][0].ToString() == lineCount.ToString())
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            System.Diagnostics.ProcessStartInfo info1 = new System.Diagnostics.ProcessStartInfo();
            // info.FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\etm_com.exe";

            info1.FileName = (@"D:\pratinidhi\etm_com.exe ");
            info1.Arguments = "3";
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo = info1;
            process.StartInfo.UseShellExecute = false;
            System.Diagnostics.Process.Start(@"D:\pratinidhi\etm_com.exe ", "9");
            process.Close();
            //process.Kill();

            string scp = "<script type='text/javascript'>alert('Waybill Deleted');window.location.href='DownloadWaybill.aspx'</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);

        }
        else
        {
            Response.Write("<script>alert ('Please Contact to system Administrator') </script>");
        }



    }

    protected void btn_etm_reset_Click(object sender, EventArgs e)
    {
        try
        {
            killprocess();
            if (System.IO.File.Exists(@"d:\\pratinidhi\\ETM_OUT.txt"))
            {
                System.IO.File.Delete(@"d:\\pratinidhi\\ETM_OUT.txt");
            }
        }
        catch (Exception ex)
        {

        }
    }

    public void GenerateFile(string[] data)
    {
        string FolderPath = "WayBill Folder\\";

        bool exists = System.IO.Directory.Exists(Server.MapPath(FolderPath));

        if (!exists)
            System.IO.Directory.CreateDirectory(Server.MapPath(FolderPath));

        string FileName = "\\WayBill_" + Convert.ToString(Request.QueryString["receipno"].ToString());
        //string FileName = "\\WayBill_0123458.txt";

        StreamWriter sw = null;
        try
        {
            sw = new StreamWriter(Server.MapPath(FolderPath) + FileName + ".txt", true);
            sw.WriteLine("-----------Downloading Data of Waybill no :- " + Request.QueryString["receipno"].ToString() + " on " + DateTime.Now.ToString() + " -----------");
            foreach (string val in data)
            {
                sw.WriteLine(val.ToString());
            }
            sw.Flush();
            sw.Close();
        }
        catch (Exception ex)
        {

        }
    }


}