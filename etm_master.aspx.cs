﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using DataManager;
using System.Windows;
namespace DataManager
{


    public partial class etm_master : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        DataManager dm = new DataManager();
        String selQuery = "select code,EtmNo from etm_master where is_delete=0 ";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Opt1"] == "getconcession")
            {
                string sttray = "";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select etmno from etm_master where etmno='" + txtetvm.Text + "' and isnull(is_delete,0) = 0", con);
                con.Open();
                da.Fill(ds);
                con.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("notexist~");
                }
                else
                {
                    Response.Write("exist~" + sttray.ToString() + "~");
                }
            }
            if (!IsPostBack)
            {
                dm.ExeCuteGridBind(selQuery, grdDriver);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("select etmno from etm_master where etmno='" + txtetvm.Text.Trim() + "' and isnull(is_delete,0) = 0", con);
            con.Open();
            da.Fill(ds);
            con.Close();
            string drivername = txtetvm.Text;
            if (drivername.ToString().Trim() == "")
            {
                Response.Write("<script>alert ('Enter ETM No') </script>");
            }
            else if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert ('Already Exist') </script>");
            }
            else
            {
                try
                {
                    cmd = new SqlCommand();
                    cmd.CommandText = "etm_MASTER_ADD_UPDATE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.Add("@etm_no", SqlDbType.NVarChar).Value = txtetvm.Text;
                    if (btnSave.Text == "Save")
                    {
                        cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@code", SqlDbType.Int).Value = 0;
                    }
                    else if (btnSave.Text == "Update")
                    {
                        cmd.Parameters.Add("@ActionId", SqlDbType.Int).Value = 1;
                        cmd.Parameters.Add("@code", SqlDbType.Int).Value = Convert.ToInt32(hdDriverCode.Value);
                        btnSave.Text = "Save";

                    }
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                }
                dm.ExeCuteGridBind(selQuery, grdDriver);
                Response.Redirect("etm_master.aspx");
            }
        }
        protected void grdDriver_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            {
                hdDriverCode.Value = e.CommandArgument.ToString();
                if (e.CommandName == "EditRow")
                {
                    btnSave.Text = "Update";
                    dr = dm.GetDataReader("Select etmno from etm_master where code=" + e.CommandArgument);
                    while (dr.Read())
                    {
                        txtetvm.Text = dr[0].ToString();
                    }
                }
                if (e.CommandName == "DeleteRow")
                {
                    if (hdDel.Value.ToString() != "1")
                    {
                        dm.ExecuteNonQuery("update etm_master set is_delete=1 where code=" + e.CommandArgument);
                        dm.ExeCuteGridBind(selQuery, grdDriver);
                    }
                    hdDel.Value = "0";
                }
            }
        }
        protected void grdDriver_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdDriver.PageIndex = e.NewPageIndex;
            dm.ExeCuteGridBind(selQuery, grdDriver);
        }
    }
}