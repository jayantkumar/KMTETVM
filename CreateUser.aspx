﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CreateUser.aspx.cs" Inherits="CreateUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="id01" style="display: none" class="modal">
        <span class="close" title="Close Modal">×</span>
        <div class="modal-content animate">
            <div class="modal-header">
                <h3 style="text-align: center; font-size: 15px; font-weight: bold;">
                    <span class="extra-title muted">Registration</span></h3>
            </div>
            <div class="container">
                <label><b>Username</b></label>
                <input type="text" placeholder="Enter Username" style="width: 550px;" id="email" maxlength="15" name="email" autocomplete="off" required>
                <label><b>Password</b></label>
                <input type="password" placeholder="Enter Password" style="width: 550px;" maxlength="8" id="psw" name="psw" autocomplete="off" required>
                <label><b>Confirm Password</b></label>
                <input type="password" placeholder="Confirm Password" maxlength="8" style="width: 550px;" id="psw_confirm" autocomplete="off" name="psw_confirm" required>
                <div class="clearfix">
                    <button type="button" onclick="location.href = 'ServiceOperator.aspx';" id="btncancel" class="cancelbtn">Cancel</button>

                    <button type="submit" onclick="fnCreateUser()" class="signupbtn">Sign Up</button>
                </div>
            </div>
        </div>
    </div>
    <link type="text/css" rel="stylesheet" href="css/Registration.css" media="screen" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" language="javascript">
        // Get the modal
        var modal = document.getElementById('id01');

        var user = "<%= Session["UserName"]%>";
      
        $(document).ready(function () {
            document.getElementById("id01").style.display = "block";
        });
        function fnCreateUser() {

            if (document.getElementById("email").value == "") {
                alert("Enter username");
                return false;
            }
            else if (document.getElementById("psw").value.length < 3) {
                alert("Passwords length should be greater than three.");
                return false;
            }
            else if (document.getElementById("psw").value != document.getElementById("psw_confirm").value) {
                alert("Passwords do not match.");
                return false;
            }
            else {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Login.aspx/SignUp",
                    data: "{'username':'" + document.getElementById("email").value + "','password':'" + document.getElementById("psw").value + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == "1") {
                            alert("User already exists");
                            return false;
                        }
                        else if (obj == "2") {
                            alert("User successfuly registered");
                            return false;
                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
        }
    </script>
</asp:Content>

