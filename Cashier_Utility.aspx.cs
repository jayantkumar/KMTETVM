﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

public partial class Cashier_Utility : System.Web.UI.Page
{

    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblcond.Visible = false;
            lbldeponame.Visible = false;
            lbldriver.Visible = false;
            lblschedule.Visible = false;
            lblvech.Visible = false;
                
            binddropdown();
        }
    }

    /// <summary>
    /// try {
//    cmd = new SqlCommand();
//    cmd.Connection = con;
//    if (con.State != ConnectionState.Open) {
//        con.Open();
//    }
//    cmd.Transaction = con.BeginTransaction;
//    int i = 0;
//    Save_Data();
//    cmd.Transaction.Commit();
//    Show_Msg();
//} catch (Exception ex) {
//    Log_Exceptions bo = new Log_Exceptions();
//    bo.LogException(ex);
//    cmd.Transaction.Rollback();
//    Msg_Failure();
//} finally {
//    con.Close();
//}
//    /// </summary>



    public void binddropdown()


    {

         //con = new SqlConnection(str);
        string com = "select  WayBillNo from WayBill_Details where is_cashier_utility=0";
        SqlDataAdapter adpt = new SqlDataAdapter(com, con);
        DataTable dt = new DataTable();
        adpt.Fill(dt);
        ddlwaybillno.DataSource = dt;
        ddlwaybillno.DataBind();
        ddlwaybillno.DataTextField = "WayBillNo";
        ddlwaybillno.DataValueField = "WayBillNo";
        ddlwaybillno.DataBind();
    
    
    }
    protected void ddlwaybillno_SelectedIndexChanged(object sender, EventArgs e)
    {
        string com1 = "select * from WayBill_Details where waybillno='"+ddlwaybillno.SelectedItem.Text+"'";
        SqlDataAdapter adpt1 = new SqlDataAdapter(com1, con);
        DataTable dt1 = new DataTable();
        adpt1.Fill(dt1);
        lblcond.Text = dt1.Rows[0][3].ToString();
        lbldriver.Text = dt1.Rows[0][5].ToString();
        lbldeponame.Text = dt1.Rows[0][8].ToString();
        lblschedule.Text = dt1.Rows[0][11].ToString();

        lblvech.Text = dt1.Rows[0][7].ToString();
            
            
            //dt1.Rows[0].Item("conductor_name").ToString();


      
       
    }
}