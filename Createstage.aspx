﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Createstage.aspx.cs" Inherits="Createstage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%;">
                <tr>
                    <td colspan="3" align="center">
                        <h2>Stage Master</h2>
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">
                        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="12pt" Text="Root_Name :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 261px">
                        <asp:TextBox runat="server" Height="25px" Width="220px" ID="Txt_rootname" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="12pt" Text="Stage_Name in English :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 261px">
                        <asp:TextBox runat="server" Height="25px" Width="220px" ID="Txt_English" MaxLength="11"></asp:TextBox>
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">
                        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="12pt" Text="Stage_Name in Marathi :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 261px">
                        <asp:TextBox ID="Txt_Marathi" runat="server" Font-Bold="True" Font-Size="12pt" Height="25px"
                            Width="220px" MaxLength="16"></asp:TextBox>
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="12pt" Text="Fare Stage No :"></asp:Label>
                    </td>
                    <td class="modal-sm" style="width: 261px">
                        <asp:TextBox ID="Txt_Fare" runat="server" Font-Bold="True" Font-Size="12pt" Height="25px"
                            Width="220px" MaxLength="3" onkeypress='return isNumberKey(event)'></asp:TextBox>
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">&nbsp;
                    </td>
                    <td class="modal-sm" style="width: 261px">
                        <asp:Button ID="Button1" runat="server" Font-Bold="True" Font-Size="12pt" Height="29px"
                            OnClick="Button1_Click" Text="Add" Width="90px" OnClientClick="CheckFareStage(this);" />
                        &nbsp;<asp:Button ID="Btn_back" runat="server" Font-Bold="True" Font-Size="12pt"
                            Height="29px" OnClick="Btn_back_Click" Text="Back" Width="90px" />
                        &nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">&nbsp;
                    </td>
                    <td class="modal-sm" style="width: 261px">
                        <asp:Label ID="Lbl_msg" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">&nbsp;
                    </td>
                    <td class="modal-sm" style="width: 261px">&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">&nbsp;
                    </td>
                    <td class="modal-sm" style="width: 261px">
                        <asp:GridView ID="Gridstage" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="SrNo.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Text='<%#Container.DataItemIndex + 1%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Root_Name" DataField="rootname" />
                                <asp:BoundField HeaderText="Stage_name" DataField="stage_name" />
                                <asp:BoundField HeaderText="Stg_regional_name" DataField="stg_regional_name" />
                                <asp:BoundField HeaderText="Stage_No" DataField="stage_no" />
                                <asp:BoundField HeaderText="Fare_stage_NO" DataField="fare_change_stage_no" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">&nbsp;</td>
                    <td class="modal-sm" style="width: 261px">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="modal-sm" style="width: 220px">&nbsp;</td>
                    <td class="modal-sm" style="width: 261px">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">

        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function CheckFareStage(val) {
            if (document.getElementById('ContentPlaceHolder1_Gridstage') != null) {
                var rowNo = document.getElementById('<%=Gridstage.ClientID %>').rows.length;
                var table = document.getElementById('<%=Gridstage.ClientID %>');
                var Row = table.rows[rowNo - 1];
                var td = Row.cells[5];
                if (td.innerText <= parseInt(val.value)) {
                    alert("Value must be greater than previous value.");
                }
            }
        }

    </script>
</asp:Content>
