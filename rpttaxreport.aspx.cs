﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace DataManager
{
    public partial class rpttaxreport : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;
        SqlDataAdapter da;
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        double a, b, z;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    DataSet ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (Convert.ToString(txt_waybill.Text) != "")
                        cmd.CommandText = "view_denomination_wise_tax_waybill";
                    else
                        cmd.CommandText = "view_denomination_wise_tax";
                    cmd.Parameters.Add(new SqlParameter("@fromdate", txtfromdate.Text));
                    cmd.Parameters.Add(new SqlParameter("@todate", txttodate.Text));
                    cmd.Parameters.Add(new SqlParameter("@waybillno", txt_waybill.Text));
                    adp.SelectCommand = cmd;
                    adp.Fill(ds);
                    grdWayBill.DataSource = ds;
                    grdWayBill.DataBind();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblfromdate.Text = "From Date :" + txtfromdate.Text;
                        lbltodate.Text = "To Date :" + txttodate.Text;
                    }
                    else
                    {
                        lblfromdate.Text = "";
                        lbltodate.Text = "";
                    }

                }
                catch (Exception ex)
                {
                    con.Close();
                }
                finally
                {
                    con.Close();
                }
            }
        }

        protected void btnexcel_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("Denominationwise_tax_report.xls", grdWayBill);
        }
        protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[1].Text == "TOTAL")
                {
                    e.Row.Cells[0].Text = " ";
                    e.Row.Cells[1].Font.Size = 12;
                    e.Row.Cells[2].Font.Size = 12;
                    e.Row.Cells[3].Font.Size = 12;
                    e.Row.Cells[4].Font.Size = 12;
                    e.Row.Cells[5].Font.Size = 12;
                    e.Row.Cells[6].Font.Size = 12;
                    e.Row.Cells[7].Font.Size = 12;


                    e.Row.Cells[1].Font.Bold = true;
                    e.Row.Cells[2].Font.Bold = true;
                    e.Row.Cells[3].Font.Bold = true;
                    e.Row.Cells[4].Font.Bold = true;
                    e.Row.Cells[5].Font.Bold = true;
                    e.Row.Cells[6].Font.Bold = true;
                    e.Row.Cells[7].Font.Bold = true;
                }
            }
        }
        [System.Web.Services.WebMethod]
        public static List<string> GetCompletionwaybill(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["Con"];
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select WayBillNo from WayBill_Details where " +
                    "WayBillNo like  '%' + @SearchText + '%'";
                    cmd.Parameters.AddWithValue("@SearchText", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> customers = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(sdr["WayBillNo"].ToString());
                        }
                    }
                    conn.Close();
                    return customers;
                }
            }
        }
    }
}