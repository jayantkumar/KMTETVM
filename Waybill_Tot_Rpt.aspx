﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Waybill_Tot_Rpt.aspx.cs" Inherits="Waybill_Tot_Rpt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td colspan="4" align="center">
                <h2>
                    Waybill Report</h2>
            </td>
        </tr>
        <tr>
            <td>
                Way Bill Number</td>
            <td>
                <asp:TextBox ID="txtWayNo" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnView" runat="server" class="submit-btn2" 
                    OnClick="btnView_Click" Text="View Report" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" 
                    AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" 
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" class="datagrid_heading" 
                    EmptyDataText="No Records Available !!!" Font-Bold="False" Font-Italic="False" 
                    Font-Overline="False" Font-Size="Smaller" Font-Strikeout="False" 
                    Font-Underline="False"  Width="946px">
                    <AlternatingRowStyle BackColor="White" CssClass="datagrid_row" 
                        Font-Bold="False" Font-Italic="False" Font-Names="Verdana" 
                        Font-Overline="False" Font-Size="Smaller" Font-Strikeout="False" 
                        Font-Underline="False" ForeColor="Black" />
                    <RowStyle CssClass="datagrid_row1" Font-Bold="False" Font-Italic="False" 
                        Font-Names="Verdana" Font-Overline="False" Font-Size="Smaller" 
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#000099" />
                    <HeaderStyle CssClass="datagrid_heading" Font-Bold="True" Font-Italic="False" 
                        Font-Names="Arial" Font-Overline="False" Font-Size="Small" 
                        Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                    <Columns>
                        <asp:BoundField DataField="waybill_no" HeaderText="WayBill No" />
                        <asp:BoundField DataField="totamt" HeaderText="Amount" />
                        <asp:BoundField DataField="tot_passenger" HeaderText="Total Passenger Travelled" />
                       <%-- <asp:BoundField DataField="routeno" HeaderText="Route No" />--%>
                        <asp:BoundField DataField="totkm" HeaderText="Tot km" />
                   
                    </Columns>
                    <EmptyDataTemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                    border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                    height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" 
                                ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

