﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;

public partial class ticketReport : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlConnection conSar = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConSar"]);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        getlaldarwaza();
        getSaranpur();
        trFomTO.Visible = true;
        lal.Visible = true;
        sarang.Visible = true;
        fromLable.Text = txtfromdate.Text;
        toLable.Text = txttodate.Text;
    }
    protected void btnexcel_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=Ticket_Report.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel1.RenderControl(hw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    private void getlaldarwaza()
    {
        SqlCommand cmd = new SqlCommand();
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter();
        cmd.Connection = con;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "TicketReport_SP";
        cmd.Parameters.Add(new SqlParameter("@datefrom", txtfromdate.Text));
        cmd.Parameters.Add(new SqlParameter("@dateto", txttodate.Text));

        //'  cmd.Parameters.Add(New SqlParameter("@bustype", DropDownList1.SelectedItem))

        adp.SelectCommand = cmd;
        adp.Fill(ds);
        grdWayBill.DataSource = ds;
        

        grdWayBill.DataBind();
       
    }
    private void getSaranpur()
    {
        SqlCommand cmd = new SqlCommand();
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter();
        cmd.Connection = conSar;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "TicketReport_SP";
        cmd.Parameters.Add(new SqlParameter("@datefrom", txtfromdate.Text));
        cmd.Parameters.Add(new SqlParameter("@dateto", txttodate.Text));

        //'  cmd.Parameters.Add(New SqlParameter("@bustype", DropDownList1.SelectedItem))

        adp.SelectCommand = cmd;
        adp.Fill(ds);
        
        GridView1.DataSource = ds;

        GridView1.DataBind();
    }
}