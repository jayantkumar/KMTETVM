﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DataManager
{
    public partial class FareMaster : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        static SqlConnection cons = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
        SqlCommand cmd;
        SqlDataReader dr;

        String selQuery = "select distinct fare_id , fare_stepnumber ,startkm , endkm , adult_fare , child_fare from stage_wise_FareMaster$ order by fare_id";
        DataManager dm = new DataManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["user_type"]) != "5")
                {
                    Response.Redirect("ServiceOperator.aspx");
                }
                else
                    dm.ExeCuteGridBind(selQuery, gridFare);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < gridFare.Rows.Count; i++)
                {
                    GridViewRow row = gridFare.Rows[i];

                    string id = ((Label)(row.Cells[0].FindControl("lbl_id"))).Text;
                    string fare_step_no = ((Label)(row.Cells[0].FindControl("txt_fare_stepno"))).Text;
                    string adult_fare = ((TextBox)(row.Cells[0].FindControl("txt_adult_fare"))).Text;
                    string child_fare = ((TextBox)(row.Cells[0].FindControl("txt_child_fare"))).Text;

                    con.Open();
                    cmd = new SqlCommand();
                    cmd.CommandText = "Sp_Updatefarechart";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.Add("@fareid", SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@adultfare", SqlDbType.Decimal).Value = Convert.ToDecimal(adult_fare);
                    cmd.Parameters.Add("@childfare", SqlDbType.Decimal).Value = Convert.ToDecimal(0);
                    cmd.Parameters.Add("@loopid", SqlDbType.Int).Value = i;

                    if (i == (Convert.ToInt32(gridFare.Rows.Count) - 1))
                        cmd.Parameters.Add("@Action", SqlDbType.Int).Value = 1;
                    else
                        cmd.Parameters.Add("@Action", SqlDbType.Int).Value = 0;
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                Response.Write("<script>alert ('Fare updated successfuly') </script>");
                return;
            }
            catch (Exception ex)
            {

            }
        }

        protected void gridFare_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}