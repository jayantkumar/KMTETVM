﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="Datewise_all_over.aspx.cs"
    Inherits="Datewise_all_over" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <style media="Print" type="text/css">
        .ctrl
        {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize
        {
            font-family: Times New Roman;
            font-size: 12px;
        }
    </style>
</head>
<head>
    <title>Datewise Waybill Report</title>
    <body class="body">
        <form id="Form1" runat="server">
        <a href="reports.aspx" class="ctrl">Back</a>
        <table style="width: 100%">
            <tr>
                <td colspan="4" align="center">
                    <h2>
                        Waybill Collection Report</h2>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <table border="0" width="100%">
                        <tr class="ctrl">
                            <td align="right">
                                <font style="font-family: arial; font-size: 12px;">Date From: </font>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtfromdate" runat="server" Width="80px">
                                </asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                    PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                                <asp:ScriptManager ID="Scriptmanager1" runat="server">
                                </asp:ScriptManager>
                            </td>
                            <td align="right">
                                <font style="font-family: arial; font-size: 12px;">Date To </font>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txttodate" runat="server" Width="80px">
                                </asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                    PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                            </td>
                        </tr>
                        <tr style="height: 50px" class="ctrl">
                            <td>
                            </td>
                            <td colspan="2" align="center">
                                <asp:Button ID="Search" runat="server" Text="Search" OnClick="Search_Click" />
                                <asp:Button ID="btnexcel" runat="server" OnClick="btnexcel_Click" Text="Convert To Excel" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:label id="lblfromdate" runat="server"></asp:label>
                </td>
                <td align="Right">
                    <asp:label id="lbltodate" runat="server">></asp:label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" ShowFooter="true"
                        AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                        Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                        Font-Strikeout="False" Font-Underline="False" Width="100%" OnRowDataBound="grdWayBill_RowDataBound"
                        OnSelectedIndexChanged="grdWayBill_SelectedIndexChanged">
                        <alternatingrowstyle backcolor="White" cssclass="datagrid_row" font-bold="False"
                            font-italic="False" font-names="Verdana" font-overline="False" font-size="Medium"
                            font-strikeout="False" font-underline="False" forecolor="Black" />
                        <rowstyle cssclass="datagrid_row1" font-bold="False" font-italic="False" font-names="Verdana"
                            font-overline="False" font-size="Medium" font-strikeout="False" font-underline="False"
                            forecolor="#000099" />
                        <headerstyle cssclass="datagrid_heading" font-bold="True" font-italic="False" font-names="Arial"
                            font-overline="False" font-size="Small" font-strikeout="False" font-underline="False"
                            forecolor="Black" />
                        <columns>
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%#Container.DataItemIndex  +1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="waybill_no" HeaderText="WayBill No" />
                        <asp:BoundField DataField="route_no" HeaderText="Route Name" />
                        <asp:BoundField DataField="amount" HeaderText="Amount"  />
                        <%-- <asp:BoundField DataField="routeno" HeaderText="Route No" />--%>
                        <asp:BoundField DataField="km" HeaderText="Tot km" />
                        <asp:BoundField DataField="trip" HeaderText="No Of Trips" />
                        <asp:BoundField DataField="no_of_tic" HeaderText="No Of Ticket" />
                        <asp:BoundField DataField="cap_date" DataFormatString="{0:dd-M-yyyy}" HeaderText="Download Date" />
                    </columns>
                        <emptydatatemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                            border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                            height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </emptydatatemplate>
                        <footerstyle backcolor="#1C5E55" font-bold="True" forecolor="White" font-size="Medium" />
                    </asp:GridView>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        </form>
    </body>
</html>
