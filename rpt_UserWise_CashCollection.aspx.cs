﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class rpt_UserWise_CashCollection : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    static SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfromdate.Text = DateTime.Now.Date.ToString("dd-MMM-yyyy");
            txttodate.Text = DateTime.Now.Date.ToString("dd-MMM-yyyy");
        }
    }

    [ScriptMethod()]
    [WebMethod]
    public static string[] GetUser(string prefix)
    {
        List<string> user = new List<string>();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select distinct top 10 upper(isnull(username,'')) username from User_Master where UserName like  '%' + @SearchText + '%'";
                cmd.Parameters.AddWithValue("@SearchText", prefix);
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        user.Add(sdr["username"].ToString());
                    }
                }
                conn.Close();
            }
        }
        catch (Exception ex)
        {

        }
        return user.ToArray();
    }


    protected void btnView_Click(object sender, EventArgs e)
    {
        try
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandTimeout = 500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_userwise_cash_collection";
            cmd.Parameters.Add(new SqlParameter("@user", txtuser.Text));
            cmd.Parameters.Add(new SqlParameter("@from", txtfromdate.Text));
            cmd.Parameters.Add(new SqlParameter("@to", txttodate.Text));

            adp.SelectCommand = cmd;
            adp.Fill(ds);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grdWayBill.DataSource = ds;
                    grdWayBill.DataBind();

                    decimal Cash_Collected = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("Cash_Collected"));
                    decimal etvm_tic_amt = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("etvm_tic_amt"));
                    decimal manual_tic_amt = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("manual_tic_amt"));
                    decimal total_tic_amt = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("total_tic_amt"));
                    decimal excess_amount = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("excess_amount"));


                    grdWayBill.FooterRow.Cells.RemoveAt(1);
                    grdWayBill.FooterRow.Cells.RemoveAt(2);
                    grdWayBill.FooterRow.Cells.RemoveAt(3);
                    grdWayBill.FooterRow.Cells.RemoveAt(0);

                    grdWayBill.FooterRow.Cells[1].Text = "Total";
                    grdWayBill.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;
                    grdWayBill.FooterRow.Cells[1].ColumnSpan = 5;

                    grdWayBill.FooterRow.Cells[2].Text = Cash_Collected.ToString("N2");
                    grdWayBill.FooterRow.Cells[3].Text = etvm_tic_amt.ToString("N2");
                    grdWayBill.FooterRow.Cells[4].Text = manual_tic_amt.ToString("N2");
                    grdWayBill.FooterRow.Cells[5].Text = total_tic_amt.ToString("N2");
                    grdWayBill.FooterRow.Cells[6].Text = excess_amount.ToString("N2");
                }
                else
                {
                    grdWayBill.DataSource = null;
                    grdWayBill.DataBind();
                }
            }
            else
            {
                grdWayBill.DataSource = null;
                grdWayBill.DataBind();
            }
        }
        catch (Exception ex)
        {
            con.Close();
        }
        finally
        {
            con.Close();
        }
    }

    protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}