﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Rpt_conductorwise.aspx.cs" Inherits="DataManager.Rpt_conductorwise" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script src="Scripts/json2.js" type="text/javascript"></script>
<script src="scripts/jquery-1.6.2.js" type="text/javascript"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1 {
        }

        .style3 {
            /*width: 270px;*/
        }
    </style>
    <script type="text/javascript" language="javascript">
        function concheck() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Rpt_conductorwise.aspx/validate",
                data: "{'conname':'" + document.getElementById("txtconductor").value + "'}",
                dataType: "json",
                success: function (data) {
                    var obj = data.d;
                    if (obj == 'true') {
                        //alert("Please select Correct Conductor name");
                        document.getElementById("txtconductor").focus();
                        $("#txtconductor").css("color", "red");
                        document.getElementById("lblconname").innerHTML = "";
                        return false;
                    }
                    else {
                        $("#txtconductor").css("color", "black");
                        document.getElementById("lblconname").innerHTML = obj;
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table style="width: 100%;">
                <tr>
                    <td colspan="3">
                        <a class="ctrl" href="reports.aspx">Back</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <h3>Conductor wise cash Collection Report</h3>
                    </td>
                    <%--<td align="center">&nbsp;</td>--%>
                </tr>
                <tr>
                    <td style="width: 130px">From Date :</td>
                    <td>
                        <asp:TextBox ID="txtfromdate" runat="server" Width="85px" Height="22px"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                            PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                        </asp:CalendarExtender>
                        <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                        <asp:ScriptManager ID="Scriptmanager1" runat="server">
                        </asp:ScriptManager>
                    </td>
                    <td style="width: 130px">To Date :</td>
                    <td>
                        <asp:TextBox ID="txttodate" runat="server" Width="85px" Height="22px"></asp:TextBox>
                        &nbsp;<asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                            PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                        </asp:CalendarExtender>
                        &nbsp;</td>
                    <td style="width: 130px">Conductor: </td>
                    <td>
                        <asp:TextBox ID="txtconductor" MaxLength="11" runat="server"
                            onchange="concheck();"></asp:TextBox>
                        <asp:AutoCompleteExtender ServiceMethod="GetCompletionconductor" MinimumPrefixLength="1"
                            CompletionInterval="10" EnableCaching="false"
                            CompletionSetCount="1" TargetControlID="txtconductor"
                            ID="AutoCompleteExtender2" runat="server"
                            FirstRowSelected="false">
                        </asp:AutoCompleteExtender>
                        <asp:Label ID="lblconname" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">&emsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <asp:Button ID="Btn_search" runat="server" Text="Search"
                            OnClick="Search_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">&emsp;
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="Lbl_fromdate" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                    <td class="style3">&nbsp;</td>
                    <td class="style3">&nbsp;</td>
                    <td>
                        <asp:Label ID="Lbl_Todate" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style1" colspan="6">
                        <asp:GridView ID="Grid_conductor" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="Sr.No">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex  +1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Conductor_Name" DataField="con_full_name" />
                                <asp:BoundField HeaderText="Conductor No" DataField="emp_code" />
                                <asp:BoundField HeaderText="First_Ticket_Date" DataField="ModifiedDate" />
                                <asp:BoundField HeaderText="Duty No" DataField="Schedule" />
                                <asp:BoundField HeaderText="WayBill No" DataField="waybill_no" />
                                <asp:BoundField HeaderText="Etm Ticket Count" DataField="ETVM_tic_count" />
                                <asp:BoundField HeaderText="Etm Ticket Amount" DataField="etvm_tic_amt" />
                                <asp:BoundField HeaderText="Manual Ticket Count" DataField="manual_tic_count" />
                                <asp:BoundField HeaderText="Manual Cash" DataField="manual_tic_amt" />


                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="style1">&nbsp;</td>
                    <td class="style3">&nbsp;</td>
                    <td class="style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
