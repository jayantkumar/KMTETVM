﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="rpttaxreport.aspx.cs" Inherits="DataManager.rpttaxreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script src="Scripts/json2.js" type="text/javascript"></script>
<script src="scripts/jquery-1.6.2.js" type="text/javascript"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <style media="Print" type="text/css">
        .ctrl {
            display: none;
        }
    </style>
    <style type="text/css">
        .fontsize {
            font-family: Times New Roman;
            font-size: 12px;
        }
        /*.style1
        {
            height: 28px;
        }
        .style2
        {
            width: 170px;
        }
        .style3
        {
            width: 235px;
        }*/
    </style>


</head>
<head>
    <title>ETVM and Manual</title>
    <body class="body">
        <form id="Form1" runat="server">
            <a href="reports.aspx" class="ctrl">Back</a>
            <table style="width: 100%">
                <tr>
                    <td colspan="6" align="center">
                        <h3>Denomination wise Tax Report</h3>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px">From Date :</td>
                    <td>
                        <asp:TextBox ID="txtfromdate" runat="server" Width="85px" Height="22px"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                            PopupButtonID="imgPopBtnfrom" TargetControlID="txtfromdate">
                        </asp:CalendarExtender>
                        <asp:ImageButton ID="imgPopBtnfrom" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                        <asp:ScriptManager ID="Scriptmanager1" runat="server">
                        </asp:ScriptManager>
                    </td>
                    <td style="width: 130px">To Date :</td>
                    <td>
                        <asp:TextBox ID="txttodate" runat="server" Width="85px" Height="22px"></asp:TextBox>
                        &nbsp;<asp:ImageButton ID="imgPopBtnto" runat="server" ImageAlign="AbsBottom" ImageUrl="~/images/imgCalendar.png" />
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                            PopupButtonID="imgPopBtnto" TargetControlID="txttodate">
                        </asp:CalendarExtender>
                        &nbsp;</td>
                    <td style="width: 130px">WayBill No: </td>
                    <td>
                        <asp:TextBox ID="txt_waybill" MaxLength="11" runat="server"></asp:TextBox>
                        <asp:AutoCompleteExtender ServiceMethod="GetCompletionwaybill" MinimumPrefixLength="1"
                            CompletionInterval="10" EnableCaching="false"
                            CompletionSetCount="1" TargetControlID="txt_waybill"
                            ID="AutoCompleteExtender2" runat="server"
                            FirstRowSelected="false">
                        </asp:AutoCompleteExtender>

                    </td>
                    <td>
                        <asp:Label ID="lblconname" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">&emsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <asp:Button ID="Search" runat="server" Text="Search" OnClick="Search_Click" />
                        <asp:Button ID="btnexcel" runat="server" OnClick="btnexcel_Click" Text="Convert To Excel" />
                    </td>
                </tr>
                 <tr>
                    <td colspan="6" align="center">&emsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style2">
                        <asp:label id="lblfromdate" runat="server"></asp:label>
                    </td>
                    <td align="left" class="style3">&nbsp;</td>
                    <td align="Right">
                        <asp:label id="lbltodate" runat="server"></asp:label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <asp:GridView ID="grdWayBill" runat="server" AllowSorting="True" ShowFooter="true"
                            AutoGenerateColumns="False" BackColor="WhiteSmoke" BorderColor="#404040" BorderStyle="Solid"
                            BorderWidth="1px" CellPadding="4" class="datagrid_heading" EmptyDataText="No Records Available !!!"
                            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Smaller"
                            Font-Strikeout="False" Font-Underline="False" Width="100%"
                            OnRowDataBound="grdWayBill_RowDataBound">
                            <alternatingrowstyle backcolor="White" cssclass="datagrid_row" font-bold="False"
                                font-italic="False" font-names="Verdana" font-overline="False" font-size="Medium"
                                font-strikeout="False" font-underline="False" forecolor="Black" />
                            <rowstyle cssclass="datagrid_row1" font-bold="False" font-italic="False" font-names="Verdana"
                                font-overline="False" font-size="Medium" font-strikeout="False" font-underline="False"
                                forecolor="#000099" />
                            <headerstyle cssclass="datagrid_heading" font-bold="True" font-italic="False" font-names="Arial"
                                font-overline="False" font-size="Small" font-strikeout="False" font-underline="False"
                                forecolor="Black" />
                            <columns>
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%#Container.DataItemIndex  +1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:BoundField DataField="ticdesc" ItemStyle-Wrap="false" HeaderText="Ticket Type" />
                        <asp:BoundField DataField="Denomination" ItemStyle-Wrap="false" HeaderText="Denomination(A)" />
                        <asp:BoundField DataField="Ticket_count" HeaderText="Ticket Count(B)" />
                        <asp:BoundField DataField="tic_Amount" HeaderText="Amount (C) A * B = C" />
                        <asp:BoundField DataField="tax_015" HeaderText="Tax CNF (D = B * 0.15)" />
                        <asp:BoundField DataField="amount_minus_tax015" HeaderText="Total (E)(A * B) - (B * 0.15) = E" />
                         <asp:BoundField DataField="tax_035" HeaderText="Tax By 3.5% (F)((A * B) - (B * 0.15)) * 0.035 = F" />

                    </columns>
                            <emptydatatemplate>
                        <div style="border-right: peachpuff thin solid; border-top: peachpuff thin solid;
                            border-left: peachpuff thin solid; width: 300px; border-bottom: peachpuff thin solid;
                            height: 100px; background-color: beige; text-align: center">
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="DarkOrange" Text="&lt;b&gt;Sorry!!! No Records Available.&lt;/b&gt;"></asp:Label>
                        </div>
                    </emptydatatemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
