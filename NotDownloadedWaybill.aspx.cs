﻿using DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NotDownloadedWaybill : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd = new SqlCommand();
    SqlDataReader dr;
    SqlDataAdapter da;

    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    bool inUse = true;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ReadData();
    }

    public void ReadData()
    {
        try
        {
            string filePt = @"D:\Faisal\WayBill Issue\1028418.txt";
            if (System.IO.File.Exists(filePt))
            {
                string[] lines12 = System.IO.File.ReadAllLines(@"D:\Faisal\WayBill Issue\1028418.txt");

                foreach (string line12 in lines12)
                {
                    string[] Obj = line12.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries); //--line12.Split(delimiterChars);

                    if (Obj[2].ToString().Substring(0, 4) != "CASE" && Obj[9].ToString() != "0")
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        
                        cmd.Connection = con;
                        cmd.Parameters.Clear();
                        cmd.CommandText = "insert_ticket_data";
                        cmd.Parameters.AddWithValue("@waybillno", "1015334");
                        library.WriteErrorLog("waybillno : " + "1015334");
                        cmd.Parameters.AddWithValue("@tripno", Obj[0].ToString());
                        library.WriteErrorLog("tripno : " + Obj[0].ToString());

                        cmd.Parameters.AddWithValue("@routeno", Obj[1].ToString());
                        library.WriteErrorLog("routeno : " + Obj[1].ToString());

                        cmd.Parameters.AddWithValue("@ticketno", Obj[2].ToString());
                        library.WriteErrorLog("ticket No : " + Obj[2].ToString());

                        cmd.Parameters.AddWithValue("@startstage", Obj[3].ToString());
                        library.WriteErrorLog("startstage : " + Obj[3].ToString());
                        cmd.Parameters.AddWithValue("@endstage", Obj[4].ToString());
                        library.WriteErrorLog("endstage : " + Obj[4].ToString());
                        //----------New Code Added By Amit on 22/7/2015------------//
                        cmd.Parameters.AddWithValue("@adultcount", Obj[5].ToString());
                        library.WriteErrorLog("adultcount : " + Obj[5].ToString());
                        cmd.Parameters.AddWithValue("@childcount", Obj[6].ToString());
                        library.WriteErrorLog("childcount : " + Obj[6].ToString());
                        cmd.Parameters.AddWithValue("@luggagecount", Obj[7].ToString());
                        library.WriteErrorLog("luggagecount : " + Obj[7].ToString());
                        cmd.Parameters.AddWithValue("@passcount", Obj[8].ToString());
                        library.WriteErrorLog("passcount : " + Obj[8].ToString());
                        library.WriteErrorLog("ticketcount : " + Obj[5].ToString() + "," + Obj[6].ToString() + "," + Obj[7].ToString() + "," + Obj[8].ToString());
                        int totalTicketCount = int.Parse(Obj[5].ToString()) + int.Parse(Obj[6].ToString()) +
                                               int.Parse(Obj[7].ToString()) + int.Parse(Obj[8].ToString());
                        cmd.Parameters.AddWithValue("@ticketcount", totalTicketCount);
                        library.WriteErrorLog("ticketcount : " + Obj[5].ToString() + "," + Obj[6].ToString() + "," + Obj[7].ToString() + "," + Obj[8].ToString());
                        if (Obj.Count() == 19)// count is 19 when pass/Conc/CASE No. Exist or else the count is 18
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", Obj[12].ToString());
                            library.WriteErrorLog("cardnumber : " + Obj[12].ToString());

                            cmd.Parameters.AddWithValue("@cardtype", Obj[13].ToString());
                            library.WriteErrorLog("cardtype : " + Obj[13].ToString());
                            cmd.Parameters.AddWithValue("@adultamount", Obj[14].ToString());
                            library.WriteErrorLog("adultamount : " + Obj[14].ToString());
                            cmd.Parameters.AddWithValue("@childamount", Obj[15].ToString());
                            library.WriteErrorLog("childamount : " + Obj[15].ToString());
                            cmd.Parameters.AddWithValue("@luggageamount", Obj[16].ToString());
                            library.WriteErrorLog("luggageamount : " + Obj[16].ToString());
                            cmd.Parameters.AddWithValue("@passamount", Obj[17].ToString());
                            library.WriteErrorLog("passamount : " + Obj[17].ToString());
                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[18].ToString());
                            library.WriteErrorLog("ticketstatus : " + Obj[18].ToString());

                        }
                        else if (Obj.Count() == 20) // where pass age in one digit like M 6  instead of M06
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", "");
                            library.WriteErrorLog("cardnumber : " + "");
                            cmd.Parameters.AddWithValue("@cardtype", 0);
                            library.WriteErrorLog("cardtype : " + 0);
                            cmd.Parameters.AddWithValue("@adultamount", Obj[15].ToString());
                            library.WriteErrorLog("adultamount : " + Obj[15].ToString());
                            cmd.Parameters.AddWithValue("@childamount", Obj[16].ToString());
                            library.WriteErrorLog("childamount : " + Obj[16].ToString());
                            cmd.Parameters.AddWithValue("@luggageamount", Obj[17].ToString());
                            library.WriteErrorLog("luggageamount : " + Obj[17].ToString());
                            cmd.Parameters.AddWithValue("@passamount", Obj[18].ToString());
                            library.WriteErrorLog("passamount : " + Obj[18].ToString());
                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[19].ToString());
                            library.WriteErrorLog("ticketstatus : " + Obj[19].ToString());
                        }
                        else if (Obj.Count() == 21) // where pass age in one digit like M 6  instead of M06
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", Obj[12].ToString());
                            library.WriteErrorLog("cardnumber : " + Obj[12].ToString());

                            cmd.Parameters.AddWithValue("@cardtype", Obj[15].ToString());
                            library.WriteErrorLog("cardtype : " + Obj[15].ToString());

                            cmd.Parameters.AddWithValue("@adultamount", Obj[16].ToString());
                            library.WriteErrorLog("adultamount : " + Obj[16].ToString());

                            cmd.Parameters.AddWithValue("@childamount", Obj[17].ToString());
                            library.WriteErrorLog("childamount : " + Obj[17].ToString());

                            cmd.Parameters.AddWithValue("@luggageamount", Obj[18].ToString());
                            library.WriteErrorLog("luggageamount : " + Obj[18].ToString());

                            cmd.Parameters.AddWithValue("@passamount", Obj[19].ToString());
                            library.WriteErrorLog("passamount : " + Obj[19].ToString());

                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[20].ToString());
                            library.WriteErrorLog("ticketstatus : " + Obj[20].ToString());
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", "");
                            library.WriteErrorLog("cardnumber : " + "");
                            cmd.Parameters.AddWithValue("@cardtype", 0);
                            library.WriteErrorLog("cardtype : " + 0);
                            cmd.Parameters.AddWithValue("@adultamount", Obj[13].ToString());
                            library.WriteErrorLog("adultamount : " + Obj[13].ToString());
                            cmd.Parameters.AddWithValue("@childamount", Obj[14].ToString());
                            library.WriteErrorLog("childamount : " + Obj[14].ToString());
                            cmd.Parameters.AddWithValue("@luggageamount", Obj[15].ToString());
                            library.WriteErrorLog("luggageamount : " + Obj[15].ToString());
                            cmd.Parameters.AddWithValue("@passamount", Obj[16].ToString());
                            library.WriteErrorLog("passamount : " + Obj[16].ToString());
                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[17].ToString());
                            library.WriteErrorLog("ticketstatus : " + Obj[17].ToString());
                        }
                        cmd.Parameters.AddWithValue("@ticketamount", Obj[9].ToString());
                        library.WriteErrorLog("ticketamount : " + Obj[9].ToString());

                        if (Obj[2].ToString().Substring(0, 4) == "CONC")
                        {
                            cmd.Parameters.AddWithValue("@consec_code", Obj[12].ToString());
                            library.WriteErrorLog("consec_code : " + Obj[12].ToString());
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@consec_code", "00");
                            library.WriteErrorLog("consec_code : 00");
                        }
                        cmd.Parameters.AddWithValue("@dateandtime", Obj[10].ToString() + ' ' + Obj[11].ToString());
                        library.WriteErrorLog("dateandtime : " + Obj[11].ToString());

                        cmd.Parameters.AddWithValue("@userid", Session["user_id"].ToString());
                        library.WriteErrorLog("userid : " + Session["user_id"].ToString());
                        //-------------------------Ends Here-----------------------//
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            library.WriteErrorLog("Error occured in Save_Data func " + ex.Message + " " + ex.Source + " waybill no. " + Convert.ToString(Request.QueryString["receipno"].ToString()));
        }
        finally
        {
            con.Close();
        }
    }

    public void DownloadData()
    {
        try
        {
            string filePt = @"D:\pratinidhi\WAYBILL_ND.txt";
            if (System.IO.File.Exists(filePt))
            {
                string[] lines12 = System.IO.File.ReadAllLines(@"D:\pratinidhi\WAYBILL_ND.txt");

                var f = File.ReadAllLines(filePt);

                bool IsRead = false;
                foreach (string line12 in lines12)
                {
                    string[] Obj = line12.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    if (Obj.Count() > 0)
                    {

                        if (Obj[3] == "waybillno")
                        {
                            if (Convert.ToString(Session["Way_Bill_No"]) != Obj[5])
                            {
                                if (Convert.ToString(Session["Way_Bill_No"]) != null && Convert.ToString(Session["Way_Bill_No"]) != "")
                                {
                                    Cashier_(Convert.ToString(Session["Way_Bill_No"]));
                                }
                                Session["Way_Bill_No"] = Obj[5].ToString();
                            }

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.Clear();
                            cmd.CommandText = "insert_ticket_data";
                            cmd.Parameters.AddWithValue("@waybillno", Obj[5].ToString());
                            cmd.Parameters.AddWithValue("@cardnumber", " ");
                            cmd.Parameters.AddWithValue("@cardtype", 0);
                            IsRead = true;
                        }
                        else if (Obj[3] == "tripno")
                            cmd.Parameters.AddWithValue("@tripno", Obj[5].ToString());
                        else if (Obj[3] == "routeno")
                            cmd.Parameters.AddWithValue("@routeno", Obj[5].ToString());
                        else if (Obj[3] == "ticket")
                            cmd.Parameters.AddWithValue("@ticketno", Obj[6].ToString());
                        else if (Obj[3] == "startstage")
                            cmd.Parameters.AddWithValue("@startstage", Obj[5].ToString());
                        else if (Obj[3] == "endstage")
                            cmd.Parameters.AddWithValue("@endstage", Obj[5].ToString());
                        else if (Obj[3] == "adultcount")
                            cmd.Parameters.AddWithValue("@adultcount", Obj[5].ToString());
                        else if (Obj[3] == "childcount")
                            cmd.Parameters.AddWithValue("@childcount", Obj[5].ToString());
                        else if (Obj[3] == "luggagecount")
                            cmd.Parameters.AddWithValue("@luggagecount", Obj[5].ToString());
                        else if (Obj[3] == "passcount")
                            cmd.Parameters.AddWithValue("@passcount", Obj[5].ToString());
                        else if (Obj[3] == "ticketcount" && IsRead == true)
                        {
                            string[] val = Obj[5].Split(',');

                            int totalTicketCount = int.Parse(val[0].ToString()) + int.Parse(val[1].ToString()) +
                                              int.Parse(val[2].ToString()) + int.Parse(val[3].ToString());

                            cmd.Parameters.AddWithValue("@ticketcount", totalTicketCount);
                            IsRead = false;
                        }

                        else if (Obj[3] == "adultamount")
                            cmd.Parameters.AddWithValue("@adultamount", Obj[5].ToString());
                        else if (Obj[3] == "childamount")
                            cmd.Parameters.AddWithValue("@childamount", Obj[5].ToString());
                        else if (Obj[3] == "luggageamount")
                            cmd.Parameters.AddWithValue("@luggageamount", Obj[5].ToString());
                        else if (Obj[3] == "passamount")
                            cmd.Parameters.AddWithValue("@passamount", Obj[5].ToString());
                        else if (Obj[3] == "ticketamount")
                            cmd.Parameters.AddWithValue("@ticketamount", Obj[5].ToString());
                        else if (Obj[3] == "ticketstatus")
                            cmd.Parameters.AddWithValue("@ticketstatus", Obj[5].ToString());
                        else if (Obj[3] == "dateandtime")
                            cmd.Parameters.AddWithValue("@dateandtime", Convert.ToDateTime(Obj[0]).ToString("dd/MM/yy") + " " + Convert.ToDateTime(Obj[5]).TimeOfDay);
                        else if (Obj[3] == "consec_code")
                            cmd.Parameters.AddWithValue("@consec_code", Obj[5].ToString());

                        else if (Obj[3] == "userid")
                        {
                            cmd.Parameters.AddWithValue("@userid", Obj[5].ToString());
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
                if (Convert.ToString(Session["Way_Bill_No"]) != null && Convert.ToString(Session["Way_Bill_No"]) != "")
                {
                    Cashier_(Convert.ToString(Session["Way_Bill_No"]));
                }
                Session["Way_Bill_No"] = "";

            }
        }
        catch (Exception ex)
        {
            library.WriteErrorLog("Error occured in DownloadData func " + ex.Message + " " + ex.Source);
        }
        finally
        {
            con.Close();
        }
    }

    public void Cashier_(string waybillno)
    {

        SqlCommand cmd1 = new SqlCommand();
        DataSet ds = new DataSet();
        try
        {
            ds = Getdata(waybillno);

            if (ds.Tables[0].Rows.Count > 0)
            {
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;

                cmd1 = new SqlCommand("ins_cashier_master", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@waybill", Convert.ToString(ds.Tables[0].Rows[0]["WayBillNo"]));
                cmd1.Parameters.AddWithValue("@cond", Convert.ToString(ds.Tables[0].Rows[0]["ConductorName"]));
                cmd1.Parameters.AddWithValue("@drv", Convert.ToString(ds.Tables[0].Rows[0]["DriverName"]));
                cmd1.Parameters.AddWithValue("@vehicle", Convert.ToString(ds.Tables[0].Rows[0]["VehicleCode"]));
                cmd1.Parameters.AddWithValue("@totcash", Convert.ToString(ds.Tables[0].Rows[0]["totcash"]));
                cmd1.Parameters.AddWithValue("@userid", Convert.ToString(ds.Tables[0].Rows[0]["user_id"]));
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
            }
        }
        catch (Exception ex)
        {
            library.WriteErrorLog("Error occured in Cashier_ func " + ex.Message + " " + ex.Source);
            Response.Write(ex.Message);
        }
    }

    public DataSet Getdata(string waybillno)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();

            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_GetWaybillData";
            cmd.Parameters.Add(new SqlParameter("@WaybillNo", waybillno));

            adp.SelectCommand = cmd;
            adp.Fill(ds);

        }
        catch (Exception ex)
        {
            library.WriteErrorLog("Error occured in Getdata func " + ex.Message + " " + ex.Source);
            con.Close();
        }
        finally
        {
            con.Close();
        }
        return ds;
    }
}