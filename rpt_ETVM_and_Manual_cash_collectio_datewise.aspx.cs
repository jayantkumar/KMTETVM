﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class rpt_ETVM_and_Manual_cash_collectio_datewise : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    SqlCommand cmd;
    SqlDataReader dr;
    SqlDataAdapter da;
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    double a, b, z;

    protected void Page_Load(object sender, EventArgs e)
    {
         if (!IsPostBack)
        {
            txtfromdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            txttodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            string SelddlDepo = "select UserId,UserName from User_Master where is_delete=0";
            FillDDl(SelddlDepo, ddlUserName, "UserId", "UserName", "---ALL---");
            //string conductor= "select CondCode,ConductorName from Conductor_Master ";
            string conductor = "select CondCode,isnull(emp_code,ConductorName) as ConductorName from Conductor_Master ";  //jayantkumar on 09-03-2018
            FillDDl(conductor, ddlconductor, "CondCode", "ConductorName", "--Select Conductors--");

        }
    }
    public void FillDDl(string Query, DropDownList source, string valuefield, string textfield, string firstItem)
    {
        con.Open();
        cmd = new SqlCommand(Query, con);
        da = new SqlDataAdapter(cmd);
        da.SelectCommand.CommandTimeout = 0;
        try
        {
            dt.Clear();
            da.Fill(dt);
            source.DataValueField = valuefield;
            source.DataTextField = textfield;
            source.DataSource = dt;
            source.DataBind();
            source.Items.Insert(0, new ListItem(firstItem, "0"));
        }
        catch (Exception ex)
        {
            cmd.Dispose();
            da.Dispose();
            con.Close();
        }
        finally
        {
            con.Close();
        }
    }
    protected void Search_Click(object sender, EventArgs e)
    {
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if(ddlconductor.SelectedValue=="0")
                {
                    cmd.CommandText = "ETVM_and_Manual_cash_collectio_datewise";
                }
                else
                {
                    cmd.CommandText = "ETVM_and_Manual_cash_collectio_datewise_Condwise";
                }
                
                cmd.Parameters.Add(new SqlParameter("@fromdate", txtfromdate.Text));
                cmd.Parameters.Add(new SqlParameter("@todate", txttodate.Text));
                //cmd.Parameters.Add(new SqlParameter("@routeno", '0'));
                cmd.Parameters.Add(new SqlParameter("@Userid", ddlUserName.SelectedValue));
               cmd.Parameters.Add(new SqlParameter("@cname", ddlconductor.SelectedValue));
                cmd.CommandTimeout = 0;
                adp.SelectCommand = cmd;
                adp.Fill(ds);
                grdWayBill.DataSource = ds;
                grdWayBill.DataBind();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    decimal totalcashcollected = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("totalcashcollected"));
                    decimal etvm_tic_amt = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("etvm_tic_amt"));
                    decimal manual_tic_amt = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("manual_tic_amt"));
                    decimal Shortage = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("Shortage"));
                    decimal Excess = ds.Tables[0].AsEnumerable().Sum(row => row.Field<decimal>("Excess"));

                    grdWayBill.FooterRow.Font.Bold = true;
                    grdWayBill.FooterRow.Font.Size = 13;
                    grdWayBill.FooterRow.Cells.RemoveAt(1);
                    grdWayBill.FooterRow.Cells.RemoveAt(2);
                    grdWayBill.FooterRow.Cells.RemoveAt(3);
                    grdWayBill.FooterRow.Cells.RemoveAt(4);
                    grdWayBill.FooterRow.Cells.RemoveAt(5);
                    grdWayBill.FooterRow.Cells.RemoveAt(6);
                    grdWayBill.FooterRow.Cells.RemoveAt(7);
                    grdWayBill.FooterRow.Cells.RemoveAt(8);
                    grdWayBill.FooterRow.Cells.RemoveAt(0);

                    grdWayBill.FooterRow.Cells[1].Text = "Total";
                    grdWayBill.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;
                    grdWayBill.FooterRow.Cells[1].ColumnSpan = 10;

                    grdWayBill.FooterRow.Cells[2].Text = etvm_tic_amt.ToString("N2");
                    grdWayBill.FooterRow.Cells[3].Text = "";
                    grdWayBill.FooterRow.Cells[4].Text = manual_tic_amt.ToString("N2");
                    grdWayBill.FooterRow.Cells[5].Text = "";
                    grdWayBill.FooterRow.Cells[6].Text = Convert.ToDecimal(etvm_tic_amt + manual_tic_amt).ToString("N2");
                    grdWayBill.FooterRow.Cells[7].Text = totalcashcollected.ToString("N2");
                    grdWayBill.FooterRow.Cells[8].Text = "";
                    grdWayBill.FooterRow.Cells[9].Text = "";
                    grdWayBill.FooterRow.Cells[10].Text = Shortage.ToString("N2");
                    grdWayBill.FooterRow.Cells[11].Text = Excess.ToString("N2");
                    lblfromdate.Text = "From Date :" + txtfromdate.Text;
                    lbltodate.Text = "To Date :" + txttodate.Text;
                }
                else
                {
                    lblfromdate.Text = "";
                    lbltodate.Text = "";
                }

            }
            catch (Exception ex)
            {
                grdWayBill.DataSource = null;
                grdWayBill.DataBind();
            }
            finally
            {
                con.Close();
            }
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("ETVM_and_manual_cash_collection.xls", grdWayBill);
    }
    protected void grdWayBill_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[1].Text == "Total")
            {
                e.Row.Cells[0].Text = " ";
                e.Row.Cells[1].Font.Size = 12;
                e.Row.Cells[2].Font.Size = 12;
                e.Row.Cells[3].Font.Size = 12;
                e.Row.Cells[4].Font.Size = 12;
                e.Row.Cells[5].Font.Size = 12;
                e.Row.Cells[6].Font.Size = 12;
                e.Row.Cells[7].Font.Size = 12;
                e.Row.Cells[8].Font.Size = 12;
                e.Row.Cells[9].Font.Size = 12;
                e.Row.Cells[10].Font.Size = 12;

                e.Row.Cells[1].Font.Bold = true;
                e.Row.Cells[2].Font.Bold = true;
                e.Row.Cells[3].Font.Bold = true;
                e.Row.Cells[4].Font.Bold = true;
                e.Row.Cells[5].Font.Bold = true;
                e.Row.Cells[6].Font.Bold = true;
                e.Row.Cells[7].Font.Bold = true;
                e.Row.Cells[8].Font.Bold = true;
                e.Row.Cells[9].Font.Bold = true;
                e.Row.Cells[10].Font.Bold = true;
            }
        }
    }
    protected void grdWayBill_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
