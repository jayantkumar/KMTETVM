﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Text;

public partial class rptWayBill_Downloaded_and_not : System.Web.UI.Page
{
    string connect = System.Configuration.ConfigurationSettings.AppSettings["Con"].ToString();
    SqlConnection con;
    SqlCommand cmd;
    StringBuilder strTemp1 = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Txt_Date.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            Txt_Todate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
 
        }
    }

    protected void Btn_search_Click(object sender, EventArgs e)
    {
        try
        {

            con = new SqlConnection(connect);
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandTimeout = 500;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_WayBill_Download_or_not";
            cmd.Parameters.Add(new SqlParameter("@fromdate", Txt_Date.Text));
            cmd.Parameters.Add(new SqlParameter("@todate", Txt_Todate.Text));

            adp.SelectCommand = cmd;
            adp.Fill(ds);

            DataSet ds2 = new DataSet();

            if (Convert.ToInt32(ds.Tables.Count) > 0)
            {
                double waybilltotal = ds.Tables[0].Rows.Count;
                Lbl_Date.Text = "From Date :" + Txt_Date.Text;
                Lbldate.Text = "To Date:" + Txt_Todate.Text;
                createTable(ds);
            }
            else
            {
                Recordnotfound();
            }
        }
        catch (Exception ex)
        {
            con.Close();
        }
        finally
        {
            con.Close();
        }
    }

    public DataSet getData()
    {
        DataSet ds2 = new DataSet();
        try
        {
            con = new SqlConnection(connect);
            SqlCommand cmd1 = new SqlCommand();

            SqlDataAdapter adp1 = new SqlDataAdapter();
            cmd1.Connection = con;
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandText = "notdownloadedwaybill";
            cmd1.CommandTimeout = 480;
            cmd1.Parameters.Add(new SqlParameter("@fromdate", Txt_Date.Text));
            cmd1.Parameters.Add(new SqlParameter("@todate", Txt_Todate.Text));

            adp1.SelectCommand = cmd1;
            adp1.Fill(ds2);
        }
        catch (Exception ex)
        {

        }
        return ds2;
    }

    public void createTable(DataSet ds)
    {
        DataTable dt = new DataTable(); DataTable dt_not = new DataTable(); DataTable dtfiler = new DataTable();
        decimal manual = 0, etm = 0, total = 0, previouswaybill = 0, downloadedwaybill = 0;
        StringBuilder str = new StringBuilder();
        Int32 totalwaybill = 0;


        if (Convert.ToInt32(ds.Tables.Count) > 1)
        {
            dt_not = ds.Tables[3];
            dt = ds.Tables[0];
            totalwaybill = ds.Tables[1].Rows.Count + ds.Tables[3].Rows.Count;
            downloadedwaybill = Convert.ToInt32(ds.Tables[1].Rows.Count);

            DataView dvV = new DataView(dt, "", "", DataViewRowState.CurrentRows);
            dtfiler = dvV.ToTable(true, "waybill_no", "con_full_name", "emp_code", "Schedule", "ETM_code", "ModifiedDate", "etvm_tic_amt", "manual_tic_amt", "total_tic_amt", "sysdate");

            manual = Convert.ToInt32(dt.Compute("SUM(manual_tic_amt)", string.Empty));

            etm = Convert.ToInt32(dt.Compute("SUM(etvm_tic_amt)", string.Empty));

            total = Convert.ToDecimal(etm + manual);

            previouswaybill = Convert.ToInt32(ds.Tables[2].Rows.Count);
        }
        else if (Convert.ToInt32(ds.Tables[0].Rows.Count) > 0)
        {
            dt_not = ds.Tables[0];
        }
        else
        {
            Recordnotfound();
            return;
        }
        try
        {
            #region Header
            string h = string.Format(@"<table id='Table2' style='font-size:20px;' width='100%' border='1' cellspacing='0' cellpadding='0'>
    <tbody>
        <tr><td><label>Date </label></td><td><label>{6}</label></td><td colspan='9'></td></tr>
        <tr><td><label>Total waybill Created </label></td>
            <td><label>{5}</label></td>
            <td><label>waybill downloaded </label></td>
            <td><label>{0}</label></td>
            <td><label>Total cash From ETM </label></td>
            <td><label>{1}</label></td>
            <td><label>Total manual Cash </label></td>
            <td><label>{2}</label></td>
            <td><label>Total cash </label></td>
            <td colspan='2'><label>{3}</label></td>
        </tr>
        <tr>
            <td><label>Total duties issued </label></td>
            <td><label id='Label7'>{4}</label></td>
            <td><label>Previous Waybill downloaded </label></td>
            <td><label id='Label7'>{7}</label></td>
            <td colspan='7'><label></label></td>
        </tr>
        <tr>
            <td colspan='11' style='text-align:center;font-weight:bold;font-size:20px;'><label>Downloaded way bill</label></td>
        </tr>
    </tbody>
</table>", downloadedwaybill, etm, manual, total, "", totalwaybill, Convert.ToString(Txt_Date.Text), previouswaybill);

            #endregion

            if (Convert.ToInt32(ds.Tables.Count) > 1)
            {
                strTemp1 = new StringBuilder();
                strTemp1.Append(h);
                strTemp1.Append("<div id='con'><table id='Table1'  border='1' cellspacing='0' cellpadding='0' width='100%'>");
                strTemp1.Append("<tbody   id='tbodyR'>");
                strTemp1.Append("<tr style='font-weight:bold;background-color:#f7e9d9; text-align:center;' class='gridhead'>");
                strTemp1.Append("<td>Sr No.</td><td>WayBill No</td><td>Conductor Name</td><td>Conductor No</td><td>Duty No</td><td>Machine Number</td><td>Way Bill Date</td><td>ETM Cash</td><td>Manual Cash</td><td>Total Cash</td><td>Downloaded Time</td></tr>");

                decimal etm_cash = 0, manual_cash = 0;
                for (int i = 0; i < dtfiler.Rows.Count; i++)
                {
                    if (i % 2 == 0)
                        strTemp1.Append("<tr style='background-color:WhiteSmoke;color:#000099;font-family:Verdana;font-size:Medium;font-weight:normal;font-style:normal;text-decoration:none;'>");
                    else
                        strTemp1.Append("<tr style='background-color:#f3ecec;color:Black;font-family:Verdana;font-size:Medium;font-weight:normal;font-style:normal;text-decoration:none;'>");
                    strTemp1.Append("<td>" + (i + 1) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["waybill_no"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["con_full_name"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["emp_code"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["Schedule"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["ETM_code"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["ModifiedDate"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["etvm_tic_amt"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["manual_tic_amt"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToString(dtfiler.Rows[i]["total_tic_amt"]) + "</td>");
                    strTemp1.Append("<td>" + Convert.ToDateTime(dtfiler.Rows[i]["sysdate"]).ToString("dd/MMM/yyyy hh:mm tt") + "</td>");
                    strTemp1.Append("</tr>");

                    etm_cash += Convert.ToDecimal(dtfiler.Rows[i]["etvm_tic_amt"]);
                    manual_cash += Convert.ToDecimal(dtfiler.Rows[i]["manual_tic_amt"]);

                    if (i == (Convert.ToInt32(dtfiler.Rows.Count) - 1))
                        strTemp1.Append("<tr style='text-align:center;font-weight:bold;font-size:20px;'><td colspan='7'>Total</td><td style='font-weight:bold;'>" + etm_cash + "</td><td style='font-weight:bold;'>" + manual_cash + "</td><td style='font-weight:bold;'>" + (etm_cash + manual_cash) + "</td><td></td></tr>");
                }
                strTemp1.Append("</tbody></table></div>");
            }

            strTemp1.Append("<div id='cons'><table id='Table3' border='1' cellspacing='0' cellpadding='0' width='100%'>");
            strTemp1.Append("<tbody   id='tbodyR1'>");
            strTemp1.Append("<tr><td colspan='8' style='text-align:center;font-weight:bold;font-size:20px;'>Not Downloaded way bill</td></tr><tr style='font-weight:bold;background-color:#f7e9d9; text-align:center;' class='gridhead'>");
            strTemp1.Append("<td>Sr No.</td><td>WayBill No</td><td>Conductor Name</td><td>Conductor No</td><td>Duty No</td><td>Machine Number</td><td>Way Bill Date</td></tr>");

            for (int j = 0; j < dt_not.Rows.Count; j++)
            {
                if (j % 2 == 0)
                    strTemp1.Append("<tr style='background-color:WhiteSmoke;color:#000099;font-family:Verdana;font-size:Medium;font-weight:normal;font-style:normal;text-decoration:none;'>");
                else
                    strTemp1.Append("<tr style='background-color:#f3ecec;color:Black;font-family:Verdana;font-size:Medium;font-weight:normal;font-style:normal;text-decoration:none;'>");
                strTemp1.Append("<td>" + (j + 1) + "</td>");
                strTemp1.Append("<td>" + Convert.ToString(dt_not.Rows[j]["waybillno"]) + "</td>");
                strTemp1.Append("<td>" + Convert.ToString(dt_not.Rows[j]["con_full_name"]) + "</td>");
                strTemp1.Append("<td>" + Convert.ToString(dt_not.Rows[j]["emp_code"]) + "</td>");
                strTemp1.Append("<td>" + Convert.ToString(dt_not.Rows[j]["Schedule"]) + "</td>");
                strTemp1.Append("<td>" + Convert.ToString(dt_not.Rows[j]["ETM_code"]) + "</td>");
                strTemp1.Append("<td>" + Convert.ToString(dt_not.Rows[j]["CreatedDate"]) + "</td>");

                strTemp1.Append("</tr>");

            }
            strTemp1.Append("</tbody></table></div>");
            divtbl.InnerHtml = strTemp1.ToString();
        }
        catch (Exception ex)
        {
            Recordnotfound();
        }

    }

    protected void Btn_ConvertExcl_Click(object sender, EventArgs e)
    {
        try
        {
            Response.AppendHeader("content-disposition", "attachment;filename=Waybill_Downloaded_and_notDownloadedrpt" + Convert.ToDateTime(DateTime.Now).ToString() + ".xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            this.EnableViewState = false;
            Response.Write(divtbl.InnerHtml);
            Response.End();
        }
        catch (Exception ex)
        {

        }
    }

    public void Recordnotfound()
    {
        divtbl.InnerHtml = @"<br /><br /><div id='con'><table id='Table1'  border='0' cellspacing='0' cellpadding='0' width='100%'><tr><td>&emsp;</td><td>&emsp;</td><td>&emsp;</td><td>&emsp;</td><td>&emsp;</td><td><lable style='text-align:center;font-weight:bold;font-size:18px;color:red;'>Record not found</lable></td></tr></table></div>";
    }

    public void createTablefor_notdownload(DataSet ds)
    {
        DataTable dt = new DataTable(); DataTable dt_not = new DataTable();
        StringBuilder str = new StringBuilder();
        dt = ds.Tables[0]; dt_not = ds.Tables[3];

    }
}