﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Route_creation.aspx.cs" Inherits="Route_creation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function stage(id, id2) {
            var requestUrl = window.location.search.substr(1).split('&')

            var query = requestUrl[0].split('=')[1]
            var isIntegerre = /^\s*(\+|-)?\d+\s*$/;

            var pattern12 = /^\S*$/;
            var pattern123 = /^[a-zA-Z ]*$/;
            var pattern3211 = /^(0|[1-9][0-9]{0,2}(?:(,[0-9]{3})*|[0-9]*))(\.[0-9]+){0,1}$/;
            var keycode = (id.which) ? id.which : id.keyCode;
            if (!pattern12.test(id.value) || pattern3211.test(id.value) == true || ((keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57))) {
            }

            var str = id.value;

            var res = str.substring(0, 3);

            id2.value = res;
            var kkk = 1;
            for (var iii = 0; iii <= query; iii++) {

                id2 = 'TextBoxU' + iii.toString() + kkk.toString();
                if (id2 != id.name.toString()) {

                    if (document.getElementById(id2).value == (id).value) {
                        alert('Stages cannot be same');
                        id.focus();
                        return false;
                    }
                }
            }
        }

        function CheckNumeric(key, key2, act) {
            var pattern = /^[0-9]{0,12}(\.[0-9]{1,2})?$/;
            var requestUrl = window.location.search.substr(1).split('&')
            var query12 = requestUrl[0].split('=')[1]
            var Kms = requestUrl[5].split('=')[1]
            var ggmn = 2
            var id567 = 'TextBoxU' + ((parseInt(query12 - 1))).toString() + ggmn.toString();

            if (!pattern.test(key.value)) {
                alert('Invalid Value Should be in the format 0000 or 0000.00');
                key.focus();
                return false;
            }
            if (act == 0 && key.name != id567) {
                if (parseFloat(key2.value) >= parseFloat(key.value) || parseFloat(key.value) >= parseFloat(Kms)) {

                    alert('Invalid Km');
                    key.focus();
                    return false;
                }
            }
            else {
                var pattern123 = RegExp('/^\s*\d+\s*$/')
                if (pattern123.test(key.value) == true) {
                    alert('Incorrect Format');
                    key.focus();
                    return false;
                }
                if (parseInt(key.value) < parseInt(key2.value)) {
                    alert('Invalid Fare...!!');

                    key.focus();
                    return false;
                }
            }
            for (var ii = 1; ii <= query12; ii++) {

                var id2 = 'TextBoxU12' + ii.toString() + ii.toString();
                var idF = 'TextBoxU1211';
                var idL = 'TextBoxU12' + ((parseInt(query12 - 1))).toString() + '1';
                var id3 = 'TextBoxU12' + (ii + 1).toString() + (ii + 1).toString();
                var id4 = 'TextBoxU12' + ((parseInt(query12 - 1))).toString() + (ii).toString();
                if (document.getElementById(id4).value != "") {
                    if (document.getElementById(id2).value != null && document.getElementById(id3).value != null) {
                        if (parseFloat(document.getElementById(id3).value) < parseFloat(document.getElementById(idF).value)) {
                            alert('Invalid Fare');
                            return true;
                        }
                    }
                    if (document.getElementById(id4).value != null) {
                        for (var gg = 1; gg <= query12; gg++) {
                            for (var hh = 1; hh <= gg; gg++) {
                                var jhj = 1;
                                var id5 = 'TextBoxU12' + ((parseInt(query12 - 1))).toString() + gg.toString();
                                var id51 = 'TextBoxU12' + ((parseInt(query12 - 1))).toString() + jhj.toString();
                                var id52 = 'TextBoxU1211'

                                var id6 = 'TextBoxU12' + ((parseInt(gg + 1))).toString() + (gg + 1).toString();
                                if (parseFloat(document.getElementById(id6).value) > parseFloat(document.getElementById(id5).value) || parseFloat(key.value) < parseFloat(document.getElementById(id52).value) || parseFloat(key.value) > parseFloat(document.getElementById(id51).value) && act != "0") {

                                    alert('Invalid Fare!');
                                    key.focus();
                                    return false;
                                }
                            }
                        }
                    }
                }
                else {
                    if ((id2.substring(id2.length - 1, id2.length)) == "1") {
                        return true;
                    }
                    else {
                        if ((id2.substring(id2.length - 2, id2.length - 1)) != (parseInt(query12 - 1)).toString()) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                }
            }
        }

        function checkEmpty() {
            var id = "";

            var id2 = "";
            var requestUrl = window.location.search.substr(1).split('&')

            var query = requestUrl[0].split('=')[1]
            for (var i = 0; i < query; i++) {
                for (var j = 0; j < 5; j++) {
                    id = 'TextBoxU' + i.toString() + j.toString();
                    if (document.getElementById(id).value == null || document.getElementById(id).value == "") {
                        alert('You cannot continue with blank fields');
                    }
                }
            }
            i
            for (var ii = 1; ii <= query - 1; ii++) {
                for (var kk = 1; kk <= ii; kk++) {
                    id2 = 'TextBoxU12' + ii.toString() + kk.toString();
                    if (document.getElementById(id2).value == null || document.getElementById(id2).value == "") {
                        alert('You cannot continue with blank Fare');
                        document.getElementById(id2).focus();
                        return false;
                    }
                }
            }
        }

    </script>


    <script language="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                alert('An attempt was made to submit this form more than once; this extra attempt will be ignored.');
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%">
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td align="center" style="background-color: #FF9933">INSERT FARE DETAILS
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Panel ID="Panel1" runat="server">
                        </asp:Panel>
                    </td>
                    <td align="left" valign="bottom">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#66FF33" ForeColor="White">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    <%-- <asp:Button ID="Button3" runat="server" OnClientClick="return checkEmpty()" OnClick="Button3_Click"--%>
                        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" OnClientClick="return CheckIsRepeat();" Text="Save" />
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
