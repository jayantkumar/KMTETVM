﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data;

public partial class routeupdate : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Con"]);
    protected void Page_Load(object sender, EventArgs e)
    {

        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adp = new SqlDataAdapter();
        DataSet ds = new DataSet();
        string date12 = "";

      //  date12 = Request["date"].ToString;

        date12 = Request.QueryString["date"];

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "getFare_update";
        cmd.Parameters.Add(new SqlParameter("@route_name", date12));


        //'  cmd.Parameters.Add(New SqlParameter("@bustype", DropDownList1.SelectedItem))



        cmd.Connection = con;
        adp.SelectCommand = cmd;
        adp.Fill(ds);
        grdCons.DataSource = ds;

        grdCons.DataBind();

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        int j = 0;
      SqlCommand  cmd12 = new SqlCommand();
        DateTime Seas_Date = default(DateTime);
        DateTime Effc_date = default(DateTime);

      
      
        try
        {
            if ((con.State == ConnectionState.Closed))
            {
                con.Open();
            }
           // cmd12.Transaction = con.BeginTransaction();

            for (j = 0; j <= grdCons.Rows.Count - 1; j++)
            {
               // string fare = ((TextBox)grdCons.Rows[j].FindControl("TextBox1")).Text;

                string fare = ((TextBox)grdCons.Rows[j].FindControl("TextBox1")).Text;
                //string id = (grdCons.Rows[j].FindControl("TextBox1").ClientID);
                //string strfare =   Request.Form[id].ToString();

                string fare12 = ((TextBox)grdCons.Rows[j].Cells[4].FindControl("TextBox1")).Text;

              ///  ((TextBox)grdCons.Rows[j])
                cmd12.Parameters.Clear();
                cmd12.Connection = con;
                cmd12.CommandType = CommandType.StoredProcedure;
                cmd12.CommandText = "update_fare";
                cmd12.Parameters.Add(new SqlParameter("@fair", fare));
                cmd12.Parameters.Add(new SqlParameter("@from_loc", grdCons.Rows[j].Cells[2].Text));
                cmd12.Parameters.Add(new SqlParameter("@to_loc", grdCons.Rows[j].Cells[3].Text));
                cmd12.Parameters.Add(new SqlParameter("@fareid", grdCons.Rows[j].Cells[0].Text));

                cmd12.ExecuteNonQuery();

            }

          //  cmd12.Transaction.Commit();
            Show_Msg();
        }
        catch (Exception ex)
        {
          
            Msg_Failure();
        }
        finally
        {
            con.Close();

        }
    }

    public void Show_Msg()
    {
        string scp = "<script type='text/javascript'>alert('Data Saved Successfully');window.location.replace('Update_fare.aspx');</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);
    }
    public void Msg_Failure()
    {
        string scp = "<script type='text/javascript'>alert('Sorry some error occured while saving information. Please try after sometime');</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "script1", scp);
    }

}